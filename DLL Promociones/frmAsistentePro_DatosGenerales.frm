VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form frmAsistentePro_DatosGenerales 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   0
      ScaleHeight     =   1050
      ScaleWidth      =   12210
      TabIndex        =   30
      Top             =   1680
      Width           =   12240
      Begin VB.Frame frame_toolbar 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   810
         Left            =   30
         TabIndex        =   31
         Top             =   140
         Width           =   11505
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   0
            TabIndex        =   32
            Top             =   0
            Width           =   11595
            _ExtentX        =   20452
            _ExtentY        =   1429
            ButtonWidth     =   2408
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   10
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F3) Agr&egar"
                  Key             =   "Agregar"
                  Object.ToolTipText     =   "Agregar una Nueva Ficha"
                  ImageIndex      =   2
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "APRODUCTO"
                        Object.Tag             =   "APRODUCTO"
                        Text            =   "Agregar"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "AIMPORTAR"
                        Object.Tag             =   "AIMPORTAR"
                        Text            =   "Agregar Importar "
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "imparchivo"
                        Text            =   "Importar Archivo"
                     EndProperty
                  EndProperty
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F2) &Buscar"
                  Key             =   "Buscar"
                  Object.ToolTipText     =   "Buscar una Ficha"
                  ImageIndex      =   6
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F5) &Modificar"
                  Key             =   "Modificar"
                  Object.ToolTipText     =   "Modificar esta Ficha"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F7) &Cancelar"
                  Key             =   "Cancelar"
                  Object.ToolTipText     =   "Cancelar esta Ficha"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F6) Bo&rrar"
                  Key             =   "Eliminar"
                  Object.ToolTipText     =   "Borrar esta Ficha"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F4) &Grabar"
                  Key             =   "Grabar"
                  Object.ToolTipText     =   "Grabar esta Ficha"
                  ImageIndex      =   7
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F12) &Salir"
                  Key             =   "Salir"
                  Object.ToolTipText     =   "Salir de Ficheros"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Caption         =   "&Ayuda"
                  Key             =   "Ayuda"
                  Object.ToolTipText     =   "Ayuda"
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Codigo"
      Height          =   7875
      Left            =   240
      TabIndex        =   28
      Top             =   2880
      Width           =   11805
      Begin VB.TextBox txt_Moneda 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   11400
         TabIndex        =   55
         Top             =   1560
         Visible         =   0   'False
         Width           =   330
      End
      Begin VB.CommandButton CmdMonedaMonto 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   7800
         Picture         =   "frmAsistentePro_DatosGenerales.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1800
         Width           =   435
      End
      Begin VB.OptionButton optNoAfiliados 
         BackColor       =   &H00E7E8E8&
         Caption         =   "No Afiliados"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   1860
         MaskColor       =   &H00585A58&
         TabIndex        =   18
         Top             =   5040
         Width           =   1935
      End
      Begin VB.TextBox txtHoraFin 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   6720
         TabIndex        =   16
         Top             =   3960
         Width           =   3405
      End
      Begin VB.TextBox txtHoraInicio 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   1560
         TabIndex        =   15
         Top             =   3960
         Width           =   3225
      End
      Begin VB.TextBox TxtFechaHasta 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   6720
         TabIndex        =   7
         Top             =   2880
         Width           =   3375
      End
      Begin VB.TextBox txtFechaDesde 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   1560
         TabIndex        =   6
         Top             =   2880
         Width           =   3195
      End
      Begin VB.CheckBox chkCumpleanneros 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Solo en fecha de Cumplea�os"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   3960
         TabIndex        =   20
         Top             =   5640
         Width           =   4455
      End
      Begin VB.OptionButton optAfiliados 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Afiliados"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   3960
         MaskColor       =   &H00585A58&
         TabIndex        =   19
         Top             =   5040
         Width           =   1935
      End
      Begin VB.OptionButton optTodos 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         MaskColor       =   &H00585A58&
         TabIndex        =   17
         Top             =   5040
         Width           =   1455
      End
      Begin VB.CheckBox chkDias 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Domingo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Index           =   7
         Left            =   10320
         TabIndex        =   14
         Top             =   3360
         Width           =   1365
      End
      Begin VB.CheckBox chkDias 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Sabado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Index           =   6
         Left            =   8880
         TabIndex        =   13
         Top             =   3360
         Width           =   1365
      End
      Begin VB.CheckBox chkDias 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Viernes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Index           =   5
         Left            =   7440
         TabIndex        =   12
         Top             =   3360
         Width           =   1365
      End
      Begin VB.CheckBox chkDias 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Jueves"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Index           =   4
         Left            =   6000
         TabIndex        =   11
         Top             =   3360
         Width           =   1335
      End
      Begin VB.CheckBox chkDias 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Miercoles"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Index           =   3
         Left            =   4440
         TabIndex        =   10
         Top             =   3360
         Width           =   1485
      End
      Begin VB.CheckBox chkDias 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Martes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Index           =   2
         Left            =   3000
         TabIndex        =   9
         Top             =   3360
         Width           =   1245
      End
      Begin VB.CheckBox chkDias 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Lunes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Index           =   1
         Left            =   1560
         TabIndex        =   8
         Top             =   3360
         Width           =   1245
      End
      Begin VB.TextBox txtPrioridad 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   1140
         TabIndex        =   23
         Top             =   7020
         Width           =   1620
      End
      Begin VB.CheckBox chkCombinada 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Esta promocion aplica en conjunto con otras promociones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   120
         TabIndex        =   22
         Top             =   6480
         Width           =   11475
      End
      Begin VB.TextBox txtMontoMaximo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   5760
         TabIndex        =   4
         Top             =   1800
         Width           =   1920
      End
      Begin VB.TextBox txtMontoMinimo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   1980
         TabIndex        =   3
         Top             =   1800
         Width           =   1860
      End
      Begin VB.TextBox txtCampanna 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   1140
         Width           =   8100
      End
      Begin VB.CommandButton cmdSiguiente 
         Caption         =   "Siguiente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8640
         TabIndex        =   25
         Top             =   7320
         Width           =   1455
      End
      Begin VB.TextBox txtDescripcion 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   4560
         TabIndex        =   1
         Top             =   540
         Width           =   5000
      End
      Begin VB.ComboBox cmbGrupoClientes 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   5640
         Width           =   2085
      End
      Begin VB.TextBox txtCodigo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   1260
         TabIndex        =   0
         Top             =   540
         Width           =   1800
      End
      Begin VB.CommandButton cmdAtras 
         Caption         =   "Atras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7080
         TabIndex        =   24
         Top             =   7320
         Width           =   1455
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   10200
         TabIndex        =   26
         Top             =   7320
         Width           =   1455
      End
      Begin MSComCtl2.DTPicker dtpFechaInicio 
         CausesValidation=   0   'False
         Height          =   360
         Left            =   1560
         TabIndex        =   43
         Top             =   2880
         Visible         =   0   'False
         Width           =   3525
         _ExtentX        =   6218
         _ExtentY        =   635
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy hh:mm:ss tt"
         Format          =   112328707
         CurrentDate     =   36446
      End
      Begin MSComCtl2.DTPicker dtpFechaFin 
         CausesValidation=   0   'False
         Height          =   360
         Left            =   6720
         TabIndex        =   44
         Top             =   2880
         Visible         =   0   'False
         Width           =   3705
         _ExtentX        =   6535
         _ExtentY        =   635
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy hh:mm:ss tt"
         Format          =   112328707
         CurrentDate     =   36446
      End
      Begin MSComCtl2.DTPicker dtpHoraInicio 
         Height          =   360
         Left            =   1560
         TabIndex        =   49
         Top             =   3960
         Visible         =   0   'False
         Width           =   3540
         _ExtentX        =   6244
         _ExtentY        =   635
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   112328706
         CurrentDate     =   36336
      End
      Begin MSComCtl2.DTPicker dtpHoraFin 
         Height          =   360
         Left            =   6720
         TabIndex        =   50
         Top             =   3960
         Visible         =   0   'False
         Width           =   3720
         _ExtentX        =   6562
         _ExtentY        =   635
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   112328706
         CurrentDate     =   36336
      End
      Begin VB.Label lbl_Moneda 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Moneda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   11160
         TabIndex        =   56
         Top             =   2040
         Visible         =   0   'False
         Width           =   420
      End
      Begin VB.Label lblNotaPrioridad 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "* A N�mero mas alto mayor prioridad *"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   2880
         TabIndex        =   54
         Top             =   7080
         Width           =   4125
      End
      Begin VB.Label lblAvisoImpuestosMontos 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "* Los montos minimo y maximo no consideran impuestos *"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   540
         Left            =   8340
         TabIndex        =   53
         Top             =   1740
         Width           =   3285
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblSeccionOtros 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Otros"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   52
         Top             =   6180
         Visible         =   0   'False
         Width           =   465
      End
      Begin VB.Line LnSeccionOtros 
         BorderColor     =   &H00AE5B00&
         Visible         =   0   'False
         X1              =   840
         X2              =   11640
         Y1              =   6300
         Y2              =   6300
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   960
         X2              =   11760
         Y1              =   4680
         Y2              =   4680
      End
      Begin VB.Label lblSeccionClientes 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Clientes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   51
         Top             =   4560
         Width           =   675
      End
      Begin VB.Label lblHoraFin 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Hora Fin"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   5520
         TabIndex        =   48
         Top             =   3960
         Width           =   1050
      End
      Begin VB.Label lblHoraInicio 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Hora Inicio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   47
         Top             =   3960
         Width           =   1275
      End
      Begin VB.Label lblFechaFin 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha Fin"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   5520
         TabIndex        =   46
         Top             =   2880
         Width           =   1110
      End
      Begin VB.Label lblFechaInicio 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha Inicio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   45
         Top             =   2880
         Width           =   1275
      End
      Begin VB.Label lblDias 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dias"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   42
         Top             =   3360
         Width           =   885
      End
      Begin VB.Label lblPrioridad 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   120
         TabIndex        =   41
         Top             =   7080
         Width           =   930
      End
      Begin VB.Label lblCampanna 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Campa�a"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   40
         Top             =   1200
         Width           =   1170
      End
      Begin VB.Label lblMontoMaximo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Monto Maximo de Compra"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   540
         Left            =   3900
         TabIndex        =   39
         Top             =   1740
         Width           =   1770
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblMontoMinimo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Monto Minimo de Compra"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   510
         Left            =   240
         TabIndex        =   38
         Top             =   1740
         Width           =   1740
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblSeccionPeriodo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Periodo Activo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   37
         Top             =   2460
         Width           =   1215
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   1440
         X2              =   11760
         Y1              =   2580
         Y2              =   2580
      End
      Begin VB.Label lblGrupo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   36
         Top             =   5640
         Width           =   825
      End
      Begin VB.Label lblDescripcion 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   3300
         TabIndex        =   35
         Top             =   600
         Width           =   1200
      End
      Begin VB.Label lblCodigo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   34
         Top             =   600
         Width           =   945
      End
      Begin VB.Label lblSeccionDatosPromo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Datos de la Promocion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   120
         TabIndex        =   33
         Top             =   120
         Width           =   1920
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00AE5B00&
         X1              =   2160
         X2              =   11760
         Y1              =   240
         Y2              =   240
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   10440
      Top             =   2500
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_DatosGenerales.frx":0802
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_DatosGenerales.frx":2594
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_DatosGenerales.frx":326E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_DatosGenerales.frx":3F48
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_DatosGenerales.frx":5CDA
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_DatosGenerales.frx":7A6C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_DatosGenerales.frx":97FE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Image CmdConfigurarTeclado 
      Height          =   660
      Left            =   7140
      MousePointer    =   99  'Custom
      Picture         =   "frmAsistentePro_DatosGenerales.frx":B590
      Stretch         =   -1  'True
      Top             =   1020
      Width           =   660
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_DatosGenerales.frx":D312
      Top             =   360
      Width           =   2700
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   27
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   29
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_DatosGenerales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private EventoProgramado As Boolean
Private mGrabo              As Boolean

'Private Sub ChkCombinarProductos_Click()
'    If ChkCombinarProductos.Value = vbChecked Then
'        txtCantLlevar.Visible = True
'        txtCantPagar.Visible = True
'        lblCantLlevar.Visible = True
'        lblCantPagar.Visible = True
'    Else
'        txtCantLlevar.Visible = False
'        txtCantPagar.Visible = False
'        lblCantLlevar.Visible = False
'        lblCantPagar.Visible = False
'        txtCantLlevar.Text = Empty
'        txtCantPagar.Text = Empty
'    End If
'End Sub

Private Sub chkCombinada_Click()
    If chkCombinada.Value = vbChecked Then
        If (TipoPromo = 16 Or TipoPromo = 17 Or TipoPromo = 18 _
        Or TipoPromo = 19 Or TipoPromo = 20 Or TipoPromo = 21) Then
        Else
            lblPrioridad.Visible = True
            lblNotaPrioridad.Visible = True
            txtPrioridad.Visible = True
        End If
    Else
        lblPrioridad.Visible = False
        lblNotaPrioridad.Visible = False
        txtPrioridad.Visible = False
    End If
End Sub

Private Sub cmbGrupoClientes_Click()
    If Not EventoProgramado Then
        '
    Else
        EventoProgramado = False
    End If
End Sub

Private Sub CmdAtras_Click()
    Unload Me
End Sub

Private Sub CmdCancelar_Click()
    If Mensaje(False, Replace(StellarMensaje(2620), "$(Line)", vbNewLine)) Then
        CancelarPromo_Salir = True
        Form_Activate
        Exit Sub
    End If
End Sub

Private Sub CmdConfigurarTeclado_Click()
    SeleccionarTexto CampoT
    TecladoAvanzado CampoT
End Sub

Private Sub CmdMonedaMonto_Click()
    CmdMonedaMonto_KeyDown vbKeyF2, 0
End Sub

Private Sub CmdMonedaMonto_KeyDown(KeyCode As Integer, Shift As Integer)
    If Not CmdMonedaMonto.Visible Then Exit Sub
    Select Case KeyCode
        Case Is = vbKeyF2
            Tecla_Pulsada = True
            Set Campo_Txt = txt_Moneda
            Set Campo_Lbl = lbl_Moneda
            Call MAKE_VIEW("MA_MONEDAS", "c_CodMoneda", "c_Descripcion", _
            "" & StellarMensaje(2855) & "", Me, "GENERICO", True, Campo_Txt, Campo_Lbl)
            MonedaDoc.BuscarMonedas , txt_Moneda
            lblMontoMinimo.Caption = "(" & MonedaDoc.SimMoneda & ") " & StellarMensajeLocal(20)
            lblMontoMaximo.Caption = "(" & MonedaDoc.SimMoneda & ") " & StellarMensajeLocal(21)
            Tecla_Pulsada = False
    End Select
End Sub

Private Sub CmdSiguiente_Click()
    Form_KeyDown vbKeyF4, 0
    If mGrabo Then
        Dim MetodoCarga As New frmAsistentePro_TipoMetodoCarga
        MetodoCarga.TipoSeleccion = False
        MetodoCarga.Show vbModal
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF7
            Call Cancelar_Registro
        Case vbKeyF4
            Call Grabar_Registro
        Case vbKeyReturn
            SafeSendKeys "{TAB}"
    End Select
End Sub

Private Sub Form_Load()
    
    If CancelarPromo_Salir Then Exit Sub
    
    lblTitulo.Caption = StellarMensajeLocal(125)
    
    Toolbar1.Buttons("Cancelar").Caption = StellarMensaje(105) 'cancelar
    Toolbar1.Buttons("Salir").Caption = StellarMensaje(2629) ' F12 Salir
    
    cmdAtras.Caption = StellarMensajeLocal(12)
    cmdSiguiente.Caption = StellarMensajeLocal(10)
    CmdCancelar.Caption = StellarMensajeLocal(11)
    
    lblSeccionDatosPromo.Caption = StellarMensajeLocal(149)
    lblSeccionPeriodo.Caption = StellarMensajeLocal(150)
    lblSeccionClientes.Caption = StellarMensajeLocal(151)
    lblSeccionOtros.Caption = StellarMensajeLocal(187)
    
    txt_Moneda.Text = FrmAppLink.CodMonedaPref
    lbl_Moneda.Caption = FrmAppLink.DesMonedaPref
    lbl_Moneda.Tag = FrmAppLink.SimMonedaPref
    
    Set MonedaTmp = FrmAppLink.ClaseMonedaTemp
    Set MonedaDoc = FrmAppLink.ClaseMonedaDocumento
    
    MonedaDoc.BuscarMonedas , txt_Moneda
    
    lblCodigo.Caption = StellarMensajeLocal(13)
    lblDescripcion.Caption = StellarMensajeLocal(14)
    lblPrioridad.Caption = StellarMensajeLocal(15)
    lblNotaPrioridad.Caption = StellarMensajeLocal(196) '* A N�mero mas alto mayor prioridad *
    lblCampanna.Caption = StellarMensajeLocal(24)
    lblAvisoImpuestosMontos.Caption = StellarMensajeLocal(184)
    lblFechaInicio.Caption = StellarMensajeLocal(16)
    lblFechaFin.Caption = StellarMensajeLocal(17)
    lblMontoMinimo.Caption = "(" & MonedaDoc.SimMoneda & ") " & StellarMensajeLocal(20)
    lblMontoMaximo.Caption = "(" & MonedaDoc.SimMoneda & ") " & StellarMensajeLocal(21)
    CmdMonedaMonto.Enabled = FrmAppLink.Promocion_CampoMonedaMontos
    CmdMonedaMonto.Visible = CmdMonedaMonto.Enabled
    lblDias.Caption = StellarMensajeLocal(22)
    lblHoraInicio.Caption = StellarMensajeLocal(18)
    lblHoraFin.Caption = StellarMensajeLocal(19)
    lblGrupo.Caption = StellarMensajeLocal(23)
    
    chkCombinada.Caption = StellarMensajeLocal(25)
    
    chkDias(1).Caption = StellarMensajeLocal(26)
    chkDias(2).Caption = StellarMensajeLocal(27)
    chkDias(3).Caption = StellarMensajeLocal(28)
    chkDias(4).Caption = StellarMensajeLocal(29)
    chkDias(5).Caption = StellarMensajeLocal(30)
    chkDias(6).Caption = StellarMensajeLocal(31)
    chkDias(7).Caption = StellarMensajeLocal(32)
    chkCumpleanneros.Caption = StellarMensajeLocal(33)
    
    optTodos.Caption = StellarMensajeLocal(34)
    optNoAfiliados.Caption = StellarMensajeLocal(36)
    optAfiliados.Caption = StellarMensajeLocal(35)
    
    'dtpFechaInicio.Value = Date
    'dtpFechaFin.Value = EndOfDay(DateSerial(9999, 1, 1))
    'dtpHoraInicio.Value = CDate(0)
    'dtpHoraFin.Value = EndOfDay(dtpHoraInicio.Value)
    
'    txtFechaDesde.Text = SDate(Now)
'    txtFechaDesde.Tag = CDbl(Date)
'    TxtFechaHasta.Text = SDate(DateSerial(9999, 1, 1))
'    TxtFechaHasta.Tag = CDbl(DateSerial(9999, 1, 1))
'
'    txtHoraInicio.Text = STime(CDate(0))
'    txtHoraInicio.Tag = CDbl(CDate(0))
'    txtHoraFin.Text = STime(EndOfDay(Now))
'    txtHoraFin.Tag = CDbl(EndOfDay(CDate(0)))
    
    Form_KeyDown vbKeyF7, 0
    
    ClsGrupos.cTipoGrupo = "CLI"
    ClsGrupos.CargarComboGrupos FrmAppLink.CnADM, cmbGrupoClientes, False
    FrmAppLink.ListRemoveItems cmbGrupoClientes, "Ninguno" ' Quitar dichos elementos unicamente aqu� sin alterar la funci�n anterior.
    
    txtCampanna.Text = Campanna
    
    If mCodPromo_Actual = Empty Then
        txtCodigo.Text = No_Consecutivo("Cod_Promocion", False) ' Espera
    Else
        If mCodPromo_Origen = Empty Then
            txtCodigo.Text = mCodPromo_Actual
        Else
            txtCodigo.Text = mCodPromo_Origen
        End If
    End If
    
    txtCodigo.Enabled = False
    
    Select Case TipoPromo
        
        Case 1, 2, 3
            
            chkCombinada.Enabled = True
            lblSeccionOtros.Visible = True
            LnSeccionOtros.Visible = True
            
        Case 16, 17, 18, 19, 20, 21
            
            chkCombinada.Enabled = True
            lblSeccionOtros.Visible = True
            LnSeccionOtros.Visible = True
            lblPrioridad.Visible = False
            lblNotaPrioridad.Visible = False
            txtPrioridad.Visible = False
            txtPrioridad.Text = 0
            chkCombinada.Height = 800
            lblAvisoImpuestosMontos.Visible = False
            
        Case Else
            
            chkCombinada.Visible = False
            lblSeccionOtros.Visible = False
            LnSeccionOtros.Visible = False
            
            'ChkCombinarProductos.Visible = True
            
            'lblCantLlevar.Visible = False
            'lblCantPagar.Visible = False
            'txtCantLlevar.Visible = False
            'txtCantPagar.Visible = False
            
    End Select
    
    txtPrioridad.Visible = False
    lblPrioridad.Visible = False
    lblNotaPrioridad.Visible = False
    
    'Select Case TipoPromo
        'Case 1, 3, 5, 7
            'chkConsideraImpuesto.Enabled = True
        'Case 2, 4, 6
            'chkConsideraImpuesto.Enabled = False
    'End Select
    
    CargarTemporal
    
    Call AjustarPantalla(Me)
    
End Sub

Private Sub Form_Activate()
    If CancelarPromo_Salir Or PromocionCreada Then
        Unload Me
        Exit Sub
    End If
End Sub

Private Sub CargarTemporal()
    
    On Error GoTo Error
    
    Dim RsPromociones As ADODB.Recordset
    Set RsPromociones = New ADODB.Recordset
    
    Set RsPromociones = FrmAppLink.CnADM.Execute("SELECT TOP 1 * FROM TMP_PROMOCION " & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ")
    
    With RsPromociones
        
        If Not .EOF Then
            
            txtDescripcion.Text = !Descripcion
            txtCampanna.Text = Campanna
            txtFechaDesde.Tag = CDbl(!Fecha_Inicio)
            txtFechaDesde.Text = SDate(CDate(txtFechaDesde.Tag))
            TxtFechaHasta.Tag = CDbl(!Fecha_Fin)
            TxtFechaHasta.Text = SDate(CDate(TxtFechaHasta.Tag))
            txtHoraInicio.Tag = CDbl(CDate(Split(!Aplica_Hora_Inicio, ".")(0)))
            txtHoraInicio.Text = STime(CDate(txtHoraInicio.Tag))
            txtHoraFin.Tag = CDbl(CDate(Split(!Aplica_Hora_Fin, ".")(0)))
            txtHoraFin.Text = STime(CDate(txtHoraFin.Tag))
            
            For I = 1 To 7
                chkDias(I).Value = IIf(!Aplica_Dia_Semana Like "*" & I & "*", vbChecked, vbUnchecked)
            Next I
            
            chkCombinada.Value = IIf(!Combina_Promocion, vbChecked, vbUnchecked)
            txtPrioridad.Text = SDec(!Prioridad_Promocion)
            
            Select Case !Tipo_Cliente
                Case 0
                    optTodos.Value = True
                Case 1 And Not chkCumpleanneros.Value = vbChecked
                    optAfiliados.Value = True
                    chkCumpleanneros.Value = vbUnchecked
                Case 2
                    optNoAfiliados.Value = True
                Case 3
                    optAfiliados.Value = True
                    chkCumpleanneros.Value = vbChecked
            End Select
            
            FrmAppLink.ListSafeItemSelection cmbGrupoClientes, !Grupo_Cliente
            
            txtMontoMinimo.Text = FormatNumber(!Monto_Minimo_Venta)
            txtMontoMaximo.Text = FormatNumber(!Monto_Maximo_Venta)
            
            'If lblSeccionMxN.Visible Then
                'If !Cantidad_Productos_Requerir > 0 Then
                    'ChkCombinarProductos.Value = vbChecked
                    'txtCantLlevar.Text = !Cantidad_Productos_Requerir
                    'txtCantPagar.Text = !Cantidad_Productos_Pagar
                'Else
                    'ChkCombinarProductos.Value = vbUnchecked
                    'txtCantLlevar.Text = Empty
                    'txtCantPagar.Text = Empty
                'End If
            'End If
            
            'chkConsideraImpuesto.Value = IIf(!Considera_Impuesto, _
            vbChecked, vbUnchecked)
            
            If CmdMonedaMonto.Enabled Then
                
                If Trim(!Cod_Moneda_Monto) <> Empty Then
                    txt_Moneda.Text = !Cod_Moneda_Monto
                Else
                    txt_Moneda.Text = FrmAppLink.CodMonedaPref
                End If
                
                MonedaDoc.BuscarMonedas , txt_Moneda
                ' De aqui en adelante hasta que culmine el proceso, no debemos cambiar la moneda
                ' de este objeto, la usaremos mas adelante. Si se necesita buscar otra moneda, usar
                ' una clase de moneda distinta. Ejemplo FrmAppLink.ClaseMonedaTemp
                
                lbl_Moneda.Caption = MonedaDoc.DesMoneda
                lbl_Moneda.Tag = MonedaDoc.SimMoneda
                
                lblMontoMinimo.Caption = "(" & MonedaDoc.SimMoneda & ") " & StellarMensajeLocal(20)
                lblMontoMaximo.Caption = "(" & MonedaDoc.SimMoneda & ") " & StellarMensajeLocal(21)
                
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub Header_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub optAfiliados_Click()
    If optAfiliados.Value Then
        chkCumpleanneros.Visible = True
    Else
        chkCumpleanneros.Value = vbUnchecked
        chkCumpleanneros.Visible = False
    End If
End Sub

Private Sub optNoAfiliados_Click()
    If optNoAfiliados.Value Then
        chkCumpleanneros.Value = vbUnchecked
        chkCumpleanneros.Visible = False
    End If
End Sub

Private Sub optTodos_Click()
    If optTodos.Value Then
        chkCumpleanneros.Value = vbUnchecked
        chkCumpleanneros.Visible = False
    End If
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        
        Case Is = "AGREGAR"
            'Call Form_KeyDown(vbKeyF3, 0)
        Case Is = "BUSCAR"
            'Call Form_KeyDown(vbKeyF2, 0)
        
        Case Is = "MODIFICAR"
            'Call Form_KeyDown(vbKeyF5, 0)
        
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF7, 0)
        
        Case Is = "BORRAR"
            ''Call Form_KeyDown(vbKeyF6, 0)
            
        Case Is = "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
        
        Case Is = "IMPRIMIR"
            'Call imprimir_registro
        
        Case Is = "SALIR"
            Unload Me
            'Call Form_KeyDown(vbKeyF12, 0)
        
        Case Is = "AYUDA"
            
    End Select
    
End Sub

Private Sub Cancelar_Registro()
    
    txtDescripcion.Text = Empty
    chkCombinada.Value = vbUnchecked
    txtPrioridad.Text = 0
    txtCampanna.Text = Campanna
    
    txtFechaDesde.Text = SDate(Now)
    txtFechaDesde.Tag = CDbl(Date)
    TxtFechaHasta.Text = SDate(DateSerial(9999, 1, 1))
    TxtFechaHasta.Tag = CDbl(DateSerial(9999, 1, 1))
    
    txtHoraInicio.Text = STime(CDate(0))
    txtHoraInicio.Tag = CDbl(CDate(0))
    txtHoraFin.Text = STime(EndOfDay(Now))
    txtHoraFin.Tag = CDbl(EndOfDay(CDate(0)))
    
    txtMontoMinimo.Text = 0
    txtMontoMaximo.Text = 0
    
    For I = 1 To 7
        chkDias(I).Value = vbChecked
    Next I
    
    optTodos.Value = vbChecked
    
    EventoProgramado = True
    ListSafeIndexSelection cmbGrupoClientes, 0 '(cmbGrupoClientes.ListCount - 1)
    EventoProgramado = False
    
    'ChkCombinarProductos.Value = vbUnchecked
    
    'Select Case TipoPromo
        'Case 1, 3, 5, 7
            'chkConsideraImpuesto.Value = vbUnchecked
        'Case 2, 4, 6
            'chkConsideraImpuesto.Value = vbChecked
    'End Select
    
    'txtCantLlevar.Text = Empty
    'txtCantPagar.Text = Empty
    
End Sub

Private Sub Grabar_Registro()
    
    On Error GoTo Error
    
    mGrabo = False
    
    Dim DiasActivos As String
    
    If txtDescripcion.Text = Empty Then
        Call Mensaje(True, StellarMensajeLocal(119))
        Exit Sub
    End If
    
    For I = 1 To 7
        If chkDias(I).Value = 1 Then
            DiasActivos = DiasActivos & CStr(chkDias(I).Index)
        End If
    Next I
    
    If Not IsNumeric(txtPrioridad.Text) Then txtPrioridad.Text = 0
    
    If (TipoPromo = 16 Or TipoPromo = 17 Or TipoPromo = 18 _
    Or TipoPromo = 19 Or TipoPromo = 20 Or TipoPromo = 21) Then
    Else
        If (chkCombinada.Value = vbChecked) And SVal(txtPrioridad.Text) = 0 Then
            'If Not Mensaje(False, "Ha especificado que la promoci�n combina con otras, pero no ha especificado un orden de prioridad. �Est� seguro de continuar?") Then
            If Not Mensaje(False, StellarMensajeLocal(202)) Then
                Exit Sub
            End If
        End If
    End If
    
    Dim RsPromociones As New ADODB.Recordset
    
    Call Apertura_Recordset(RsPromociones)
    
    RsPromociones.Open "SELECT TOP 1 * FROM TMP_PROMOCION " & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", _
    FrmAppLink.CnADM, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    With RsPromociones
        
        If Not RsPromociones.EOF Then
            .Update
        Else
            .AddNew
            !Cod_Promocion = mCodPromo_Actual
            !Considera_Impuesto = True
        End If
        
        !Descripcion = txtDescripcion.Text
        !Campa�a = Campanna '"Campanna Prueba"
        !Fecha_Inicio = FechaBD(CDate(txtFechaDesde.Tag), , True) 'dtpFechaInicio.Value
        !Fecha_Fin = FechaBD(CDate(TxtFechaHasta.Tag), , True) 'dtpFechaFin.Value
        !Aplica_Dia_Semana = DiasActivos
        !Aplica_Hora_Inicio = FechaBD(CDate(txtHoraInicio.Tag), FBD_HoraFull, True) 'dtpHoraInicio.Value
        !Aplica_Hora_Fin = FechaBD(CDate(txtHoraFin.Tag), FBD_HoraFull, True) 'dtpHoraFin.Value
        !Estatus = "TMP"
        !Combina_Promocion = (chkCombinada.Value = vbChecked)
        !Prioridad_Promocion = CDbl(txtPrioridad.Text)
        !Tipo_Promocion = TipoPromo
        
        Select Case True
            Case optTodos.Value
                !Tipo_Cliente = 0
            Case optAfiliados.Value And Not chkCumpleanneros.Value = vbChecked
                !Tipo_Cliente = 1
            Case optNoAfiliados.Value
                !Tipo_Cliente = 2
            Case optAfiliados.Value And chkCumpleanneros.Value = vbChecked
                !Tipo_Cliente = 3
        End Select
        
        !Grupo_Cliente = IIf(Trim(cmbGrupoClientes.Text) = vbNullString, _
        vbNullString, cmbGrupoClientes.Text)
        If Not IsNumeric(txtMontoMinimo.Text) Then txtMontoMinimo.Text = 0
        If Not IsNumeric(txtMontoMaximo.Text) Then txtMontoMaximo.Text = 0
        !Monto_Minimo_Venta = CDbl(txtMontoMinimo.Text)
        !Monto_Maximo_Venta = CDbl(txtMontoMaximo.Text)
        !CodUsuario = FrmAppLink.GetCodUsuario
        
        'If ChkCombinarProductos.Value = vbChecked _
        And IsNumeric(txtCantLlevar.Text) _
        And IsNumeric(txtCantPagar.Text) Then
            'CombinarProductos = True
            '!Cantidad_Productos_Requerir = CDbl(txtCantLlevar.Text)
            '!Cantidad_Productos_Pagar = CDbl(txtCantPagar.Text)
            'CantLlevar = !Cantidad_Productos_Requerir
            'CantPagar = !Cantidad_Productos_Pagar
        'Else
            '!Cantidad_Productos_Requerir = 0
            '!Cantidad_Productos_Pagar = 0
            'ChkCombinarProductos.Value = vbUnchecked
            'CombinarProductos = False
            'CantLlevar = 0: CantPagar = 0
        'End If
        
        '!Considera_Impuesto = (chkConsideraImpuesto.Value = vbChecked)
        
        If CmdMonedaMonto.Enabled Then
            !Cod_Moneda_Monto = txt_Moneda.Text
        End If
        
        .UpdateBatch
        
        mGrabo = True
        
    End With
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub txtDescripcion_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtDescripcion_GotFocus()
    Set CampoT = txtDescripcion
End Sub

Private Sub txtFechaDesde_Click()
    DatePicker_Fecha txtFechaDesde
End Sub

Private Sub TxtFechaHasta_Click()
    DatePicker_Fecha TxtFechaHasta
End Sub

Private Sub txtHoraFin_Click()
    DatePicker_Hora txtHoraFin
End Sub

Private Sub txtHoraInicio_Click()
    DatePicker_Hora txtHoraInicio
End Sub

Private Sub txtMontoMaximo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtMontoMaximo_GotFocus()
    Set CampoT = txtMontoMaximo
    SeleccionarTexto CampoT
End Sub

Private Sub txtMontoMaximo_KeyDown(KeyCode As Integer, Shift As Integer)
    CmdMonedaMonto_KeyDown KeyCode, Shift
End Sub

Private Sub txtMontoMinimo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtMontoMinimo_GotFocus()
    Set CampoT = txtMontoMinimo
    SeleccionarTexto CampoT
End Sub

Private Sub txtMontoMinimo_KeyDown(KeyCode As Integer, Shift As Integer)
    CmdMonedaMonto_KeyDown KeyCode, Shift
End Sub

Private Sub txtPrioridad_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtPrioridad_GotFocus()
    Set CampoT = txtPrioridad
End Sub
