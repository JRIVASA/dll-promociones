VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frmAsistentePro_ConfigListaBIN 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtMonedaLimite 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   4560
      TabIndex        =   19
      Top             =   0
      Visible         =   0   'False
      Width           =   330
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Excluir"
      Height          =   8895
      Left            =   180
      TabIndex        =   8
      Top             =   1860
      Width           =   11925
      Begin VB.TextBox txtItems 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1080
         Left            =   240
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   0
         Top             =   1560
         Width           =   8220
      End
      Begin VB.PictureBox Medir 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   435
         Left            =   540
         ScaleHeight     =   375
         ScaleWidth      =   495
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   5280
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.VScrollBar ScrollGrid 
         Height          =   5175
         LargeChange     =   10
         Left            =   11100
         TabIndex        =   11
         Top             =   2820
         Width           =   675
      End
      Begin VB.TextBox TxtNumPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   8220
         Width           =   780
      End
      Begin VB.TextBox txtPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   1980
         TabIndex        =   2
         Top             =   8220
         Width           =   780
      End
      Begin VB.CommandButton CmdSiguiente 
         Caption         =   "Siguiente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8760
         TabIndex        =   5
         Top             =   8220
         Width           =   1455
      End
      Begin VB.CommandButton CmdAtras 
         Caption         =   "Atras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7200
         TabIndex        =   4
         Top             =   8220
         Width           =   1455
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   10320
         TabIndex        =   6
         Top             =   8220
         Width           =   1455
      End
      Begin MSFlexGridLib.MSFlexGrid Grid 
         DragMode        =   1  'Automatic
         Height          =   5175
         Left            =   120
         TabIndex        =   1
         Top             =   2820
         Width           =   11655
         _ExtentX        =   20558
         _ExtentY        =   9128
         _Version        =   393216
         Cols            =   12
         FixedCols       =   0
         BackColor       =   16448250
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         ScrollBars      =   2
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblLimiteDctoTarjeta 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Limite de Tarjeta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   10920
         TabIndex        =   18
         Top             =   2220
         Visible         =   0   'False
         Width           =   750
         WordWrap        =   -1  'True
      End
      Begin VB.Image CmdLimiteDctoTarjeta 
         Height          =   600
         Left            =   10920
         Picture         =   "frmAsistentePro_ConfigListaBIN.frx":0000
         Stretch         =   -1  'True
         Top             =   1560
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.Label lblNotaPrioridad 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   $"frmAsistentePro_ConfigListaBIN.frx":4FDF
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   810
         Left            =   240
         TabIndex        =   17
         Top             =   600
         Width           =   11145
         WordWrap        =   -1  'True
      End
      Begin VB.Image CmdArchivo 
         Height          =   600
         Left            =   10200
         Picture         =   "frmAsistentePro_ConfigListaBIN.frx":512D
         Stretch         =   -1  'True
         Top             =   1560
         Width           =   600
      End
      Begin VB.Label lblArchivoPlano 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Archivo Plano"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   330
         Left            =   10200
         TabIndex        =   16
         Top             =   2220
         Width           =   630
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblDescBtnBorrar 
         BackStyle       =   0  'Transparent
         Caption         =   "Eliminar Fila"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   345
         Left            =   5040
         TabIndex        =   15
         Top             =   8280
         Width           =   690
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblDescBtnExcluir 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Excluir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   165
         Left            =   9480
         TabIndex        =   14
         Top             =   2355
         Visible         =   0   'False
         Width           =   510
      End
      Begin VB.Label lblDescBtnIncluir 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Incluir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   165
         Left            =   8700
         TabIndex        =   13
         Top             =   2355
         Width           =   495
      End
      Begin VB.Image CmdDelete 
         Height          =   480
         Left            =   4500
         Picture         =   "frmAsistentePro_ConfigListaBIN.frx":6EAF
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Image ImgNext 
         Height          =   480
         Left            =   1140
         Picture         =   "frmAsistentePro_ConfigListaBIN.frx":BCD5
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Image ImgPrev 
         Height          =   480
         Left            =   300
         Picture         =   "frmAsistentePro_ConfigListaBIN.frx":10A64
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   2880
         X2              =   3060
         Y1              =   8640
         Y2              =   8280
      End
      Begin VB.Image CmdExcluir 
         Height          =   600
         Left            =   9420
         Picture         =   "frmAsistentePro_ConfigListaBIN.frx":13F49
         Stretch         =   -1  'True
         Top             =   1605
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.Image CmdIncluir 
         Height          =   600
         Left            =   8640
         Picture         =   "frmAsistentePro_ConfigListaBIN.frx":17B36
         Stretch         =   -1  'True
         Top             =   1605
         Width           =   600
      End
      Begin VB.Label lblCriterios 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Lista de BINES"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   180
         TabIndex        =   10
         Top             =   120
         Width           =   1830
      End
      Begin VB.Line LnCriterios 
         BorderColor     =   &H00AE5B00&
         X1              =   2220
         X2              =   11750
         Y1              =   240
         Y2              =   240
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   6600
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigListaBIN.frx":18800
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigListaBIN.frx":1A592
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigListaBIN.frx":1B26C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigListaBIN.frx":1BF46
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigListaBIN.frx":1DCD8
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigListaBIN.frx":1FA6A
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigListaBIN.frx":217FC
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblMonedaLimite 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Moneda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   270
      Left            =   4320
      TabIndex        =   20
      Top             =   480
      Visible         =   0   'False
      Width           =   420
   End
   Begin VB.Image CmdConfigurarTeclado 
      Height          =   660
      Left            =   7080
      MousePointer    =   99  'Custom
      Picture         =   "frmAsistentePro_ConfigListaBIN.frx":2358E
      Stretch         =   -1  'True
      Top             =   1020
      Width           =   660
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_ConfigListaBIN.frx":25310
      Top             =   360
      Width           =   2700
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   7
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_ConfigListaBIN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private AnchoScrollBar      As Long
Private AnchoCampoScroll    As Long
Private pCargandoArchivo    As Boolean
Private pCargarLinea        As Boolean

Private DragStart, DragStop, DragRows, mDragEndRow

Private Enum GridCol
    ColRowID
    ColTipoCondicion
    ColItemNumber
    [ColCount]
End Enum

Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
  
  Dim Ctl As Control
  Dim bHandled As Boolean
  Dim bOver As Boolean
  
  For Each Ctl In Controls
    ' Is the mouse over the control
    On Error Resume Next
    bOver = (Ctl.Visible And IsOver(Ctl.hWnd, Xpos, Ypos))
    On Error GoTo 0
    
    If bOver Then
      ' If so, respond accordingly
      bHandled = True
      Select Case True
      
        Case TypeOf Ctl Is MSFlexGrid
          FlexGridScroll Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is PictureBox
          PictureBoxZoom Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is ListBox, TypeOf Ctl Is TextBox, TypeOf Ctl Is ComboBox
          ' These controls already handle the mousewheel themselves, so allow them to:
          If Ctl.Enabled Then Ctl.SetFocus
          
        Case Else
          bHandled = False

      End Select
      If bHandled Then Exit Sub
    End If
    bOver = False
  Next Ctl
  
  ' Scroll was not handled by any controls, so treat as a general message send to the form
  'Me.Caption = "Form Scroll " & IIf(Rotation < 0, "Down", "Up")
End Sub

Private Sub CmdArchivo_Click()
    
    On Error GoTo Error
    
    Set gClsArcBD = CreateObject("recsun.obj_ArchivoDB")
    
    gClsArcBD.Inicializar 0
    
    gClsArcBD.Adicionar_CamposDisponibles "BIN", "BIN", 3000
    
    Dim mValores, LenArr
    mValores = gClsArcBD.EJECUTAR
    If IsEmpty(mValores) Then Exit Sub
    
    MousePointer = vbHourglass
    DoEvents
    pCargandoArchivo = True
    ResetearCriterios
    
    LenArr = UBound(mValores)
    
    Dim pCuentaExito As Double, pCuentaFallo As Double, pCantRegistros As Double
    Dim mInfoFallidos As String: mInfoFallidos = Empty
    
    For I = 1 To LenArr
        
        Dim CodigoEntrada As String
        
        txtItems.Text = QuitarComillasSimples(Trim(mValores(I)(0)))
        
        CodigoEntrada = txtItems
        
        DoEvents
        
        pCargarLinea = False
        
        CmdIncluir_Click
        
        pCantRegistros = pCantRegistros + 1
        
        If pCargarLinea Then
            pCuentaExito = pCuentaExito + 1
        Else
            pCuentaFallo = pCuentaFallo + 1
            mInfoFallidos = mInfoFallidos & _
            "" & StellarMensaje(6144) & ": " & (I) & ", " & StellarMensaje(142) & ": " & CodigoEntrada & vbNewLine
        End If
        
    Next
    
    Dim mMsjEstatus As String
    
    mMsjEstatus = "" & StellarMensajeLocal(224) & ": (" & pCuentaExito & " " & _
    StellarMensajeLocal(169) & " " & pCantRegistros & ")" & vbNewLine
    
    If pCuentaFallo > 0 Then
        ActivarMensajeGrande 100
        mMsjEstatus = mMsjEstatus & _
        "" & StellarMensajeLocal(225) & ": (" & pCuentaFallo & ")" & vbNewLine
        mMsjEstatus = mMsjEstatus & _
        "" & StellarMensajeLocal(226) & ":" & vbNewLine & vbNewLine
        mMsjEstatus = mMsjEstatus & mInfoFallidos
    End If
    
    Mensaje True, mMsjEstatus
    
    CargarTemporal
    
Finally:
    
    txtItems = Empty
    
    pCargandoArchivo = False
    
    MousePointer = vbDefault
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CmdArchivo_Click)"
    
    GoTo Finally
    
End Sub

Private Sub CmdConfigurarTeclado_Click()
    SeleccionarTexto CampoT
    TecladoAvanzado CampoT
End Sub

Private Sub ValidarIngresarLimiteDescuento()
    
    Dim mMsj1, mMsj2, TmpCodMonedaLimite, TmpMontoLimite
    
    TmpCodMonedaLimite = BuscarValorBD("CodMonedaLimite", _
    "SELECT Top 1 Cod_Dpto As CodMonedaLimite FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
    "AND Tipo_Condicion = 9 ", Empty, FrmAppLink.CnADM)
    
    TmpMontoLimite = BuscarValorBD("MontoLimite", _
    "SELECT Top 1 Monto_Descuento As MontoLimite FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
    "AND Tipo_Condicion = 9 ", Empty, FrmAppLink.CnADM)
    
    If Trim(TmpCodMonedaLimite) <> Empty Then
        
        MonedaTmp.BuscarMonedas , TmpCodMonedaLimite
        
        'mMsj1 = "El L�mite de Descuento actualmente establecido es de " & _
        FormatoDecimalesDinamicos(CDec(TmpMontoLimite), , MonedaTmp.DecMoneda) & " " & _
        MonedaTmp.SimMoneda & " " & "(" & MonedaTmp.DesMoneda & "). "
        
        'mMsj2 = "Si desea modificar el l�mite actual y / o su moneda presione Aceptar."
        
        mMsj1 = Replace(StellarMensajeLocal(253), "$(ValorFmt)", _
        FormatoDecimalesDinamicos(CDec(TmpMontoLimite), , MonedaTmp.DecMoneda) & " " & _
        MonedaTmp.SimMoneda & " " & "(" & MonedaTmp.DesMoneda & ")")
        
        mMsj2 = StellarMensajeLocal(254)
        
    Else
        
        'mMsj1 = "Actualmente no ha definido un Monto L�mite de Descuento para la promoci�n. " & _
        "Si desea establecerlo, presione Aceptar, o Cancelar para regresar."
        
        mMsj1 = StellarMensajeLocal(255)
        
        mMsj2 = Empty
        
    End If
    
    If Mensaje(False, mMsj1 & mMsj2) Then
        
        Set Campo_Txt = txtMonedaLimite
        Set Campo_Lbl = lblMonedaLimite
        
        Campo_Txt = Empty
        Campo_Lbl = Empty
        
        Call MAKE_VIEW("MA_MONEDAS_USO_EN_POS", "c_CodMoneda", "c_Descripcion", _
        "" & StellarMensaje(2855) & "", Me, "GENERICO", True, Campo_Txt, Campo_Lbl)
        
        If Trim(Campo_Txt) <> Empty Then
            
            MonedaTmp.BuscarMonedas , Campo_Txt
            
            'Campo_Lbl = QuickInputRequest("La moneda seleccionada es " & MonedaTmp.DesMoneda & " " & _
            "(" & MonedaTmp.SimMoneda & "). Si es correcto, indique el monto l�mite de descuento.")
            Campo_Lbl = QuickInputRequest(Replace(StellarMensajeLocal(256), "$(CurrencyData)", _
            "" & MonedaTmp.DesMoneda & " " & "(" & MonedaTmp.SimMoneda & ")"))
            
            If SDec(Campo_Lbl) >= 0 Then
                
                'If Mensaje(False, "El monto introducido es: " & _
                FormatoDecimalesDinamicos(SDec(Campo_Lbl), , MonedaTmp.DecMoneda) & " " & _
                MonedaTmp.SimMoneda & vbNewLine & vbNewLine & "Si es correcto presione Aceptar " & _
                "para confirmar el cambio.") Then
                If Mensaje(False, Replace(StellarMensajeLocal(257), "$(Param1)", _
                FormatoDecimalesDinamicos(SDec(Campo_Lbl), , MonedaTmp.DecMoneda) & " " & _
                MonedaTmp.SimMoneda & vbNewLine & vbNewLine)) Then
                    
                    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
                    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
                    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
                    "AND Tipo_Condicion = 9 "
                    
                    If SDec(Campo_Lbl) > 0 Then
                        
                        FrmAppLink.CnADM.Execute "INSERT INTO TMP_PROMOCION_CONDICION_VALORES " & _
                        "(Cod_Promocion, CodUsuario, Tipo_Condicion, Cod_Dpto, Monto_Descuento) " & _
                        "SELECT '" & QuitarComillasSimples(mCodPromo_Actual) & "', " & _
                        "'" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "', " & _
                        "9, '" & MonedaTmp.CodMoneda & "', " & _
                        "(" & CDec(FormatoDecimalesDinamicos(CDec(Campo_Lbl), , MonedaTmp.DecMoneda)) & ") "
                        
                    End If
                    
                End If
                
            Else
                Mensaje True, StellarMensaje(16136) ' Dato inv�lido.
            End If
            
        End If
        
    End If
    
End Sub

Private Sub CmdLimiteDctoTarjeta_Click()
    ValidarIngresarLimiteDescuento
End Sub

Private Sub CmdSiguiente_Click()
    
    If Not ((Grid.TextMatrix(1, ColRowID) <> Empty _
    And Grid.TextMatrix(1, ColTipoCondicion) <> Empty) _
    Or SVal(TxtNumPag) > 1) Then
        Mensaje True, StellarMensajeLocal(197)
        Exit Sub
    End If
    
    If TipoPromo = 16 Then
        TipoPromo = 19
    ElseIf TipoPromo = 17 Then
        TipoPromo = 20
    ElseIf TipoPromo = 18 Then
        TipoPromo = 21
    End If
    
    frmAsistentePro_CargarSucursales.Show vbModal
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
    If TipoPromo = 19 Then
        TipoPromo = 16
    ElseIf TipoPromo = 20 Then
        TipoPromo = 17
    ElseIf TipoPromo = 21 Then
        TipoPromo = 18
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Cancel = 0 Then
        Call WheelUnHook(Me.hWnd)
    End If
End Sub

Private Sub Grid_Scroll()
    'DoEvents
    'ScrollGrid.Value = Grid.Row
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
    Exit Sub
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Private Sub CmdAtras_Click()
    Unload Me
End Sub

Private Sub CmdCancelar_Click()
    If Mensaje(False, Replace(StellarMensaje(2620), "$(Line)", vbNewLine)) Then
        CancelarPromo_Salir = True
        Form_Activate
        Exit Sub
    End If
End Sub

Private Sub Grid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CmdDelete_Click
    End If
End Sub

Private Sub Grid_DragDrop(Source As Control, x As Single, y As Single)
    
    If Grid.RowHeightMin = 0 Then Exit Sub
    
    On Error Resume Next
    
    DragStop = y
    
    Drag = (DragStop - DragStart)
    
    If Abs(Drag / Grid.RowHeightMin) > 1 Then
        
        DragRows = (Drag / Grid.RowHeightMin)
        
        mDragEndRow = (Grid.TopRow + (-1 * DragRows))
        
        If mDragEndRow < Grid.FixedRows Then
            mDragEndRow = Grid.FixedRows
        ElseIf mDragEndRow > Grid.Rows Then
            mDragEndRow = Grid.Rows - 1
        End If
        
        Grid.TopRow = mDragEndRow
        ScrollGrid.Value = Grid.TopRow
        
    End If
    
    DragStart = 0
    
End Sub

Private Sub Grid_DragOver(Source As Control, x As Single, y As Single, State As Integer)
    
    On Error Resume Next
    
    If DragStart = 0 Then
        DragStart = y
    End If
    
End Sub

Private Sub ImgNext_Click()
    
    If SVal(txtPag.Text) <= SVal(TxtNumPag.Text) Then
        txtPag.Text = SVal(txtPag.Text) + 1
        If SVal(txtPag.Text) > SVal(TxtNumPag.Text) Then txtPag.Text = SVal(TxtNumPag.Text)
    Else
        txtPag.Text = SVal(TxtNumPag.Text)
    End If
    
    CargarTemporal
    
End Sub

Private Sub ImgPrev_Click()
    
    If SVal(txtPag.Text) >= 1 Then
        txtPag.Text = SVal(txtPag.Text) - 1
        If SVal(txtPag.Text) <= 1 Then txtPag.Text = 1
    Else
        txtPag.Text = 1
    End If
    
    CargarTemporal
    
End Sub

Private Sub CargarGrid()
    
    With Grid
        
        .Rows = 1
        .Row = 0
        .Clear
        .Cols = ColCount
        .RowHeightMin = 600
        
        .Col = ColRowID
        .TextMatrix(0, .Col) = "Ln" '"RowID"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColTipoCondicion
        .TextMatrix(0, .Col) = "TipoCondicion"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColItemNumber
        .TextMatrix(0, .Col) = "BIN / IIN (BANK / ISSUER IDENTIFICATION NUMBER)"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .DragMode = 0
        .AllowUserResizing = flexResizeColumns
        '.AllowUserResizing = flexResizeNone
        .ColWidth(ColRowID) = 375
        .ColWidth(ColTipoCondicion) = 0
        .ColWidth(ColItemNumber) = 11250
        
        AnchoCampoScroll = .ColWidth(ColItemNumber)
        
    End With
    
End Sub

Private Sub Form_Load()
    
    If CancelarPromo_Salir Then Exit Sub
    
    lblTitulo.Caption = StellarMensajeLocal(125) 'Asistente para Crear Promoci�n
    lblCriterios.Caption = StellarMensajeLocal(259) 'lista de bines
    
    CmdAtras.Caption = StellarMensajeLocal(12)
    CmdSiguiente.Caption = StellarMensajeLocal(10)
    CmdCancelar.Caption = StellarMensajeLocal(11)
    
    lblDescBtnIncluir.Caption = StellarMensajeLocal(175) '"Incluir
    lblDescBtnExcluir.Caption = StellarMensajeLocal(260) '"Reiniciar
    lblDescBtnBorrar.Caption = StellarMensajeLocal(177) '"Eliminar Fila
    
    lblArchivoPlano.Caption = StellarMensajeLocal(216) 'archivo plano
    lblLimiteDctoTarjeta.Caption = StellarMensajeLocal(270) 'limite de tarjeta
    
    lblDescBtnExcluir.Visible = True
    CmdExcluir.Visible = True
    
    CmdLimiteDctoTarjeta.Visible = False 'True
    lblLimiteDctoTarjeta.Visible = False 'True
    
    CargarGrid
    CargarTemporal
    
    Call AjustarPantalla(Me)
    
    If Not FrmAppLink.DebugVBApp Then
        Call WheelHook(Me.hWnd)
    End If
    
End Sub

Private Sub Form_Activate()
    If CancelarPromo_Salir Or PromocionCreada Then
        Unload Me
    End If
End Sub

Private Sub Header_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        Case Is = "AGREGAR"
            'Call Form_KeyDown(vbKeyF3, 0)
        Case Is = "BUSCAR"
            'Call Form_KeyDown(vbKeyF2, 0)
        
        Case Is = "MODIFICAR"
            'Call Form_KeyDown(vbKeyF5, 0)
        
        Case Is = "CANCELAR"
            'Call Form_KeyDown(vbKeyF7, 0)
        
        Case Is = "BORRAR"
            ''Call Form_KeyDown(vbKeyF6, 0)
        
        Case Is = "GRABAR"
            'Call Grabar_Registro
            'Call Form_KeyDown(vbKeyF4, 0)
        
        Case Is = "IMPRIMIR"
            'Call imprimir_registro
        
        Case Is = "SALIR"
            Unload Me
            'Call Form_KeyDown(vbKeyF12, 0)
        
        Case Is = "AYUDA"
                
    End Select
    
End Sub

Private Sub InsertarCondicion(ByVal pTipo As Integer)
    
    On Error GoTo Error
    
    Dim RsCondicion As New ADODB.Recordset
    Dim RsValor As New ADODB.Recordset
    Dim SQL As String
    Dim SqlV As String
    
    If Replace(Replace(Trim(txtItems), vbCr, Empty), vbLf, Empty) <> Empty Then
        
        'If (pTipo = 8) Then
            
            'If mValidarObligatorios Then
                
                Dim mRsDatos As ADODB.Recordset
                Set mRsDatos = New ADODB.Recordset
                Apertura_RecordsetC mRsDatos
                
                Dim mCriterio As String
                
                Dim mListaItems As Variant
                
                mListaItems = Split(txtItems, ",", , vbTextCompare)
                
                If pTipo = 8 And UBound(mListaItems) >= 0 Then
                    
                    For I = 0 To UBound(mListaItems)
                        
                        SQL = _
                        "IF NOT EXISTS( " & _
                        "SELECT Marca FROM TMP_PROMOCION_CONDICION_VALORES " & _
                        "WHERE 1 = 1 " & _
                        "AND Tipo_Condicion = 8 " & _
                        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
                        "AND CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & _
                        "AND Marca = '" & QuitarComillasSimples(Trim(mListaItems(I))) & "') " & _
                        "INSERT INTO TMP_PROMOCION_CONDICION_VALORES " & _
                        "(CodUsuario, Tipo_Condicion, Cod_Promocion, Marca) " & _
                        "SELECT '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' AS CodUsuario, " & _
                        "8 AS Tipo_Condicion, '" & QuitarComillasSimples(mCodPromo_Actual) & "' AS Cod_Promocion, " & _
                        "'" & QuitarComillasSimples(Trim(mListaItems(I))) & "' AS Marca "
                        
                        FrmAppLink.CnADM.Execute SQL, RecordsAffected
                        
                    Next
                    
                    If Not pCargandoArchivo Then
                        ShowTooltip StellarMensajeLocal(185), 700, 1250, CmdIncluir, 0, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "7", True)
                    End If
                    
                    pCargarLinea = True
                    
                'ElseIf pTipo = 6 Then
                    
                End If
                
                ResetearCriterios
                
            'Else
                
            'End If
            
        'Else
            'If Not pCargandoArchivo Then
                'Mensaje True, StellarMensajeLocal(123) '"Los datos deben ser num�ricos y positivos."
                'If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                'ShowTooltip "<--", 500, 800, txtCampoPromo, 3, _
                &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
            'End If
            'Exit Sub
        'End If
        
    Else
        If Not pCargandoArchivo Then
            Mensaje True, StellarMensaje(16404) '"Debe seleccionar al menos un criterio"
        End If
    End If
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    If Not pCargandoArchivo Then
        MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(InsertarCondicion)"
    End If
    
End Sub

Private Sub CmdIncluir_Click()
    'If mVarEsPremio Then
        'InsertarCondicion 3
    'Else
        InsertarCondicion 8
    'End If
    If Not pCargandoArchivo Then
        CargarTemporal
    End If
End Sub

Private Sub CmdExcluir_Click()
    
    If Mensaje(False, StellarMensaje(16159)) Then
        
        FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND Tipo_Condicion = 8 "
        
        CargarTemporal
        
    End If
    
End Sub

Private Sub CargarTemporal()
    
    On Error GoTo Error
    
    'CargarGrid
    
    Dim LastRow As Integer, LastCtl As Object
    Dim NumRows As Double, NumPag As Double
    Dim RsCondicion As ADODB.Recordset
    Set RsCondicion = New ADODB.Recordset
    
    Dim mSQL As String
    
    mSQL = "SELECT isNULL(COUNT(ID), 0) AS NumReg " & GetLines & _
    "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & GetLines & _
    "AND Tipo_Condicion = 8 " & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' "
    
    Set RsCondicion = FrmAppLink.CnADM.Execute(mSQL)
    
    With RsCondicion
        
        If Not .EOF Then
            
            NumRows = !NumReg
            
            If NumRows > 0 Then
                
                NumPag = Round(NumRows / MaxGridRowsxPag, 8)
                If (NumPag - Fix(NumPag)) <> 0 Then NumPag = Fix(NumPag + 1)
                
                TxtNumPag.Text = NumPag
                
                txtPag.Text = Fix(SVal(txtPag))
                
                If SVal(txtPag.Text) >= 1 And SVal(txtPag.Text) <= NumPag Then
                    txtPag.Text = Fix(SVal(txtPag))
                Else
                    txtPag.Text = 1
                End If
                
                mSQL = "WITH AllRows AS (" & GetLines & _
                "SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowID, *" & GetLines & _
                "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
                "WHERE CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
                "AND Tipo_Condicion = 8 " & _
                "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
                ")" & GetLines & _
                "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                "((" & txtPag.Text & " - 1) * (" & MaxGridRowsxPag & ") + 1) AND " & _
                "(" & txtPag.Text & " * " & MaxGridRowsxPag & ")" & GetLines
                
                RsCondicion.Close
                
                Set RsCondicion = New Recordset
                Apertura_RecordsetC RsCondicion
                
                .Open mSQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If Not .EOF Then
                    
                    .Sort = "RowID"
                    
                    Dim I As Integer
                    
                    LastRow = Grid.Row
                    Set LastCtl = Screen.ActiveControl
                    
                    Grid.Visible = False
                    
                    Grid.Rows = 1
                    
                    ScrollGrid.Min = 0
                    ScrollGrid.Max = .RecordCount
                    ScrollGrid.Value = ScrollGrid.Min
                    
                    While Not .EOF
                        
                        Grid.Rows = Grid.Rows + 1
                        I = I + 1
                        
                        Grid.TextMatrix(I, ColRowID) = I
                        Grid.TextMatrix(I, ColTipoCondicion) = !Tipo_Condicion
                        Grid.TextMatrix(I, ColItemNumber) = !Marca
                        
                        .MoveNext
                        
                    Wend
                    
                    Grid.Visible = True
                    
                    If LastRow >= 1 And LastRow <= Grid.Rows - 1 Then
                        Grid.Row = LastRow
                        Grid.TopRow = Grid.Row
                    Else
                        Grid.Row = 1
                        Grid.TopRow = Grid.Row
                    End If
                    
                    If Not LastCtl Is Nothing Then
                        If PuedeObtenerFoco(LastCtl) Then LastCtl.SetFocus
                    End If
                    
                End If
                
            Else
                NumPag = 1
                txtPag.Text = NumPag
                Grid.Rows = 1
                Grid.Rows = 2
            End If
            
            If Grid.Rows > 8 Then
                ScrollGrid.Visible = True
                Grid.ScrollBars = flexScrollBarVertical
                AnchoScrollBar = ScrollGrid.Width
                Grid.ColWidth(ColItemNumber) = (AnchoCampoScroll - AnchoScrollBar)
            Else
                Grid.ScrollBars = flexScrollBarHorizontal
                ScrollGrid.Visible = False
                AnchoScrollBar = 0
                Grid.ColWidth(ColItemNumber) = AnchoCampoScroll
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    'Resume ' Debug
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CargarTemporal)"
    
    CargarGrid
    Grid.Visible = True
    
End Sub

Private Sub CmdDelete_Click()
    
    If Grid.Row > 0 And Grid.TextMatrix(Grid.Row, ColTipoCondicion) <> Empty Then
        
        On Error GoTo Error
        
        Dim RowD As Integer, mSQL As String
        
        RowD = Grid.Row
        
        mSQL = "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
        "WHERE Marca = '" & QuitarComillasSimples(Grid.TextMatrix(RowD, ColItemNumber)) & "' " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
        "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
        "AND Tipo_Condicion = " & QuitarComillasSimples(Grid.TextMatrix(RowD, ColTipoCondicion)) & " " & GetLines
        
        FrmAppLink.CnADM.Execute mSQL
        
        CargarTemporal
        
        Grid.ColSel = ColCount - 1
        
    End If
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub txtPag_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtPag_GotFocus()
    Set CampoT = txtPag
    txtPag.Tag = SVal(txtPag.Text)
End Sub

Private Sub txtPag_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtPag_LostFocus
    End If
End Sub

Private Sub txtPag_LostFocus()
    If SVal(txtPag.Text) <> SVal(txtPag.Tag) Then
        CargarTemporal
    End If
End Sub

Private Sub ResetearCriterios( _
Optional pCheckAll As Boolean = False)
    txtItems = Empty
End Sub
