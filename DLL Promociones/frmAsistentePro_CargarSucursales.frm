VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form frmAsistentePro_CargarSucursales 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9540
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9540
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   7395
      Left            =   240
      TabIndex        =   10
      Top             =   1920
      Width           =   11685
      Begin VB.VScrollBar ScrollGrid 
         Height          =   4935
         LargeChange     =   10
         Left            =   10740
         TabIndex        =   13
         Top             =   1560
         Width           =   675
      End
      Begin VB.CommandButton CmdSiguiente 
         Caption         =   "Finalizar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8340
         TabIndex        =   5
         Top             =   6720
         Width           =   1455
      End
      Begin VB.TextBox txtPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   1920
         TabIndex        =   3
         Top             =   6720
         Width           =   780
      End
      Begin VB.TextBox TxtNumPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   3180
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   6720
         Width           =   780
      End
      Begin VB.TextBox txtCodSucursal 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2160
         TabIndex        =   0
         Top             =   960
         Width           =   1425
      End
      Begin VB.CommandButton CmdBuscarSucursales 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Left            =   3720
         Picture         =   "frmAsistentePro_CargarSucursales.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   960
         Width           =   435
      End
      Begin VB.CommandButton CmdAtras 
         Caption         =   "Atras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   6780
         TabIndex        =   4
         Top             =   6720
         Width           =   1455
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9900
         TabIndex        =   6
         Top             =   6720
         Width           =   1455
      End
      Begin MSFlexGridLib.MSFlexGrid Grid 
         Height          =   4935
         Left            =   240
         TabIndex        =   2
         Top             =   1560
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   8705
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         ScrollBars      =   2
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Image CmdDeleteSucursales 
         Height          =   480
         Left            =   8100
         Picture         =   "frmAsistentePro_CargarSucursales.frx":0802
         Stretch         =   -1  'True
         Top             =   900
         Width           =   480
      End
      Begin VB.Image CmdDelete 
         Height          =   480
         Left            =   5100
         Picture         =   "frmAsistentePro_CargarSucursales.frx":5628
         Stretch         =   -1  'True
         Top             =   6720
         Width           =   480
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   2820
         X2              =   3000
         Y1              =   7140
         Y2              =   6780
      End
      Begin VB.Image ImgPrev 
         Height          =   480
         Left            =   240
         Picture         =   "frmAsistentePro_CargarSucursales.frx":A44E
         Stretch         =   -1  'True
         Top             =   6720
         Width           =   480
      End
      Begin VB.Image ImgNext 
         Height          =   480
         Left            =   1080
         Picture         =   "frmAsistentePro_CargarSucursales.frx":D933
         Stretch         =   -1  'True
         Top             =   6720
         Width           =   480
      End
      Begin VB.Label lblSucursal 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4260
         TabIndex        =   7
         Top             =   960
         UseMnemonic     =   0   'False
         Width           =   3720
      End
      Begin VB.Label lblSubtitulo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione las Sucursales donde se aplicara la Promoción"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   2100
         TabIndex        =   11
         Top             =   300
         Width           =   6030
      End
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoción"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   9
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_CargarSucursales.frx":126C2
      Top             =   360
      Width           =   2700
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_CargarSucursales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Enum GridCol
    ColRowID
    ColCodSuc
    ColDescSuc
    ColDireccion
    ColGerente
    [ColCount]
End Enum

Private AnchoScrollBar      As Long
Private AnchoCampoScroll    As Long

Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
  
  Dim Ctl As Control
  Dim bHandled As Boolean
  Dim bOver As Boolean
  
  For Each Ctl In Controls
    ' Is the mouse over the control
    On Error Resume Next
    bOver = (Ctl.Visible And IsOver(Ctl.hWnd, Xpos, Ypos))
    On Error GoTo 0
    
    If bOver Then
      ' If so, respond accordingly
      bHandled = True
      Select Case True
      
        Case TypeOf Ctl Is MSFlexGrid
          FlexGridScroll Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is PictureBox
          PictureBoxZoom Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is ListBox, TypeOf Ctl Is TextBox, TypeOf Ctl Is ComboBox
          ' These controls already handle the mousewheel themselves, so allow them to:
          If Ctl.Enabled Then Ctl.SetFocus
          
        Case Else
          bHandled = False

      End Select
      If bHandled Then Exit Sub
    End If
    bOver = False
  Next Ctl
  
  ' Scroll was not handled by any controls, so treat as a general message send to the form
  'Me.Caption = "Form Scroll " & IIf(Rotation < 0, "Down", "Up")
End Sub

Private Sub CmdAtras_Click()
    Unload Me
End Sub

Private Sub CmdBuscarSucursales_Click()
    
    On Error GoTo Error
    
    Dim Frm_Super_Consultas As Object
    Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    Frm_Super_Consultas.Inicializar "SELECT TOP " & MaxGridRowsxPag & " " & _
    "c_Codigo, c_Descripcion, c_Direccion, c_Gerente FROM MA_SUCURSALES", _
    UCase(Stellar_Mensaje(1254)), FrmAppLink.CnADM, , True
    
    Frm_Super_Consultas.Add_ItemLabels UCase(Stellar_Mensaje(142)), "c_Codigo", 3000, 0
    Frm_Super_Consultas.Add_ItemLabels UCase(Stellar_Mensaje(143)), "c_Descripcion", 5000, 0
    Frm_Super_Consultas.Add_ItemLabels UCase(Stellar_Mensaje(6099)), "c_Gerente", 2000, 0
    Frm_Super_Consultas.Add_ItemLabels Replace(UCase(StellarMensaje(130)), ":", Empty), _
    "c_Direccion", 10000, 0
    
    Frm_Super_Consultas.Add_ItemSearching UCase(Stellar_Mensaje(143)), "c_Descripcion"
    Frm_Super_Consultas.Add_ItemSearching UCase(Stellar_Mensaje(142)), "c_Codigo"
    Frm_Super_Consultas.Add_ItemSearching Replace(UCase(StellarMensaje(130)), ":", Empty), "c_Direccion"
    Frm_Super_Consultas.Add_ItemSearching UCase(Stellar_Mensaje(6099)), "c_Gerente"
    
    Frm_Super_Consultas.txtDato.Text = "%"
    Frm_Super_Consultas.StrOrderBy = "c_Descripcion"
    Frm_Super_Consultas.BusquedaInstantanea = True
    Frm_Super_Consultas.Show vbModal
    
    Dim mResultados As Collection
    Set mResultados = Frm_Super_Consultas.ArrResultado
    
    Set Frm_Super_Consultas = Nothing
    
    If Not mResultados Is Nothing Then
        
        Dim TmpItm, TransActive As Boolean, mSQL As String
        
        FrmAppLink.CnADM.BeginTrans
        TransActive = True
        
        For Each TmpItm In mResultados
            
            mSQL = "IF NOT EXISTS(SELECT Cod_Localidad FROM TMP_PROMOCION_SUCURSAL " & _
            "WHERE CodUsuario = '" & FrmAppLink.GetCodUsuario & "' AND Cod_Promocion = '' AND Cod_Localidad = '" & TmpItm(0) & "') " & GetLines & _
            "INSERT INTO TMP_PROMOCION_SUCURSAL (Cod_Promocion, Cod_Localidad, CodUsuario, Des_Localidad, Dir_Localidad, Ger_Localidad) " & _
            "VALUES ('" & mCodPromo_Actual & "', '" & TmpItm(0) & "', '" & FrmAppLink.GetCodUsuario & "', " & _
            "'" & TmpItm(1) & "', '" & TmpItm(2) & "', '" & TmpItm(3) & "')"
            
            FrmAppLink.CnADM.Execute mSQL
            
        Next
        
        FrmAppLink.CnADM.CommitTrans
        TransActive = False
        
        CargarTemporal
        
    End If
    
    Exit Sub
    
Error:
    
    If TransActive Then
        FrmAppLink.CnADM.RollbackTrans
        TransActive = False
    End If

    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub CmdCancelar_Click()
    If Mensaje(False, Replace(StellarMensaje(2620), "$(Line)", vbNewLine)) Then
        CancelarPromo_Salir = True
        Form_Activate
        Exit Sub
    End If
End Sub

Private Sub CargarGrid()
    
    With Grid
        
        .Rows = 1
        .Row = 0
        .Clear
        .Cols = ColCount
        .RowHeight(0) = 425
        
        .Col = ColRowID
        .TextMatrix(0, .Col) = "Ln" '"RowID"
        .ColAlignment(.Col) = flexAlignCenterCenter 'flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter 'flexAlignRightCenter
        
        .Col = ColCodSuc
        .TextMatrix(0, .Col) = StellarMensaje(142) '"Codigo"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDescSuc
        .TextMatrix(0, .Col) = StellarMensaje(212) '"Sucursal"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDireccion
        .TextMatrix(0, .Col) = Replace(StellarMensaje(130), ":", Empty) 'Direccion
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColGerente
        .TextMatrix(0, .Col) = StellarMensaje(6099) '"Gerente"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .RowHeightMin = 600
        .DragMode = 0
        '.AllowUserResizing = flexResizeColumns
        .AllowUserResizing = flexResizeNone
        .ColWidth(ColRowID) = 600
        .ColWidth(ColCodSuc) = 2000
        .ColWidth(ColDescSuc) = 3500
        .ColWidth(ColDireccion) = 2800
        .ColWidth(ColGerente) = 2250
        AnchoCampoScroll = .ColWidth(ColDescSuc)
        
    End With
    
End Sub

Private Sub CmdDelete_Click()
    
    If Grid.Row > 0 Then
        
        On Error GoTo Error
        
        Dim RowD As Integer, mSQL As String
        
        RowD = Grid.Row
        
        mSQL = "DELETE FROM TMP_PROMOCION_SUCURSAL " & GetLines & _
        "WHERE Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & GetLines & _
        "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
        "AND Cod_Localidad = '" & QuitarComillasSimples(Grid.TextMatrix(RowD, ColCodSuc)) & "' " & GetLines
        
        FrmAppLink.CnADM.Execute mSQL
        
        CargarTemporal
        
        Grid.ColSel = ColCount - 1
        
    End If
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub CmdDeleteSucursales_Click()
    
    On Error GoTo Error
    
    Dim Frm_Super_Consultas As Object
    Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    Frm_Super_Consultas.Inicializar "SELECT TOP " & MaxGridRowsxPag & " " & _
    "c_Codigo, c_Descripcion, c_Direccion, c_Gerente FROM MA_SUCURSALES", _
    UCase(Stellar_Mensaje(1254)), FrmAppLink.CnADM, , True
    
    Frm_Super_Consultas.Add_ItemLabels UCase(Stellar_Mensaje(142)), "c_Codigo", 3000, 0
    Frm_Super_Consultas.Add_ItemLabels UCase(Stellar_Mensaje(143)), "c_Descripcion", 5000, 0
    Frm_Super_Consultas.Add_ItemLabels UCase(Stellar_Mensaje(6099)), "c_Gerente", 2000, 0
    Frm_Super_Consultas.Add_ItemLabels Replace(UCase(StellarMensaje(130)), ":", Empty), _
    "c_Direccion", 10000, 0
    
    Frm_Super_Consultas.Add_ItemSearching UCase(Stellar_Mensaje(143)), "c_Descripcion"
    Frm_Super_Consultas.Add_ItemSearching UCase(Stellar_Mensaje(142)), "c_Codigo"
    Frm_Super_Consultas.Add_ItemSearching Replace(UCase(StellarMensaje(130)), ":", Empty), "c_Direccion"
    Frm_Super_Consultas.Add_ItemSearching UCase(Stellar_Mensaje(6099)), "c_Gerente"
    
    Frm_Super_Consultas.txtDato.Text = "%"
    Frm_Super_Consultas.StrOrderBy = "c_Descripcion"
    Frm_Super_Consultas.BusquedaInstantanea = True
    Frm_Super_Consultas.Show vbModal
    
    Dim mResultados As Collection
    Set mResultados = Frm_Super_Consultas.ArrResultado
    
    Set Frm_Super_Consultas = Nothing
    
    If Not mResultados Is Nothing Then
        
        Dim TmpItm, TransActive As Boolean, mSQL As String
        
        FrmAppLink.CnADM.BeginTrans
        TransActive = True
        
        For Each TmpItm In mResultados
            
            mSQL = "DELETE FROM TMP_PROMOCION_SUCURSAL " & GetLines & _
            "WHERE Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & GetLines & _
            "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
            "AND Cod_Localidad = '" & QuitarComillasSimples(TmpItm(0)) & "' " & GetLines
            
            FrmAppLink.CnADM.Execute mSQL
            
        Next
        
        FrmAppLink.CnADM.CommitTrans
        TransActive = False
        
        CargarTemporal
        
    End If
    
    Exit Sub
    
Error:
    
    If TransActive Then
        FrmAppLink.CnADM.RollbackTrans
        TransActive = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub CmdSiguiente_Click()
    
    Dim RsSucursales As ADODB.Recordset
    Set RsSucursales = New ADODB.Recordset
    
    Dim mSQL As String
    
    mSQL = "SELECT isNULL(COUNT(ID), 0) AS NumReg " & GetLines & _
    "FROM TMP_PROMOCION_SUCURSAL " & GetLines & _
    "WHERE CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines
    
    Set RsSucursales = FrmAppLink.CnADM.Execute(mSQL)
    
    If Not RsSucursales.EOF Then
        If RsSucursales!NumReg <= 0 Then
            Mensaje True, StellarMensajeLocal(134)
            Exit Sub
        End If
    End If
    
    If Mensaje(False, StellarMensajeLocal(126)) Then
        
        ' Coming Soon
        
        Set FrmSeleccion = FrmAppLink.GetFrmSeleccionDual
        
        With FrmSeleccion
            
            .lbl_Organizacion.Caption = StellarMensajeLocal(125)
            .CmdResumido.Caption = StellarMensajeLocal(133)
            .cmdDetallado.Caption = StellarMensajeLocal(127)
            .AccionBotonResumido = "DPE"
            .AccionBotonDetallado = "DWT"
            .CmdResumido.Enabled = (FrmAppLink.GetNivelUsuario >= _
            Val(BuscarReglaNegocioStr("Promociones_NivelGrabar", "1")))
            If Not .CmdResumido.Enabled Then
                .CmdResumido.BackColor = .BackColor
                .CmdResumido.ForeColor = lblSubtitulo.ForeColor
            End If
            .Show vbModal
            SaveStatus = .Accion
            Set FrmSeleccion = Nothing
            
        End With
        
        If SaveStatus = "DPE" Then
            If GrabarPromocion Then
                PromocionCreada = True
                Form_Activate
                Exit Sub
            End If
        ElseIf SaveStatus = "DWT" Then
            If GrabarEnEspera Then
                PromocionCreada = True
                Form_Activate
                Exit Sub
            End If
        End If
        
    End If
    
End Sub

Private Sub Form_Load()
    
    If CancelarPromo_Salir Then Exit Sub
    
    lblTitulo.Caption = StellarMensajeLocal(125)
    lblSubtitulo.Caption = StellarMensajeLocal(124)
    
    CmdAtras.Caption = StellarMensajeLocal(12)
    CmdSiguiente.Caption = StellarMensajeLocal(10)
    CmdCancelar.Caption = StellarMensajeLocal(11)
    
    CargarGrid
    CargarTemporal
    
    Call AjustarPantalla(Me)
    
    If Not FrmAppLink.DebugVBApp Then
        Call WheelHook(Me.hWnd)
    End If
    
End Sub

Private Sub Form_Activate()
    If CancelarPromo_Salir Or PromocionCreada Then
        Unload Me
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Cancel = 0 Then
        Call WheelUnHook(Me.hWnd)
    End If
End Sub

Private Sub Grid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CmdDelete_Click
    End If
End Sub

Private Sub ImgNext_Click()
    
    If SVal(txtPag.Text) <= SVal(TxtNumPag.Text) Then
        txtPag.Text = SVal(txtPag.Text) + 1
        If SVal(txtPag.Text) > SVal(TxtNumPag.Text) Then txtPag.Text = SVal(TxtNumPag.Text)
    Else
        txtPag.Text = SVal(TxtNumPag.Text)
    End If
    
    CargarTemporal
    
End Sub

Private Sub ImgPrev_Click()
    
    If SVal(txtPag.Text) >= 1 Then
        txtPag.Text = SVal(txtPag.Text) - 1
        If SVal(txtPag.Text) <= 1 Then txtPag.Text = 1
    Else
        txtPag.Text = 1
    End If
    
    CargarTemporal
    
End Sub

Private Sub txtCodSucursal_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        CmdBuscarSucursales_Click
    ElseIf KeyCode = vbKeyF6 Then
        CmdDeleteSucursales_Click
    End If
End Sub

Private Sub txtPag_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtPag_GotFocus()
    Set CampoT = txtPag
    txtPag.Tag = SVal(txtPag.Text)
End Sub

Private Sub txtPag_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtPag_LostFocus
    End If
End Sub

Private Sub txtPag_LostFocus()
    If SVal(txtPag.Text) <> SVal(txtPag.Tag) Then
        CargarTemporal
    End If
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
    Exit Sub
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Private Function GrabarEnEspera() As Boolean
    
    On Error GoTo Error
    
    Dim ActiveTrans As Boolean
    
    FrmAppLink.CnADM.BeginTrans
    ActiveTrans = True
    
    If mCodPromo_Origen = Empty Then
        
        mCodPromo_New = No_Consecutivo("Cod_Promocion_Espera", True, _
        FrmAppLink.CnADM, "" & FrmAppLink.Srv_Remote_BD_ADM & "")
        
    ElseIf UCase(Left(mCodPromo_Origen, 1)) = "E" Then
        
        FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION " & _
        "WHERE Cod_Promocion = " & _
        "'" & QuitarComillasSimples(mCodPromo_Origen) & "' ", RecordsAffected
        
        FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & _
        "WHERE Cod_Promocion = " & _
        "'" & QuitarComillasSimples(mCodPromo_Origen) & "' ", RecordsAffected
        
        FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_SUCURSAL " & _
        "WHERE Cod_Promocion = " & _
        "'" & QuitarComillasSimples(mCodPromo_Origen) & "' ", RecordsAffected
        
        mCodPromo_New = mCodPromo_Origen
        
    Else ' Hecha a partir de otra promocion ya creada.
        
        mCodPromo_New = No_Consecutivo("Cod_Promocion_Espera", True, _
        FrmAppLink.CnADM, "" & FrmAppLink.Srv_Remote_BD_ADM & "")
        
    End If
    
    Dim LastRow As Integer
    Dim NumRows As Double, NumPag As Double
    Dim Rs As ADODB.Recordset
    Set RsSucursales = New ADODB.Recordset
    
    FrmAppLink.CnADM.Execute "UPDATE TMP_PROMOCION SET Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_New) & "', Estatus = 'DWT' " & _
    "WHERE Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
    "AND CodUsuario = " & _
    "'" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' ", RecordsAffected
    
    FrmAppLink.CnADM.Execute "UPDATE TMP_PROMOCION_CONDICION_VALORES SET Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_New) & "' " & _
    "WHERE Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
    "AND CodUsuario = " & _
    "'" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' ", RecordsAffected
    
    FrmAppLink.CnADM.Execute "UPDATE TMP_PROMOCION_SUCURSAL SET Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_New) & "' " & _
    "WHERE Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
    "AND CodUsuario = " & _
    "'" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' ", RecordsAffected
    
    FrmAppLink.CnADM.CommitTrans
    ActiveTrans = False
    
    GrabarEnEspera = True
    
    Exit Function
    
Error:
    
    If ActiveTrans Then
        FrmAppLink.CnADM.RollbackTrans
        ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(GrabarEspera)"
    
    CargarGrid
    Grid.Visible = True
    
End Function

Private Function GrabarPromocion() As Boolean
    
    On Error GoTo Error
    
    Dim ActiveTrans As Boolean
    
    FrmAppLink.CnADM.BeginTrans
    ActiveTrans = True
    
    If mCodPromo_Origen <> Empty And UCase(Left(mCodPromo_Origen, 1)) = "E" Then
        
        FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION " & _
        "WHERE Cod_Promocion = " & _
        "'" & QuitarComillasSimples(mCodPromo_Origen) & "' ", RecordsAffected
        
        FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & _
        "WHERE Cod_Promocion = " & _
        "'" & QuitarComillasSimples(mCodPromo_Origen) & "' ", RecordsAffected
        
        FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_SUCURSAL " & _
        "WHERE Cod_Promocion = " & _
        "'" & QuitarComillasSimples(mCodPromo_Origen) & "' ", RecordsAffected
        
    End If
    
    If TipoPromo = 19 Or TipoPromo = 20 Or TipoPromo = 21 Then
        
        FrmAppLink.CnADM.Execute _
        "UPDATE TMP_PROMOCION SET Tipo_Promocion = " & TipoPromo & " " & _
        "WHERE Cod_Promocion = " & _
        "'" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
        "AND CodUsuario = " & _
        "'" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' ", RecordsAffected
        
    End If
    
    mCodPromo_New = No_Consecutivo("Cod_Promocion", True, _
    FrmAppLink.CnADM, "" & FrmAppLink.Srv_Remote_BD_ADM & "")
    
    Dim LastRow As Integer
    Dim NumRows As Double, NumPag As Double
    Dim Rs As ADODB.Recordset
    Set RsSucursales = New ADODB.Recordset
    
    Dim mSQL As String
    
    Call ExecuteSafeSQL("USE " & FrmAppLink.Srv_Remote_BD_ADM & "", _
    FrmAppLink.CnADM, RecordsAffected)
    
    mSQL = "--USE " & FrmAppLink.Srv_Remote_BD_ADM & "" & vbNewLine & _
    vbNewLine & _
    "DECLARE @Cod_Promocion NVARCHAR(50)" & vbNewLine & _
    "DECLARE @Cod_Usuario NVARCHAR(50)" & vbNewLine & _
    "DECLARE @Cod_Tmp NVARCHAR(50)" & vbNewLine & _
    vbNewLine & _
    "DECLARE @Cols NVARCHAR(MAX)" & vbNewLine & _
    "DECLARE @Cols2 NVARCHAR(MAX)" & vbNewLine & _
    "DECLARE @JoinCols NVARCHAR(MAX)" & vbNewLine & _
    "DECLARE @TmpQuery NVARCHAR(MAX)" & vbNewLine & _
    vbNewLine & _
    "SET @Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_New) & "'" & vbNewLine & _
    "SET @Cod_Tmp = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & vbNewLine & _
    "SET @Cod_Usuario = '" & FrmAppLink.GetCodUsuario & "'" & vbNewLine & _
    vbNewLine & _
    "--SET XACT_ABORT ON;" & vbNewLine & _
    vbNewLine & _
    "--BEGIN TRANSACTION" & vbNewLine & _
    vbNewLine & _
    "--BEGIN TRY" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    "------------ VALORES DE LA PROMOCIÓN (PRECIOS, DESCUENTOS, ETC) ------------" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = (SELECT 'TR_PROMOCION_VALORES.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TR_PROMOCION_VALORES'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion', 'Linea_Valor')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = SUBSTRING(@Cols, 1, LEN(@Cols) - 1)" & vbNewLine & _
    "SET @Cols2 = @Cols" & vbNewLine & _
    vbNewLine & _
    "SET @JoinCols = (SELECT 'AND COND.' + COLUMN_NAME + CASE WHEN Collation_Name IS NOT NULL THEN ' COLLATE Modern_Spanish_CI_AS' ELSE '' END + ' = VAL.' + COLUMN_NAME + CASE WHEN Collation_Name IS NOT NULL THEN ' COLLATE Modern_Spanish_CI_AS' ELSE '' END + '[VBNEWLINE]' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TR_PROMOCION_VALORES'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion', 'Linea_Valor')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    "SET @TmpQuery = 'WITH VALORES_UNICOS AS (' + CHAR(13) +" & vbNewLine & _
    "'SELECT ROW_NUMBER() OVER (ORDER BY MIN(ID)) AS RowID, Cod_Promocion, CodUsuario,' + CHAR(13) +" & vbNewLine & _
    "'' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_VALORES.', '') + '' + CHAR(13) +" & vbNewLine & _
    "'FROM TMP_PROMOCION_CONDICION_VALORES' + CHAR(13) +" & vbNewLine & _
    "'WHERE Cod_Promocion = ''' + @Cod_Tmp + '''' + CHAR(13) +" & vbNewLine & _
    "'AND CodUsuario = ''' + @Cod_Usuario + '''' + CHAR(13) +" & vbNewLine & _
    "'GROUP BY Cod_Promocion, CodUsuario, ' + CHAR(13) +" & vbNewLine & _
    "'' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_VALORES.', '') + '' + CHAR(13) +" & vbNewLine & _
    "') INSERT INTO TR_PROMOCION_VALORES (Cod_Promocion, Linea_Valor,' + CHAR(13) +" & vbNewLine & _
    "'' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_VALORES.', '') + ')' + CHAR(13) +" & vbNewLine & _
    "'SELECT ''' + @Cod_Promocion + ''' AS Cod_Promocion, RowID AS Linea_Valor, ' + CHAR(13) +" & vbNewLine & _
    "'' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_VALORES.', '') + '' + CHAR(13) +" & vbNewLine & _
    "'FROM VALORES_UNICOS ORDER BY RowID'" & vbNewLine & _
    vbNewLine & _
    "EXEC (@TmpQuery)" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    "------------- CONDICIONES (INCLUIR, EXCLUIR, PREMIOS, EXCLUIR PREMIOS, ETC) ------------" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = (SELECT 'TR_PROMOCION_CONDICION.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TR_PROMOCION_CONDICION'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion', 'Linea_Condicion', 'Linea_Valor')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = SUBSTRING(@Cols, 1, LEN(@Cols) - 1)" & vbNewLine & _
    vbNewLine & _
    "SET @TmpQuery = 'WITH VALORES_UNICOS AS (' + CHAR(13) +" & vbNewLine & _
    "'SELECT ROW_NUMBER() OVER (ORDER BY MIN(ID)) AS RowID, Cod_Promocion, CodUsuario,' + CHAR(13) +" & vbNewLine & _
    "'' + REPLACE(@Cols2 COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_VALORES.', '') + '' + CHAR(13) +" & vbNewLine & _
    "'FROM TMP_PROMOCION_CONDICION_VALORES' + CHAR(13) +" & vbNewLine & _
    "'WHERE Cod_Promocion = ''' + @Cod_Tmp + '''' + CHAR(13) +" & vbNewLine & _
    "'AND CodUsuario = ''' + @Cod_Usuario + '''' + CHAR(13) +" & vbNewLine & _
    "'GROUP BY Cod_Promocion, CodUsuario, ' + CHAR(13) +" & vbNewLine & _
    "'' + REPLACE(@Cols2 COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_VALORES.', '') + '' + CHAR(13) +" & vbNewLine & _
    "') INSERT INTO TR_PROMOCION_CONDICION (Cod_Promocion, Linea_Condicion, Linea_Valor,' + CHAR(13) +" & vbNewLine & _
    "'' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_CONDICION.', '') + ')' + CHAR(13) +" & vbNewLine & _
    "'SELECT ''' + @Cod_Promocion + ''' AS Cod_Promocion, ' + CHAR(13) +" & vbNewLine
    
    mSQL = mSQL & _
    "'ROW_NUMBER() OVER (ORDER BY ID) AS Linea_Condicion, VAL.RowID as Linea_Valor, ' + CHAR(13) +" & vbNewLine & _
    "'' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_CONDICION.', '') + '' + CHAR(13) +" & vbNewLine & _
    "'FROM TMP_PROMOCION_CONDICION_VALORES COND INNER JOIN VALORES_UNICOS VAL' + CHAR(13) +" & vbNewLine & _
    "'ON COND.Cod_Promocion COLLATE Modern_Spanish_CI_AS = VAL.Cod_Promocion COLLATE Modern_Spanish_CI_AS AND COND.CodUsuario COLLATE Modern_Spanish_CI_AS = VAL.CodUsuario COLLATE Modern_Spanish_CI_AS' + CHAR(13) +" & vbNewLine & _
    "REPLACE(@JoinCols COLLATE Modern_Spanish_CI_AS, '[VBNEWLINE]', CHAR(13))" & vbNewLine & _
    vbNewLine & _
    "EXEC (@TmpQuery)" & vbNewLine & _
    vbNewLine & _
    "------------ LOCALIDADES DONDE APLICA LA PROMOCION ------------" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = (SELECT 'MA_PROMOCION_SUCURSAL.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MA_PROMOCION_SUCURSAL'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = SUBSTRING(@Cols, 1, LEN(@Cols) - 1)" & vbNewLine & _
    vbNewLine & _
    "SET @TmpQuery = 'INSERT INTO MA_PROMOCION_SUCURSAL (Cod_Promocion, ' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'MA_PROMOCION_SUCURSAL.', '') + ')' + CHAR(13) +" & vbNewLine & _
    "'SELECT ''' + @Cod_Promocion + ''' AS Cod_Promocion, ' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'MA_PROMOCION_SUCURSAL.', '') + ' ' + CHAR(13) +" & vbNewLine & _
    "'FROM TMP_PROMOCION_SUCURSAL WHERE Cod_Promocion = ''' + @Cod_Tmp + ''' AND CodUsuario = ''' + @Cod_Usuario + ''''" & vbNewLine & _
    vbNewLine & _
    "EXEC (@TmpQuery)" & vbNewLine & _
    vbNewLine
    
    ' GRABAR EL MA_PROMOCION DE ULTIMO, POR SI TOCA HACER TRIGGERS EN EL FUTURO, YA LOS DATOS DEL TR, LOS DETALLES _
    Y TODO LO DEMAS YA ESTEN GRABADOS PREVIAMENTE. SI SE GRABARA LA TABLA DE CABECERO DE PRIMERA, Y SE NECESITARA HACER UN TRIGGER, _
    EN ESE INSTANTE LA PROMOCION TODAVIA NO TENDRIA DETALLES.
    
    mSQL = mSQL & _
    "------------ DATOS GENERALES DE LA PROMOCION ------------" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = (SELECT 'MA_PROMOCION.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MA_PROMOCION'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion', 'Estatus', 'Fecha_Add', 'Cod_Usuario_Add', 'Host_Name')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = SUBSTRING(@Cols, 1, LEN(@Cols) - 1)" & vbNewLine & _
    vbNewLine & _
    "SET @TmpQuery = 'INSERT INTO MA_PROMOCION (Cod_Promocion, Cod_Usuario_Add, Estatus, ' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'MA_PROMOCION.', '') + ')' + CHAR(13) +" & vbNewLine & _
    "'SELECT ''' + @Cod_Promocion + ''' AS Cod_Promocion, ''' + @Cod_Usuario + ''' AS Cod_Usuario_Add, ''DPE'' AS Estatus, ' + CHAR(13) +" & vbNewLine & _
    "REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'MA_PROMOCION.', '') + ' ' + CHAR(13) +" & vbNewLine & _
    "'FROM TMP_PROMOCION WHERE Cod_Promocion = ''' + @Cod_Tmp + ''' AND CodUsuario = ''' + @Cod_Usuario + ''''" & vbNewLine & _
    vbNewLine & _
    "EXEC (@TmpQuery)" & vbNewLine & _
    vbNewLine
    
    If ManejaPOS(FrmAppLink.CnADM) Or ManejaSucursales(FrmAppLink.CnADM) Then
        
        mSQL = mSQL & _
        "------------ ENVIAR PENDIENTES A SINCRONIZAR POR LOCALIDAD ------------" & vbNewLine & _
        vbNewLine & _
        "INSERT INTO TR_PEND_ENVIAR_PROMOCIONES (c_Documento, c_Localidad, c_Status)" & vbNewLine & _
        "SELECT @Cod_Promocion AS c_Documento, Cod_Localidad, 'DPE' AS c_Status" & vbNewLine & _
        "FROM TMP_PROMOCION_SUCURSAL WHERE Cod_Promocion = @Cod_Tmp AND CodUsuario = @Cod_Usuario" & vbNewLine & _
        vbNewLine
        
    End If
    
    mSQL = mSQL & _
    "------------ DELETE TMPs AND FINISH -------------" & vbNewLine & _
    vbNewLine & _
    "DELETE FROM TMP_PROMOCION WHERE Cod_Promocion = @Cod_Tmp AND CodUsuario = @Cod_Usuario" & vbNewLine & _
    "DELETE FROM TMP_PROMOCION_CONDICION_VALORES WHERE Cod_Promocion = @Cod_Tmp AND CodUsuario = @Cod_Usuario" & vbNewLine & _
    "DELETE FROM TMP_PROMOCION_SUCURSAL WHERE Cod_Promocion = @Cod_Tmp AND CodUsuario = @Cod_Usuario" & vbNewLine & _
    vbNewLine & _
    "--COMMIT TRANSACTION" & vbNewLine & _
    vbNewLine & _
    "--END TRY" & vbNewLine & _
    vbNewLine & _
    "--BEGIN CATCH" & vbNewLine & _
    "    --Print 'ERROR. ROLLBACK'" & vbNewLine & _
    "    --IF @@TRANCOUNT > 0" & vbNewLine & _
    "        --ROLLBACK TRANSACTION --RollBack in case of Error" & vbNewLine & _
    vbNewLine & _
    "--END CATCH"
    
    FrmAppLink.ExecuteSQLProcedure FrmAppLink.CnADM, pSQLText:=mSQL, CallerErrorHandling:=True
    
    If FrmAppLink.oResp.Item("Err_Number") <> 0 Then
        Err.Raise FrmAppLink.oResp.Item("Err_Number"), _
        FrmAppLink.oResp.Item("Err_Source"), _
        FrmAppLink.oResp.Item("Err_Desc")
    End If
    
    DatosLog = BuscarValorBD("LogInfo", _
    "SELECT Client_Net_Address + '|' + HOST_NAME() + '|' + APP_NAME() AS LogInfo " & _
    "FROM Sys.DM_Exec_Connections WHERE Session_id = @@SPID", , FrmAppLink.CnADM)
    
    Call InsertarAuditoria(4, "Crear Promocion", _
    "Promocion [" & mCodPromo_New & "]. " & _
    IIf(mCodPromo_Origen <> Empty, _
    "A partir de Prom. " & IIf(UCase(Left(mCodPromo_Origen, 1)) = "E", "Espera ", Empty) _
    & "[" & mCodPromo_Origen & "]. ", Empty) & _
    IIf(DatosLog <> Empty, "LogInfo => [" & DatosLog & "]", Empty), _
    Me.Name, "Promocion", mCodPromo_New, FrmAppLink.CnADM)
    
    FrmAppLink.CnADM.CommitTrans
    ActiveTrans = False
    
    GrabarPromocion = True
    
    Exit Function
    
Error:
    
    If ActiveTrans Then
        FrmAppLink.CnADM.RollbackTrans
        ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(GrabarPromo)"
    
    CargarGrid
    Grid.Visible = True
    CargarTemporal
    
End Function

Private Sub CargarTemporal()
    
    On Error GoTo Error
    
    'CargarGrid
    
    Dim LastRow As Integer, LastCtl As Object
    Dim NumRows As Double, NumPag As Double
    Dim RsSucursales As ADODB.Recordset
    Set RsSucursales = New ADODB.Recordset
    
    Dim mSQL As String
    
    mSQL = "SELECT isNULL(COUNT(ID), 0) AS NumReg " & GetLines & _
    "FROM TMP_PROMOCION_SUCURSAL " & GetLines & _
    "WHERE CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines
    
    Set RsSucursales = FrmAppLink.CnADM.Execute(mSQL)
    
    With RsSucursales
        
        If Not .EOF Then
        
            NumRows = !NumReg
            
            If NumRows > 0 Then
                
                NumPag = Round(NumRows / MaxGridRowsxPag, 8)
                If (NumPag - Fix(NumPag)) <> 0 Then NumPag = Fix(NumPag + 1)
                
                TxtNumPag.Text = NumPag
                
                txtPag.Text = Fix(SVal(txtPag))
                
                If SVal(txtPag.Text) >= 1 And SVal(txtPag.Text) <= NumPag Then
                    txtPag.Text = Fix(SVal(txtPag))
                Else
                    txtPag.Text = 1
                End If
                
                mSQL = "WITH AllRows AS (" & GetLines & _
                "SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowID, *" & GetLines & _
                "FROM TMP_PROMOCION_SUCURSAL" & GetLines & _
                "WHERE CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
                "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
                ")" & GetLines & _
                "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                "((" & txtPag.Text & " - 1) * (" & MaxGridRowsxPag & ") + 1) AND " & _
                "(" & txtPag.Text & " * " & MaxGridRowsxPag & ")" & GetLines
                
                RsSucursales.Close
                
                Set RsSucursales = New Recordset
                Apertura_RecordsetC RsSucursales
                
                .Open mSQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If Not .EOF Then
                    
                    .Sort = "RowID"
                    
                    Dim I As Integer
                    
                    Set LastCtl = Screen.ActiveControl
                    Grid.Visible = False
                    
                    Grid.Rows = 1
                    
                    ScrollGrid.Min = 0
                    ScrollGrid.Max = .RecordCount
                    ScrollGrid.Value = ScrollGrid.Min
                    
                    While Not .EOF
                    
                        Grid.Rows = Grid.Rows + 1
                        I = I + 1 '!RowID
                        
                        Grid.TextMatrix(I, ColRowID) = I
                        Grid.TextMatrix(I, ColCodSuc) = !Cod_Localidad
                        Grid.TextMatrix(I, ColDescSuc) = !Des_Localidad
                        Grid.TextMatrix(I, ColDireccion) = !Dir_Localidad
                        Grid.TextMatrix(I, ColGerente) = !Ger_Localidad
                        
                        .MoveNext
                        
                    Wend
                    
                    Grid.Visible = True
                    
                    If LastRow >= 1 And LastRow <= Grid.Rows - 1 Then
                        Grid.Row = LastRow
                        Grid.TopRow = Grid.Row
                    Else
                        Grid.Row = 1
                        Grid.TopRow = Grid.Row
                    End If
                    
                    If Not LastCtl Is Nothing Then
                        If PuedeObtenerFoco(LastCtl) Then LastCtl.SetFocus
                    End If
                    
                End If
                
            Else
                NumPag = 1
                txtPag.Text = NumPag
                Grid.Rows = 1
                Grid.Rows = 2
            End If
            
            If Grid.Rows > 7 Then
                ScrollGrid.Visible = True
                Grid.ScrollBars = flexScrollBarVertical
                AnchoScrollBar = ScrollGrid.Width
                Grid.ColWidth(ColDescSuc) = (AnchoCampoScroll - AnchoScrollBar)
            Else
                Grid.ScrollBars = flexScrollBarHorizontal
                ScrollGrid.Visible = False
                AnchoScrollBar = 0
                Grid.ColWidth(ColDescSuc) = AnchoCampoScroll
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    CargarGrid
    Grid.Visible = True
    
End Sub
