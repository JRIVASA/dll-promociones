VERSION 5.00
Begin VB.Form frmAsistentePro_TipoPromoDetPag 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9540
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9540
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   7395
      Left            =   240
      TabIndex        =   7
      Top             =   1920
      Width           =   11685
      Begin VB.CommandButton Selected 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   180
         MaskColor       =   &H80000005&
         Picture         =   "frmAsistentePro_TipoPromoDetPag.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   6300
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton CmdParcialPorcentaje 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   6120
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CommandButton CmdParcialMonto 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   660
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   3300
         Width           =   1095
      End
      Begin VB.CommandButton CmdPagoTotal 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   660
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   0
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CommandButton CmdAtras 
         Caption         =   "Atras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8400
         TabIndex        =   3
         Top             =   6720
         Width           =   1455
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9960
         TabIndex        =   4
         Top             =   6720
         Width           =   1455
      End
      Begin VB.Label lblNota1 
         BackStyle       =   0  'Transparent
         Caption         =   "** Nota: Estas modalidades de descuento se aplican sobre el total a pagar (por encima de otros descuentos de primera fase)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   240
         TabIndex        =   17
         Top             =   5760
         Width           =   11055
      End
      Begin VB.Label lblEjemploParcialMonto 
         BackStyle       =   0  'Transparent
         Caption         =   $"frmAsistentePro_TipoPromoDetPag.frx":1D82
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   1005
         Left            =   1980
         TabIndex        =   16
         Top             =   4035
         Width           =   3855
      End
      Begin VB.Label lblEjemploParcialPorcentaje 
         BackStyle       =   0  'Transparent
         Caption         =   $"frmAsistentePro_TipoPromoDetPag.frx":1E15
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   1305
         Left            =   7500
         TabIndex        =   15
         Top             =   1995
         Width           =   3855
      End
      Begin VB.Label lblEjemploPagoTotal 
         BackStyle       =   0  'Transparent
         Caption         =   "Ejemplo: Si paga el total de la factura con Efectivo USD, obtiene 5% de descuento en toda la factura."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   885
         Left            =   1980
         TabIndex        =   14
         Top             =   1995
         Width           =   3855
      End
      Begin VB.Label lblTipoParcialPorcentaje 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Pago parcial con % m�nimo de factura"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   570
         Left            =   7500
         TabIndex        =   12
         Top             =   1380
         Width           =   2820
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblTipoParcialMonto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Pago parcial con monto m�nimo de factura"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   570
         Left            =   1980
         TabIndex        =   11
         Top             =   3360
         Width           =   2820
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblGuiaUsuario 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione el Tipo de Promoci�n de M�todo de Pago a crear:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   660
         TabIndex        =   10
         Top             =   840
         Width           =   6450
      End
      Begin VB.Label lblTipoPagoTotal 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Pago total con un M�todo de Pago"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   570
         Left            =   1980
         TabIndex        =   9
         Top             =   1380
         Width           =   3780
         WordWrap        =   -1  'True
      End
      Begin VB.Label LblDescripcionAsistente 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Este asistente le permitir� crear una Promoci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   240
         TabIndex        =   8
         Top             =   210
         Width           =   4995
      End
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   6
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_TipoPromoDetPag.frx":1EC6
      Top             =   360
      Width           =   2700
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_TipoPromoDetPag"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private TipoSeleccionado As Integer

Private Function ValidarTipoPromo() As Boolean
    
    If TipoSeleccionado <> TipoPromo And TipoPromo <> -1 Then
        
        If Not ((TipoSeleccionado = 16 And TipoPromo = 19) _
        Or (TipoSeleccionado = 17 And TipoPromo = 20) _
        Or (TipoSeleccionado = 18 And TipoPromo = 21) _
        Or (TipoSeleccionado = 19 And TipoPromo = 16) _
        Or (TipoSeleccionado = 20 And TipoPromo = 17) _
        Or (TipoSeleccionado = 21 And TipoPromo = 18)) Then ' Estos tipos si son intercambiables entre si.
            
            If Mensaje(False, StellarMensajeLocal(156)) Then
                
                ' Cuando se cambia el tipo de promoci�n reiniciar todos los valores cuya relevancia son
                ' de acuerdo al tipo de promoci�n ya que sino podr�an generar inconsistencias si se
                ' mantienen y mezclan los datos.
                
                ExecuteSafeSQL "UPDATE TMP_PROMOCION SET " & _
                "Cantidad_Productos_Requerir = 0, Cantidad_Productos_Pagar = 0, Combina_Promocion = 0, Prioridad_Promocion = 0 " & _
                "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & _
                "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", _
                FrmAppLink.CnADM, RecordsAffected
                
                ExecuteSafeSQL "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & _
                "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & _
                "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", _
                FrmAppLink.CnADM, RecordsAffected
                
                ValidarSeleccion
                
            Else
                Exit Function
            End If
            
        End If
        
    End If
    
    TipoPromo = TipoSeleccionado
    
    frmAsistentePro_DatosGenerales.Show vbModal
    
    If Not (PromocionCreada Or CancelarPromo_Salir) Then
        ValidarSeleccion
    End If
    
End Function

Private Sub CmdAtras_Click()
    Unload Me
End Sub

Private Sub CmdCancelar_Click()
    If Mensaje(False, Replace(StellarMensaje(2620), "$(Line)", vbNewLine)) Then
        CancelarPromo_Salir = True
        Form_Activate
        Exit Sub
    End If
End Sub

Private Sub CmdPagoTotal_Click()
    TipoSeleccionado = 16
    Call ValidarTipoPromo
End Sub

Private Sub CmdParcialPorcentaje_Click()
    TipoSeleccionado = 17
    Call ValidarTipoPromo
End Sub

Private Sub CmdParcialMonto_Click()
    TipoSeleccionado = 18
    Call ValidarTipoPromo
End Sub

Private Sub Form_Load()
    
    If CancelarPromo_Salir Then Exit Sub
    
    LblDescripcionAsistente.Caption = StellarMensajeLocal(101)
    lblGuiaUsuario.Caption = StellarMensajeLocal(229)
    CmdAtras.Caption = StellarMensajeLocal(12)
    CmdCancelar.Caption = StellarMensajeLocal(11)
    
    lblTipoPagoTotal.Caption = StellarMensajeLocal(245)
    lblEjemploPagoTotal.Caption = StellarMensajeLocal(246)
    lblTipoParcialPorcentaje.Caption = StellarMensajeLocal(247)
    lblEjemploParcialPorcentaje.Caption = StellarMensajeLocal(248)
    lblTipoParcialMonto.Caption = StellarMensajeLocal(249)
    lblEjemploParcialMonto.Caption = StellarMensajeLocal(250)
    
    lblNota1.Caption = StellarMensajeLocal(236)
    
    'CmdDctoBINTarjeta.Enabled = Val(BuscarReglaNegocioStr( _
    "OyP_PromosxMetodoDePago_DctoBIN", 0)) = 1 _
    And ExisteTablaV3("TMP_OYP_PRM_DETPAG_BIN", FrmAppLink.CnADM)
    
    'CmdDctoBINTarjeta.Visible = CmdDctoBINTarjeta.Enabled
    'lblTipoDctoBINTarjeta.Visible = CmdDctoBINTarjeta.Enabled
    'lblEjemploDctoBINTarjeta.Visible = CmdDctoBINTarjeta.Enabled
    
    ValidarSeleccion
    
    Call AjustarPantalla(Me)

End Sub

Private Sub Form_Activate()
    If CancelarPromo_Salir Or PromocionCreada Then
        Unload Me
    End If
End Sub

Private Sub Header_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub ValidarSeleccion()
    If TipoPromo = 16 Or TipoPromo = 19 Then
        CmdPagoTotal.Picture = Selected.Picture
        CmdParcialPorcentaje.Picture = LoadPicture()
        CmdParcialMonto.Picture = LoadPicture()
    ElseIf TipoPromo = 17 Or TipoPromo = 20 Then
        CmdParcialPorcentaje.Picture = Selected.Picture
        CmdPagoTotal.Picture = LoadPicture()
        CmdParcialMonto.Picture = LoadPicture()
    ElseIf TipoPromo = 18 Or TipoPromo = 21 Then
        CmdParcialMonto.Picture = Selected.Picture
        CmdPagoTotal.Picture = LoadPicture()
        CmdParcialPorcentaje.Picture = LoadPicture()
    End If
End Sub
