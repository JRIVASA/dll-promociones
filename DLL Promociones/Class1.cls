VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsModPromo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Property Let Entrada(ByVal pValor As Object)
    Set FrmAppLink = pValor
    Set Ent.BDD = FrmAppLink.CnAdm
    Set Ent.POS = FrmAppLink.CnPos
    Set Ent.SHAPE_ADM = FrmAppLink.CnADMShape
    Set Ent.SHAPE_POS = FrmAppLink.CnPOSShape
    Set Ent.RsReglasComerciales = FrmAppLink.RsReglasComerciales
    Set RsEureka = FrmAppLink.GetRsEureka
    Set gCls = Me
End Property

Public Sub InterfazPromociones()
    frmPromociones.Show vbModal
End Sub

Public Sub ReportePromocionesAplicadas()
    REP_PROMOCIONES_APLICADAS.Show vbModal
End Sub
