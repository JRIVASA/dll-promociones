VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frmPromociones 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   Icon            =   "frmPromociones.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FrameDelete 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   7800
      TabIndex        =   16
      Top             =   1140
      Visible         =   0   'False
      Width           =   615
      Begin VB.Image CmdDelete 
         Height          =   480
         Left            =   75
         Picture         =   "frmPromociones.frx":628A
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.Frame FrameInfo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   5640
      TabIndex        =   15
      Top             =   1140
      Visible         =   0   'False
      Width           =   615
      Begin VB.Image CmdInfo 
         Height          =   480
         Left            =   75
         Picture         =   "frmPromociones.frx":6F54
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   7035
      LargeChange     =   10
      Left            =   14400
      TabIndex        =   13
      Top             =   3720
      Width           =   915
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   6975
      Left            =   240
      TabIndex        =   3
      Top             =   3720
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   12303
      _Version        =   393216
      Cols            =   7
      FixedCols       =   0
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton CmdBuscar 
      Appearance      =   0  'Flat
      Caption         =   "&Buscar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   14160
      Picture         =   "frmPromociones.frx":7C1E
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2130
      Width           =   1035
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Buscar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1695
      Left            =   3480
      TabIndex        =   11
      Top             =   1920
      Width           =   10575
      Begin VB.CheckBox chkFiltrarProducto 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Filtrar Por Art�culo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   17
         Top             =   1150
         Width           =   9855
      End
      Begin VB.ComboBox cmbItemBusqueda 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         ItemData        =   "frmPromociones.frx":99A0
         Left            =   240
         List            =   "frmPromociones.frx":99A7
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   360
         Width           =   3210
      End
      Begin VB.TextBox txtDato 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3630
         TabIndex        =   1
         Top             =   435
         Width           =   5580
      End
      Begin MSComctlLib.ProgressBar Barra_Prg 
         Height          =   120
         Left            =   3600
         TabIndex        =   14
         Top             =   900
         Width           =   5520
         _ExtentX        =   9737
         _ExtentY        =   212
         _Version        =   393216
         Appearance      =   0
         Max             =   1000
         Scrolling       =   1
      End
      Begin VB.Image CmdTeclado 
         Height          =   600
         Left            =   9555
         MouseIcon       =   "frmPromociones.frx":99B2
         MousePointer    =   99  'Custom
         Picture         =   "frmPromociones.frx":9CBC
         Stretch         =   -1  'True
         Top             =   300
         Width           =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   240
         X2              =   10500
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label lblTeclado 
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         Height          =   750
         Left            =   9315
         MousePointer    =   99  'Custom
         TabIndex        =   12
         Top             =   225
         Width           =   1095
      End
   End
   Begin VB.Frame Frame_NewContainer 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Buscar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1695
      Left            =   120
      TabIndex        =   8
      Top             =   1920
      Width           =   3255
      Begin VB.Frame Frame_New 
         Appearance      =   0  'Flat
         BackColor       =   &H00AE5B00&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H0000C000&
         Height          =   495
         Left            =   175
         TabIndex        =   9
         Top             =   330
         Width           =   2895
         Begin VB.Label lbl_New 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Agregar Promoci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   375
            Left            =   0
            TabIndex        =   10
            Top             =   120
            Width           =   2895
         End
      End
   End
   Begin VB.Frame FrameBarraTitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   15360
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "frmPromociones.frx":9D68
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12435
         TabIndex        =   5
         Top             =   105
         Width           =   1815
      End
   End
   Begin VB.Label lbl_Titulo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Promociones"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   26.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00333333&
      Height          =   615
      Left            =   1440
      TabIndex        =   7
      Top             =   840
      Width           =   11295
   End
   Begin VB.Image logoForm 
      Appearance      =   0  'Flat
      Height          =   960
      Left            =   240
      Picture         =   "frmPromociones.frx":BAEA
      Top             =   720
      Width           =   960
   End
End
Attribute VB_Name = "frmPromociones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public EvitarActivate As Boolean
Public BusquedaInstantanea As Boolean
Public CustomFontSize As String
Public StrOrderBy As String
Public StrCriterioBase As String
Public ArrResultado

Private mRowCell As Long, mColCell As Long, Fila As Long

Private ContPuntos As Long, ContItems As Long, ContFound As Long, StrCont As String
Private StrSQL As String
Private ArrCamposBusquedas()
Public Conex_SC As ADODB.Connection
Private Band As Boolean
Private StrSqlMasCondicion As String
Private StrCondicion As String

Private OriginalDescColWidth As Long

Private EventoProgramado As Boolean

Private mCodPromoCopiar As String

Private Enum GridCol
    ColRowID
    ColCodPro
    ColDesPro
    ColCampa�a
    ColNumTipo
    ColTipo
    ColIntEstatus
    ColEstatus
    ColView
    ColDelete
    [ColCount]
End Enum

Private Function IgnorarActivate() As Boolean
    EvitarActivate = True: IgnorarActivate = EvitarActivate
End Function

Private Sub chkFiltrarProducto_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then
        If chkFiltrarProducto.Value = vbChecked Then
            
            'MsgBox "Sacar Ventana de Filtro"
            
            mArrProducto = BuscarInfoProducto_Basica(FrmAppLink.CnADM, , , , , VistaPrecioCliente)
            
            If Not IsEmpty(mArrProducto) Then
                chkFiltrarProducto.Tag = mArrProducto(0) & "||" & mArrProducto(1)
                'chkFiltrarProducto.Caption = "Filtrar Por Art�culo: " & mArrProducto(1)
                chkFiltrarProducto.Caption = StrConv(StellarMensajeLocal(326), vbProperCase) & ": " & mArrProducto(1)
            Else
                chkFiltrarProducto.Value = vbUnchecked
                chkFiltrarProducto_MouseUp vbLeftButton, Shift, x, y
            End If
            
        Else
SinFiltrar:
            'chkFiltrarProducto.Caption = "Filtrar Por Art�culo"
            chkFiltrarProducto.Caption = StrConv(StellarMensajeLocal(326), vbProperCase)
            chkFiltrarProducto.Tag = Empty
        End If
    Else
        If chkFiltrarProducto.Value = vbChecked Then
            mArrProducto = Split(chkFiltrarProducto.Tag, "||", , vbTextCompare)
            Mensaje True, mArrProducto(0) & " - " & mArrProducto(1)
        Else
            chkFiltrarProducto.Value = vbChecked
            chkFiltrarProducto_MouseUp vbLeftButton, Shift, x, y
        End If
    End If
End Sub

Private Sub cmbItemBusqueda_Click()
    If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
End Sub

Private Sub cmbItemBusqueda_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 49 To 57
            On Error Resume Next
            cmbItemBusqueda.ListIndex = Val(Chr(KeyAscii)) - 1
        Case vbKeyReturn
            If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
    End Select
End Sub

Private Sub CmdBuscar_Click()
    txtDato_KeyPress vbKeyReturn
End Sub

Private Sub CancelarPromocion()
    
    On Error GoTo Error
    
    Dim ActiveTrans As Boolean
    
    FrmAppLink.CnADM.BeginTrans
    ActiveTrans = True
    
    Dim DatosLog, DL1, DL2 As String, ArrDatosLog
    
    DatosLog = BuscarValorBD("LogInfo", _
    "SELECT Client_Net_Address + '|' + HOST_NAME() + '|' + APP_NAME() AS LogInfo " & _
    "FROM Sys.DM_Exec_Connections WHERE Session_id = @@SPID", , FrmAppLink.CnADM)
    
    ArrDatosLog = Split(DatosLog, "|", , vbTextCompare)
    
    If UBound(ArrDatosLog) >= 2 Then
        DL2 = ArrDatosLog(1) & "|" & ArrDatosLog(2)
    End If
    
    FrmAppLink.CnADM.Execute "UPDATE MA_PROMOCION SET Estatus = 'ANU', " & _
    "Cod_Usuario_Upd = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "', " & _
    "Fecha_Upd = GetDate(), Host_Name = '" & QuitarComillasSimples(DL2) & "' " & _
    "WHERE Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", RecordsAffected
    
    If ManejaPOS(FrmAppLink.CnADM) Or ManejaSucursales(FrmAppLink.CnADM) Then
        
        FrmAppLink.CnADM.Execute "INSERT INTO TR_PEND_ENVIAR_PROMOCIONES " & _
        "(c_Documento, c_Localidad, c_Status) " & GetLines & _
        "SELECT '" & QuitarComillasSimples(mCodPromo_Actual) & "' AS c_Documento, " & _
        "Cod_Localidad, 'ANU' AS Status FROM MA_PROMOCION_SUCURSAL " & GetLines & _
        "WHERE Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", RecordsAffected
        
    End If
    
    Call InsertarAuditoria(5, "Anular Promocion", _
    IIf(DatosLog <> Empty, "LogInfo => [" & DatosLog & "]", Empty), _
    Me.Name, "Promocion", QuitarComillasSimples(mCodPromo_Actual), FrmAppLink.CnADM)
    
    FrmAppLink.CnADM.CommitTrans
    ActiveTrans = False
    
    Mensaje True, StellarMensaje(16600)
    
    Exit Sub
    
Error:
    
    If ActiveTrans Then
        FrmAppLink.CnADM.RollbackTrans
        ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub EliminarPromocion()
    
    On Error GoTo Error
    
    Dim ActiveTrans As Boolean
    
    FrmAppLink.CnADM.BeginTrans
    ActiveTrans = True
    
    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION " & _
    "WHERE Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", RecordsAffected
    
    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & _
    "WHERE Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", RecordsAffected
    
    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_SUCURSAL " & _
    "WHERE Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", RecordsAffected
    
    FrmAppLink.CnADM.CommitTrans
    ActiveTrans = False
    
    Mensaje True, StellarMensaje(16600)
    
    Exit Sub
    
Error:
    
    If ActiveTrans Then
        FrmAppLink.CnADM.RollbackTrans
        ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(EliminarPromocion)"
    
End Sub

Private Sub CmdDelete_Click()
    
    If Grid.Rows <= 1 Then Exit Sub
    
    mCodPromo_Actual = Grid.TextMatrix(Grid.Row, ColCodPro)
    EstatusPromo = Grid.TextMatrix(Grid.Row, ColIntEstatus)
    
    Select Case EstatusPromo
        Case "DCO", "ANU"
            Mensaje True, StellarMensajeLocal(143)
        Case "DPE"
            If Mensaje(False, StellarMensajeLocal(144)) Then
                CancelarPromocion
                CmdBuscar_Click
            End If
        Case "DWT"
            If Mensaje(False, StellarMensajeLocal(145)) Then
                EliminarPromocion
                CmdBuscar_Click
            End If
        Case Else
    End Select
    
End Sub

Private Sub CmdInfo_Click()
    Grid_DblClick
End Sub

Private Sub CmdTeclado_Click()
    TecladoAvanzado CampoT, True
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    
    If EvitarActivate Then EvitarActivate = False: Exit Sub
    
    Dim Longitud As Long
    
    'Call AjustarPantalla(Me)
    
    'Medir.FontName = lblTitulo.FontName
    'Medir.FontSize = lblTitulo.FontSize
    'Medir.FontBold = lblTitulo.FontBold
    
    Me.lbl_Organizacion.Caption = StellarMensaje(51)
    
    'Longitud = Medir.TextWidth(lblTitulo.Caption)
    
    'If Longitud > 0 Then
        'Line1.X1 = Line1.X1 + (Longitud)
    'End If
    
    If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
    
    txtDato.SelStart = Len(txtDato.Text)
            
    If Trim(CustomFontSize) <> vbNullString Then Grid.Font.Size = Val(CustomFontSize)
    
    If StrOrderBy = vbNullString Then StrOrderBy = "Cod_Promocion DESC"
    
    If BusquedaInstantanea Then txtDato_KeyPress vbKeyReturn
    
End Sub

Private Sub CargarGrid()
        
    With Grid
        
        .Rows = 1
        .Row = 0
        .Clear
        .Cols = ColCount
        .RowHeightMin = 600
        
        .Col = ColRowID
        .TextMatrix(0, .Col) = "Ln"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodPro
        .TextMatrix(0, .Col) = StellarMensaje(142) '"Codigo"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesPro
        .TextMatrix(0, .Col) = StellarMensaje(143) '"Descripcion"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCampa�a
        .TextMatrix(0, .Col) = StellarMensajeLocal(136) '"Campa�a"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColNumTipo
        .TextMatrix(0, .Col) = "nTipo"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColTipo
        .TextMatrix(0, .Col) = StellarMensajeLocal(135) '"Tipo"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColIntEstatus
        .TextMatrix(0, .Col) = "InternalEstatus"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColEstatus
        .TextMatrix(0, .Col) = StellarMensajeLocal(137) '"Estatus"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColView
        .TextMatrix(0, .Col) = Empty
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDelete
        .TextMatrix(0, .Col) = Empty
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .AllowUserResizing = flexResizeColumns
        
        .ColWidth(ColRowID) = 555
        .ColWidth(ColCodPro) = 2100
        .ColWidth(ColDesPro) = 4000
        .ColWidth(ColCampa�a) = 3240
        .ColWidth(ColNumTipo) = 0
        .ColWidth(ColTipo) = 2200
        .ColWidth(ColIntEstatus) = 0
        .ColWidth(ColEstatus) = 1300
        .ColWidth(ColView) = 720
        .ColWidth(ColDelete) = 720
        
        OriginalDescColWidth = .ColWidth(ColDesPro)
        
    End With
    
End Sub

Private Sub Form_Load()
    
    lbl_Titulo.Caption = StellarMensajeLocal(131) '"Promociones"
    lbl_New.Caption = StellarMensajeLocal(132) '"Nueva Promocion"
    CmdBuscar.Caption = "&" & Stellar_Mensaje(102) 'Buscar
    
    cmbItemBusqueda.Clear
    
    cmbItemBusqueda.AddItem StellarMensajeLocal(325) '"Activadas"
    cmbItemBusqueda.AddItem StellarMensajeLocal(129) '"Completadas"
    cmbItemBusqueda.AddItem StellarMensajeLocal(130) '"Anuladas"
    cmbItemBusqueda.AddItem StellarMensajeLocal(127) '"En Espera"
    cmbItemBusqueda.AddItem StellarMensajeLocal(323) '"Activa - No iniciada aun"
    cmbItemBusqueda.AddItem StellarMensajeLocal(324) '"Activa - En Curso"
    cmbItemBusqueda.AddItem StellarMensajeLocal(34) '"Todas"
    
    cmbItemBusqueda.ListIndex = 0
    
    chkFiltrarProducto.Caption = StrConv(StellarMensajeLocal(326), vbProperCase) 'que contenga articulo
    
    ArrCamposBusquedas = Array("Cod_Promocion", "Descripcion", "Campa�a")
    
    CargarGrid
    
    ExecuteSafeSQL "UPDATE MA_PROMOCION SET " & _
    "Estatus = 'DCO' " & _
    "WHERE Estatus = 'DPE' " & _
    "AND CAST(GetDate() AS DATE) > CAST(Fecha_Fin AS DATE)", _
    FrmAppLink.CnADM, RecordsAffected
    
    If Conex_SC Is Nothing Then Set Conex_SC = FrmAppLink.CnADM
    
    Call AjustarPantalla(Me)
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EvitarActivate = False
    BusquedaInstantanea = False
    CustomFontSize = vbNullString
    StrOrderBy = vbNullString
    StrCriterioBase = vbNullString
End Sub

Private Sub FrameBarraTitulo_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub FrameDelete_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    CmdDelete_Click
End Sub

Private Sub FrameInfo_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    CmdInfo_Click
End Sub

Private Sub Grid_EnterCell()
    
    On Error Resume Next
    
    'Exit Sub
    
    If Grid.Rows = 1 Then
        FrameDelete.Visible = False
        FrameInfo.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> Grid.Row Then
            
            If Fila > 0 Then
                Grid.RowHeight(Fila) = 600
            End If
            
            Grid.RowHeight(Grid.Row) = 720
            Fila = Grid.Row
            
            EventoProgramado = True
            ScrollGrid.Value = Fila + 1
            EventoProgramado = False
            
        End If
    
        MostrarEditorTexto2 Me, Grid, CmdDelete, mRowCell, mColCell
        Grid.ColSel = 0
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    Else
        Band = False
    End If
    
End Sub

Public Sub MostrarEditorTexto2(pFrm As Form, pGrd As Object, ByRef txteditor As Object, ByRef cellRow As Long, ByRef cellCol As Long, Optional pResp As Boolean = False)
    
    On Error Resume Next
    
    With pGrd

        .RowSel = .Row
        
        If .Col <> ColView Then Band = True
        .Col = ColView
        
        FrameInfo.BackColor = pGrd.BackColorSel
        
        FrameInfo.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        CmdInfo.Move ((FrameInfo.Width / 2) - (CmdInfo.Width / 2)), ((FrameInfo.Height / 2) - (CmdInfo.Height / 2))
        
        FrameInfo.Visible = True: CmdInfo.Visible = True
        FrameInfo.ZOrder
        
        If .Col <> ColDelete Then Band = True
        .Col = ColDelete
        
        FrameDelete.BackColor = pGrd.BackColorSel
        FrameDelete.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        CmdDelete.Move ((FrameDelete.Width / 2) - (CmdDelete.Width / 2)), ((FrameDelete.Height / 2) - (CmdDelete.Height / 2))
        
        FrameDelete.Visible = True: CmdDelete.Visible = True
        FrameDelete.ZOrder
        
        cellRow = .Row
        cellCol = .Col
        
    End With
    
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_DblClick()
    
    If Grid.Rows <= 1 Then Exit Sub
    
    mCodPromo_Actual = Grid.TextMatrix(Grid.Row, ColCodPro)
    TipoPromo = Grid.TextMatrix(Grid.Row, ColNumTipo)
    EstatusPromo = Grid.TextMatrix(Grid.Row, ColIntEstatus)
    
    frmFichaPromociones.Show vbModal
    
    bImportar = (frmFichaPromociones.ModificarEspera Or frmFichaPromociones.ImportarNuevo)
    
    If frmFichaPromociones.ImportarNuevo Then
        mCodPromoCopiar = mCodPromo_Actual
    End If
    
    Set frmFichaPromociones = Nothing
    
    If bImportar Then
        lbl_New_Click
    End If
    
End Sub

Private Sub CargarEspera_Temp()
    
    On Error GoTo Error
    
    mCodPromo_Origen = mCodPromo_Actual
    mCodPromo_Actual = "TempDWT"
    
    Dim ActiveTrans As Boolean
    
    FrmAppLink.CnADM.BeginTrans
    ActiveTrans = True
    
    Call ExecuteSafeSQL("USE " & FrmAppLink.Srv_Remote_BD_ADM, FrmAppLink.CnADM, RecordsAffected)
    
    ' --------------------
    
    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION " & _
    "WHERE Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
    "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' ", RecordsAffected
    
    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & _
    "WHERE Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
    "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' ", RecordsAffected
    
    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_SUCURSAL " & _
    "WHERE Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
    "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' ", RecordsAffected
    
    ' --------------------
    
    Dim mSQL As String
    
    mSQL = "--USE " & FrmAppLink.Srv_Remote_BD_ADM & "" & vbNewLine & _
    vbNewLine & _
    "DECLARE @Cod_Promocion_Espera NVARCHAR(50)" & vbNewLine & _
    "DECLARE @Cod_Usuario NVARCHAR(50)" & vbNewLine & _
    "DECLARE @Cod_Tmp NVARCHAR(50)" & vbNewLine & _
    vbNewLine & _
    "DECLARE @Cols NVARCHAR(MAX)" & vbNewLine & _
    "DECLARE @Cols2 NVARCHAR(MAX)" & vbNewLine & _
    "DECLARE @JoinCols NVARCHAR(MAX)" & vbNewLine & _
    "DECLARE @TmpQuery NVARCHAR(MAX)" & vbNewLine & _
    vbNewLine & _
    "SET @Cod_Promocion_Espera = '" & QuitarComillasSimples(mCodPromo_Origen) & "'" & vbNewLine & _
    "SET @Cod_Tmp = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & vbNewLine & _
    "SET @Cod_Usuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "'" & vbNewLine & _
    vbNewLine & _
    "--SET XACT_ABORT ON;" & vbNewLine & _
    vbNewLine & _
    "--BEGIN TRANSACTION" & vbNewLine & _
    vbNewLine & _
    "--BEGIN TRY" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    "------------ DATOS GENERALES DE LA PROMOCION ------------" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = (SELECT 'TMP_PROMOCION.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TMP_PROMOCION'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion', 'Estatus', 'CodUsuario')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = SUBSTRING(@Cols, 1, LEN(@Cols) - 1)" & vbNewLine & _
    vbNewLine & _
    "SET @TmpQuery = 'INSERT INTO TMP_PROMOCION (Cod_Promocion, Estatus, CodUsuario, ' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TMP_PROMOCION.', '') + ')' + CHAR(13) +" & vbNewLine & _
    "'SELECT ''' + @Cod_Tmp + ''' AS Cod_Promocion, ''TMP'' AS Estatus, ''' + @Cod_Usuario + ''' AS CodUsuario, ' + CHAR(13) +" & vbNewLine & _
    "REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TMP_PROMOCION.', '') + ' ' + CHAR(13) +" & vbNewLine & _
    "'FROM TMP_PROMOCION WHERE Cod_Promocion = ''' + @Cod_Promocion_Espera + ''''" & vbNewLine & _
    vbNewLine & _
    "EXEC (@TmpQuery)" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    "------------ CONDICIONES Y VALORES DE LA PROMOCI�N ------------" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = (SELECT 'TMP_PROMOCION_CONDICION_VALORES.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TMP_PROMOCION_CONDICION_VALORES'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion', 'CodUsuario')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = SUBSTRING(@Cols, 1, LEN(@Cols) - 1)" & vbNewLine & _
    vbNewLine & _
    "SET @TmpQuery = 'INSERT INTO TMP_PROMOCION_CONDICION_VALORES (Cod_Promocion, CodUsuario, ' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TMP_PROMOCION_CONDICION_VALORES.', '') + ')' + CHAR(13) +" & vbNewLine & _
    "'SELECT ''' + @Cod_Tmp + ''' AS Cod_Promocion, ''' + @Cod_Usuario + ''' AS CodUsuario, ' + CHAR(13) +" & vbNewLine & _
    "REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TMP_PROMOCION_CONDICION_VALORES.', '') + ' ' + CHAR(13) +" & vbNewLine & _
    "'FROM TMP_PROMOCION_CONDICION_VALORES WHERE Cod_Promocion = ''' + @Cod_Promocion_Espera + ''''" & vbNewLine & _
    vbNewLine & _
    "EXEC (@TmpQuery)" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    "------------ LOCALIDADES DONDE APLICA LA PROMOCION ------------" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = (SELECT 'TMP_PROMOCION_SUCURSAL.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TMP_PROMOCION_SUCURSAL'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion', 'CodUsuario')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = SUBSTRING(@Cols, 1, LEN(@Cols) - 1)" & vbNewLine & _
    vbNewLine & _
    "SET @TmpQuery = 'INSERT INTO TMP_PROMOCION_SUCURSAL (Cod_Promocion, CodUsuario, ' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TMP_PROMOCION_SUCURSAL.', '') + ')' + CHAR(13) +" & vbNewLine & _
    "'SELECT ''' + @Cod_Tmp + ''' AS Cod_Promocion, ''' + @Cod_Usuario + ''' AS CodUsuario, ' + CHAR(13) +" & vbNewLine & _
    "REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TMP_PROMOCION_SUCURSAL.', '') + ' ' + CHAR(13) +" & vbNewLine & _
    "'FROM TMP_PROMOCION_SUCURSAL WHERE Cod_Promocion = ''' + @Cod_Promocion_Espera + ''''" & vbNewLine & _
    vbNewLine & _
    "EXEC (@TmpQuery)" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    vbNewLine & _
    "--COMMIT TRANSACTION" & vbNewLine & _
    vbNewLine & _
    "--END TRY" & vbNewLine & _
    vbNewLine & _
    "--BEGIN CATCH" & vbNewLine & _
    "    --Print 'ERROR. ROLLBACK'" & vbNewLine & _
    "    --IF @@TRANCOUNT > 0" & vbNewLine & _
    "        --ROLLBACK TRANSACTION --RollBack in case of Error" & vbNewLine & _
    vbNewLine & _
    "--END CATCH "
    
    FrmAppLink.ExecuteSQLProcedure FrmAppLink.CnADM, pSQLText:=mSQL, CallerErrorHandling:=True
    
    If FrmAppLink.oResp.Item("Err_Number") <> 0 Then
        Err.Raise FrmAppLink.oResp.Item("Err_Number"), _
        FrmAppLink.oResp.Item("Err_Source"), _
        FrmAppLink.oResp.Item("Err_Desc")
    End If
    
    FrmAppLink.CnADM.CommitTrans
    ActiveTrans = False
    
    Exit Sub
    
Error:
    
    If ActiveTrans Then
        FrmAppLink.CnADM.RollbackTrans
        ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CargarEspera_Temp)"
    
    mCodPromo_Origen = Empty
    mCodPromo_Actual = Empty
    TipoPromo = -1
    
End Sub

Private Sub CopiarPromo_Temp()
    
    On Error GoTo Error
    
    mCodPromo_Origen = mCodPromo_Actual
    mCodPromo_Actual = Empty
    
    Dim ActiveTrans As Boolean
    
    FrmAppLink.CnADM.BeginTrans
    ActiveTrans = True
    
    Call ExecuteSafeSQL("USE " & FrmAppLink.Srv_Remote_BD_ADM & "", _
    FrmAppLink.CnADM, RecordsAffected)
    
    ' --------------------
    
    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION " & _
    "WHERE Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
    "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' ", RecordsAffected
    
    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & _
    "WHERE Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
    "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' ", RecordsAffected
    
    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_SUCURSAL " & _
    "WHERE Cod_Promocion = " & _
    "'" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
    "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' ", RecordsAffected
    
    ' --------------------
    
    Dim mSQL As String
    
    mSQL = "--USE " & FrmAppLink.Srv_Remote_BD_ADM & "" & vbNewLine & _
    vbNewLine & _
    "DECLARE @Cod_Promocion_Copiar NVARCHAR(50)" & vbNewLine & _
    "DECLARE @Cod_Usuario NVARCHAR(50)" & vbNewLine & _
    "DECLARE @Cod_Tmp NVARCHAR(50)" & vbNewLine & _
    vbNewLine & _
    "DECLARE @Cols NVARCHAR(MAX)" & vbNewLine & _
    "DECLARE @Cols2 NVARCHAR(MAX)" & vbNewLine & _
    "DECLARE @JoinCols NVARCHAR(MAX)" & vbNewLine & _
    "DECLARE @ColsC NVARCHAR(MAX)" & vbNewLine & _
    "DECLARE @ColsV NVARCHAR(MAX)" & vbNewLine & _
    "DECLARE @TmpQuery NVARCHAR(MAX)" & vbNewLine & _
    vbNewLine & _
    "SET @Cod_Promocion_Copiar = '" & QuitarComillasSimples(mCodPromo_Origen) & "'" & vbNewLine & _
    "SET @Cod_Tmp = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & vbNewLine & _
    "SET @Cod_Usuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "'" & vbNewLine & _
    vbNewLine & _
    "--SET XACT_ABORT ON;" & vbNewLine & _
    vbNewLine & _
    "--BEGIN TRANSACTION" & vbNewLine & _
    vbNewLine & _
    "--BEGIN TRY" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    "------------ DATOS GENERALES DE LA PROMOCION ------------" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = (SELECT 'TMP_PROMOCION.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TMP_PROMOCION'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion', 'Estatus', 'CodUsuario')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = SUBSTRING(@Cols, 1, LEN(@Cols) - 1)" & vbNewLine & _
    vbNewLine & _
    "SET @TmpQuery = 'INSERT INTO TMP_PROMOCION (Cod_Promocion, Estatus, CodUsuario, ' + REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TMP_PROMOCION.', '') + ')' + CHAR(13) +" & vbNewLine & _
    "'SELECT ''' + @Cod_Tmp + ''' AS Cod_Promocion, ''TMP'' AS Estatus, ''' + @Cod_Usuario + ''' AS CodUsuario, ' + CHAR(13) +" & vbNewLine & _
    "REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TMP_PROMOCION.', '') + ' ' + CHAR(13) +" & vbNewLine & _
    "'FROM MA_PROMOCION WHERE Cod_Promocion = ''' + @Cod_Promocion_Copiar + ''''" & vbNewLine & _
    vbNewLine & _
    "EXEC (@TmpQuery)" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    "------------ CONDICIONES Y VALORES DE LA PROMOCI�N ------------" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = (SELECT 'TMP_PROMOCION_CONDICION_VALORES.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TMP_PROMOCION_CONDICION_VALORES'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'CodUsuario', 'Cod_Promocion', 'Des_Proveedor', 'Des_Dpto', 'Des_Grupo', 'Des_Subgrupo', 'Des_Producto')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = SUBSTRING(@Cols, 1, LEN(@Cols) - 1)" & vbNewLine & _
    vbNewLine & _
    "SET @ColsC = (SELECT 'TR_PROMOCION_CONDICION.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TR_PROMOCION_CONDICION'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion', 'Linea_Condicion', 'Linea_Valor') " & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @ColsC = SUBSTRING(@ColsC, 1, LEN(@ColsC) - 1)" & vbNewLine & _
    vbNewLine & _
    "SET @ColsV = (SELECT 'TR_PROMOCION_VALORES.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TR_PROMOCION_VALORES'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion', 'Linea_Valor')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @ColsV = SUBSTRING(@ColsV, 1, LEN(@ColsV) - 1)" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    "SET @TmpQuery = 'INSERT INTO TMP_PROMOCION_CONDICION_VALORES (Cod_Promocion, CodUsuario, " & vbNewLine & _
    "' + REPLACE(@ColsC COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_CONDICION.', '') + CHAR(13) +" & vbNewLine & _
    "', ' + REPLACE(@ColsV COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_VALORES.', '') + CHAR(13) +" & vbNewLine & _
    "', Des_Proveedor, Des_Dpto, Des_Grupo, Des_Subgrupo, Des_Producto)' + CHAR(13) +" & vbNewLine & _
    "'SELECT ''' + @Cod_Tmp + ''' AS Cod_Promocion, ''' + @Cod_Usuario + ''' AS CodUsuario, ' + CHAR(13) +" & vbNewLine & _
    "REPLACE(@ColsC COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_CONDICION.', 'C.') + ', ' + CHAR(13) +" & vbNewLine & _
    "REPLACE(@ColsV COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_VALORES.', 'V.') + ', ' + CHAR(13) +" & vbNewLine & _
    "'isNULL(PV.c_Descripcio, '''') AS Des_Proveedor, isNULL(DP.c_Descripcio, '''') AS Des_Dpto, " & _
    "isNULL(GP.c_Descripcio, '''') AS Des_Grupo, isNULL(SG.c_Descripcio, '''') AS Des_Subgrupo, " & _
    "isNULL(PR.c_Descri, '''') AS Des_Producto' + CHAR(13) +" & vbNewLine & _
    "'FROM TR_PROMOCION_CONDICION C INNER JOIN TR_PROMOCION_VALORES V' + CHAR(13) +" & vbNewLine & _
    "'ON C.Cod_Promocion COLLATE Modern_Spanish_CI_AS = V.Cod_Promocion COLLATE Modern_Spanish_CI_AS AND C.Linea_Valor = V.Linea_Valor' + CHAR(13) +" & vbNewLine & _
    "'LEFT JOIN MA_PROVEEDORES PV ON C.Cod_Proveedor COLLATE Modern_Spanish_CI_AS = PV.c_CodProveed COLLATE Modern_Spanish_CI_AS' + CHAR(13) +" & vbNewLine & _
    "'LEFT JOIN MA_DEPARTAMENTOS DP ON C.Cod_Dpto COLLATE Modern_Spanish_CI_AS = DP.c_Codigo COLLATE Modern_Spanish_CI_AS' + CHAR(13) +" & vbNewLine & _
    "'LEFT JOIN MA_GRUPOS GP ON C.Cod_Grupo COLLATE Modern_Spanish_CI_AS = GP.c_Codigo COLLATE Modern_Spanish_CI_AS AND C.Cod_Dpto COLLATE Modern_Spanish_CI_AS = GP.c_Departamento COLLATE Modern_Spanish_CI_AS' + CHAR(13) +" & vbNewLine & _
    "'LEFT JOIN MA_SUBGRUPOS SG ON C.Cod_Subgrupo COLLATE Modern_Spanish_CI_AS = SG.c_Codigo COLLATE Modern_Spanish_CI_AS AND C.Cod_Grupo COLLATE Modern_Spanish_CI_AS = SG.c_In_Grupo COLLATE Modern_Spanish_CI_AS AND C.Cod_Dpto COLLATE Modern_Spanish_CI_AS = SG.c_In_Departamento COLLATE Modern_Spanish_CI_AS' + CHAR(13) +" & vbNewLine & _
    "'LEFT JOIN MA_PRODUCTOS PR ON C.Cod_Producto COLLATE Modern_Spanish_CI_AS = PR.c_Codigo COLLATE Modern_Spanish_CI_AS' + CHAR(13) +" & vbNewLine & _
    "'WHERE C.Cod_Promocion = ''' + @Cod_Promocion_Copiar + ''''" & vbNewLine & _
    vbNewLine & _
    "EXEC (@TmpQuery)" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    "------------ LOCALIDADES DONDE APLICA LA PROMOCION ------------" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = (SELECT 'TMP_PROMOCION_SUCURSAL.' + COLUMN_NAME + ',' AS 'data()'" & vbNewLine & _
    "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TMP_PROMOCION_SUCURSAL'" & vbNewLine & _
    "AND COLUMN_NAME COLLATE Modern_Spanish_CI_AS NOT IN ('ID', 'Cod_Promocion', 'CodUsuario', 'Des_Localidad', 'Dir_Localidad', 'Ger_Localidad')" & vbNewLine & _
    "FOR XML PATH (''))" & vbNewLine & _
    vbNewLine & _
    "SET @Cols = SUBSTRING(@Cols, 1, LEN(@Cols) - 1)" & vbNewLine & _
    vbNewLine & _
    "SET @TmpQuery = 'INSERT INTO TMP_PROMOCION_SUCURSAL (Cod_Promocion, CodUsuario, ' + CHAR(13) +" & vbNewLine & _
    "REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TMP_PROMOCION_SUCURSAL.', '') + ', Des_Localidad, Dir_Localidad, Ger_Localidad)' + CHAR(13) +" & vbNewLine & _
    "'SELECT ''' + @Cod_Tmp + ''' AS Cod_Promocion, ''' + @Cod_Usuario + ''' AS CodUsuario, ' + CHAR(13) +" & vbNewLine & _
    "REPLACE(@Cols COLLATE Modern_Spanish_CI_AS, 'TMP_PROMOCION_SUCURSAL.', '') + ', ' + CHAR(13) +" & vbNewLine & _
    "'isNULL(SUC.c_Descripcion, '''') AS Des_Localidad, isNULL(SUC.c_Direccion, '''') AS Dir_Localidad, " & _
    "isNULL(SUC.c_Gerente, '''') AS Ger_Localidad ' + CHAR(13) +" & vbNewLine & _
    "'FROM MA_PROMOCION_SUCURSAL MPS ' + CHAR(13) +" & vbNewLine & _
    "'LEFT JOIN MA_SUCURSALES SUC ' + CHAR(13) +" & vbNewLine & _
    "'ON MPS.Cod_Localidad COLLATE Modern_Spanish_CI_AS = SUC.c_Codigo COLLATE Modern_Spanish_CI_AS ' + CHAR(13) +" & vbNewLine & _
    "'WHERE Cod_Promocion = ''' + @Cod_Promocion_Copiar + ''''" & vbNewLine & _
    vbNewLine & _
    "EXEC (@TmpQuery)" & vbNewLine & _
    vbNewLine
    
    mSQL = mSQL & _
    vbNewLine & _
    "--COMMIT TRANSACTION" & vbNewLine & _
    vbNewLine & _
    "--END TRY" & vbNewLine & _
    vbNewLine & _
    "--BEGIN CATCH" & vbNewLine & _
    "    --Print 'ERROR. ROLLBACK'" & vbNewLine & _
    "    --IF @@TRANCOUNT > 0" & vbNewLine & _
    "        --ROLLBACK TRANSACTION --RollBack in case of Error" & vbNewLine & _
    vbNewLine & _
    "--END CATCH "
    
    FrmAppLink.ExecuteSQLProcedure FrmAppLink.CnADM, pSQLText:=mSQL, CallerErrorHandling:=True
    
    If FrmAppLink.oResp.Item("Err_Number") <> 0 Then
        Err.Raise FrmAppLink.oResp.Item("Err_Number"), _
        FrmAppLink.oResp.Item("Err_Source"), _
        FrmAppLink.oResp.Item("Err_Desc")
    End If
    
    FrmAppLink.CnADM.CommitTrans
    ActiveTrans = False
    
    Exit Sub
    
Error:
    
    If ActiveTrans Then
        FrmAppLink.CnADM.RollbackTrans
        ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    mCodPromo_Origen = Empty
    mCodPromo_Actual = Empty
    TipoPromo = -1
    
End Sub

Private Sub lbl_New_Click()
    
    If Grid.Rows > 1 Then
        mCodPromo_Actual = Grid.TextMatrix(Grid.Row, ColCodPro)
        TipoPromo = Grid.TextMatrix(Grid.Row, ColNumTipo)
        EstatusPromo = Grid.TextMatrix(Grid.Row, ColIntEstatus)
    End If
    
    If EstatusPromo = "DWT" And mCodPromo_Actual <> Empty Then
        If Mensaje(False, StellarMensajeLocal(161)) Then
            CargarEspera_Temp
        Else
            GoTo NewPromo
        End If
    ElseIf EstatusPromo <> "DWT" And mCodPromo_Actual <> Empty And mCodPromoCopiar <> Empty Then
        If Mensaje(False, StellarMensajeLocal(200)) Then
            CopiarPromo_Temp
        Else
            GoTo NewPromo
        End If
        mCodPromoCopiar = Empty
    Else
NewPromo:
        mCodPromo_Origen = Empty
        mCodPromo_Actual = Empty
        mCodPromoCopiar = Empty
        TipoPromo = -1
    End If
    
    frmAsistentePro_Campanna.Show vbModal
    
    If CancelarPromo_Salir Then
        CancelarPromo_Salir = False
    ElseIf PromocionCreada Then
        PromocionCreada = False
        ' Agregada? Volver a cargar
        CmdBuscar_Click
    End If
    
    mCodPromo_Origen = Empty
    mCodPromoCopiar = Empty
    
End Sub

Public Function Consulta_Mostrar() As Boolean
    
    Dim RsConsulta As New ADODB.Recordset
    Dim Cnx As New ADODB.Connection
    Dim Cont As Long, ContItems As Long
    
    Cont = 0
    
    If Trim(StrSqlMasCondicion) = Empty Then
        Exit Function
    End If
    
    'Call Desactivar_Objetos(False)
    
    On Error GoTo GetError
    
    Screen.MousePointer = 11
    
    Grid.Rows = 1
    
    Cnx.ConnectionString = Conex_SC.ConnectionString
    Cnx.CursorLocation = adUseServer
    Cnx.Open
    
    If StrOrderBy <> Empty Then
        StrSqlMasCondicion = StrSqlMasCondicion & " ORDER BY " & Me.StrOrderBy
    End If
    
    RsConsulta.CursorLocation = adUseServer
    
    Apertura_Recordset RsConsulta
    
    'Debug.Print StrSqlMasCondicion
    
     ' Antes de Buscar Completar cualquier promoci�n que este fuera de rango
    
    ExecuteSafeSQL "UPDATE MA_PROMOCION SET " & _
    "Estatus = 'DCO' " & _
    "WHERE Estatus = 'DPE' " & _
    "AND CAST(GetDate() AS DATE) > CAST(Fecha_Fin AS DATE)", _
    FrmAppLink.CnADM, RecordsAffected
    
    RsConsulta.Open StrSqlMasCondicion, Cnx, adOpenKeyset, adLockReadOnly
    
    If Not RsConsulta.EOF Then
        
        RsConsulta.MoveLast
        RsConsulta.MoveFirst
        
        ContFound = 0
        
        Me.Barra_Prg.Min = 0
        Me.Barra_Prg.Value = 0
        'Me.Tim_Progreso.Enabled = True
        Me.Barra_Prg.Max = RsConsulta.RecordCount
        
        ContFound = RsConsulta.RecordCount
        
        I = 0
        
        ScrollGrid.Min = 0
        ScrollGrid.Max = ContFound
        ScrollGrid.Value = 0
        
        Do Until RsConsulta.EOF
            
            DoEvents

            I = I + 1
            
            If I > Grid.Rows - 1 Then Grid.Rows = Grid.Rows + 1
                
            Grid.TextMatrix(I, ColRowID) = I
            Grid.TextMatrix(I, ColCodPro) = RsConsulta!Cod_Promocion
            Grid.TextMatrix(I, ColDesPro) = RsConsulta!Descripcion
            Grid.TextMatrix(I, ColNumTipo) = RsConsulta!Tipo_Promocion
            
            Select Case Val(RsConsulta!Tipo_Promocion)
                Case 1
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(104)
                Case 2
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(105)
                Case 3
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(106)
                Case 4
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(110)
                Case 5
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(165)
                Case 6
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(166)
                Case 7
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(167)
                Case 8
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(188)
                Case 9
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(189)
                Case 10
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(190)
                Case 11
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(191)
                Case 16
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(261)
                Case 17
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(262)
                Case 18
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(263)
                Case 19
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(264)
                Case 20
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(265)
                Case 21
                    Grid.TextMatrix(I, ColTipo) = StellarMensajeLocal(266)
                Case Else
                    'NotImplementedYet
            End Select
            
            Grid.TextMatrix(I, ColCampa�a) = RsConsulta!Campa�a
            Grid.TextMatrix(I, ColIntEstatus) = RsConsulta!Estatus
            
            Select Case UCase(RsConsulta!Estatus)
                Case "DPE"
                    If RsConsulta!FechaHoraActualSrv >= CDate(SDate(RsConsulta!Fecha_Inicio)) _
                    And RsConsulta!FechaHoraActualSrv <= EndOfDay(RsConsulta!Fecha_Fin) Then
                        Grid.TextMatrix(I, ColEstatus) = StellarMensajeLocal(324)
                    Else
                        Grid.TextMatrix(I, ColEstatus) = StellarMensajeLocal(323)
                    End If
                Case "DCO"
                    Grid.TextMatrix(I, ColEstatus) = StellarMensajeLocal(140)
                Case "ANU"
                    Grid.TextMatrix(I, ColEstatus) = StellarMensajeLocal(142)
                Case "DWT"
                    Grid.TextMatrix(I, ColEstatus) = StellarMensajeLocal(127)
                Case Else
                    Grid.TextMatrix(I, ColEstatus) = "N/A"
            End Select
            
            Grid.TextMatrix(I, ColView) = Empty
            Grid.TextMatrix(I, ColDelete) = Empty
            
            Me.Barra_Prg.Value = ContItems
            
            RsConsulta.MoveNext
            
            ContItems = ContItems + 1
            
        Loop
        
        If Grid.Rows > 11 Then
            ScrollGrid.Visible = True
            Grid.ScrollBars = flexScrollBarBoth
            ReduccionScrollBar = ScrollGrid.Width
            Grid.ColWidth(ColDesPro) = (OriginalDescColWidth - ReduccionScrollBar)
        Else
            Grid.ScrollBars = flexScrollBarHorizontal
            ScrollGrid.Visible = False
            ReduccionScrollBar = 0
            Grid.ColWidth(ColDesPro) = OriginalDescColWidth
        End If
        
        Me.Barra_Prg.Value = Me.Barra_Prg.Max
        
        'Call Desactivar_Objetos(True)
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        
        Fila = 0
        Grid_EnterCell
        
    Else
        Grid.Col = Grid.Cols - 1
        Grid.ColSel = Grid.Col
        Grid_EnterCell
        ScrollGrid.Visible = False
        SeleccionarTexto txtDato
        'IgnorarActivate
        'MsgBox True, Stellar_Mensaje(16059) '"No hay ning�n Item que cumpla con los Par�metros de B�squeda."
    End If
    
    RsConsulta.Close
    
    Screen.MousePointer = 0
    
    Me.Barra_Prg.Value = 0
    
    On Error GoTo 0
    
    Consulta_Mostrar = True
    
    'Call Desactivar_Objetos(True)
    
    Fila = 0
        
    Exit Function
    
GetError:
        
    'Call Desactivar_Objetos(True)
    
    Screen.MousePointer = 0
    Me.Barra_Prg.Value = 0
    
    Call IgnorarActivate
    MsgBox Err.Description 'Call Mensaje(False, "Error N0." & Err.Number)
    
    Err.Clear
    
    Consulta_Mostrar = False
    
    If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    
End Function

Private Sub PrepararBusqueda()
    SeleccionarTexto txtDato
    Set CampoT = txtDato
End Sub

Private Sub lblTeclado_Click()
    CmdTeclado_Click
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If EventoProgramado Then Exit Sub
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Private Sub txtDato_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtDato_GotFocus()
    PrepararBusqueda
End Sub

Private Sub txtDato_KeyPress(KeyAscii As Integer)
    
    Dim mDatTemp As String
    
    On Error GoTo GetError
    
    Select Case KeyAscii
        Case Is = 39
            KeyAscii = 0
        Case Is = vbKeyReturn
            
            mDatTemp = txtDato.Text
            
            'If Trim(txtDato) = Empty Then txtDato.Text = "%"
            
            Select Case Me.cmbItemBusqueda.ListIndex
                Case 0 ' Mostrar Activas
                    StrCondicion = " WHERE (Estatus = 'DPE')"
                    StrSQL = "SELECT TOP (50) *, GetDate() AS FechaHoraActualSrv FROM MA_PROMOCION"
                Case 1 ' Mostrar Completadas
                    StrCondicion = " WHERE (Estatus = 'DCO')"
                    StrSQL = "SELECT TOP (50) *, GetDate() AS FechaHoraActualSrv FROM MA_PROMOCION"
                Case 2 ' Mostrar Anuladas
                    StrCondicion = " WHERE (Estatus = 'ANU')"
                    StrSQL = "SELECT TOP (50) *, GetDate() AS FechaHoraActualSrv FROM MA_PROMOCION"
                Case 3
                    StrSQL = "SELECT TOP (50) *, GetDate() AS FechaHoraActualSrv FROM TMP_PROMOCION"
                    StrCondicion = " WHERE (Estatus = 'DWT')"
                Case 4
                    StrCondicion = " WHERE (Estatus = 'DPE') AND CAST(GetDate() AS DATE) < CAST(Fecha_Inicio AS DATE)"
                    StrSQL = "SELECT TOP (50) *, GetDate() AS FechaHoraActualSrv FROM MA_PROMOCION"
                Case 5
                    StrCondicion = " WHERE (Estatus = 'DPE') " & _
                    "AND CAST(GetDate() AS DATE) BETWEEN CAST(Fecha_Inicio AS DATE) AND CAST(Fecha_Fin AS DATE)"
                    StrSQL = "SELECT TOP (50) *, GetDate() AS FechaHoraActualSrv FROM MA_PROMOCION"
                Case cmbItemBusqueda.ListCount - 1 ' Mostrar Todos
                    StrSQL = "SELECT TOP (50) *, GetDate() AS FechaHoraActualSrv FROM MA_PROMOCION"
                    StrCondicion = " WHERE 1 = 1"
            End Select
            
            StrCondicion = StrCondicion & " AND ("
            
            For Each Element In ArrCamposBusquedas
                StrCondicion = StrCondicion & " " & Element & " LIKE '%" & txtDato.Text & "%' OR"
            Next Element
            
            StrCondicion = Left(StrCondicion, Len(StrCondicion) - Len("OR")) & ")"
            
            'If txtDato = "%" Then 'And strCadBusCod = "" And strCadBusDes = "" Then
                
                'StrSqlMasCondicion = StrSQL
                
                'If Trim(StrCriterioBase) <> vbNullString Then
                    'StrSqlMasCondicion = StrSqlMasCondicion & _
                    'IIf(UCase(StrSqlMasCondicion) Like UCase("*WHERE*"), _
                    'GetLines & "AND ", GetLines & "WHERE ") & StrCriterioBase
                'End If
                
                'Consulta_Mostrar
                'txtDato.Text = mDatTemp
                'PrepararBusqueda
                
            'Else
                
                'If UCase(StrCadBusDes) = UCase("c_Codigo") Then
                    
                    'StrCondicion = GetLines & "LEFT JOIN MA_CODIGOS Codigos " & GetLines & _
                    '"ON Producto.c_Codigo = Codigos.c_CodNasa" & GetLines & _
                    '"WHERE Codigos.c_Codigo LIKE '" + Me.txtDato + "%'"
                                                                                                                                                
                    'StrSqlMasCondicion = StrSQL + StrCondicion
                                       
                'Else
                    
                    StrSqlMasCondicion = StrSQL + StrCondicion
                    
                'End If
                
                If Trim(StrCriterioBase) <> vbNullString Then
                    StrSqlMasCondicion = StrSqlMasCondicion & _
                    IIf(UCase(StrSqlMasCondicion) Like UCase("*WHERE*"), _
                    GetLines & "AND ", GetLines & "WHERE ") & StrCriterioBase
                End If
                
                If chkFiltrarProducto.Value = vbChecked And chkFiltrarProducto.Tag <> Empty Then
                    
                    mFiltroProd = vbNewLine
                    
                    mArrProducto = Split(chkFiltrarProducto.Tag, "||", , vbTextCompare)
                    
                    mFiltroProd = mFiltroProd & _
                    "AND Cod_Promocion IN ( " & vbNewLine & _
                    "SELECT Cod_Promocion FROM TR_PROMOCION_CONDICION WHERE Cod_Producto = '" & mArrProducto(0) & "' AND Tipo_Condicion IN (1, 3) " & vbNewLine & _
                    "UNION " & vbNewLine & _
                    "SELECT Cod_Promocion FROM TR_PROMOCION_CONDICION WHERE (Cod_Dpto + ';' + Cod_Grupo + ';' + Cod_Subgrupo) IN ( " & vbNewLine & _
                    "SELECT TOP 1 (c_Departamento + ';' + c_Grupo + ';' + c_Subgrupo) FROM MA_PRODUCTOS WHERE c_Codigo = '" & mArrProducto(0) & "' " & vbNewLine & _
                    ") AND Tipo_Condicion IN (1, 3) AND (Cod_Dpto + ';' + Cod_Grupo + ';' + Cod_Subgrupo) <> ';;' " & vbNewLine & _
                    "UNION " & vbNewLine & _
                    "SELECT Cod_Promocion FROM TR_PROMOCION_CONDICION WHERE Marca LIKE ( " & vbNewLine & _
                    "SELECT TOP 1 c_Marca FROM MA_PRODUCTOS WHERE c_Codigo = '" & mArrProducto(0) & "' " & vbNewLine & _
                    ") AND Tipo_Condicion IN (1, 3) AND Marca <> '' " & vbNewLine & _
                    "UNION " & vbNewLine & _
                    "SELECT Cod_Promocion FROM TR_PROMOCION_CONDICION WHERE Modelo LIKE ( " & vbNewLine & _
                    "SELECT TOP 1 c_Modelo FROM MA_PRODUCTOS WHERE c_Codigo = '" & mArrProducto(0) & "' " & vbNewLine & _
                    ") AND Tipo_Condicion IN (1, 3) AND Modelo <> '' " & vbNewLine & _
                    "UNION " & vbNewLine & _
                    "SELECT Cod_Promocion FROM TR_PROMOCION_CONDICION WHERE Cod_Proveedor IN ( " & vbNewLine & _
                    "SELECT c_CodProvee FROM MA_PRODXPROV WHERE c_Codigo = '" & mArrProducto(0) & "' " & vbNewLine & _
                    ") AND Tipo_Condicion IN (1, 3) AND Cod_Proveedor <> '' " & vbNewLine & _
                    ") " & vbNewLine
                    
                    mFiltroProd = mFiltroProd & _
                    "AND Cod_Promocion NOT IN ( " & vbNewLine & _
                    "SELECT Cod_Promocion FROM TR_PROMOCION_CONDICION WHERE Cod_Producto = '" & mArrProducto(0) & "' AND Tipo_Condicion IN (2, 4) " & vbNewLine & _
                    "UNION " & vbNewLine & _
                    "SELECT Cod_Promocion FROM TR_PROMOCION_CONDICION WHERE (Cod_Dpto + ';' + Cod_Grupo + ';' + Cod_Subgrupo) IN ( " & vbNewLine & _
                    "SELECT TOP 1 (c_Departamento + ';' + c_Grupo + ';' + c_Subgrupo) FROM MA_PRODUCTOS WHERE c_Codigo = '" & mArrProducto(0) & "' " & vbNewLine & _
                    ") AND Tipo_Condicion IN (2, 4) AND (Cod_Dpto + ';' + Cod_Grupo + ';' + Cod_Subgrupo) <> ';;' " & vbNewLine & _
                    "UNION " & vbNewLine & _
                    "SELECT Cod_Promocion FROM TR_PROMOCION_CONDICION WHERE Marca LIKE ( " & vbNewLine & _
                    "SELECT TOP 1 c_Marca FROM MA_PRODUCTOS WHERE c_Codigo = '" & mArrProducto(0) & "' " & vbNewLine & _
                    ") AND Tipo_Condicion IN (2, 4) AND Marca <> '' " & vbNewLine & _
                    "UNION " & vbNewLine & _
                    "SELECT Cod_Promocion FROM TR_PROMOCION_CONDICION WHERE Modelo LIKE ( " & vbNewLine & _
                    "SELECT TOP 1 c_Modelo FROM MA_PRODUCTOS WHERE c_Codigo = '" & mArrProducto(0) & "' " & vbNewLine & _
                    ") AND Tipo_Condicion IN (2, 4) AND Modelo <> '' " & vbNewLine & _
                    "UNION " & vbNewLine & _
                    "SELECT Cod_Promocion FROM TR_PROMOCION_CONDICION WHERE Cod_Proveedor IN ( " & vbNewLine & _
                    "SELECT c_CodProvee FROM MA_PRODXPROV WHERE c_Codigo = '" & mArrProducto(0) & "' " & vbNewLine & _
                    ") AND Tipo_Condicion IN (2, 4) AND Cod_Proveedor <> '' " & vbNewLine & _
                    ") " & vbNewLine
                    
                    StrSqlMasCondicion = StrSqlMasCondicion & mFiltroProd
                    
                End If
                
                Consulta_Mostrar
                txtDato.Text = mDatTemp
                PrepararBusqueda
                
            'End If
            
    End Select
    
    Screen.MousePointer = 0
    
    On Error GoTo 0
    
    Exit Sub
    
GetError:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Call IgnorarActivate
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    Err.Clear
    
End Sub
