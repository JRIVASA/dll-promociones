VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frmAsistentePro_ConfigFormaPago 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Excluir"
      Height          =   8895
      Left            =   180
      TabIndex        =   12
      Top             =   1860
      Width           =   11925
      Begin VB.TextBox txtMonedaLimite 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   11040
         TabIndex        =   31
         Top             =   2400
         Visible         =   0   'False
         Width           =   330
      End
      Begin VB.CommandButton CmdMonedaLimite 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   10200
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   2760
         Width           =   435
      End
      Begin VB.TextBox txtMontoLimiteDcto 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   6480
         TabIndex        =   29
         Top             =   2760
         Visible         =   0   'False
         Width           =   3660
      End
      Begin VB.CheckBox ChkDctoLimiteFactura 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Monto de Descuento Limite M�ximo por Factura"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   180
         TabIndex        =   28
         Top             =   2880
         Visible         =   0   'False
         Width           =   5595
      End
      Begin VB.TextBox txtVueltoMaximo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   6480
         TabIndex        =   27
         Top             =   2160
         Width           =   4140
      End
      Begin VB.CheckBox ChkVueltoPromocion 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Monto m�ximo de vuelto durante la promoci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   180
         TabIndex        =   26
         Top             =   2280
         Width           =   5595
      End
      Begin VB.CheckBox ChkAplicarPorCriterio 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Aplica porcentaje m�nimo por m�todo de pago individual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   4800
         TabIndex        =   25
         Top             =   1725
         Width           =   6015
      End
      Begin VB.PictureBox Medir 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   435
         Left            =   540
         ScaleHeight     =   375
         ScaleWidth      =   495
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   5280
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.CheckBox chkPagoMinimo 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Pago M�nimo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   180
         TabIndex        =   2
         Top             =   1725
         Width           =   2475
      End
      Begin VB.TextBox txtMoneda 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1800
         TabIndex        =   0
         Top             =   480
         Width           =   1425
      End
      Begin VB.CommandButton CmdMoneda 
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3300
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":0802
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   480
         Width           =   435
      End
      Begin VB.VScrollBar ScrollGrid 
         Height          =   3495
         LargeChange     =   10
         Left            =   11100
         TabIndex        =   19
         Top             =   4500
         Width           =   675
      End
      Begin VB.TextBox TxtNumPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   8220
         Width           =   780
      End
      Begin VB.TextBox txtPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   1980
         TabIndex        =   5
         Top             =   8220
         Width           =   780
      End
      Begin VB.TextBox txtValorMinimo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   2820
         TabIndex        =   3
         Top             =   1620
         Width           =   1860
      End
      Begin VB.TextBox txtDenominacion 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1800
         TabIndex        =   1
         Top             =   1080
         Width           =   1440
      End
      Begin VB.CommandButton CmdDenominacion 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3300
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1004
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1080
         Width           =   435
      End
      Begin VB.CommandButton CmdSiguiente 
         Caption         =   "Siguiente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8760
         TabIndex        =   8
         Top             =   8220
         Width           =   1455
      End
      Begin VB.CommandButton CmdAtras 
         Caption         =   "Atras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7200
         TabIndex        =   7
         Top             =   8220
         Width           =   1455
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   10320
         TabIndex        =   9
         Top             =   8220
         Width           =   1455
      End
      Begin MSFlexGridLib.MSFlexGrid Grid 
         DragMode        =   1  'Automatic
         Height          =   3495
         Left            =   120
         TabIndex        =   4
         Top             =   4500
         Width           =   11655
         _ExtentX        =   20558
         _ExtentY        =   6165
         _Version        =   393216
         Cols            =   12
         FixedCols       =   0
         BackColor       =   16448250
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         ScrollBars      =   2
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblMonedaLimite 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Moneda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   10800
         TabIndex        =   32
         Top             =   2880
         Visible         =   0   'False
         Width           =   420
      End
      Begin VB.Label lblDescBtnBorrar 
         BackStyle       =   0  'Transparent
         Caption         =   "Eliminar Fila"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   345
         Left            =   5040
         TabIndex        =   24
         Top             =   8280
         Width           =   690
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblDescBtnExcluir 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Excluir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   165
         Left            =   10080
         TabIndex        =   23
         Top             =   4275
         Visible         =   0   'False
         Width           =   510
      End
      Begin VB.Label lblDescBtnIncluir 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Incluir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   165
         Left            =   9300
         TabIndex        =   22
         Top             =   4275
         Width           =   495
      End
      Begin VB.Image CmdDelete 
         Height          =   480
         Left            =   4500
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1806
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Image ImgNext 
         Height          =   480
         Left            =   1140
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":662C
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Image ImgPrev 
         Height          =   480
         Left            =   300
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":B3BB
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   2880
         X2              =   3060
         Y1              =   8640
         Y2              =   8280
      End
      Begin VB.Image CmdExcluir 
         Height          =   600
         Left            =   10020
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":E8A0
         Stretch         =   -1  'True
         Top             =   3600
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.Image CmdIncluir 
         Height          =   600
         Left            =   9240
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1248D
         Stretch         =   -1  'True
         Top             =   3600
         Width           =   600
      End
      Begin VB.Label lblDescMoneda 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3840
         TabIndex        =   18
         Top             =   480
         UseMnemonic     =   0   'False
         Width           =   6780
      End
      Begin VB.Label LBL_DEPARTAMENTO 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3840
         TabIndex        =   17
         Top             =   1080
         UseMnemonic     =   0   'False
         Width           =   6765
      End
      Begin VB.Label lblDenominacion 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Denominaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   16
         Top             =   1080
         Width           =   1350
      End
      Begin VB.Label lblMoneda 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Moneda (*)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   15
         Top             =   600
         Width           =   1155
      End
      Begin VB.Label lblCriterios 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   180
         TabIndex        =   14
         Top             =   120
         Width           =   960
      End
      Begin VB.Line LnCriterios 
         BorderColor     =   &H00AE5B00&
         X1              =   1260
         X2              =   11760
         Y1              =   240
         Y2              =   240
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   6600
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":13157
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":14EE9
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":15BC3
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1689D
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1862F
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1A3C1
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1C153
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Image CmdConfigurarTeclado 
      Height          =   660
      Left            =   7080
      MousePointer    =   99  'Custom
      Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1DEE5
      Stretch         =   -1  'True
      Top             =   1020
      Width           =   660
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1FC67
      Top             =   360
      Width           =   2700
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   11
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_ConfigFormaPago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private AnchoScrollBar      As Long
Private AnchoCampoScroll    As Long
Private pCargandoArchivo    As Boolean
Private pCargarLinea        As Boolean

Private DragStart, DragStop, DragRows, mDragEndRow

Private Enum GridCol
    ColRowID
    ColTipoCondicion
    ColCodProd
    ColDesProd
    ColMarca
    ColModelo
    ColValorPromo1
    ColValorPromo2
    ColValorPromo3
    ColValorMxN
    [ColCount]
End Enum

Private mVarEsPremio As Boolean ' True -> Seleccionar Premios, False -> Seleccionar Condiciones.

Public Property Let TipoSeleccion(ByVal pPremios As Boolean)
    mVarEsPremio = pPremios
End Property

Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
  
  Dim Ctl As Control
  Dim bHandled As Boolean
  Dim bOver As Boolean
  
  For Each Ctl In Controls
    ' Is the mouse over the control
    On Error Resume Next
    bOver = (Ctl.Visible And IsOver(Ctl.hWnd, Xpos, Ypos))
    On Error GoTo 0
    
    If bOver Then
      ' If so, respond accordingly
      bHandled = True
      Select Case True
      
        Case TypeOf Ctl Is MSFlexGrid
          FlexGridScroll Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is PictureBox
          PictureBoxZoom Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is ListBox, TypeOf Ctl Is TextBox, TypeOf Ctl Is ComboBox
          ' These controls already handle the mousewheel themselves, so allow them to:
          If Ctl.Enabled Then Ctl.SetFocus
          
        Case Else
          bHandled = False

      End Select
      If bHandled Then Exit Sub
    End If
    bOver = False
  Next Ctl
  
  ' Scroll was not handled by any controls, so treat as a general message send to the form
  'Me.Caption = "Form Scroll " & IIf(Rotation < 0, "Down", "Up")
End Sub

Private Sub ChkCombinarProductos_Click()
    
    If ChkCombinarProductos.Value = vbChecked Then
        
        Dim mValidarObligatorios As Boolean
        
        If Not IsNumeric(txtCantLlevar.Text) Then txtCantLlevar.Text = 0
        If Not IsNumeric(txtCantPagar.Text) Then txtCantPagar.Text = 0
        
        Select Case TipoPromo
            Case 4
                mValidarObligatorios = _
                CDbl(txtCantLlevar.Text) > 0 And CDbl(txtCantPagar.Text) > 0 And _
                CDbl(txtCantLlevar.Text) > CDbl(txtCantPagar.Text)
                If Not mValidarObligatorios Then
                    MensajeValidacion = StellarMensajeLocal(172)
                    If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
                End If
            Case 5, 6, 7
                mValidarObligatorios = _
                CDbl(txtCantLlevar.Text) > 0 And CDbl(txtCantPagar.Text) > 0 And _
                CDbl(txtCantLlevar.Text) >= CDbl(txtCantPagar.Text)
                If Not mValidarObligatorios Then
                    MensajeValidacion = StellarMensajeLocal(173)
                    If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
                End If
        End Select
        
        If mValidarObligatorios Then
            
            ExecuteSafeSQL "UPDATE TMP_PROMOCION SET " & vbNewLine & _
            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & vbNewLine & _
            "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
            "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
            FrmAppLink.CnADM
            
            ExecuteSafeSQL "UPDATE TMP_PROMOCION_CONDICION_VALORES SET " & vbNewLine & _
            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), " & vbNewLine & _
            "Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & vbNewLine & _
            "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
            "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
            FrmAppLink.CnADM
            
            txtPag.Tag = "0"
            txtPag_LostFocus
            
            txtCantLlevar.Locked = True
            txtCantPagar.Locked = True
            
        End If
        
    Else
        txtCantLlevar.Locked = False
        txtCantPagar.Locked = False
        ExecuteSafeSQL "UPDATE TMP_PROMOCION SET " & vbNewLine & _
        "Cantidad_Productos_Requerir = 0, Cantidad_Productos_Pagar = 0 " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
        FrmAppLink.CnADM
    End If
End Sub

Private Sub ChkSeleccionarTodo_Click()
    If ChkSeleccionarTodo.Value = vbChecked Then
        ResetearCriterios True
    End If
End Sub

Private Sub CmdArchivo_Click()
    
    On Error GoTo Error
    
    Dim CodigoProducto As String, ValorCampoPromo1 As Double, _
    ValorCampoPromo2 As Double, ValorCampoPromo3 As Double, _
    ValorMxNLlevar As Double, ValorMxNPagar As Double
    
    Set gClsArcBD = CreateObject("recsun.obj_ArchivoDB")
    
    gClsArcBD.Inicializar 0
    
    gClsArcBD.Adicionar_CamposDisponibles "CodProducto", "" & StellarMensaje(16524) & "", 3000
    
    Select Case TipoPromo
        Case 1 'Precio Oferta
CasePrc1:
            If (TipoPromo = 9 Or TipoPromo = 13) And Not mVarEsPremio Then
            Else
                gClsArcBD.Adicionar_CamposDisponibles "Prc", "" & StellarMensajeLocal(104) & "", 3000
            End If
        Case 2 '% Descuento
CasePorc1:
            If (TipoPromo = 10 Or TipoPromo = 14) And Not mVarEsPremio Then
            Else
                gClsArcBD.Adicionar_CamposDisponibles "Desc1", "" & StellarMensajeLocal(105) & " 1", 3000
                gClsArcBD.Adicionar_CamposDisponibles "Desc2", "" & StellarMensajeLocal(105) & " 2", 3000
                gClsArcBD.Adicionar_CamposDisponibles "Desc3", "" & StellarMensajeLocal(105) & " 3", 3000
            End If
        Case 3 'Monto Descuento
CaseMonto1:
            If (TipoPromo = 11 Or TipoPromo = 15) And Not mVarEsPremio Then
            Else
                gClsArcBD.Adicionar_CamposDisponibles "Monto", "" & StellarMensajeLocal(106) & "", 3000
            End If
        Case 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
            Select Case TipoPromo
                Case 4, 8, 12
                    gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                    If TipoPromo = 4 Then
                        gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(222) & "", 3000
                    End If
                Case 5, 9, 13 'Precio Oferta
                    gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                    If TipoPromo = 5 Then
                        gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(223) & "", 3000
                    End If
                    GoTo CasePrc1
                Case 6, 10, 14 '% Descuento
                    gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                    If TipoPromo = 6 Then
                        gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(223) & "", 3000
                    End If
                    GoTo CasePorc1
                Case 7, 11, 15 'Monto Descuento
                    gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                    If TipoPromo = 7 Then
                        gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(223) & "", 3000
                    End If
                    GoTo CaseMonto1
            End Select
        Case 16, 17, 18
            gClsArcBD.Adicionar_CamposDisponibles "Desc1", "" & StellarMensajeLocal(105) & " 1", 3000
    End Select
    
    Dim mValores, LenArr
    mValores = gClsArcBD.EJECUTAR
    If IsEmpty(mValores) Then Exit Sub
    
    MousePointer = vbHourglass
    DoEvents
    pCargandoArchivo = True
    ResetearCriterios
    
    LenArr = UBound(mValores)
    
    Dim pCuentaExito As Double, pCuentaFallo As Double, pCantRegistros As Double
    Dim mInfoFallidos As String: mInfoFallidos = Empty
    
    For I = 1 To LenArr
        
        Dim CodigoEntrada As String
        
        CodigoEntrada = mValores(I)(0)
        
        txtCodigoProducto.Text = CodigoEntrada
        txtCodigoProducto_LostFocus
        
        Select Case TipoPromo
            
            Case 1 'Precio Oferta
CasePrc2:
                If IsNumeric(mValores(I)(1)) Then
                    ValorCampoPromo1 = CDbl(mValores(I)(1))
                Else
                    ValorCampoPromo1 = 0
                End If
                
            Case 2 '% Descuento
CasePorc2:
                If IsNumeric(mValores(I)(1)) Then
                    ValorCampoPromo1 = CDbl(mValores(I)(1))
                Else
                    ValorCampoPromo1 = 0
                End If
                
                If IsNumeric(mValores(I)(2)) Then
                    ValorCampoPromo2 = CDbl(mValores(I)(2))
                Else
                    ValorCampoPromo2 = 0
                End If
                
                If IsNumeric(mValores(I)(3)) Then
                    ValorCampoPromo3 = CDbl(mValores(I)(3))
                Else
                    ValorCampoPromo3 = 0
                End If
                
            Case 3 'Monto Descuento
CaseMonto2:
                If IsNumeric(mValores(I)(1)) Then
                    ValorCampoPromo1 = CDbl(mValores(I)(1))
                Else
                    ValorCampoPromo1 = 0
                End If
                
            Case 4, 5, 6, 7
                
                Select Case TipoPromo
                
                    Case 4
                        
                        If IsNumeric(mValores(I)(1)) Then
                            ValorMxNLlevar = CDbl(mValores(I)(1))
                        Else
                            ValorMxNLlevar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(2)) Then
                            ValorMxNPagar = CDbl(mValores(I)(2))
                        Else
                            ValorMxNPagar = 0
                        End If
                        
                    Case 5 'Precio Oferta
                        
                        If IsNumeric(mValores(I)(1)) Then
                            ValorMxNLlevar = CDbl(mValores(I)(1))
                        Else
                            ValorMxNLlevar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(2)) Then
                            ValorMxNPagar = CDbl(mValores(I)(2))
                        Else
                            ValorMxNPagar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(3)) Then
                            ValorCampoPromo1 = CDbl(mValores(I)(3))
                        Else
                            ValorCampoPromo1 = 0
                        End If
                        
                    Case 6 '% Descuento
                        
                        If IsNumeric(mValores(I)(1)) Then
                            ValorMxNLlevar = CDbl(mValores(I)(1))
                        Else
                            ValorMxNLlevar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(2)) Then
                            ValorMxNPagar = CDbl(mValores(I)(2))
                        Else
                            ValorMxNPagar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(3)) Then
                            ValorCampoPromo1 = CDbl(mValores(I)(3))
                        Else
                            ValorCampoPromo1 = 0
                        End If
                        
                        If IsNumeric(mValores(I)(4)) Then
                            ValorCampoPromo2 = CDbl(mValores(I)(4))
                        Else
                            ValorCampoPromo2 = 0
                        End If
                        
                        If IsNumeric(mValores(I)(5)) Then
                            ValorCampoPromo3 = CDbl(mValores(I)(5))
                        Else
                            ValorCampoPromo3 = 0
                        End If
                        
                    Case 7 'Monto Descuento
                        
                        If IsNumeric(mValores(I)(1)) Then
                            ValorMxNLlevar = CDbl(mValores(I)(1))
                        Else
                            ValorMxNLlevar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(2)) Then
                            ValorMxNPagar = CDbl(mValores(I)(2))
                        Else
                            ValorMxNPagar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(3)) Then
                            ValorCampoPromo1 = CDbl(mValores(I)(3))
                        Else
                            ValorCampoPromo1 = 0
                        End If
                        
                End Select
                
            Case 16, 17, 18
                
                If IsNumeric(mValores(I)(1)) Then
                    ValorCampoPromo1 = CDbl(mValores(I)(1))
                Else
                    ValorCampoPromo1 = 0
                End If
                
                ValorCampoPromo2 = 0
                ValorCampoPromo3 = 0
                
        End Select
        
        txtCampoPromo.Text = ValorCampoPromo1
        txtCampo2.Text = ValorCampoPromo2
        txtCampo3.Text = ValorCampoPromo3
        
        If ChkCombinarProductos.Value = vbChecked Then
            '
        Else
            txtCantLlevar.Text = ValorMxNLlevar
            txtCantPagar.Text = ValorMxNPagar
        End If
        
        'Debug.Assert Not ValorCampoPromo1 = 0
        
        DoEvents
        
        pCargarLinea = False
        
        CmdIncluir_Click
        
        pCantRegistros = pCantRegistros + 1
        
        If pCargarLinea Then
            pCuentaExito = pCuentaExito + 1
        Else
            pCuentaFallo = pCuentaFallo + 1
            mInfoFallidos = mInfoFallidos & _
            "" & StellarMensaje(6144) & ": " & (I) & ", " & StellarMensaje(142) & ": " & CodigoEntrada & vbNewLine
        End If
        
    Next
    
    Dim mMsjEstatus As String
    
    mMsjEstatus = "" & StellarMensajeLocal(224) & ": (" & pCuentaExito & " " & _
    StellarMensajeLocal(169) & " " & pCantRegistros & ")" & vbNewLine
    
    If pCuentaFallo > 0 Then
        ActivarMensajeGrande 100
        mMsjEstatus = mMsjEstatus & _
        "" & StellarMensajeLocal(225) & ": (" & pCuentaFallo & ")" & vbNewLine
        mMsjEstatus = mMsjEstatus & _
        "" & StellarMensajeLocal(226) & ":" & vbNewLine & vbNewLine
        mMsjEstatus = mMsjEstatus & mInfoFallidos
    End If
    
    Mensaje True, mMsjEstatus
    
    CargarTemporal
    
Finally:
    
    pCargandoArchivo = False
    
    MousePointer = vbDefault
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    GoTo Finally
    
End Sub

Private Sub CmdBuscarProducto_Click()
    txtCodigoProducto_KeyDown vbKeyF2, 0
End Sub

Private Sub CmdConfigurarTeclado_Click()
    SeleccionarTexto CampoT
    TecladoAvanzado CampoT
End Sub

Private Sub CmdMxN_Click()
    If Not pCargandoArchivo Then
        Call MostrarFrameMxN(Not FrameSeccionMxN.Visible)
    End If
End Sub

Private Sub CmdMxNSalir_Click()
    Call MostrarFrameMxN(Not FrameSeccionMxN.Visible)
End Sub

Private Sub MostrarFrameMxN(Valor As Boolean)
    
    If Valor Then
        FrameSeccionMxN.Visible = True
        For I = 0 To FrameSeccionMxN.Width + (Frame1.Left * 2)
            FrameSeccionMxN.Left = FrameSeccionMxN.Left - 1
            If (FrameSeccionMxN.Left Mod 250) = 0 Then DoEvents
        Next I
        FrameSeccionMxN.ZOrder 0
        If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
        SeleccionarTexto txtCantLlevar
    Else
        For I = 0 To FrameSeccionMxN.Width + (Frame1.Left * 2)
            FrameSeccionMxN.Left = FrameSeccionMxN.Left + 1
            If (FrameSeccionMxN.Left Mod 250) = 0 Then DoEvents
        Next I
        FrameSeccionMxN.Visible = False
    End If
    
    'Call HabilitarVentana(Not Valor)
    
End Sub

Private Sub GrabarOtrosDatos()
    Select Case TipoPromo
    Case 8, 9, 10, 11, 12, 13, 14, 15
        If mVarEsPremio Then GoTo Aplica
    Case Else
Aplica:
        ExecuteSafeSQL "UPDATE TMP_PROMOCION SET " & vbNewLine & _
        "Considera_Impuesto = " & IIf(chkConsideraImpuesto.Value = vbChecked, 1, 0) & " " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
        FrmAppLink.CnADM
    End Select
End Sub

Private Sub CmdDenominacion_Click()

End Sub

Private Sub CmdMoneda_Click()
    CmdMoneda_KeyDown vbKeyF2, 0
End Sub

Private Sub CmdMoneda_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Not CmdMoneda.Visible Then Exit Sub
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Tecla_Pulsada = True
            
            Set Campo_Txt = txtMoneda
            Set Campo_Lbl = lblDescMoneda
            
            Call MAKE_VIEW("MA_MONEDAS_USO_EN_POS", "c_CodMoneda", "c_Descripcion", _
            "" & StellarMensaje(2855) & "", Me, "GENERICO", True, Campo_Txt, Campo_Lbl)
            
            MonedaTmp.BuscarMonedas , txtMoneda
            
            Campo_Lbl.Tag = MonedaTmp.SimMoneda
            
            If TipoPromo = 17 Then
                
                If ChkAplicarPorCriterio.Value = vbChecked Then
                    chkPagoMinimo.Caption = Replace(StellarMensajeLocal(239), "$(Curr)", "(" & MonedaTmp.SimMoneda & ")")
                Else
                    chkPagoMinimo.Caption = Replace(StellarMensajeLocal(239), "$(Curr)", "(" & MonedaDoc.SimMoneda & ")")
                End If
                
                ChkVueltoPromocion.Caption = StellarMensajeLocal(242) & " (" & MonedaTmp.SimMoneda & ")"
                
            End If
            
            Tecla_Pulsada = False
            
    End Select
    
End Sub

Private Sub CmdMonedaLimite_Click()
    CmdMonedaLimite_KeyDown vbKeyF2, 0
End Sub

Private Sub CmdMonedaLimite_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Not CmdMonedaLimite.Visible Then Exit Sub
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Tecla_Pulsada = True
            
            Set Campo_Txt = txtMonedaLimite
            Set Campo_Lbl = lblMonedaLimite
            
            Call MAKE_VIEW("MA_MONEDAS_USO_EN_POS", "c_CodMoneda", "c_Descripcion", _
            "" & StellarMensaje(2855) & "", Me, "GENERICO", True, Campo_Txt, Campo_Lbl)
            
            MonedaTmp.BuscarMonedas , txtMonedaLimite
            
            Campo_Lbl.Tag = MonedaTmp.SimMoneda
            
            ChkDctoLimiteFactura.Caption = StellarMensajeLocal(243) & " (" & MonedaTmp.SimMoneda & ")"
            
            If ChkDctoLimiteFactura.Value = vbChecked Then
                txtMontoLimiteDcto.Text = FormatNumber(0, 2)
                SafeFocus txtMontoLimiteDcto
            End If
            
            SeleccionarTexto txtMontoLimiteDcto
            
            Tecla_Pulsada = False
            
    End Select
    
End Sub

Private Sub CmdSiguiente_Click()
    
    If Not ((Grid.TextMatrix(1, ColRowID) <> Empty _
    And Grid.TextMatrix(1, ColTipoCondicion) <> Empty) _
    Or SVal(TxtNumPag) > 1) Then
        Mensaje True, StellarMensajeLocal(197)
        Exit Sub
    End If
    
    GrabarOtrosDatos
    
    Select Case TipoPromo
        Case 8, 9, 10, 11, 12, 13, 14, 15
            If mVarEsPremio Then
                GoTo Aplica
            Else
                Dim FrmTipoCarga As New frmAsistentePro_TipoMetodoCarga
                FrmTipoCarga.TipoSeleccion = True
                FrmTipoCarga.Show vbModal
            End If
        Case Else
Aplica:
            frmAsistentePro_CargarSucursales.Show vbModal
    End Select
    
End Sub

Private Sub departamento_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub departamento_GotFocus()
    Set CampoT = departamento
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Cancel = 0 Then
        Call WheelUnHook(Me.hWnd)
    End If
End Sub

Private Sub Grid_Scroll()
    'DoEvents
    'ScrollGrid.Value = Grid.Row
End Sub

Private Sub grupo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub grupo_GotFocus()
    Set CampoT = grupo
End Sub

Private Sub PROVEEDOR_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub PROVEEDOR_GotFocus()
    Set CampoT = PROVEEDOR
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
    Exit Sub
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Private Sub CmdAtras_Click()
    Unload Me
End Sub

Private Sub CmdCancelar_Click()
    If Mensaje(False, Replace(StellarMensaje(2620), "$(Line)", vbNewLine)) Then
        CancelarPromo_Salir = True
        Form_Activate
        Exit Sub
    End If
End Sub

Private Sub Grid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CmdDelete_Click
    End If
End Sub

Private Sub Grid_DragDrop(Source As Control, x As Single, y As Single)
    
    If Grid.RowHeightMin = 0 Then Exit Sub
    
    On Error Resume Next
    
    DragStop = y
    
    Drag = (DragStop - DragStart)
    
    If Abs(Drag / Grid.RowHeightMin) > 1 Then
        
        DragRows = (Drag / Grid.RowHeightMin)
        
        mDragEndRow = (Grid.TopRow + (-1 * DragRows))
        
        If mDragEndRow < Grid.FixedRows Then
            mDragEndRow = Grid.FixedRows
        ElseIf mDragEndRow > Grid.Rows Then
            mDragEndRow = Grid.Rows - 1
        End If
        
        Grid.TopRow = mDragEndRow
        ScrollGrid.Value = Grid.TopRow
        
    End If
    
    DragStart = 0
    
End Sub

Private Sub Grid_DragOver(Source As Control, x As Single, y As Single, State As Integer)
    
    On Error Resume Next
    
    If DragStart = 0 Then
        DragStart = y
    End If
    
End Sub

Private Sub bproveedor_Click()
    PROVEEDOR.SetFocus
    Call PROVEEDOR_KeyDown(vbKeyF2, 0)
End Sub

Private Sub ImgNext_Click()
    
    If SVal(txtPag.Text) <= SVal(TxtNumPag.Text) Then
        txtPag.Text = SVal(txtPag.Text) + 1
        If SVal(txtPag.Text) > SVal(TxtNumPag.Text) Then txtPag.Text = SVal(TxtNumPag.Text)
    Else
        txtPag.Text = SVal(TxtNumPag.Text)
    End If
    
    CargarTemporal
    
End Sub

Private Sub ImgPrev_Click()
    
    If SVal(txtPag.Text) >= 1 Then
        txtPag.Text = SVal(txtPag.Text) - 1
        If SVal(txtPag.Text) <= 1 Then txtPag.Text = 1
    Else
        txtPag.Text = 1
    End If
    
    CargarTemporal
    
End Sub

Private Sub PROVEEDOR_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            PROVEEDOR.Text = Empty
            Call MAKE_VIEW("MA_PROVEEDORES", "c_CodProveed", "c_Descripcio", _
            StellarMensaje(403), Me, "GENERICO", , PROVEEDOR, lbl_PROVEEDOR) '"P R O V E E D O R E S"
        Case Is = vbKeyDelete
            PROVEEDOR = Empty
            lbl_PROVEEDOR = Empty
    End Select
End Sub

Private Sub PROVEEDOR_LostFocus()
    
    Dim RsProveedor As New ADODB.Recordset, cProveedor As String
    
    If Trim(PROVEEDOR.Text) <> Empty Then
        RsProveedor.Open "SELECT * FROM MA_PROVEEDORES WHERE c_CodProveed = '" & _
        QuitarComillasSimples(PROVEEDOR.Text) & "' ", _
        FrmAppLink.CnADM, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsProveedor.EOF Then
            lbl_PROVEEDOR.Caption = RsProveedor!c_Descripcio
        Else
            lbl_PROVEEDOR.Caption = Empty
            PROVEEDOR.Text = Empty
            Mensaje True, StellarMensaje(16105) '"No existe proveedor con este c�digo.")
        End If
        
        RsProveedor.Close
    Else
        lbl_PROVEEDOR.Caption = Empty
        PROVEEDOR.Text = Empty
    End If
    
End Sub

Private Sub bdpto_Click()
    departamento.SetFocus
    Call departamento_KeyDown(vbKeyF2, 0)
End Sub

Private Sub departamento_KeyDown(KeyCode As Integer, Shift As Integer)
    Set Forma = Me
    Select Case KeyCode
        Case Is = vbKeyF2
            Tabla = "MA_DEPARTAMENTOS"
            Titulo = UCase(Stellar_Mensaje(361)) '"D E P A R T A M E N T O S"
            Call DGS(Forma, Titulo, Tabla)
        
        Case Is = vbKeyDelete
            departamento.Tag = Empty
            departamento.Text = Empty
            LBL_DEPARTAMENTO.Caption = Empty
            grupo.Tag = Empty
            grupo.Text = Empty
            LBL_GRUPO.Caption = Empty
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
    End Select
End Sub

Private Sub departamento_LostFocus()
    If departamento.Text <> Empty Then
        Set Forma = Me
        Call Carga_DGS(departamento, "MA_DEPARTAMENTOS", 0)
    Else
        Call departamento_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub bgrupo_Click()
    grupo.SetFocus
    Call grupo_KeyDown(vbKeyF2, 0)
End Sub

Private Sub grupo_KeyDown(KeyCode As Integer, Shift As Integer)
    Set Forma = Me
    Select Case KeyCode
        Case Is = vbKeyF2
            Tabla = "MA_GRUPOS"
            Titulo = UCase(Stellar_Mensaje(362)) 'G R U P O S"
            Call DGS(Forma, Titulo, Tabla)
        
        Case Is = vbKeyDelete
            grupo.Tag = Empty
            grupo.Text = Empty
            LBL_GRUPO.Caption = Empty
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
    End Select
End Sub

Private Sub grupo_LostFocus()
    If grupo.Text <> Empty Then
        Set Forma = Me
        Call Carga_DGS(grupo, "MA_GRUPOS", 1)
    Else
        Call grupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub bsubgrupo_Click()
    subgrupo.SetFocus
    Call subgrupo_KeyDown(vbKeyF2, 0)
End Sub

Private Sub subgrupo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub subgrupo_GotFocus()
    Set CampoT = subgrupo
End Sub

Private Sub subgrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    Set Forma = Me
    Select Case KeyCode
        Case Is = vbKeyF2
            Tabla = "MA_SUBGRUPOS"
            Titulo = UCase(Stellar_Mensaje(363)) 'S U B - G R U P O S"
            Call DGS(Forma, Titulo, Tabla)
        
        Case Is = vbKeyDelete
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
    End Select
End Sub

Private Sub subgrupo_LostFocus()
    If subgrupo.Text <> Empty Then
        Set Forma = Me
        Call Carga_DGS(subgrupo, "MA_SUBGRUPOS", 2)
    Else
        Call subgrupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub CargarGrid()
    
    With Grid
        
        .Rows = 1
        .Row = 0
        .Clear
        .Cols = ColCount
        .RowHeightMin = 600
        
        .Col = ColRowID
        .TextMatrix(0, .Col) = "Ln" '"RowID"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColTipoCondicion
        .TextMatrix(0, .Col) = "TipoCondicion"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColCodProd
        .TextMatrix(0, .Col) = StellarMensaje(142) 'C�digo
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesProd
        .TextMatrix(0, .Col) = StellarMensaje(5010) '"Producto"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColMarca
        .TextMatrix(0, .Col) = StellarMensaje(3022) '"Marca"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColModelo
        .TextMatrix(0, .Col) = StellarMensaje(222) '"Modelo"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColValorPromo1
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 1, 5, 9, 11
                .TextMatrix(0, .Col) = StellarMensajeLocal(104) '"Precio Oferta"
            Case 2, 6, 10, 14
                .TextMatrix(0, .Col) = StellarMensajeLocal(105) '"% Descuento"
            Case 3, 7, 11, 15
                .TextMatrix(0, .Col) = StellarMensajeLocal(106) '"Descuento Fijo"
            Case 16, 17, 18
                .TextMatrix(0, .Col) = StellarMensajeLocal(105) '"% Descuento"
            Case Else 'NotImplementedYet
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .Col = ColValorPromo2
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 2, 6, 10, 14
                .TextMatrix(0, .Col) = StellarMensajeLocal(120) '"% Desc 2"
            Case Else 'NotImplementedYet
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .Col = ColValorPromo3
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 2, 6, 10, 14
                .TextMatrix(0, .Col) = StellarMensajeLocal(121) '"% Desc 3"
            Case Else 'NotImplementedYet
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .Col = ColValorMxN
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 4, 5, 6, 7
                .TextMatrix(0, .Col) = "MxN"
            Case 8, 9, 10, 11, 12, 13, 14, 15
                .TextMatrix(0, .Col) = "Req."
            Case Else
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .DragMode = 0
        .AllowUserResizing = flexResizeColumns
        '.AllowUserResizing = flexResizeNone
        .ColWidth(ColRowID) = 375
        .ColWidth(ColTipoCondicion) = 0
        .ColWidth(ColCodProd) = 1335
        .ColWidth(ColDesProd) = 4235
        .ColWidth(ColMarca) = 1445
        .ColWidth(ColModelo) = 1445
        
        Select Case TipoPromo
            Case 2, 6, 10, 14
                .ColWidth(ColValorPromo1) = 1300
                .ColWidth(ColValorPromo2) = 750
                .ColWidth(ColValorPromo3) = 750
            Case Else
                .ColWidth(ColValorPromo1) = 2800
                .ColWidth(ColValorPromo2) = 0
                .ColWidth(ColValorPromo3) = 0
        End Select
        
        Select Case TipoPromo
            Case 4, 8, 12
                .ColWidth(ColValorMxN) = 1500
                .ColWidth(ColDesProd) = (.ColWidth(ColDesProd) - .ColWidth(ColValorMxN))
                .ColWidth(ColDesProd) = (.ColWidth(ColDesProd) + .ColWidth(ColValorPromo1))
                .ColWidth(ColValorPromo1) = 0
            Case 5, 6, 7, 9, 10, 11, 13, 14, 15
                .ColWidth(ColValorMxN) = 800
                .ColWidth(ColDesProd) = (.ColWidth(ColDesProd) - .ColWidth(ColValorMxN))
            Case Else
                .ColWidth(ColValorMxN) = 0
        End Select
        
        AnchoCampoScroll = .ColWidth(ColDesProd)
        
    End With
    
End Sub

Private Sub Form_Load()
    
    If CancelarPromo_Salir Then Exit Sub
    
    lblTitulo.Caption = StellarMensajeLocal(125)
    lblCriterios.Caption = StellarMensaje(10164) 'criterios
    
    CmdAtras.Caption = StellarMensajeLocal(12)
    CmdSiguiente.Caption = StellarMensajeLocal(10)
    CmdCancelar.Caption = StellarMensajeLocal(11)
    
    lblMoneda.Caption = Replace(StellarMensaje(134), ":", Empty) & "(*)" 'Moneda
    lblDenominacion.Caption = StellarMensaje(2008) 'Denominacion
    
    If TipoPromo = 16 Or TipoPromo = 18 Then
        chkPagoMinimo.Caption = StellarMensajeLocal(238)
        ChkAplicarPorCriterio.Caption = StellarMensajeLocal(240)
    ElseIf TipoPromo = 17 Then
        chkPagoMinimo.Caption = Replace(StellarMensajeLocal(239), "$(Curr)", Empty)
        ChkAplicarPorCriterio.Caption = StellarMensajeLocal(241)
    End If
    
    ChkVueltoPromocion.Caption = StellarMensajeLocal(242)
    
    Set MonedaTmp = FrmAppLink.ClaseMonedaTemp
    
    txtMonedaLimite.Text = MonedaDoc.CodMoneda 'FrmAppLink.CodMonedaPref
    lblMonedaLimite.Caption = MonedaDoc.DesMoneda 'FrmAppLink.DesMonedaPref
    lblMonedaLimite.Tag = MonedaDoc.SimMoneda 'FrmAppLink.SimMonedaPref
    
    ChkDctoLimiteFactura.Caption = StellarMensajeLocal(243) & " (" & MonedaDoc.SimMoneda & ")"
    
    lblDescBtnIncluir.Caption = StellarMensajeLocal(175) '"Incluir
    lblDescBtnExcluir.Caption = StellarMensajeLocal(176) '"Excluir
    lblDescBtnBorrar.Caption = StellarMensajeLocal(177) '"Eliminar Fila
    
    CargarGrid
    CargarTemporal
    
    Call AjustarPantalla(Me)
    
    If Not FrmAppLink.DebugVBApp Then
        Call WheelHook(Me.hWnd)
    End If
    
End Sub

Private Sub Form_Activate()
    If CancelarPromo_Salir Or PromocionCreada Then
        Unload Me
    End If
End Sub

Private Sub Header_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        Case Is = "AGREGAR"
            'Call Form_KeyDown(vbKeyF3, 0)
        Case Is = "BUSCAR"
            'Call Form_KeyDown(vbKeyF2, 0)
        
        Case Is = "MODIFICAR"
            'Call Form_KeyDown(vbKeyF5, 0)
        
        Case Is = "CANCELAR"
            'Call Form_KeyDown(vbKeyF7, 0)
        
        Case Is = "BORRAR"
            ''Call Form_KeyDown(vbKeyF6, 0)
        
        Case Is = "GRABAR"
            'Call Grabar_Registro
            'Call Form_KeyDown(vbKeyF4, 0)
        
        Case Is = "IMPRIMIR"
            'Call imprimir_registro
        
        Case Is = "SALIR"
            Unload Me
            'Call Form_KeyDown(vbKeyF12, 0)
        
        Case Is = "AYUDA"
                
    End Select
    
End Sub

Private Sub AyudaPatronLike()
    '"Cuadro Vac�o => No tomar en cuenta para la b�squeda" & GetLines & _
    "*Criterio* => Buscar que contenga la palabra ""Criterio"" " & GetLines & _
    "Criterio* => Buscar lo que empieze por la palabra ""Criterio"" " & GetLines & _
    "*Criterio => Buscar lo que termine con la palabra ""Criterio"" " & GetLines & _
    "[] => Buscar los que esten vac�os" & GetLines
    ShowTooltip Replace(StellarMensajeLocal(122), "$(Line)", vbNewLine), _
    TxtMarca.Width * 3, 7500, CampoT, 1
End Sub

Private Sub txtCampo2_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCampo3_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCampoPromo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCampoPromo_GotFocus()
    Set CampoT = txtCampoPromo
End Sub

Private Sub txtCampo2_GotFocus()
    Set CampoT = txtCampo2
End Sub

Private Sub txtCampo3_GotFocus()
    Set CampoT = txtCampo3
End Sub

Private Sub txtCantLlevar_Change()
    Select Case TipoPromo
        Case 5, 6, 7
            txtCantPagar.Text = txtCantLlevar.Text
    End Select
End Sub

Private Sub txtCantLlevar_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCantLlevar_GotFocus()
    Set CampoT = txtCantLlevar
End Sub

Private Sub txtCantPagar_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCantPagar_GotFocus()
    Set CampoT = txtCantPagar
End Sub

Private Sub txtCodigoProducto_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCodigoProducto_GotFocus()
    Set CampoT = txtCodigoProducto
End Sub

Private Sub txtCodigoProducto_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            'Call MAKE_VIEW("MA_PRODUCTOS", "c_Codigo", "c_Descri", _
            StellarMensaje(254), Me, "GENERICO", , txtCodigoProducto, lblDescripcionProducto) '"P R O D U C T O S"
            mArrProducto = BuscarInfoProducto_Basica(FrmAppLink.CnADM, , , , , VistaPrecioCliente)
            
            If Not IsEmpty(mArrProducto) Then
                txtCodigoProducto.Text = mArrProducto(0)
                txtCodigoProducto_LostFocus
            End If
        Case Is = vbKeyReturn
            txtCodigoProducto_LostFocus
    End Select
End Sub

Private Sub txtCodigoProducto_LostFocus()
    
    On Error GoTo Error
    
    Dim RsProducto As New ADODB.Recordset
    
    If Trim(txtCodigoProducto.Text) <> Empty Then
    
        Call Apertura_Recordset(RsProducto)
        
        RsProducto.Open "SELECT * FROM MA_PRODUCTOS " & _
        "WHERE c_Codigo IN ( " & _
        "SELECT c_CodNasa FROM MA_CODIGOS " & _
        "WHERE c_Codigo = '" & QuitarComillasSimples(txtCodigoProducto.Text) & "') ", _
        FrmAppLink.CnADM, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsProducto.EOF Then
            txtCodigoProducto.Text = RsProducto!c_Codigo
            lblDescripcionProducto.Caption = RsProducto!c_Descri
            If Not pCargandoArchivo Then
                If ChkExcluirInactivos.Value = vbChecked And RsProducto!n_Activo = 0 Then
                    'Mensaje True, "El producto [" & txtCodigoProducto & "][" & lblDescripcionProducto & "] est� inactivo."
                    Mensaje True, Replace(StellarMensajeLocal(204), "$(Info)", " [" & txtCodigoProducto & "][" & lblDescripcionProducto & "] ")
                    GoTo Limpiar1
                End If
            Else
                If ChkExcluirInactivos.Value = vbChecked And RsProducto!n_Activo = 0 Then
                    GoTo Limpiar1
                End If
            End If
            If TipoPromo = 1 Or TipoPromo = 5 Or (TipoPromo = 9 And mVarEsPremio) Or (TipoPromo = 13 And mVarEsPremio) Then
                txtCampo2.Visible = True
                txtCampo2.Locked = True
                txtCampo2.Width = 2250
                lblCampo2.Visible = True
                lblCampo2.Caption = StellarMensajeLocal(201) & ": "  '" Prc. Actual Ref.: "
                txtCampo2.Text = FormatNumber(RsProducto!n_Precio1 * (1 + IIf(chkConsideraImpuesto = vbChecked, ((RsProducto!n_Impuesto1 + RsProducto!n_Impuesto2 + RsProducto!n_Impuesto3) / 100), 0)))
            End If
        Else
            If Not pCargandoArchivo Then
                Call Mensaje(True, Stellar_Mensaje(16285)) '"No existe un producto con este c�digo.")
            End If
Limpiar1:
            lblDescripcionProducto.Caption = Empty
            txtCodigoProducto.Text = Empty
        End If
        
        RsProducto.Close
        
    Else
        
        lblDescripcionProducto.Caption = Empty
        txtCodigoProducto.Text = Empty
        
    End If
    
    Exit Sub
    
Error:
    
End Sub

Private Sub TxtMarca_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtMarca_GotFocus()
    Set CampoT = TxtMarca
End Sub

Private Sub TxtModelo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtModelo_GotFocus()
    Set CampoT = TxtModelo
End Sub

Private Sub Tooltip1_Click()
    If PuedeObtenerFoco(TxtMarca) Then TxtMarca.SetFocus
    AyudaPatronLike
End Sub

Private Sub Tooltip2_Click()
    If PuedeObtenerFoco(TxtModelo) Then TxtModelo.SetFocus
    AyudaPatronLike
End Sub

Private Sub InsertarCondicion(ByVal pTipo As Integer)
    
    On Error GoTo Error
    
    Dim RsCondicion As New ADODB.Recordset
    Dim RsValor As New ADODB.Recordset
    Dim SQL As String
    Dim SqlV As String
    
    Dim CampoPromo1 As Double
    Dim CampoPromo2 As Double
    Dim CampoPromo3 As Double
    
    If Trim(PROVEEDOR.Text) <> Empty Or Trim(departamento.Text) <> Empty Or _
    Trim(grupo.Text) <> Empty Or Trim(subgrupo.Text) <> Empty Or _
    Trim(TxtMarca.Text) <> Empty Or Trim(TxtModelo.Text) <> Empty _
    Or Trim(txtCodigoProducto.Text) <> Empty _
    Or ChkSeleccionarTodo.Value = vbChecked Then
        
        If ((pTipo = 1 Or pTipo = 3) And IsNumeric(txtCampoPromo.Text)) Or (pTipo = 2 Or pTipo = 4) Then
            
            If pTipo = 2 And Not IsNumeric(txtCampoPromo.Text) Then txtCampoPromo.Text = FormatNumber(0)
            
            CampoPromo1 = CDbl(txtCampoPromo.Text)
            
            If Not IsNumeric(txtCantLlevar.Text) Then txtCantLlevar.Text = 0
            If Not IsNumeric(txtCantPagar.Text) Then txtCantPagar.Text = 0
            
            Dim mValidarObligatorios As Boolean
            
            Select Case TipoPromo
                Case 8, 9, 10, 11, 12, 13, 14, 15
                    If mVarEsPremio Then
                        mValidarObligatorios = ((CampoPromo1 > 0) Or pTipo = 2 Or TipoPromo = 8 Or TipoPromo = 12)
                    Else
                        mValidarObligatorios = True
                    End If
                Case Else
                    mValidarObligatorios = ((CampoPromo1 > 0) Or pTipo = 2 Or TipoPromo = 4)
            End Select
            
            Dim MensajeValidacion As String
            
            Select Case TipoPromo
                Case 4
                    If mValidarObligatorios Then
                        mValidarObligatorios = (CDbl(txtCantLlevar.Text) > 0 And CDbl(txtCantPagar.Text) > 0 And _
                        CDbl(txtCantLlevar.Text) > CDbl(txtCantPagar.Text)) Or pTipo = 2
                        If Not mValidarObligatorios And Not FrameSeccionMxN.Visible Then
                            CmdMxN_Click
                        End If
                        If Not mValidarObligatorios Then
                            MensajeValidacion = StellarMensajeLocal(172)
                            If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
                        End If
                    Else
                        MensajeValidacion = StellarMensajeLocal(123)
                        If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                    End If
                Case 5, 6, 7
                    If mValidarObligatorios Then
                        mValidarObligatorios = (CDbl(txtCantLlevar.Text) > 0 And CDbl(txtCantPagar.Text) > 0 And _
                        CDbl(txtCantLlevar.Text) >= CDbl(txtCantPagar.Text)) Or pTipo = 2
                        If Not mValidarObligatorios And Not FrameSeccionMxN.Visible Then
                            CmdMxN_Click
                        End If
                        If Not mValidarObligatorios Then
                            MensajeValidacion = StellarMensajeLocal(173)
                            If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
                        End If
                    Else
                        MensajeValidacion = StellarMensajeLocal(123)
                        If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                    End If
                Case 8, 9, 10, 11, 12, 13, 14, 15
                    If mValidarObligatorios Then
                        mValidarObligatorios = (CDbl(txtCantLlevar.Text) > 0) Or pTipo = 2 Or pTipo = 4
                        If Not mValidarObligatorios And Not FrameSeccionMxN.Visible Then
                            CmdMxN_Click
                        End If
                        If Not mValidarObligatorios Then
                            MensajeValidacion = StellarMensajeLocal(172)
                            If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
                        End If
                    Else
                        MensajeValidacion = StellarMensajeLocal(123)
                        If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                    End If
                Case Else
                    If Not mValidarObligatorios Then
                        MensajeValidacion = StellarMensajeLocal(123)
                        If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                    End If
            End Select
            
            If mValidarObligatorios Then
                
                If Not IsNumeric(txtCampo2.Text) Then txtCampo2.Text = 0
                If Not IsNumeric(txtCampo3.Text) Then txtCampo3.Text = 0
                
                If Not CDbl(txtCampo2.Text) >= 0 Then
                    If Not pCargandoArchivo Then
                        Mensaje True, StellarMensajeLocal(123) '"Los datos deben ser num�ricos y positivos."
                        If PuedeObtenerFoco(txtCampo2) Then txtCampo2.SetFocus
                        ShowTooltip "<--", 500, 800, txtCampo2, 3, &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
                    End If
                    Exit Sub
                End If
                
                If Not CDbl(txtCampo3.Text) >= 0 Then
                    If Not pCargandoArchivo Then
                        Mensaje True, StellarMensajeLocal(123) '"Los datos deben ser num�ricos y positivos."
                        If PuedeObtenerFoco(txtCampo3) Then txtCampo3.SetFocus
                        ShowTooltip "<--", 500, 800, txtCampo3, 3, &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
                    End If
                    Exit Sub
                End If
                
                CampoPromo2 = CDbl(txtCampo2.Text)
                CampoPromo3 = CDbl(txtCampo3.Text)
                
                Dim mRsDatos As ADODB.Recordset
                Set mRsDatos = New ADODB.Recordset
                Apertura_RecordsetC mRsDatos
                
                Dim mCriterio As String
                
                If ChkSeleccionarTodo.Value = vbChecked Then
                    mCriterio = Empty
                ElseIf Trim(txtCodigoProducto.Text) <> Empty Then
                    mCriterio = "AND PRO.c_Codigo = '" & QuitarComillasSimples(txtCodigoProducto.Text) & "' "
                Else
                    mCriterio = _
                    IIf(Trim(PROVEEDOR.Text) <> Empty, _
                    "AND PRO.c_Codigo IN (SELECT c_Codigo FROM MA_PRODxPROV WHERE c_CodProvee = " & _
                    "'" & QuitarComillasSimples(PROVEEDOR.Text) & "') ", Empty) & _
                    IIf(Trim(departamento.Text) <> Empty, _
                    "AND PRO.c_Departamento = '" & QuitarComillasSimples(departamento.Text) & "' ", Empty) & _
                    IIf(Trim(grupo.Text) <> Empty, _
                    "AND PRO.c_Grupo = '" & QuitarComillasSimples(grupo.Text) & "' ", Empty) & _
                    IIf(Trim(subgrupo.Text) <> Empty, _
                    "AND PRO.c_Subgrupo = '" & QuitarComillasSimples(subgrupo.Text) & "' ", Empty) & _
                    IIf(Trim(TxtMarca.Text) <> Empty, _
                    IIf(TxtMarca.Text = "[]", "AND PRO.c_Marca = ''", _
                    "AND PRO.c_Marca LIKE '" & QuitarComillasSimples(TxtMarca.Text) & "' "), Empty) & _
                    IIf(Trim(TxtModelo.Text) <> Empty, _
                    IIf(TxtModelo.Text = "[]", "AND PRO.c_Modelo = ''", _
                    "AND PRO.c_Modelo LIKE '" & QuitarComillasSimples(TxtModelo.Text) & "' "), Empty) & _
                    IIf(ChkExcluirInactivos.Value = vbChecked, "AND PRO.n_Activo = 1 ", Empty)
                End If
                
                SQL = "SELECT * FROM MA_PRODUCTOS PRO " & _
                "WHERE 1 = 1 " & mCriterio
                
                mRsDatos.Open SQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If mRsDatos.RecordCount <= 0 Then
                    If Not pCargandoArchivo Then
                        Call Mensaje(True, StellarMensaje(16441))
                    End If
                    Exit Sub
                End If
                
                If pTipo = 1 Or pTipo = 3 Then
                    
                    SQL = _
                    "INSERT INTO TMP_PROMOCION_CONDICION_VALORES" & GetLines & _
                    "(CodUsuario, Tipo_Condicion, Cod_Promocion, Marca, Modelo, " & GetLines & _
                    "Cod_Producto, Des_Producto)" & _
                    "SELECT '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' AS CodUsuario, " & _
                    pTipo & " AS Tipo_Condicion, '" & QuitarComillasSimples(mCodPromo_Actual) & "' AS Cod_Promocion, " & _
                    vbNewLine & _
                    "PRO.c_Marca AS Marca, PRO.c_Modelo AS Modelo," & GetLines & _
                    "PRO.c_Codigo AS Cod_Producto, PRO.c_Descri AS Des_Producto" & GetLines & _
                    "FROM MA_PRODUCTOS PRO LEFT JOIN TMP_PROMOCION_CONDICION_VALORES COND" & GetLines & _
                    "ON PRO.c_Codigo = COND.Cod_Producto" & GetLines & _
                    "AND COND.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
                    "AND COND.CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "'" & GetLines & _
                    "WHERE 1 = 1" & GetLines & _
                    mCriterio & GetLines & _
                    "AND COND.Cod_Producto IS NULL"
                    
                    FrmAppLink.CnADM.Execute SQL, RecordsAffected
                    
                    SQL = "UPDATE TMP_PROMOCION_CONDICION_VALORES" & GetLines
                    
                    Select Case TipoPromo
                        Case 1
                            SQL = SQL & "SET Precio_Oferta = (" & CampoPromo1 & ")" & GetLines
                        Case 2
                            SQL = SQL & "SET Porcentaje_Descuento1 = (" & CampoPromo1 & "), " & GetLines & _
                            "Porcentaje_Descuento2 = (" & CampoPromo2 & ")," & GetLines & _
                            "Porcentaje_Descuento3 = (" & CampoPromo3 & ") " & GetLines
                        Case 3
                            SQL = SQL & "SET Monto_Descuento = (" & CampoPromo1 & ")" & GetLines
                        Case 4
                            SQL = SQL & "SET " & GetLines & _
                            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), " & GetLines & _
                            "Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & GetLines
                        Case 5
                            SQL = SQL & "SET Precio_Oferta = (" & CampoPromo1 & "), " & GetLines & _
                            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), " & GetLines & _
                            "Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & GetLines
                        Case 6
                            SQL = SQL & "SET Porcentaje_Descuento1 = (" & CampoPromo1 & "), " & GetLines & _
                            "Porcentaje_Descuento2 = (" & CampoPromo2 & ")," & GetLines & _
                            "Porcentaje_Descuento3 = (" & CampoPromo3 & "), " & GetLines & _
                            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), " & GetLines & _
                            "Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & GetLines
                        Case 7
                            SQL = SQL & "SET Monto_Descuento = (" & CampoPromo1 & "), " & GetLines & _
                            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), " & GetLines & _
                            "Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & GetLines
                        Case 8, 9, 10, 11, 12, 13, 14, 15
                            If mVarEsPremio Then
                                SQL = SQL & "SET " & GetLines & _
                                "Cantidad_Productos_Pagar = (" & CDbl(txtCantLlevar.Text) & ")"
                            Else
                                SQL = SQL & "SET " & GetLines & _
                                "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & ")"
                            End If
                            If pTipo = 3 Then
                                If TipoPromo = 9 Or TipoPromo = 13 Then
                                    SQL = SQL & ", " & vbNewLine & _
                                    "Precio_Oferta = (" & CampoPromo1 & ")"
                                ElseIf TipoPromo = 10 Or TipoPromo = 14 Then
                                    SQL = SQL & ", " & vbNewLine & _
                                    "Porcentaje_Descuento1 = (" & CampoPromo1 & "), " & vbNewLine & _
                                    "Porcentaje_Descuento2 = (" & CampoPromo2 & ")," & vbNewLine & _
                                    "Porcentaje_Descuento3 = (" & CampoPromo3 & ")"
                                ElseIf TipoPromo = 11 Or TipoPromo = 15 Then
                                    SQL = SQL & ", " & vbNewLine & _
                                    "Monto_Descuento = (" & CampoPromo1 & ")"
                                End If
                            End If
                        Case 16, 17, 18
                            SQL = SQL & "SET Porcentaje_Descuento1 = (" & CampoPromo1 & ")" & GetLines
                        Case Else 'NotImplementedYet
                    End Select
                    
                    SQL = SQL & _
                    "FROM MA_PRODUCTOS PRO INNER JOIN TMP_PROMOCION_CONDICION_VALORES COND" & GetLines & _
                    "ON PRO.c_Codigo = COND.Cod_Producto" & GetLines & _
                    "AND COND.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
                    "AND COND.CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "'" & GetLines & _
                    "WHERE 1 = 1" & GetLines & _
                    mCriterio & GetLines
                    
                    FrmAppLink.CnADM.Execute SQL, RecordsAffected
                    
                    If Not pCargandoArchivo Then
                        ShowTooltip StellarMensajeLocal(185), 700, 1250, CmdIncluir, 0, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "7", True)
                    End If
                    
                    pCargarLinea = True
                    
                ElseIf pTipo = 2 Or pTipo = 4 Then
                    
                    SQL = "DELETE TMP_PROMOCION_CONDICION_VALORES" & GetLines
                    
                    SQL = SQL & _
                    "FROM MA_PRODUCTOS PRO INNER JOIN TMP_PROMOCION_CONDICION_VALORES COND" & GetLines & _
                    "ON PRO.c_Codigo = COND.Cod_Producto" & GetLines & _
                    "AND COND.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
                    "AND COND.CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "'" & GetLines & _
                    "WHERE 1 = 1" & GetLines & _
                    mCriterio & GetLines
                    
                    FrmAppLink.CnADM.Execute SQL, RecordsAffected
                    
                    ShowTooltip StellarMensajeLocal(186), 700, 1250, CmdExcluir, 0, _
                    &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "7", True)
                    
                End If
                
                ResetearCriterios
                
            Else
                If Not pCargandoArchivo Then
                    Mensaje True, MensajeValidacion
                    If Screen.ActiveControl Is txtCampoPromo Then
                        ShowTooltip "<--", 500, 800, txtCampoPromo, 3, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
                    ElseIf Screen.ActiveControl Is txtCantLlevar Then
                        ShowTooltip "-->", 500, 2000, txtCantLlevar, 2, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
                    End If
                End If
                Exit Sub
            End If
            
        Else
            If Not pCargandoArchivo Then
                Mensaje True, StellarMensajeLocal(123) '"Los datos deben ser num�ricos y positivos."
                If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                ShowTooltip "<--", 500, 800, txtCampoPromo, 3, _
                &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
            End If
            Exit Sub
        End If
        
    Else
        If Not pCargandoArchivo Then
            Mensaje True, StellarMensaje(16404) '"Debe seleccionar al menos un criterio"
        End If
    End If
    
    Exit Sub
    
Error:

    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    If Not pCargandoArchivo Then
        MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    End If
    
End Sub

Private Sub CmdIncluir_Click()
    'If mVarEsPremio Then
        'InsertarCondicion 3
    'Else
        InsertarCondicion 5
    'End If
    If Not pCargandoArchivo Then
        CargarTemporal
    End If
End Sub

Private Sub CmdExcluir_Click()
    'If mVarEsPremio Then
        'InsertarCondicion 4
    'Else
        'InsertarCondicion 2
    'End If
    'CargarTemporal
End Sub

Private Sub CargarTemporal()
    
    On Error GoTo Error
    
    'CargarGrid
    
    Dim LastRow As Integer, LastCtl As Object
    Dim NumRows As Double, NumPag As Double
    Dim RsCondicion As ADODB.Recordset
    Set RsCondicion = New ADODB.Recordset
    
    TmpConsideraImpuesto = CBool(BuscarValorBD("Considera_Impuesto", _
    "SELECT Considera_Impuesto FROM TMP_PROMOCION " & vbNewLine & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
    "0", FrmAppLink.CnADM))
    
    chkConsideraImpuesto.Value = IIf(TmpConsideraImpuesto, vbChecked, vbUnchecked)
    
    TmpCombinarProductos = Split(BuscarValorBD("CombinarProductos", _
    "SELECT (CAST(CASE WHEN Cantidad_Productos_Requerir > 0 AND Cantidad_Productos_Pagar > 0 " & _
    "THEN 1 ELSE 0 END AS NVARCHAR(MAX)) + '|' + CAST(Cantidad_Productos_Requerir AS NVARCHAR(MAX)) " & _
    "+ '|' + CAST(Cantidad_Productos_Pagar AS NVARCHAR(MAX))) AS CombinarProductos FROM TMP_PROMOCION " & vbNewLine & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
    "0|0|0", FrmAppLink.CnADM), "|")
    
    If CBool(Val(TmpCombinarProductos(0))) Then
        txtCantLlevar.Text = TmpCombinarProductos(1)
        txtCantPagar.Text = TmpCombinarProductos(2)
        ChkCombinarProductos.Value = vbChecked
    Else
        ChkCombinarProductos.Value = vbUnchecked
    End If
    
    Dim mSQL As String
    
    mSQL = "SELECT isNULL(COUNT(ID), 0) AS NumReg " & GetLines & _
    "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & GetLines & _
    "AND Cod_Producto <> '' " & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
    IIf(mVarEsPremio, "AND Tipo_Condicion IN (3, 4)", "AND Tipo_Condicion IN (1, 2)")
    
    Set RsCondicion = FrmAppLink.CnADM.Execute(mSQL)
    
    With RsCondicion
        
        If Not .EOF Then
            
            NumRows = !NumReg
            
            If NumRows > 0 Then
                
                NumPag = Round(NumRows / MaxGridRowsxPag, 8)
                If (NumPag - Fix(NumPag)) <> 0 Then NumPag = Fix(NumPag + 1)
                
                TxtNumPag.Text = NumPag
                
                txtPag.Text = Fix(SVal(txtPag))
                
                If SVal(txtPag.Text) >= 1 And SVal(txtPag.Text) <= NumPag Then
                    txtPag.Text = Fix(SVal(txtPag))
                Else
                    txtPag.Text = 1
                End If
                
                mSQL = "WITH AllRows AS (" & GetLines & _
                "SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowID, *" & GetLines & _
                "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
                "WHERE CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
                "AND Cod_Producto <> '' " & _
                "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
                IIf(mVarEsPremio, "AND Tipo_Condicion IN (3, 4)", "AND Tipo_Condicion IN (1, 2)") & _
                ")" & GetLines & _
                "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                "((" & txtPag.Text & " - 1) * (" & MaxGridRowsxPag & ") + 1) AND " & _
                "(" & txtPag.Text & " * " & MaxGridRowsxPag & ")" & GetLines
                
                RsCondicion.Close
                
                Set RsCondicion = New Recordset
                Apertura_RecordsetC RsCondicion
                
                .Open mSQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If Not .EOF Then
                    
                    .Sort = "RowID"
                    
                    Dim I As Integer
                    
                    LastRow = Grid.Row
                    Set LastCtl = Screen.ActiveControl
                    
                    Grid.Visible = False
                    
                    Grid.Rows = 1
                    
                    ScrollGrid.Min = 0
                    ScrollGrid.Max = .RecordCount
                    ScrollGrid.Value = ScrollGrid.Min
                    
                    While Not .EOF
                    
                        Grid.Rows = Grid.Rows + 1
                        I = I + 1
                        
                        Grid.TextMatrix(I, ColRowID) = I
                        Grid.TextMatrix(I, ColTipoCondicion) = !Tipo_Condicion
                        Grid.TextMatrix(I, ColMarca) = !Marca
                        Grid.TextMatrix(I, ColModelo) = !Modelo
                        Grid.TextMatrix(I, ColCodProd) = !Cod_Producto
                        Grid.TextMatrix(I, ColDesProd) = !Des_Producto
                        
                        Select Case TipoPromo
                            Case 1
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Precio_Oferta)
                            Case 2
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                                Grid.TextMatrix(I, ColValorPromo2) = FormatNumber(!Porcentaje_Descuento2)
                                Grid.TextMatrix(I, ColValorPromo3) = FormatNumber(!Porcentaje_Descuento3)
                            Case 3
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Monto_Descuento)
                            Case 4
                                Grid.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Requerir & "x" & !Cantidad_Productos_Pagar)
                            Case 5
                                Grid.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar & " " & StellarMensajeLocal(169) & " " & !Cantidad_Productos_Requerir)
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Precio_Oferta)
                            Case 6
                                Grid.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar & " " & StellarMensajeLocal(169) & " " & !Cantidad_Productos_Requerir)
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                                Grid.TextMatrix(I, ColValorPromo2) = FormatNumber(!Porcentaje_Descuento2)
                                Grid.TextMatrix(I, ColValorPromo3) = FormatNumber(!Porcentaje_Descuento3)
                            Case 7
                                Grid.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar & " " & StellarMensajeLocal(169) & " " & !Cantidad_Productos_Requerir)
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Monto_Descuento)
                            Case 8, 12
                                If mVarEsPremio Then
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Pagar
                                Else
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Requerir
                                End If
                            Case 9, 13
                                If mVarEsPremio Then
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Pagar
                                    Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Precio_Oferta)
                                Else
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Requerir
                                End If
                            Case 10, 14
                                If mVarEsPremio Then
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Pagar
                                    Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                                    Grid.TextMatrix(I, ColValorPromo2) = FormatNumber(!Porcentaje_Descuento2)
                                    Grid.TextMatrix(I, ColValorPromo3) = FormatNumber(!Porcentaje_Descuento3)
                                Else
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Requerir
                                End If
                            Case 11, 15
                                If mVarEsPremio Then
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Pagar
                                    Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Monto_Descuento)
                                Else
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Requerir
                                End If
                            Case 16, 17, 18
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                            Case Else
                                'NotImplementedYet
                        End Select
                        
                        .MoveNext
                        
                    Wend
                    
                    Grid.Visible = True
                    
                    If LastRow >= 1 And LastRow <= Grid.Rows - 1 Then
                        Grid.Row = LastRow
                        Grid.TopRow = Grid.Row
                    Else
                        Grid.Row = 1
                        Grid.TopRow = Grid.Row
                    End If
                    
                    If Not LastCtl Is Nothing Then
                        If PuedeObtenerFoco(LastCtl) Then LastCtl.SetFocus
                    End If
                    
                End If
                
            Else
                NumPag = 1
                txtPag.Text = NumPag
                Grid.Rows = 1
                Grid.Rows = 2
            End If
            
            If Grid.Rows > 5 Then
                ScrollGrid.Visible = True
                Grid.ScrollBars = flexScrollBarVertical
                AnchoScrollBar = ScrollGrid.Width
                Grid.ColWidth(ColDesProd) = (AnchoCampoScroll - AnchoScrollBar)
            Else
                Grid.ScrollBars = flexScrollBarHorizontal
                ScrollGrid.Visible = False
                AnchoScrollBar = 0
                Grid.ColWidth(ColDesProd) = AnchoCampoScroll
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    'Resume ' Debug
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    CargarGrid
    Grid.Visible = True
    
End Sub

Private Sub CmdDelete_Click()
    
    If Grid.Row > 0 And Grid.TextMatrix(Grid.Row, ColTipoCondicion) <> Empty Then
        
        On Error GoTo Error
        
        Dim RowD As Integer, mSQL As String
        
        RowD = Grid.Row
        
        mSQL = "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
        "WHERE Cod_Producto = '" & QuitarComillasSimples(Grid.TextMatrix(RowD, ColCodProd)) & "' " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
        "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
        "AND Tipo_Condicion = " & QuitarComillasSimples(Grid.TextMatrix(RowD, ColTipoCondicion)) & " " & GetLines
        
        FrmAppLink.CnADM.Execute mSQL
        
        CargarTemporal
        
        Grid.ColSel = ColCount - 1
        
    End If
    
    Exit Sub
    
Error:

    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub txtDenominacion_Change()

End Sub

Private Sub txtDenominacion_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Or KeyCode = vbKeyBack Then
        txtDenominacion.Text = Empty
        txtDenominacion.Tag = Empty
        lblDenominacion.Caption = Empty
        lblDenominacion.Tag = Empty
    End If
End Sub

Private Sub txtMoneda_Change()

End Sub

Private Sub txtMoneda_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Or KeyCode = vbKeyBack Then
        txtMoneda.Text = Empty
        txtMoneda.Tag = Empty
        lblMoneda.Caption = Empty
        lblMoneda.Tag = Empty
    End If
End Sub

Private Sub txtPag_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtPag_GotFocus()
    Set CampoT = txtPag
    txtPag.Tag = SVal(txtPag.Text)
End Sub

Private Sub txtPag_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtPag_LostFocus
    End If
End Sub

Private Sub txtPag_LostFocus()
    If SVal(txtPag.Text) <> SVal(txtPag.Tag) Then
        CargarTemporal
    End If
End Sub

Private Sub ResetearCriterios( _
Optional pCheckAll As Boolean = False)
    PROVEEDOR.Text = Empty
    PROVEEDOR_LostFocus
    departamento.Text = Empty
    departamento_LostFocus
    TxtMarca.Text = Empty
    TxtModelo.Text = Empty
    txtCodigoProducto.Text = Empty
    txtCodigoProducto_LostFocus
    If Not pCheckAll Then
        ChkSeleccionarTodo.Value = vbUnchecked
    End If
    If TipoPromo = 1 Or TipoPromo = 5 Or TipoPromo = 9 Or TipoPromo = 13 Then
        txtCampo2.Visible = False
        lblCampo2.Visible = False
    End If
End Sub
