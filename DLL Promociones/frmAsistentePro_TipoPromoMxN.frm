VERSION 5.00
Begin VB.Form frmAsistentePro_TipoPromoMxN 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9540
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9540
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   7395
      Left            =   240
      TabIndex        =   2
      Top             =   1920
      Width           =   11685
      Begin VB.CommandButton Selected 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   180
         MaskColor       =   &H80000005&
         Picture         =   "frmAsistentePro_TipoPromoMxN.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   6300
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton CmdMxN_ConMontoDescuento 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   6120
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   3180
         Width           =   1095
      End
      Begin VB.CommandButton CmdMxN_ConPorcDesc 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   6120
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1800
         Width           =   1095
      End
      Begin VB.CommandButton CmdMxN_ConPrecioOferta 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   660
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   3180
         Width           =   1095
      End
      Begin VB.CommandButton CmdMxN 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   660
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   1800
         Width           =   1095
      End
      Begin VB.CommandButton CmdAtras 
         Caption         =   "Atras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8400
         TabIndex        =   5
         Top             =   6720
         Width           =   1455
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9960
         TabIndex        =   3
         Top             =   6720
         Width           =   1455
      End
      Begin VB.Label lblEjemploMxNMontoFijo 
         BackStyle       =   0  'Transparent
         Caption         =   "Ejemplo: Si lleva 3 chocolates obtiene una rebaja de 5 $ sobre el precio que tenga en el momento."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   1005
         Left            =   7500
         TabIndex        =   19
         Top             =   3600
         Width           =   3855
      End
      Begin VB.Label lblEjemploMxNPrecioOferta 
         BackStyle       =   0  'Transparent
         Caption         =   "Ejemplo: Si lleva 5 salsas obtiene un precio especial de oferta en todas de 1 $ (Precio Original: 1.5 $)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   1005
         Left            =   1980
         TabIndex        =   18
         Top             =   3600
         Width           =   3855
      End
      Begin VB.Label lblEjemploMxNPorcDesc 
         BackStyle       =   0  'Transparent
         Caption         =   "Ejemplo: Si lleva 2 refrescos, uno lo paga con 50% de descuento."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   825
         Left            =   7500
         TabIndex        =   17
         Top             =   2160
         Width           =   3855
      End
      Begin VB.Label lblEjemploMxN 
         BackStyle       =   0  'Transparent
         Caption         =   "Ejemplo: Lleve 3 Galletas, Pague 2 (El cliente se ahorra el precio equivalente de 1 de ellas)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   885
         Left            =   1980
         TabIndex        =   16
         Top             =   2160
         Width           =   3855
      End
      Begin VB.Label lblTipoMxnMontoDesc 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "M x N con Monto Fijo de Descuento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   7500
         TabIndex        =   14
         Top             =   3240
         Width           =   3885
      End
      Begin VB.Label lblTipoMxnPorcDesc 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "M x N con % Descuento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   7500
         TabIndex        =   12
         Top             =   1860
         Width           =   3510
      End
      Begin VB.Label lblTipoMxnPrecioOferta 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "M x N a Precio Oferta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   1980
         TabIndex        =   10
         Top             =   3240
         Width           =   3105
      End
      Begin VB.Label lblGuiaUsuario 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione el Tipo de Promoci�n ""N * N"" a crear:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   660
         TabIndex        =   8
         Top             =   1080
         Width           =   5205
      End
      Begin VB.Label lblTipoMxN 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "M x N"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   1980
         TabIndex        =   7
         Top             =   1860
         Width           =   1275
      End
      Begin VB.Label LblDescripcionAsistente 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Este asistente le permitir� crear una Promoci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   240
         TabIndex        =   4
         Top             =   210
         Width           =   4995
      End
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   1
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_TipoPromoMxN.frx":1D82
      Top             =   360
      Width           =   2700
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_TipoPromoMxN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private TipoSeleccionado As Integer

Private Function ValidarTipoPromo() As Boolean
    
    If TipoSeleccionado <> TipoPromo And TipoPromo <> -1 Then
        
        If Mensaje(False, StellarMensajeLocal(156)) Then
            
            ' Cuando se cambia el tipo de promoci�n reiniciar todos los valores cuya relevancia son
            ' de acuerdo al tipo de promoci�n ya que sino podr�an generar inconsistencias si se
            ' mantienen y mezclan los datos.
            
            ExecuteSafeSQL "UPDATE TMP_PROMOCION SET " & _
            "Cantidad_Productos_Requerir = 0, Cantidad_Productos_Pagar = 0, Combina_Promocion = 0, Prioridad_Promocion = 0 " & _
            "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & _
            "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", _
            FrmAppLink.CnADM, RecordsAffected
            
            ExecuteSafeSQL "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & _
            "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & _
            "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", _
            FrmAppLink.CnADM, RecordsAffected
            
            ValidarSeleccion
            
        Else
            Exit Function
        End If
        
    End If
    
    TipoPromo = TipoSeleccionado
    
    frmAsistentePro_DatosGenerales.Show vbModal
    
    If Not (PromocionCreada Or CancelarPromo_Salir) Then
        ValidarSeleccion
    End If
    
End Function

Private Sub CmdAtras_Click()
    Unload Me
End Sub

Private Sub CmdCancelar_Click()
    If Mensaje(False, Replace(StellarMensaje(2620), "$(Line)", vbNewLine)) Then
        CancelarPromo_Salir = True
        Form_Activate
        Exit Sub
    End If
End Sub

Private Sub CmdMxN_Click()
    TipoSeleccionado = 4
    Call ValidarTipoPromo
End Sub

Private Sub CmdMxN_ConPrecioOferta_Click()
    TipoSeleccionado = 5
    Call ValidarTipoPromo
End Sub

Private Sub CmdMxN_ConPorcDesc_Click()
    TipoSeleccionado = 6
    Call ValidarTipoPromo
End Sub

Private Sub CmdMxN_ConMontoDescuento_Click()
    TipoSeleccionado = 7
    Call ValidarTipoPromo
End Sub

Private Sub Form_Load()
    
    If CancelarPromo_Salir Then Exit Sub
    
    LblDescripcionAsistente.Caption = StellarMensajeLocal(101)
    lblGuiaUsuario.Caption = StellarMensajeLocal(103)
    CmdAtras.Caption = StellarMensajeLocal(12)
    CmdCancelar.Caption = StellarMensajeLocal(11)
    
    lblTipoMxN.Caption = StellarMensajeLocal(110) & " " & StellarMensajeLocal(164)
    lblEjemploMxN.Caption = StellarMensajeLocal(179)
    lblTipoMxnPrecioOferta.Caption = StellarMensajeLocal(111)
    lblEjemploMxNPrecioOferta.Caption = StellarMensajeLocal(182)
    lblTipoMxnPorcDesc.Caption = StellarMensajeLocal(112)
    lblEjemploMxNPorcDesc.Caption = StellarMensajeLocal(180)
    lblTipoMxnMontoDesc.Caption = StellarMensajeLocal(113)
    lblEjemploMxNMontoFijo.Caption = StellarMensajeLocal(181)
    
    ValidarSeleccion
    
    Call AjustarPantalla(Me)

End Sub

Private Sub Form_Activate()
    If CancelarPromo_Salir Or PromocionCreada Then
        Unload Me
    End If
End Sub

Private Sub Header_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub ValidarSeleccion()
    If TipoPromo = 4 Then
        CmdMxN.Picture = Selected.Picture
        CmdMxN_ConPrecioOferta.Picture = LoadPicture()
        CmdMxN_ConPorcDesc.Picture = LoadPicture()
        CmdMxN_ConMontoDescuento.Picture = LoadPicture()
    ElseIf TipoPromo = 5 Then
        CmdMxN_ConPrecioOferta.Picture = Selected.Picture
        CmdMxN.Picture = LoadPicture()
        CmdMxN_ConPorcDesc.Picture = LoadPicture()
        CmdMxN_ConMontoDescuento.Picture = LoadPicture()
    ElseIf TipoPromo = 6 Then
        CmdMxN_ConPorcDesc.Picture = Selected.Picture
        CmdMxN_ConPrecioOferta.Picture = LoadPicture()
        CmdMxN.Picture = LoadPicture()
        CmdMxN_ConMontoDescuento.Picture = LoadPicture()
    ElseIf TipoPromo = 7 Then
        CmdMxN_ConMontoDescuento.Picture = Selected.Picture
        CmdMxN_ConPrecioOferta.Picture = LoadPicture()
        CmdMxN_ConPorcDesc.Picture = LoadPicture()
        CmdMxN.Picture = LoadPicture()
    End If
End Sub
