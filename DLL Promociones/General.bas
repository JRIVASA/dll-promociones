Attribute VB_Name = "General"
Public Enum Alienacion
    flexDefault = -1
    flexAlignLeftTop = 0
    flexAlignLeftCenter = 1
    flexAlignLeftBottom = 2
    flexAlignCenterTop = 3
    flexAlignCenterCenter = 4
    flexAlignCenterBottom = 5
    flexAlignRightTop = 6
    flexAlignRightCenter = 7
    flexAlignRightBottom = 8
    flexAlignGeneral = 9
End Enum

Public Function StellarMensajeLocal(pResourceID As Long) As String
    StellarMensajeLocal = Stellar_Mensaje_Local(pResourceID, True)
End Function

Public Function Stellar_Mensaje_Local(Mensaje As Long, _
Optional ByVal pDevolver As Boolean = True) As String
    
    On Error GoTo Error
    
    Texto = LoadResString(Mensaje)

    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje_Local = Texto
    End If
    
    Exit Function
    
Error:
    
    Stellar_Mensaje_Local = "CHK_RES"
    
End Function

Public Sub MSGridAsign(ByRef GridObj As Object, Fila As Integer, Columna As Integer, Texto As Variant, Optional Tamano As Long = -1, Optional Alinear As Alienacion, Optional Formato As String = "")
    GridObj.Row = Fila
    GridObj.Col = Columna
    If Tamano >= 0 Then
        GridObj.ColWidth(GridObj.ColSel) = Tamano
    End If
    If Alinear > -1 Then
        GridObj.CellAlignment = Alinear
    End If
    If Formato <> "" Then
        Texto = Format(Texto, Formato)
    End If
    GridObj.Text = IIf(IsNull(Texto), "", Texto)
End Sub

Sub SetDefMSGrid(ByRef GridObj As Object, Fila As Integer, Columna As Integer)
    GridObj.Col = Columna
    GridObj.Row = Fila
End Sub

Function MSGridRecover(ByRef GridObj As Object, ByVal Fila As Integer, ByVal Columna As Integer) As Variant
    Dim Tcol As Integer, Trow As Integer
    Tcol = GridObj.Col
    Trow = GridObj.Row
    GridObj.Row = Fila
    GridObj.Col = Columna
    MSGridRecover = GridObj.Text
    SetDefMSGrid GridObj, Trow, Tcol
End Function

Public Function Buscar_Usuario(ByVal Valor_Campo As String) As String
    
    Dim Rec_User As New ADODB.Recordset
    
    Call Apertura_Recordset(Rec_User)
    
    Rec_User.Open "SELECT Descripcion FROM MA_USUARIOS WHERE CodUsuario = '" & Valor_Campo & "'", _
    FrmAppLink.CnADM, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not Rec_User.EOF Then
        Buscar_Usuario = Rec_User!Descripcion
    Else
        Buscar_Usuario = "N/A"
    End If
    
    Call Cerrar_Recordset(Rec_User)
    
End Function

' Safe Val() - Sin el problema de que Val no soporta numeros formateados.
' Ej: Val("10,000.00") = 10 | SVal("10,000.00") = 10000 ' Como debe ser.
Public Function SVal(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 0) As Double
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SVal = CDbl(pExpression)
    Else
        SVal = pDefaultValue
    End If
End Function

' Min Val()
Public Function MinVal(ByVal pExpression As Variant, _
Optional ByVal pMinVal As Double = 0) As Double
    MinVal = SVal(pExpression, pMinVal)
    MinVal = IIf(MinVal < pMinVal, pMinVal, MinVal)
End Function

' Max Val()
Public Function MaxVal(ByVal pExpression As Variant, _
Optional ByVal pMaxVal As Double = 0) As Double
    MaxVal = SVal(pExpression, MaxVal)
    MaxVal = IIf(MaxVal > pMaxVal, pMaxVal, MaxVal)
End Function

' Min Val > 0
Public Function MinVal0(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 1) As Double
    MinVal0 = SVal(pExpression, pDefaultValue)
    MinVal0 = IIf(MinVal0 > 0, MinVal0, pDefaultValue)
End Function

Public Function SDec(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = 0) As Variant
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SDec = CDec(pExpression)
    Else
        SDec = CDec(pDefaultValue)
    End If
End Function

' Min Decimal()
Public Function MinDec(ByVal pExpression As Variant, _
Optional ByVal pMinDec As Variant = 0) As Variant
    MinDec = SDec(pExpression, pMinDec)
    MinDec = IIf(MinDec < pMinDec, pMinDec, MinDec)
End Function

' Max Decimal()
Public Function MaxDec(ByVal pExpression As Variant, _
Optional ByVal pMaxDec As Variant = 0) As Variant
    MaxDec = SDec(pExpression, MaxDec)
    MaxDec = IIf(MaxDec > pMaxDec, pMaxDec, MaxDec)
End Function

' Min Decimal > 0
Public Function MinDec0(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = 1) As Variant
    MinDec0 = SDec(pExpression, pDefaultValue)
    MinDec0 = IIf(MinDec0 > 0, MinDec0, pDefaultValue)
End Function
