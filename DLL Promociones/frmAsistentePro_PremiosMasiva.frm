VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frmAsistentePro_PremiosMasiva 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   0
      ScaleHeight     =   1050
      ScaleWidth      =   12210
      TabIndex        =   6
      Top             =   1680
      Width           =   12240
      Begin VB.Frame frame_toolbar 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   810
         Left            =   30
         TabIndex        =   7
         Top             =   140
         Width           =   11505
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   120
            TabIndex        =   8
            Top             =   0
            Width           =   10575
            _ExtentX        =   18653
            _ExtentY        =   1429
            ButtonWidth     =   2408
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   10
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F3) Agr&egar"
                  Key             =   "Agregar"
                  Object.ToolTipText     =   "Agregar una Nueva Ficha"
                  ImageIndex      =   2
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "APRODUCTO"
                        Object.Tag             =   "APRODUCTO"
                        Text            =   "Agregar"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "AIMPORTAR"
                        Object.Tag             =   "AIMPORTAR"
                        Text            =   "Agregar Importar "
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "imparchivo"
                        Text            =   "Importar Archivo"
                     EndProperty
                  EndProperty
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F2) &Buscar"
                  Key             =   "Buscar"
                  Object.ToolTipText     =   "Buscar una Ficha"
                  ImageIndex      =   6
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F5) &Modificar"
                  Key             =   "Modificar"
                  Object.ToolTipText     =   "Modificar esta Ficha"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F7) &Cancelar"
                  Key             =   "Cancelar"
                  Object.ToolTipText     =   "Cancelar esta Ficha"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F6) Bo&rrar"
                  Key             =   "Eliminar"
                  Object.ToolTipText     =   "Borrar esta Ficha"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F4) &Grabar"
                  Key             =   "Grabar"
                  Object.ToolTipText     =   "Grabar esta Ficha"
                  ImageIndex      =   7
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F12) &Salir"
                  Key             =   "Salir"
                  Object.ToolTipText     =   "Salir de Ficheros"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Caption         =   "&Ayuda"
                  Key             =   "Ayuda"
                  Object.ToolTipText     =   "Ayuda"
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Excluir"
      Height          =   7995
      Left            =   120
      TabIndex        =   1
      Top             =   2760
      Width           =   11925
      Begin VB.CommandButton Command3 
         Caption         =   "Finalizar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8760
         TabIndex        =   29
         Top             =   7440
         Width           =   1455
      End
      Begin VB.CommandButton Command2 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Left            =   3960
         Picture         =   "frmAsistentePro_PremiosMasiva.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   1080
         Width           =   435
      End
      Begin VB.TextBox Text3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3720
         TabIndex        =   27
         Top             =   480
         Width           =   4020
      End
      Begin VB.TextBox Text4 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5280
         TabIndex        =   25
         Top             =   1680
         Width           =   1740
      End
      Begin VB.CommandButton Command1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Left            =   7440
         Picture         =   "frmAsistentePro_PremiosMasiva.frx":0802
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   1080
         Width           =   435
      End
      Begin VB.CommandButton Btn_Dpto 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Left            =   11400
         Picture         =   "frmAsistentePro_PremiosMasiva.frx":1004
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   1080
         Width           =   435
      End
      Begin VB.ComboBox Combo2 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   9240
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   1080
         Width           =   2085
      End
      Begin VB.ComboBox Combo1 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   5280
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   1080
         Width           =   2085
      End
      Begin VB.ComboBox cmbDatos 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   1080
         Width           =   2085
      End
      Begin VB.TextBox Text2 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1800
         TabIndex        =   16
         Top             =   1680
         Width           =   2100
      End
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1680
         TabIndex        =   15
         Top             =   2520
         Width           =   1740
      End
      Begin VB.TextBox txtDato 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1800
         TabIndex        =   11
         Top             =   480
         Width           =   1740
      End
      Begin VB.CommandButton CmdSig 
         Caption         =   "Atras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7200
         TabIndex        =   3
         Top             =   7440
         Width           =   1455
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   10320
         TabIndex        =   2
         Top             =   7440
         Width           =   1455
      End
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   3855
         Left            =   120
         TabIndex        =   4
         Top             =   3120
         Width           =   11655
         _ExtentX        =   20558
         _ExtentY        =   6800
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Precio Oferta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   26
         Top             =   2520
         Width           =   1275
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Modelo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   4440
         TabIndex        =   24
         Top             =   1800
         Width           =   690
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Marca"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   23
         Top             =   1800
         Width           =   600
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Index           =   0
         Left            =   240
         TabIndex        =   22
         Top             =   2160
         Width           =   720
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   1080
         X2              =   11760
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "SubGrupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   8160
         TabIndex        =   14
         Top             =   1200
         Width           =   945
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   4680
         TabIndex        =   13
         Top             =   1200
         Width           =   585
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   12
         Top             =   1200
         Width           =   1410
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Proveedor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   10
         Top             =   480
         Width           =   990
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Index           =   71
         Left            =   120
         TabIndex        =   9
         Top             =   120
         Width           =   720
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00AE5B00&
         X1              =   1080
         X2              =   11760
         Y1              =   240
         Y2              =   240
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   11520
      Top             =   2040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_PremiosMasiva.frx":1806
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_PremiosMasiva.frx":3598
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_PremiosMasiva.frx":4272
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_PremiosMasiva.frx":4F4C
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_PremiosMasiva.frx":6CDE
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_PremiosMasiva.frx":8A70
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_PremiosMasiva.frx":A802
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_PremiosMasiva.frx":C594
      Top             =   360
      Width           =   2700
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   0
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_PremiosMasiva"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
   
    With MSFlexGrid1
    
        .TextMatrix(0, 0) = "Proveedor"
        .TextMatrix(0, 1) = "Departamento"
        .TextMatrix(0, 2) = "Grupo"
        .TextMatrix(0, 3) = "SubGrupo"
        .TextMatrix(0, 4) = "Marca"
        .TextMatrix(0, 5) = "Modelo"
        .TextMatrix(0, 6) = "Precio Oferta"
        
        .ColWidth(0) = 1700
        .ColWidth(1) = 1700
        .ColWidth(2) = 1600
        .ColWidth(3) = 1600
        .ColWidth(4) = 1600
        .ColWidth(5) = 1600
        .ColWidth(6) = 1600
        
    End With


End Sub

