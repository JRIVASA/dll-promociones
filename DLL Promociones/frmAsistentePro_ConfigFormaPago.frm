VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frmAsistentePro_ConfigFormaPago 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Excluir"
      Height          =   8895
      Left            =   180
      TabIndex        =   12
      Top             =   1860
      Width           =   11925
      Begin VB.PictureBox Medir 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   435
         Left            =   540
         ScaleHeight     =   375
         ScaleWidth      =   495
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   5280
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.CheckBox chkPagoMinimo 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Pago M�nimo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   180
         TabIndex        =   2
         Top             =   645
         Width           =   3555
      End
      Begin VB.TextBox txtMoneda 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   0
         Top             =   1560
         Width           =   1425
      End
      Begin VB.CommandButton CmdMoneda 
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3300
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   1560
         Width           =   435
      End
      Begin VB.VScrollBar ScrollGrid 
         Height          =   5175
         LargeChange     =   10
         Left            =   11100
         TabIndex        =   19
         Top             =   2820
         Width           =   675
      End
      Begin VB.TextBox TxtNumPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   8220
         Width           =   780
      End
      Begin VB.TextBox txtPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   1980
         TabIndex        =   5
         Top             =   8220
         Width           =   780
      End
      Begin VB.TextBox txtValorMinimo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   3900
         TabIndex        =   3
         Top             =   540
         Width           =   2700
      End
      Begin VB.TextBox txtDenominacion 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1800
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   2160
         Width           =   1440
      End
      Begin VB.CommandButton CmdDenominacion 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3300
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":0802
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   2160
         Width           =   435
      End
      Begin VB.CommandButton CmdSiguiente 
         Caption         =   "Siguiente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8760
         TabIndex        =   8
         Top             =   8220
         Width           =   1455
      End
      Begin VB.CommandButton CmdAtras 
         Caption         =   "Atras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7200
         TabIndex        =   7
         Top             =   8220
         Width           =   1455
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   10320
         TabIndex        =   9
         Top             =   8220
         Width           =   1455
      End
      Begin MSFlexGridLib.MSFlexGrid Grid 
         DragMode        =   1  'Automatic
         Height          =   5175
         Left            =   120
         TabIndex        =   4
         Top             =   2820
         Width           =   11655
         _ExtentX        =   20558
         _ExtentY        =   9128
         _Version        =   393216
         Cols            =   12
         FixedCols       =   0
         BackColor       =   16448250
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         ScrollBars      =   2
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Image CmdArchivo 
         Height          =   600
         Left            =   10800
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1004
         Stretch         =   -1  'True
         Top             =   1560
         Width           =   600
      End
      Begin VB.Label lblArchivoPlano 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Archivo Plano"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   330
         Left            =   10800
         TabIndex        =   25
         Top             =   2220
         Width           =   630
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblDescBtnBorrar 
         BackStyle       =   0  'Transparent
         Caption         =   "Eliminar Fila"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   345
         Left            =   5040
         TabIndex        =   24
         Top             =   8280
         Width           =   690
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblDescBtnExcluir 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Excluir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   165
         Left            =   10080
         TabIndex        =   23
         Top             =   2355
         Visible         =   0   'False
         Width           =   510
      End
      Begin VB.Label lblDescBtnIncluir 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Incluir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   165
         Left            =   9300
         TabIndex        =   22
         Top             =   2355
         Width           =   495
      End
      Begin VB.Image CmdDelete 
         Height          =   480
         Left            =   4500
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":2D86
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Image ImgNext 
         Height          =   480
         Left            =   1140
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":7BAC
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Image ImgPrev 
         Height          =   480
         Left            =   300
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":C93B
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   2880
         X2              =   3060
         Y1              =   8640
         Y2              =   8280
      End
      Begin VB.Image CmdExcluir 
         Height          =   600
         Left            =   10020
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":FE20
         Stretch         =   -1  'True
         Top             =   1600
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.Image CmdIncluir 
         Height          =   600
         Left            =   9240
         Picture         =   "frmAsistentePro_ConfigFormaPago.frx":13A0D
         Stretch         =   -1  'True
         Top             =   1600
         Width           =   600
      End
      Begin VB.Label lblDescMoneda 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3840
         TabIndex        =   18
         Top             =   1560
         UseMnemonic     =   0   'False
         Width           =   5220
      End
      Begin VB.Label lblDescDenomina 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3840
         TabIndex        =   17
         Top             =   2160
         UseMnemonic     =   0   'False
         Width           =   5205
      End
      Begin VB.Label lblDenominacion 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Denominaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   16
         Top             =   2160
         Width           =   1350
      End
      Begin VB.Label lblMoneda 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Moneda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   15
         Top             =   1560
         Width           =   780
      End
      Begin VB.Label lblCriterios 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   180
         TabIndex        =   14
         Top             =   120
         Width           =   960
      End
      Begin VB.Line LnCriterios 
         BorderColor     =   &H00AE5B00&
         X1              =   1260
         X2              =   11760
         Y1              =   240
         Y2              =   240
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   6600
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":146D7
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":16469
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":17143
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":17E1D
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":19BAF
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1B941
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1D6D3
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Image CmdConfigurarTeclado 
      Height          =   660
      Left            =   7080
      MousePointer    =   99  'Custom
      Picture         =   "frmAsistentePro_ConfigFormaPago.frx":1F465
      Stretch         =   -1  'True
      Top             =   1020
      Width           =   660
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_ConfigFormaPago.frx":211E7
      Top             =   360
      Width           =   2700
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   11
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_ConfigFormaPago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private AnchoScrollBar      As Long
Private AnchoCampoScroll    As Long
Private pCargandoArchivo    As Boolean
Private pCargarLinea        As Boolean

Private DragStart, DragStop, DragRows, mDragEndRow

Private Enum GridCol
    ColRowID
    ColTipoCondicion
    ColCodMoneda
    ColDesMoneda
    ColCodDenomina
    ColDesDenomina
    [ColCount]
End Enum

Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
  
  Dim Ctl As Control
  Dim bHandled As Boolean
  Dim bOver As Boolean
  
  For Each Ctl In Controls
    ' Is the mouse over the control
    On Error Resume Next
    bOver = (Ctl.Visible And IsOver(Ctl.hWnd, Xpos, Ypos))
    On Error GoTo 0
    
    If bOver Then
      ' If so, respond accordingly
      bHandled = True
      Select Case True
      
        Case TypeOf Ctl Is MSFlexGrid
          FlexGridScroll Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is PictureBox
          PictureBoxZoom Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is ListBox, TypeOf Ctl Is TextBox, TypeOf Ctl Is ComboBox
          ' These controls already handle the mousewheel themselves, so allow them to:
          If Ctl.Enabled Then Ctl.SetFocus
          
        Case Else
          bHandled = False

      End Select
      If bHandled Then Exit Sub
    End If
    bOver = False
  Next Ctl
  
  ' Scroll was not handled by any controls, so treat as a general message send to the form
  'Me.Caption = "Form Scroll " & IIf(Rotation < 0, "Down", "Up")
End Sub

Private Sub CmdArchivo_Click()
    
    On Error GoTo Error
    
    Dim CodMoneda As String, CodDenomina As String
    
    Set gClsArcBD = CreateObject("recsun.obj_ArchivoDB")
    
    gClsArcBD.Inicializar 0
    
    gClsArcBD.Adicionar_CamposDisponibles "CodMoneda", "" & Replace(StellarMensaje(134), ":", Empty) & "", 3000
    gClsArcBD.Adicionar_CamposDisponibles "CodDenomina", "" & StellarMensaje(15515) & "", 3000
    
    Dim mValores, LenArr
    mValores = gClsArcBD.EJECUTAR
    If IsEmpty(mValores) Then Exit Sub
    
    MousePointer = vbHourglass
    DoEvents
    pCargandoArchivo = True
    ResetearCriterios
    
    LenArr = UBound(mValores)
    
    Dim pCuentaExito As Double, pCuentaFallo As Double, pCantRegistros As Double
    Dim mInfoFallidos As String: mInfoFallidos = Empty
    
    For I = 1 To LenArr
        
        Dim CodigoEntrada As String
        
        txtMoneda.Text = mValores(I)(0)
        lblDescMoneda.TabIndex = mValores(I)(0)
        txtDenominacion.Text = mValores(I)(1)
        lblDescDenomina.Caption = mValores(I)(1)
        
        CodigoEntrada = txtMoneda.Text & ";" & txtDenominacion.Text
        
        DoEvents
        
        pCargarLinea = False
        
        CmdIncluir_Click
        
        pCantRegistros = pCantRegistros + 1
        
        If pCargarLinea Then
            pCuentaExito = pCuentaExito + 1
        Else
            pCuentaFallo = pCuentaFallo + 1
            mInfoFallidos = mInfoFallidos & _
            "" & StellarMensaje(6144) & ": " & (I) & ", " & StellarMensaje(142) & ": " & CodigoEntrada & vbNewLine
        End If
        
    Next
    
    Dim mMsjEstatus As String
    
    mMsjEstatus = "" & StellarMensajeLocal(224) & ": (" & pCuentaExito & " " & _
    StellarMensajeLocal(169) & " " & pCantRegistros & ")" & vbNewLine
    
    If pCuentaFallo > 0 Then
        ActivarMensajeGrande 100
        mMsjEstatus = mMsjEstatus & _
        "" & StellarMensajeLocal(225) & ": (" & pCuentaFallo & ")" & vbNewLine
        mMsjEstatus = mMsjEstatus & _
        "" & StellarMensajeLocal(226) & ":" & vbNewLine & vbNewLine
        mMsjEstatus = mMsjEstatus & mInfoFallidos
    End If
    
    Mensaje True, mMsjEstatus
    
    CargarTemporal
    
Finally:
    
    txtMoneda = Empty
    lblDescMoneda = Empty
    txtDenominacion = Empty
    lblDescDenomina = Empty
    
    pCargandoArchivo = False
    
    MousePointer = vbDefault
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CmdArchivo_Click)"
    
    GoTo Finally
    
End Sub

Private Sub chkPagoMinimo_Click()
    If TipoPromo = 16 Or TipoPromo = 19 Then
        chkPagoMinimo.Value = vbChecked
    ElseIf TipoPromo = 17 Or TipoPromo = 18 _
    Or TipoPromo = 20 Or TipoPromo = 21 Then
        If chkPagoMinimo.Value = vbChecked Then
            SeleccionarTexto txtValorMinimo
            SafeFocus txtValorMinimo
        Else
            txtValorMinimo = "0.00"
        End If
    End If
End Sub

Private Sub CmdConfigurarTeclado_Click()
    SeleccionarTexto CampoT
    TecladoAvanzado CampoT
End Sub

Private Sub GrabarOtrosDatos()
    
    On Error GoTo Error
    
    Dim ActiveTrans As Boolean
    
    FrmAppLink.CnADM.BeginTrans
    ActiveTrans = True
    
    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
    "AND Tipo_Condicion = 6 ", AffectedRecords
    
    FrmAppLink.CnADM.Execute "INSERT INTO TMP_PROMOCION_CONDICION_VALORES " & _
    "(Cod_Promocion, CodUsuario, Tipo_Condicion, Cod_Dpto, Porcentaje_Descuento1) " & _
    "SELECT '" & QuitarComillasSimples(mCodPromo_Actual) & "', " & _
    "'" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "', " & _
    "6, '" & IIf(TipoPromo = 16 Or TipoPromo = 17 Or TipoPromo = 19 Or TipoPromo = 20, "**Prc_Min**", _
    MonedaDoc.CodMoneda) & "', (" & IIf(TipoPromo = 16 Or TipoPromo = 19, 100, SDec(txtValorMinimo)) & ") ", AffectedRecords
    
    FrmAppLink.CnADM.CommitTrans
    ActiveTrans = False
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If ActiveTrans Then
        FrmAppLink.CnADM.RollbackTrans
        ActiveTrans = False
    End If
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(GrabarOtrosDatos)"
    
End Sub

Private Sub CmdMoneda_Click()
    CmdMoneda_KeyDown vbKeyF2, 0
End Sub

Private Sub CmdMoneda_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Not CmdMoneda.Visible Then Exit Sub
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Tecla_Pulsada = True
            
            Set Campo_Txt = txtMoneda
            Set Campo_Lbl = lblDescMoneda
            
            Call MAKE_VIEW("MA_MONEDAS_USO_EN_POS", "c_CodMoneda", "c_Descripcion", _
            "" & StellarMensaje(2855) & "", Me, "GENERICO", True, Campo_Txt, Campo_Lbl)
            
            MonedaTmp.BuscarMonedas , txtMoneda
            
            Campo_Lbl.Tag = MonedaTmp.SimMoneda
            
            'If TipoPromo = 17 Then
                
                'If ChkAplicarPorCriterio.Value = vbChecked Then
                    'chkPagoMinimo.Caption = StellarMensajeLocal(273) & " " & _
                    Replace(StellarMensajeLocal(239), "$(Curr)", "(" & MonedaTmp.SimMoneda & ")")
                'Else
                    'chkPagoMinimo.Caption = StellarMensajeLocal(273) & " " & _
                    Replace(StellarMensajeLocal(239), "$(Curr)", "(" & MonedaDoc.SimMoneda & ")")
                'End If
                
                'ChkVueltoPromocion.Caption = StellarMensajeLocal(242) & " (" & MonedaTmp.SimMoneda & ")"
                
            'End If
            
            Tecla_Pulsada = False
            
    End Select
    
End Sub

Private Sub CmdDenominacion_Click()
    CmdDenominacion_KeyDown vbKeyF2, 0
End Sub

Private Sub CmdDenominacion_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Trim(txtMoneda) <> Empty And Trim(lblDescMoneda) <> Empty Then
        
        Select Case KeyCode
            
            Case Is = vbKeyF2
                
                Tecla_Pulsada = True
                
                Set Campo_Txt = txtDenominacion
                Set Campo_Lbl = lblDescDenomina
                
                FrmAppLink.SetMonedaSel = txtMoneda.Text
                
                Call MAKE_VIEW("MA_DENOMINACIONES", "c_CodDenomina", "c_Denominacion", _
                "" & StellarMensaje(2716) & "", Me, "GENERICO", True, Campo_Txt, Campo_Lbl)
                
                Tecla_Pulsada = False
                
        End Select
        
    Else
        
        SafeFocus txtMoneda
        
    End If
    
End Sub

'Private Sub CmdMonedaLimite_Click()
'    CmdMonedaLimite_KeyDown vbKeyF2, 0
'End Sub
'
'Private Sub CmdMonedaLimite_KeyDown(KeyCode As Integer, Shift As Integer)
'
'    If Not CmdMonedaLimite.Visible Then Exit Sub
'
'    Select Case KeyCode
'
'        Case Is = vbKeyF2
'
'            Tecla_Pulsada = True
'
'            Set Campo_Txt = txtMonedaLimite
'            Set Campo_Lbl = lblMonedaLimite
'
'            Call MAKE_VIEW("MA_MONEDAS_USO_EN_POS", "c_CodMoneda", "c_Descripcion", _
'            "" & StellarMensaje(2855) & "", Me, "GENERICO", True, Campo_Txt, Campo_Lbl)
'
'            MonedaTmp.BuscarMonedas , txtMonedaLimite
'
'            Campo_Lbl.Tag = MonedaTmp.SimMoneda
'
'            ChkDctoLimiteFactura.Caption = StellarMensajeLocal(243) & " (" & MonedaTmp.SimMoneda & ")"
'
'            If ChkDctoLimiteFactura.Value = vbChecked Then
'                txtMontoLimiteDcto.Text = FormatNumber(0, 2)
'                SafeFocus txtMontoLimiteDcto
'            End If
'
'            SeleccionarTexto txtMontoLimiteDcto
'
'            Tecla_Pulsada = False
'
'    End Select
'
'End Sub

Private Sub CmdSiguiente_Click()
    
    If Not ((Grid.TextMatrix(1, ColRowID) <> Empty _
    And Grid.TextMatrix(1, ColTipoCondicion) <> Empty) _
    Or SVal(TxtNumPag) > 1) Then
        Mensaje True, StellarMensajeLocal(197)
        Exit Sub
    End If
    
    If (TipoPromo = 16 Or TipoPromo = 17 Or TipoPromo = 19 Or TipoPromo = 20) Then
        If SDec(txtValorMinimo) > 100 Then
            Mensaje True, StellarMensaje(3196)
            SafeFocus txtValorMinimo
            Exit Sub
        End If
    End If
    
    GrabarOtrosDatos
    
    Dim VerificarAplicaBIN As Boolean, AplicaListaBIN As Boolean
    
    VerificarAplicaBIN = Val(BuscarReglaNegocioStr( _
    "OyP_PromosxMetodoDePago_DctoBIN", 0)) = 1 _
    And ExisteTablaV3("TMP_OYP_PRM_DETPAG_BIN", FrmAppLink.CnADM)
    
    If VerificarAplicaBIN Then
        
        MinVerElec = BuscarValorBD("MinVerElec", _
        "SELECT MIN(DEN.nu_VerificacionElectronica) AS MinVerElec " & vbNewLine & _
        "FROM TMP_PROMOCION_CONDICION_VALORES COND " & vbNewLine & _
        "INNER JOIN MA_DENOMINACIONES DEN " & vbNewLine & _
        "ON COND.Cod_Dpto = DEN.c_CodMoneda " & vbNewLine & _
        "AND COND.Cod_Grupo = DEN.c_CodDenomina " & vbNewLine & _
        "WHERE COND.CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND COND.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND COND.Tipo_Condicion = 5 ", "0", FrmAppLink.CnADM)
        
        If MinVerElec > 0 Then
            
            If Mensaje(False, StellarMensajeLocal(258)) Then
                AplicaListaBIN = True
            End If
            
        End If
        
    End If
    
    If AplicaListaBIN Then
        frmAsistentePro_ConfigListaBIN.Show vbModal
    Else
        frmAsistentePro_CargarSucursales.Show vbModal
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Cancel = 0 Then
        Call WheelUnHook(Me.hWnd)
    End If
End Sub

Private Sub Grid_Scroll()
    'DoEvents
    'ScrollGrid.Value = Grid.Row
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
    Exit Sub
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Private Sub CmdAtras_Click()
    Unload Me
End Sub

Private Sub CmdCancelar_Click()
    If Mensaje(False, Replace(StellarMensaje(2620), "$(Line)", vbNewLine)) Then
        CancelarPromo_Salir = True
        Form_Activate
        Exit Sub
    End If
End Sub

Private Sub Grid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CmdDelete_Click
    End If
End Sub

Private Sub Grid_DragDrop(Source As Control, x As Single, y As Single)
    
    If Grid.RowHeightMin = 0 Then Exit Sub
    
    On Error Resume Next
    
    DragStop = y
    
    Drag = (DragStop - DragStart)
    
    If Abs(Drag / Grid.RowHeightMin) > 1 Then
        
        DragRows = (Drag / Grid.RowHeightMin)
        
        mDragEndRow = (Grid.TopRow + (-1 * DragRows))
        
        If mDragEndRow < Grid.FixedRows Then
            mDragEndRow = Grid.FixedRows
        ElseIf mDragEndRow > Grid.Rows Then
            mDragEndRow = Grid.Rows - 1
        End If
        
        Grid.TopRow = mDragEndRow
        ScrollGrid.Value = Grid.TopRow
        
    End If
    
    DragStart = 0
    
End Sub

Private Sub Grid_DragOver(Source As Control, x As Single, y As Single, State As Integer)
    
    On Error Resume Next
    
    If DragStart = 0 Then
        DragStart = y
    End If
    
End Sub

Private Sub ImgNext_Click()
    
    If SVal(txtPag.Text) <= SVal(TxtNumPag.Text) Then
        txtPag.Text = SVal(txtPag.Text) + 1
        If SVal(txtPag.Text) > SVal(TxtNumPag.Text) Then txtPag.Text = SVal(TxtNumPag.Text)
    Else
        txtPag.Text = SVal(TxtNumPag.Text)
    End If
    
    CargarTemporal
    
End Sub

Private Sub ImgPrev_Click()
    
    If SVal(txtPag.Text) >= 1 Then
        txtPag.Text = SVal(txtPag.Text) - 1
        If SVal(txtPag.Text) <= 1 Then txtPag.Text = 1
    Else
        txtPag.Text = 1
    End If
    
    CargarTemporal
    
End Sub

Private Sub CargarGrid()
    
    With Grid
        
        .Rows = 1
        .Row = 0
        .Clear
        .Cols = ColCount
        .RowHeightMin = 600
        
        .Col = ColRowID
        .TextMatrix(0, .Col) = "Ln" '"RowID"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColTipoCondicion
        .TextMatrix(0, .Col) = "TipoCondicion"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColCodMoneda
        .TextMatrix(0, .Col) = StellarMensaje(142) 'C�digo
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesMoneda
        .TextMatrix(0, .Col) = Replace(StellarMensaje(134), ":", Empty) '"Moneda"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodDenomina
        .TextMatrix(0, .Col) = StellarMensaje(142) '"C�digo"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesDenomina
        .TextMatrix(0, .Col) = StellarMensaje(15515) '"Forma de Pago"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .DragMode = 0
        .AllowUserResizing = flexResizeColumns
        '.AllowUserResizing = flexResizeNone
        .ColWidth(ColRowID) = 375
        .ColWidth(ColTipoCondicion) = 0
        .ColWidth(ColCodMoneda) = 2850
        .ColWidth(ColDesMoneda) = 2800
        .ColWidth(ColCodDenomina) = 2800
        .ColWidth(ColDesDenomina) = 2800
        
        AnchoCampoScroll = .ColWidth(ColCodMoneda)
        
    End With
    
End Sub

Private Sub Form_Load()
    
    If CancelarPromo_Salir Then Exit Sub
    
    lblTitulo.Caption = StellarMensajeLocal(125)
    lblCriterios.Caption = StellarMensaje(10164) 'criterios
    
    CmdAtras.Caption = StellarMensajeLocal(12)
    CmdSiguiente.Caption = StellarMensajeLocal(10)
    CmdCancelar.Caption = StellarMensajeLocal(11)
    
    lblMoneda.Caption = Replace(StellarMensaje(134), ":", Empty) '& "(*)" 'Moneda
    lblDenominacion.Caption = StellarMensaje(2008) 'Denominacion
    
    If TipoPromo = 16 Or TipoPromo = 17 _
    Or TipoPromo = 19 Or TipoPromo = 20 Then
        chkPagoMinimo.Caption = StellarMensajeLocal(273) & " " & StellarMensajeLocal(238) ' Solicitar Pago Minimo (% Fac)
    ElseIf TipoPromo = 18 Or TipoPromo = 21 Then
        'chkPagoMinimo.Caption = Replace(StellarMensajeLocal(239), "$(Curr)", Empty)
        chkPagoMinimo.Caption = StellarMensajeLocal(273) & " " & _
        Replace(StellarMensajeLocal(239), "$(Curr)", "(" & MonedaDoc.SimMoneda & ")") ' Solicitar Pago Minimo ($)
    End If
    
    'ChkVueltoPromocion.Caption = StellarMensajeLocal(242)
    
    Set MonedaTmp = FrmAppLink.ClaseMonedaTemp
    
    'txtMonedaLimite.Text = MonedaDoc.CodMoneda 'FrmAppLink.CodMonedaPref
    'lblMonedaLimite.Caption = MonedaDoc.DesMoneda 'FrmAppLink.DesMonedaPref
    'lblMonedaLimite.Tag = MonedaDoc.SimMoneda 'FrmAppLink.SimMonedaPref
    
    'ChkDctoLimiteFactura.Caption = StellarMensajeLocal(243) & " (" & MonedaDoc.SimMoneda & ")"
    
    lblDescBtnIncluir.Caption = StellarMensajeLocal(175) '"Incluir
    lblDescBtnExcluir.Caption = StellarMensajeLocal(176) '"Excluir
    lblDescBtnBorrar.Caption = StellarMensajeLocal(177) '"Eliminar Fila
    
    CmdExcluir.Visible = False
    lblDescBtnExcluir.Visible = False
    CmdArchivo.Left = CmdExcluir.Left
    lblArchivoPlano.Left = lblDescBtnExcluir.Left
    
    chkPagoMinimo.Value = vbChecked
    
    If TipoPromo = 16 Or TipoPromo = 19 Then
        txtValorMinimo = "100.00"
        chkPagoMinimo.Visible = False
        txtValorMinimo.Visible = False
    ElseIf TipoPromo = 17 Or TipoPromo = 18 Or TipoPromo = 20 Or TipoPromo = 21 Then
        chkPagoMinimo.Visible = True
        txtValorMinimo.Visible = True
        If TipoPromo = 17 Or TipoPromo = 20 Then
            chkPagoMinimo.Caption = StellarMensajeLocal(273) & " " & StellarMensajeLocal(238)
        ElseIf TipoPromo = 18 Or TipoPromo = 21 Then
            chkPagoMinimo.Caption = StellarMensajeLocal(273) & " " & _
            Replace(StellarMensajeLocal(239), "$(Curr)", "(" & MonedaDoc.SimMoneda & ")")
        End If
    End If
    
    CargarGrid
    CargarTemporal
    CargarPagoMinimo
    
    Call AjustarPantalla(Me)
    
    If Not FrmAppLink.DebugVBApp Then
        Call WheelHook(Me.hWnd)
    End If
    
End Sub

Private Sub Form_Activate()
    If CancelarPromo_Salir Or PromocionCreada Then
        Unload Me
    End If
End Sub

Private Sub Header_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        Case Is = "AGREGAR"
            'Call Form_KeyDown(vbKeyF3, 0)
        Case Is = "BUSCAR"
            'Call Form_KeyDown(vbKeyF2, 0)
        
        Case Is = "MODIFICAR"
            'Call Form_KeyDown(vbKeyF5, 0)
        
        Case Is = "CANCELAR"
            'Call Form_KeyDown(vbKeyF7, 0)
        
        Case Is = "BORRAR"
            ''Call Form_KeyDown(vbKeyF6, 0)
        
        Case Is = "GRABAR"
            'Call Grabar_Registro
            'Call Form_KeyDown(vbKeyF4, 0)
        
        Case Is = "IMPRIMIR"
            'Call imprimir_registro
        
        Case Is = "SALIR"
            Unload Me
            'Call Form_KeyDown(vbKeyF12, 0)
        
        Case Is = "AYUDA"
                
    End Select
    
End Sub

Private Sub InsertarCondicion(ByVal pTipo As Integer)
    
    On Error GoTo Error
    
    Dim RsCondicion As New ADODB.Recordset
    Dim RsValor As New ADODB.Recordset
    Dim SQL As String
    Dim SqlV As String
    
    Dim CampoPromo1 As Double
    'Dim CampoPromo2 As Double
    'Dim CampoPromo3 As Double
    
    If Trim(txtMoneda) <> Empty And Trim(txtDenominacion) <> Empty Then
        
        'If ((pTipo = 5) And IsNumeric(txtCampoPromo.Text)) Or (pTipo = 6) Then
            
            'If mValidarObligatorios Then
                
                Dim mRsDatos As ADODB.Recordset
                Set mRsDatos = New ADODB.Recordset
                Apertura_RecordsetC mRsDatos
                
                Dim mCriterio As String
                
                mCriterio = "AND MON.c_CodMoneda = '" & QuitarComillasSimples(txtMoneda.Text) & "' " & _
                "AND DEN.c_CodDenomina = '" & QuitarComillasSimples(txtDenominacion.Text) & "' "
                
                SQL = "SELECT * FROM MA_DENOMINACIONES DEN " & _
                "INNER JOIN MA_MONEDAS MON " & _
                "ON DEN.c_CodMoneda = MON.c_CodMoneda " & _
                "WHERE 1 = 1 " & mCriterio
                
                mRsDatos.Open SQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If mRsDatos.RecordCount <= 0 Then
                    If Not pCargandoArchivo Then
                        Call Mensaje(True, StellarMensaje(16441))
                    End If
                    Exit Sub
                End If
                
                If pTipo = 5 Then
                    
                    SQL = _
                    "INSERT INTO TMP_PROMOCION_CONDICION_VALORES" & GetLines & _
                    "(CodUsuario, Tipo_Condicion, Cod_Promocion, Cod_Dpto, Des_Dpto, " & GetLines & _
                    "Cod_Grupo, Des_Grupo)" & _
                    "SELECT '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' AS CodUsuario, " & _
                    pTipo & " AS Tipo_Condicion, '" & QuitarComillasSimples(mCodPromo_Actual) & "' AS Cod_Promocion, " & _
                    vbNewLine & _
                    "MON.c_CodMoneda, MON.c_Descripcion," & GetLines & _
                    "DEN.c_CodDenomina, DEN.c_Denominacion" & GetLines & _
                    "FROM MA_MONEDAS MON INNER JOIN MA_DENOMINACIONES DEN" & GetLines & _
                    "ON MON.c_CodMoneda = DEN.c_CodMoneda" & GetLines & _
                    "LEFT JOIN TMP_PROMOCION_CONDICION_VALORES COND" & GetLines & _
                    "ON MON.c_CodMoneda = COND.Cod_Dpto" & GetLines & _
                    "AND DEN.c_CodMoneda = COND.Cod_Dpto" & GetLines & _
                    "AND DEN.c_CodDenomina = COND.Cod_Grupo" & GetLines & _
                    "AND COND.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
                    "AND COND.CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "'" & GetLines & _
                    "AND COND.Tipo_Condicion IN (5) " & GetLines & _
                    "WHERE 1 = 1" & GetLines & _
                    mCriterio & GetLines & _
                    "AND COND.Cod_Dpto IS NULL "
                    
                    FrmAppLink.CnADM.Execute SQL, RecordsAffected
                    
                    If Not pCargandoArchivo Then
                        ShowTooltip StellarMensajeLocal(185), 700, 1250, CmdIncluir, 0, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "7", True)
                    End If
                    
                    pCargarLinea = True
                    
                'ElseIf pTipo = 6 Then
                    
                End If
                
                ResetearCriterios
                
            'Else
                
            'End If
            
        'Else
            'If Not pCargandoArchivo Then
                'Mensaje True, StellarMensajeLocal(123) '"Los datos deben ser num�ricos y positivos."
                'If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                'ShowTooltip "<--", 500, 800, txtCampoPromo, 3, _
                &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
            'End If
            'Exit Sub
        'End If
        
    Else
        If Not pCargandoArchivo Then
            Mensaje True, StellarMensaje(16404) '"Debe seleccionar al menos un criterio"
        End If
    End If
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    If Not pCargandoArchivo Then
        MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(InsertarCondicion)"
    End If
    
End Sub

Private Sub CmdIncluir_Click()
    'If mVarEsPremio Then
        'InsertarCondicion 3
    'Else
        InsertarCondicion 5
    'End If
    If Not pCargandoArchivo Then
        CargarTemporal
    End If
End Sub

Private Sub CmdExcluir_Click()
    'CargarTemporal
End Sub

Private Sub CargarPagoMinimo()
    
    If TipoPromo = 16 Or TipoPromo = 19 Then
        TmpValorMinimo = 100
    Else
        
        TmpValorMinimo = SDec(BuscarValorBD("ValorMinimo", _
        "SELECT Top 1 Porcentaje_Descuento1 AS ValorMinimo FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND Tipo_Condicion = 6 ", "0", FrmAppLink.CnADM))
        
    End If
    
    If TmpValorMinimo > 0 Then
        txtValorMinimo = TmpValorMinimo
    Else
        txtValorMinimo = 0
    End If
    
    SeleccionarTexto txtValorMinimo
    
End Sub

Private Sub CargarTemporal()
    
    On Error GoTo Error
    
    'CargarGrid
    
    Dim LastRow As Integer, LastCtl As Object
    Dim NumRows As Double, NumPag As Double
    Dim RsCondicion As ADODB.Recordset
    Set RsCondicion = New ADODB.Recordset
    
    Dim mSQL As String
    
    mSQL = "SELECT isNULL(COUNT(ID), 0) AS NumReg " & GetLines & _
    "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & GetLines & _
    "AND Tipo_Condicion = 5 " & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' "
    
    Set RsCondicion = FrmAppLink.CnADM.Execute(mSQL)
    
    With RsCondicion
        
        If Not .EOF Then
            
            NumRows = !NumReg
            
            If NumRows > 0 Then
                
                NumPag = Round(NumRows / MaxGridRowsxPag_DetPag, 8)
                If (NumPag - Fix(NumPag)) <> 0 Then NumPag = Fix(NumPag + 1)
                
                TxtNumPag.Text = NumPag
                
                txtPag.Text = Fix(SVal(txtPag))
                
                If SVal(txtPag.Text) >= 1 And SVal(txtPag.Text) <= NumPag Then
                    txtPag.Text = Fix(SVal(txtPag))
                Else
                    txtPag.Text = 1
                End If
                
                mSQL = "WITH AllRows AS (" & GetLines & _
                "SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowID, *" & GetLines & _
                "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
                "WHERE CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
                "AND Tipo_Condicion = 5 " & _
                "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
                ")" & GetLines & _
                "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                "((" & txtPag.Text & " - 1) * (" & MaxGridRowsxPag_DetPag & ") + 1) AND " & _
                "(" & txtPag.Text & " * " & MaxGridRowsxPag_DetPag & ")" & GetLines
                
                RsCondicion.Close
                
                Set RsCondicion = New Recordset
                Apertura_RecordsetC RsCondicion
                
                .Open mSQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If Not .EOF Then
                    
                    .Sort = "RowID"
                    
                    Dim I As Integer
                    
                    LastRow = Grid.Row
                    Set LastCtl = Screen.ActiveControl
                    
                    Grid.Visible = False
                    
                    Grid.Rows = 1
                    
                    ScrollGrid.Min = 0
                    ScrollGrid.Max = .RecordCount
                    ScrollGrid.Value = ScrollGrid.Min
                    
                    While Not .EOF
                        
                        Grid.Rows = Grid.Rows + 1
                        I = I + 1
                        
                        Grid.TextMatrix(I, ColRowID) = I
                        Grid.TextMatrix(I, ColTipoCondicion) = !Tipo_Condicion
                        Grid.TextMatrix(I, ColCodMoneda) = !Cod_Dpto
                        Grid.TextMatrix(I, ColDesMoneda) = !Des_Dpto
                        Grid.TextMatrix(I, ColCodDenomina) = !Cod_Grupo
                        Grid.TextMatrix(I, ColDesDenomina) = !Des_Grupo
                        
                        If Trim(Grid.TextMatrix(I, ColDesMoneda)) = Empty Then
                            Grid.TextMatrix(I, ColDesMoneda) = BuscarValorBD("c_Descripcion", _
                            "SELECT c_Descripcion FROM MA_MONEDAS " & _
                            "WHERE c_CodMoneda = '" & QuitarComillasSimples(Grid.TextMatrix(I, ColCodMoneda)) & "' ")
                        End If
                        
                        If Trim(Grid.TextMatrix(I, ColDesDenomina)) = Empty Then
                            Grid.TextMatrix(I, ColDesDenomina) = BuscarValorBD("c_Denominacion", _
                            "SELECT c_Denominacion FROM MA_DENOMINACIONES " & _
                            "WHERE c_CodMoneda = '" & QuitarComillasSimples(Grid.TextMatrix(I, ColCodMoneda)) & "' " & _
                            "AND c_CodDenomina = '" & QuitarComillasSimples(Grid.TextMatrix(I, ColCodDenomina)) & "' ")
                        End If
                        
                        .MoveNext
                        
                    Wend
                    
                    Grid.Visible = True
                    
                    If LastRow >= 1 And LastRow <= Grid.Rows - 1 Then
                        Grid.Row = LastRow
                        Grid.TopRow = Grid.Row
                    Else
                        Grid.Row = 1
                        Grid.TopRow = Grid.Row
                    End If
                    
                    If Not LastCtl Is Nothing Then
                        If PuedeObtenerFoco(LastCtl) Then LastCtl.SetFocus
                    End If
                    
                End If
                
            Else
                NumPag = 1
                txtPag.Text = NumPag
                Grid.Rows = 1
                Grid.Rows = 2
            End If
            
            If Grid.Rows > 8 Then
                ScrollGrid.Visible = True
                Grid.ScrollBars = flexScrollBarVertical
                AnchoScrollBar = ScrollGrid.Width
                Grid.ColWidth(ColCodMoneda) = (AnchoCampoScroll - AnchoScrollBar)
            Else
                Grid.ScrollBars = flexScrollBarHorizontal
                ScrollGrid.Visible = False
                AnchoScrollBar = 0
                Grid.ColWidth(ColCodMoneda) = AnchoCampoScroll
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    'Resume ' Debug
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CargarTemporal)"
    
    CargarGrid
    Grid.Visible = True
    
End Sub

Private Sub CmdDelete_Click()
    
    If Grid.Row > 0 And Grid.TextMatrix(Grid.Row, ColTipoCondicion) <> Empty Then
        
        On Error GoTo Error
        
        Dim RowD As Integer, mSQL As String
        
        RowD = Grid.Row
        
        mSQL = "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
        "WHERE Cod_Dpto = '" & QuitarComillasSimples(Grid.TextMatrix(RowD, ColCodMoneda)) & "' " & GetLines & _
        "AND Cod_Grupo = '" & QuitarComillasSimples(Grid.TextMatrix(RowD, ColCodDenomina)) & "' " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
        "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
        "AND Tipo_Condicion = " & QuitarComillasSimples(Grid.TextMatrix(RowD, ColTipoCondicion)) & " " & GetLines
        
        FrmAppLink.CnADM.Execute mSQL
        
        CargarTemporal
        
        Grid.ColSel = ColCount - 1
        
    End If
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub txtDenominacion_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Or KeyCode = vbKeyBack Then
        txtDenominacion.Text = Empty
        txtDenominacion.Tag = Empty
        lblDescDenomina.Caption = Empty
        lblDescDenomina.Tag = Empty
    End If
End Sub

Private Sub txtMoneda_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Or KeyCode = vbKeyBack Then
        txtMoneda.Text = Empty
        txtMoneda.Tag = Empty
        lblDescMoneda.Caption = Empty
        lblDescMoneda.Tag = Empty
    End If
End Sub

Private Sub txtPag_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtPag_GotFocus()
    Set CampoT = txtPag
    txtPag.Tag = SVal(txtPag.Text)
End Sub

Private Sub txtPag_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtPag_LostFocus
    End If
End Sub

Private Sub txtPag_LostFocus()
    If SVal(txtPag.Text) <> SVal(txtPag.Tag) Then
        CargarTemporal
    End If
End Sub

Private Sub ResetearCriterios( _
Optional pCheckAll As Boolean = False)
    'txtMoneda.Text = Empty
    'lblDescMoneda.Caption = Empty
    txtDenominacion.Text = Empty
    lblDescDenomina.Caption = Empty
End Sub

Private Sub txtValorMinimo_GotFocus()
    Set CampoT = txtValorMinimo
    SeleccionarTexto CampoT
End Sub
