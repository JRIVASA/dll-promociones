VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Begin VB.Form REP_PROMOCIONES_APLICADAS 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9630
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12975
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "REP_PROMOCIONES_APLICADAS.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9630
   ScaleWidth      =   12975
   Begin VB.CommandButton CmdOpenXML 
      Height          =   495
      Left            =   7725
      Picture         =   "REP_PROMOCIONES_APLICADAS.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   54
      Top             =   8300
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   65
      Top             =   0
      Width           =   13020
      Begin VB.Label lbl_Website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   10815
         TabIndex        =   67
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Reporte de Promociones Aplicadas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   66
         Top             =   75
         Width           =   6135
      End
   End
   Begin MSComDlg.CommonDialog rutaXml 
      Left            =   6720
      Top             =   8400
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&XML"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Index           =   2
      Left            =   7440
      Picture         =   "REP_PROMOCIONES_APLICADAS.frx":99C2
      Style           =   1  'Graphical
      TabIndex        =   53
      ToolTipText     =   "Vista Preliminar (F2)"
      Top             =   8160
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   " "
      ForeColor       =   &H80000008&
      Height          =   1845
      Left            =   120
      TabIndex        =   60
      Top             =   7635
      Width           =   6330
      Begin VB.ListBox Seleccionados 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   840
         Left            =   3840
         TabIndex        =   52
         Top             =   510
         Width           =   2310
      End
      Begin VB.CommandButton Borrar 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   435
         Left            =   2610
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":B744
         Style           =   1  'Graphical
         TabIndex        =   51
         ToolTipText     =   "Eliminar todos"
         Top             =   1350
         Width           =   780
      End
      Begin VB.CommandButton eliminar 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   435
         Left            =   2610
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":BF46
         Style           =   1  'Graphical
         TabIndex        =   50
         ToolTipText     =   "Eliminar de la lista de selecci�n"
         Top             =   855
         Width           =   780
      End
      Begin VB.CommandButton add 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   495
         Left            =   2610
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":C748
         Style           =   1  'Graphical
         TabIndex        =   49
         ToolTipText     =   "A�adir a la lista de selecci�n"
         Top             =   300
         Width           =   780
      End
      Begin VB.ListBox Ordenar 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   840
         Left            =   150
         TabIndex        =   48
         Top             =   510
         Width           =   2310
      End
      Begin VB.Label lblCriteriosOrden 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios de Ordenamiento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   0
         TabIndex        =   69
         Top             =   0
         Width           =   2265
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00AE5B00&
         X1              =   2415
         X2              =   6000
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   6
         Left            =   3555
         TabIndex        =   64
         ToolTipText     =   "Ordenar ..."
         Top             =   1320
         Width           =   180
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   5
         Left            =   3585
         TabIndex        =   63
         ToolTipText     =   "Ordenar Tercero"
         Top             =   1095
         Width           =   105
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   4
         Left            =   3585
         TabIndex        =   62
         ToolTipText     =   "Ordenar Segundo"
         Top             =   780
         Width           =   105
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   3
         Left            =   3585
         TabIndex        =   61
         ToolTipText     =   "Ordenar Primero"
         Top             =   480
         Width           =   105
      End
   End
   Begin VB.Frame FrameCriterios 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   7000
      Left            =   120
      TabIndex        =   58
      Top             =   525
      Width           =   12780
      Begin VB.TextBox txtMonedaVer 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1980
         Locked          =   -1  'True
         TabIndex        =   46
         ToolTipText     =   "Ingrese el c�digo de la moneda"
         Top             =   6400
         Width           =   1995
      End
      Begin VB.CommandButton CmdMonedaVer 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   345
         Left            =   4140
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":CF4A
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   6400
         Width           =   330
      End
      Begin VB.TextBox txtDec 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   6360
         TabIndex        =   43
         Text            =   "2"
         Top             =   5760
         Width           =   675
      End
      Begin VB.CheckBox ChkRedondear 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Redondear Cifras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4200
         TabIndex        =   42
         Top             =   5820
         Value           =   1  'Checked
         Width           =   2070
      End
      Begin VB.ComboBox CmbModalidad 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "REP_PROMOCIONES_APLICADAS.frx":D74C
         Left            =   120
         List            =   "REP_PROMOCIONES_APLICADAS.frx":D753
         Style           =   2  'Dropdown List
         TabIndex        =   41
         Top             =   5760
         Width           =   3840
      End
      Begin VB.TextBox txtTarjeta 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   9480
         TabIndex        =   40
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   4560
         Width           =   2655
      End
      Begin VB.CheckBox ChkPorTarjeta 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Tarjeta:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   8280
         TabIndex        =   39
         Top             =   4605
         Width           =   1110
      End
      Begin VB.TextBox txtBIN 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   5400
         TabIndex        =   38
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   4560
         Width           =   2655
      End
      Begin VB.CheckBox ChkPorBIN 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "BIN:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4320
         TabIndex        =   37
         Top             =   4605
         Width           =   870
      End
      Begin VB.CommandButton CmdFormaPago 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   450
         Left            =   10665
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":D75E
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   3840
         Width           =   500
      End
      Begin VB.TextBox txtFormaPago 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   7560
         Locked          =   -1  'True
         TabIndex        =   33
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   3855
         Width           =   3015
      End
      Begin VB.CheckBox ChkPorFormaPago 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Forma de Pago:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5520
         TabIndex        =   32
         Top             =   3900
         Width           =   1950
      End
      Begin VB.CommandButton CmdMoneda 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   450
         Left            =   4785
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":DF60
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   3840
         Width           =   500
      End
      Begin VB.TextBox txtMoneda 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   30
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   3855
         Width           =   3015
      End
      Begin VB.CheckBox ChkPorMoneda 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Moneda:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   29
         Top             =   3900
         Width           =   1350
      End
      Begin VB.TextBox txtTurno 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   1320
         TabIndex        =   36
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   4560
         Width           =   2655
      End
      Begin VB.CheckBox ChkPorTurno 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Turno:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   35
         Top             =   4605
         Width           =   990
      End
      Begin VB.CommandButton CmdCaja 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   450
         Left            =   10665
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":E762
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   3120
         Width           =   500
      End
      Begin VB.TextBox txtCaja 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   7560
         TabIndex        =   27
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   3135
         Width           =   3015
      End
      Begin VB.CheckBox ChkPorCaja 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Caja:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5520
         TabIndex        =   26
         Top             =   3180
         Width           =   1350
      End
      Begin VB.CommandButton CmdProducto 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   450
         Left            =   4785
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":EF64
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   3120
         Width           =   500
      End
      Begin VB.TextBox txtProducto 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   1680
         TabIndex        =   24
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   3135
         Width           =   3015
      End
      Begin VB.CheckBox ChkPorProducto 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Producto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   23
         Top             =   3180
         Width           =   1350
      End
      Begin VB.CommandButton CmdLocalidad 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   450
         Left            =   10665
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":F766
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   1680
         Width           =   500
      End
      Begin VB.TextBox txtLocalidades 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   7560
         TabIndex        =   15
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   1695
         Width           =   3015
      End
      Begin VB.CheckBox ChkPorLocalidad 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Localidad:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5520
         TabIndex        =   14
         Top             =   1740
         Width           =   1350
      End
      Begin VB.CommandButton CmdOrigenDoc 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         Height          =   450
         Left            =   4785
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":FF68
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   2400
         Width           =   500
      End
      Begin VB.TextBox txtOrigenDoc 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   18
         Text            =   "POS"
         Top             =   2415
         Width           =   3015
      End
      Begin VB.CheckBox ChkPorOrigenDoc 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Origen Doc.:"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   17
         Top             =   2460
         Value           =   1  'Checked
         Width           =   1470
      End
      Begin VB.CommandButton CmdDocumento 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   450
         Left            =   10665
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":1076A
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   2400
         Visible         =   0   'False
         Width           =   500
      End
      Begin VB.TextBox txtDocumento 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   7560
         TabIndex        =   21
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   2415
         Width           =   3015
      End
      Begin VB.CheckBox ChkPorDocumento 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Documento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5520
         TabIndex        =   20
         Top             =   2460
         Width           =   1470
      End
      Begin VB.OptionButton OptTipoOtros 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Personalizado / Otro"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   6000
         TabIndex        =   8
         Top             =   1140
         Value           =   -1  'True
         Width           =   2280
      End
      Begin VB.OptionButton OptTipoDetPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "De Formas de Pago"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3840
         TabIndex        =   7
         Top             =   1140
         Width           =   2040
      End
      Begin VB.OptionButton OptTipoItems 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "De Art�culos"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   2280
         TabIndex        =   6
         Top             =   1140
         Width           =   1440
      End
      Begin VB.CheckBox ChkVerTopRows 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Top"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   8520
         TabIndex        =   44
         Top             =   5820
         Width           =   630
      End
      Begin VB.TextBox txtTopNRows 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   9240
         TabIndex        =   45
         Text            =   "50"
         Top             =   5760
         Width           =   675
      End
      Begin VB.CheckBox ChkPorPromocion 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Promoci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   11
         Top             =   1740
         Width           =   1350
      End
      Begin VB.CheckBox ChkPorTipoPromocion 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Tipo de Promoci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   5
         Top             =   1140
         Width           =   2025
      End
      Begin VB.CheckBox chkVerFechas 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Desde:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   0
         Top             =   540
         Value           =   1  'Checked
         Width           =   990
      End
      Begin VB.TextBox txtPromocion 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   12
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   1695
         Width           =   3015
      End
      Begin VB.CommandButton CmdPromocion 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   450
         Left            =   4785
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":10F6C
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   1680
         Width           =   500
      End
      Begin VB.TextBox txtTipoPromocion 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   8460
         Locked          =   -1  'True
         TabIndex        =   9
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   1035
         Width           =   3375
      End
      Begin VB.CommandButton CmdTipoPromocion 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   450
         Left            =   11925
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":1176E
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1020
         Width           =   500
      End
      Begin VB.CommandButton CmdFechaFin 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   450
         Left            =   9600
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":11F70
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   480
         Visible         =   0   'False
         Width           =   500
      End
      Begin VB.TextBox txtFechaFin 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   3
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   480
         Width           =   3250
      End
      Begin VB.CommandButton CmdFechaIni 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   450
         Left            =   4620
         Picture         =   "REP_PROMOCIONES_APLICADAS.frx":12772
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   480
         Visible         =   0   'False
         Width           =   500
      End
      Begin VB.TextBox txtFechaIni 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   1260
         Locked          =   -1  'True
         TabIndex        =   1
         ToolTipText     =   "Indique el c�digo de la sucursal"
         Top             =   480
         Width           =   3250
      End
      Begin VB.Label lblMonedaVer 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Vista de Moneda:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   75
         Top             =   6405
         Width           =   1740
      End
      Begin VB.Label lblDescMonedaVer 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4605
         TabIndex        =   74
         Top             =   6405
         Width           =   3330
      End
      Begin VB.Label lblCaptionDec 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Decimales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   7200
         TabIndex        =   73
         Top             =   5820
         Width           =   1110
      End
      Begin VB.Label lblModalidad 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Modalidad:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   105
         TabIndex        =   72
         Top             =   5400
         Width           =   960
      End
      Begin VB.Label lblTopNRegistros 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Registros"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   10110
         TabIndex        =   71
         Top             =   5820
         Width           =   810
      End
      Begin VB.Label lblCriteriosBusqueda 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "  Criterios de Busqueda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   0
         TabIndex        =   68
         Top             =   60
         Width           =   1995
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00AE5B00&
         X1              =   2220
         X2              =   12500
         Y1              =   180
         Y2              =   180
      End
      Begin VB.Label lblFechaFin 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5400
         TabIndex        =   59
         Top             =   540
         Width           =   690
      End
   End
   Begin VB.CommandButton cmd_salir 
      Appearance      =   0  'Flat
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Left            =   11040
      Picture         =   "REP_PROMOCIONES_APLICADAS.frx":12F74
      Style           =   1  'Graphical
      TabIndex        =   57
      ToolTipText     =   "Salir del Reporte (F3)"
      Top             =   8160
      Width           =   1095
   End
   Begin VB.CommandButton aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Impresora"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Index           =   0
      Left            =   9840
      Picture         =   "REP_PROMOCIONES_APLICADAS.frx":14CF6
      Style           =   1  'Graphical
      TabIndex        =   56
      ToolTipText     =   "Imprimir Reporte (F8)"
      Top             =   8160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Pantalla"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Index           =   1
      Left            =   8640
      Picture         =   "REP_PROMOCIONES_APLICADAS.frx":16A78
      Style           =   1  'Graphical
      TabIndex        =   55
      ToolTipText     =   "Vista Preliminar (F2)"
      Top             =   8160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label10 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Tipo de Objeto Auditado:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   240
      TabIndex        =   70
      Top             =   6000
      Width           =   2160
   End
End
Attribute VB_Name = "REP_PROMOCIONES_APLICADAS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Criterio As String
Private lblCriterio As String

Private Tecla_Pulsada As Boolean

Const AliasTblDetallePromo = "DET"
Private BaseSelect As String

Private mResp As Variant

Private Enum CriterioBuscar
    Todos
    FechaIni
    FechaFin
    ProductoStellar
    TipoAuditoria
    Descripcion
    Detalle
    CodUsuario
    Usuario
    Proceso
    TipoObjAuditar
    CodigoAuditado
    CodigoRetorno
    AccionRealizada
End Enum

Private cb As CriterioBuscar

Private Arr_Promociones As Object
Private Arr_TipoPromo As Object
Private Arr_Localidades As Object
Private Arr_OrigenDoc As Object
Private Arr_Producto As Object
Private Arr_Cajas As Object
Private Arr_Monedas As Object
Private Arr_Denominaciones As Object

Private Sub ChkPorBIN_Click()
    If ChkPorBIN.Value = vbUnchecked Then
        txtBIN.Text = Empty
    End If
End Sub

Private Sub ChkPorCaja_Click()
    If ChkPorCaja.Value = vbUnchecked Then
        txtCaja.Text = Empty
    End If
End Sub

Private Sub ChkPorDocumento_Click()
    If ChkPorDocumento.Value = vbUnchecked Then
        txtDocumento.Text = Empty
    End If
End Sub

Private Sub ChkPorFormaPago_Click()
    If ChkPorFormaPago.Value = vbUnchecked Then
        txtFormaPago.Text = Empty
    End If
End Sub

Private Sub ChkPorLocalidad_Click()
    If ChkPorLocalidad.Value = vbUnchecked Then
        txtLocalidades.Text = Empty
    End If
End Sub

Private Sub ChkPorMoneda_Click()
    If ChkPorMoneda.Value = vbUnchecked Then
        txtMoneda.Text = Empty
    End If
End Sub

Private Sub ChkPorOrigenDoc_Click()
    If ChkPorOrigenDoc.Value = vbUnchecked Then
        txtOrigenDoc.Text = Empty
    End If
End Sub

Private Sub ChkPorProducto_Click()
    If ChkPorProducto.Value = vbUnchecked Then
        txtProducto.Text = Empty
    End If
End Sub

Private Sub ChkPorPromocion_Click()
    If ChkPorPromocion.Value = vbUnchecked Then
        txtPromocion.Text = Empty
    End If
End Sub

Private Sub ChkPorTarjeta_Click()
    If ChkPorTarjeta.Value = vbUnchecked Then
        txtTarjeta.Text = Empty
    End If
End Sub

Private Sub ChkPorTipoPromocion_Click()
    If ChkPorTipoPromocion.Value = vbUnchecked Then
        OptTipoOtros.Value = True
        txtTipoPromocion.Text = Empty
    End If
End Sub

Private Sub ChkPorTurno_Click()
    If ChkPorTurno.Value = vbUnchecked Then
        txtTurno.Text = Empty
    End If
End Sub

Private Sub chkVerFechas_Click()
    If chkVerFechas.Value = vbUnchecked Then
        txtFechaIni.Text = Empty
        txtFechaIni.Tag = Empty
        txtFechaFin.Text = Empty
        txtFechaFin.Tag = Empty
    End If
End Sub

Private Sub CmbModalidad_Click()
    RefreshForm Me
End Sub

Private Sub CmdFechaFin_Click()
    
    'ObtenerCriterioGlobal FechaFin
    
    BaseSelect = "SELECT TOP 100 CAST(Fecha AS DATE) AS Fecha " & vbNewLine & _
    "FROM MA_AUDITORIAS " & AliasTblDetallePromo & vbNewLine & _
    Criterio & _
    "GROUP BY CAST(Fecha AS DATE)"
    
    BaseSelect = "SELECT Fecha, CONVERT(NVARCHAR(MAX), Fecha, 111) AS YYYYMMDD FROM (" & vbNewLine & _
    BaseSelect & vbNewLine & ") AUD " & vbNewLine & "WHERE 1 = 1"
    
    Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With Frm_Super_Consultas
        
        .Inicializar BaseSelect, Empty, FrmAppLink.CnPos
        
        .Add_ItemLabels "" & StellarMensaje(128) & "", "Fecha", 32767, ListColumnAlignmentConstants.lvwColumnLeft, , Array("Fecha", "VbShortDate")
        .Add_ItemLabels "YYYYMMDD", "YYYYMMDD", 0, ListColumnAlignmentConstants.lvwColumnRight
        
        .Add_ItemSearching "YYYY-MM-DD", "CONVERT(NVARCHAR(MAX), Fecha, 23)"
        
        .ModoDespliegueInstantaneo = True
        .DespliegueInstantaneo_RetornarCualquierValor = False
        .BusquedaInstantanea = True
        
        .StrOrderBy = "Fecha DESC"
        
        .CustomFontSize = 14
        
        .Show vbModal
        
        mResp = .ArrResultado
        
    End With
    
    If Not IsEmpty(mResp) Then
        txtFechaFin.Tag = CDbl(CDate(mResp(1)))
        txtFechaFin.Text = GDate(CDate(txtFechaFin.Tag))
    Else
        txtFechaFin.Text = Empty
        txtFechaFin.Tag = txtFechaFin.Text
    End If
    
End Sub

Private Sub CmdFechaIni_Click()
    
    'ObtenerCriterioGlobal FechaIni
    
    BaseSelect = "SELECT TOP 100 CAST(Fecha AS DATE) AS Fecha " & vbNewLine & _
    "FROM MA_TRANSACCION_DETALLE_PROMO " & AliasTblDetallePromo & vbNewLine & _
    Criterio & _
    "GROUP BY CAST(Fecha AS DATE)"
    
    BaseSelect = "SELECT Fecha, CONVERT(NVARCHAR(MAX), Fecha, 111) AS YYYYMMDD FROM (" & vbNewLine & _
    BaseSelect & vbNewLine & ") AUD " & vbNewLine & "WHERE 1 = 1"
    
    Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With Frm_Super_Consultas
        
        .Inicializar BaseSelect, Empty, FrmAppLink.CnPos
        
        .Add_ItemLabels "" & StellarMensaje(128) & "", _
        "Fecha", 32767, ListColumnAlignmentConstants.lvwColumnLeft, , Array("Fecha", "VbShortDate")
        .Add_ItemLabels "YYYYMMDD", "YYYYMMDD", 0, ListColumnAlignmentConstants.lvwColumnRight
        
        .Add_ItemSearching "YYYY-MM-DD", "CONVERT(NVARCHAR(MAX), Fecha, 23)"
        
        .ModoDespliegueInstantaneo = True
        .DespliegueInstantaneo_RetornarCualquierValor = False
        .BusquedaInstantanea = True
        
        .StrOrderBy = "Fecha DESC"
        
        .CustomFontSize = 14
        
        .Show vbModal
        
        mResp = .ArrResultado
        
    End With
    
    If Not IsEmpty(mResp) Then
        txtFechaIni.Tag = CDbl(CDate(mResp(1)))
        txtFechaIni.Text = GDate(CDate(txtFechaIni.Tag))
    Else
        txtFechaIni.Text = Empty
        txtFechaIni.Tag = txtFechaIni.Text
    End If
    
End Sub

Private Sub CmdMonedaVer_Click()
    txtMonedaVer.SetFocus
    Call txtMonedaVer_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txtMonedaVer_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            'Tecla_Pulsada = True
            Set Campo_Txt = txtMonedaVer
            Set Campo_Lbl = lblDescMonedaVer
            Call MAKE_VIEW("MA_MONEDAS", "c_CodMoneda", "c_Descripcion", "" & StellarMensaje(2855) & "", _
            Me, "GENERICO", True, Campo_Txt, Campo_Lbl)
            'Tecla_Pulsada = False
    End Select
End Sub

Private Sub txtMonedaVer_LostFocus()
    Set MonedaTmp = FrmAppLink.ClaseMonedaTemp
    MonedaTmp.BuscarMonedas , txtMonedaVer.Text
    txtMonedaVer.Text = MonedaTmp.CodMoneda
    lblDescMonedaVer.Caption = MonedaTmp.DesMoneda
    lblDescMonedaVer.Tag = MonedaTmp.SimMoneda
End Sub

' Tipos de Promocion

Private Sub CmdTipoPromocion_Click()
    Call txtTipoPromocion_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txtDocumento_Change()
    If Trim(txtDocumento.Text) = Empty Then
        ChkPorDocumento.Value = vbUnchecked
    Else
        ChkPorDocumento.Value = vbChecked
    End If
End Sub

Private Sub txtTipoPromocion_Click()
    
    If Arr_TipoPromo Is Nothing Then
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_TipoPromo
            ListaStr = ListaStr & vbNewLine & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensajeLocal(274) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, txtTipoPromocion, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
    
End Sub

Private Sub txtTipoPromocion_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            BaseSelect = _
            "SELECT 1 AS TipoPromo, '" & "" & StellarMensajeLocal(104) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 2 AS TipoPromo, '" & "" & StellarMensajeLocal(105) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 3 AS TipoPromo, '" & "" & StellarMensajeLocal(106) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 4 AS TipoPromo, '" & "" & StellarMensajeLocal(110) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 5 AS TipoPromo, '" & "" & StellarMensajeLocal(111) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 6 AS TipoPromo, '" & "" & StellarMensajeLocal(112) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 7 AS TipoPromo, '" & "" & StellarMensajeLocal(113) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 8 AS TipoPromo, '" & "" & StellarMensajeLocal(188) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 9 AS TipoPromo, '" & "" & StellarMensajeLocal(189) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 10 AS TipoPromo, '" & "" & StellarMensajeLocal(190) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 11 AS TipoPromo, '" & "" & StellarMensajeLocal(191) & "" & "' AS Descripcion " & vbNewLine
            
            BaseSelect = BaseSelect & _
            "UNION ALL " & vbNewLine & _
            "SELECT 16 AS TipoPromo, '" & "" & StellarMensajeLocal(245) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 17 AS TipoPromo, '" & "" & StellarMensajeLocal(247) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 18 AS TipoPromo, '" & "" & StellarMensajeLocal(249) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 19 AS TipoPromo, '" & "" & StellarMensajeLocal(245) & " + BIN" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 20 AS TipoPromo, '" & "" & StellarMensajeLocal(247) & " + BIN" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 21 AS TipoPromo, '" & "" & StellarMensajeLocal(249) & " + BIN" & "' AS Descripcion " & vbNewLine
            
            BaseSelect = BaseSelect & _
            "SELECT * FROM ( " & vbNewLine & _
            BaseSelect & _
            ") TB " & vbNewLine
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, "" & StellarMensajeLocal(275) & "", FrmAppLink.CnADM, , True
                
                .Add_ItemLabels "" & "" & StellarMensajeLocal(276) & "" & "", "TipoPromo", 2500, 0
                .Add_ItemLabels "" & "" & StellarMensaje(143) & "" & "", "Descripcion", 7500, 0
                
                .Add_ItemSearching "" & StellarMensaje(143) & "", "Descripcion"
                
                .ModoDespliegueInstantaneo = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .BusquedaInstantanea = True
                
                .StrOrderBy = "TipoPromo ASC"
                
                .CustomFontSize = 14
                
                .Show vbModal
                
                Set Arr_TipoPromo = .ArrResultado
                
                If Arr_TipoPromo Is Nothing Then
                    txtTipoPromocion.Text = Empty
                    ChkPorTipoPromocion.Value = vbUnchecked
                    RefreshForm Me
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_TipoPromo
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    txtTipoPromocion.Text = ListaStr
                    
                    txtTipoPromocion_Click
                    
                    ChkPorTipoPromocion.Value = vbChecked
                    
                End If
                
            End With
            
            Set Frm_Super_Consultas = Nothing
            
    End Select
    
End Sub

' C�digos de Promoci�n

Private Sub CmdPromocion_Click()
    Call txtPromocion_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txtPromocion_Click()
    
    If Arr_Promociones Is Nothing Then
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_Promociones
            ListaStr = ListaStr & vbNewLine & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensajeLocal(277) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, txtPromocion, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
    
End Sub

Private Sub txtPromocion_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            BaseSelect = _
            "SELECT Cod_Promocion, Descripcion, Campa�a, Fecha_Inicio, Fecha_Fin " & _
            "FROM MA_PROMOCION " & _
            "WHERE 1 = 1 " '& _
            'IIf(txtFechaIni.Text <> Empty, _
            "AND Fecha_Inicio >= '" & FechaBD(txtFechaIni.Tag, FBD_FULL) & "' ", Empty) & _
            'IIf(txtFechaFin.Text <> Empty, _
            "AND Fecha_Fin <= '" & FechaBD(txtFechaFin.Tag, FBD_FULL) & "' ", Empty)
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, "" & StellarMensajeLocal(131) & "", FrmAppLink.CnADM, , True
                
                .Add_ItemLabels "" & "" & StellarMensaje(142) & "" & "", "Cod_Promocion", 2200, 0
                .Add_ItemLabels "" & "" & StellarMensaje(143) & "" & "", "Descripcion", 2800, 0
                .Add_ItemLabels "" & "" & StellarMensajeLocal(136) & "" & "", "Campa�a", 2450, 0
                .Add_ItemLabels "" & "" & StellarMensajeLocal(16) & "" & "", "Fecha_Inicio", 1830, 0
                .Add_ItemLabels "" & "" & StellarMensajeLocal(17) & "" & "", "Fecha_Fin", 1820, 0
                
                .Add_ItemSearching "" & StellarMensaje(143) & "", "Descripcion"
                .Add_ItemSearching "" & StellarMensajeLocal(136) & "", "Campa�a"
                .Add_ItemSearching "" & StellarMensaje(142) & "", "Cod_Promocion"
                .Add_ItemSearching "" & StellarMensajeLocal(16) & " YYYY-MM-DD", "CONVERT(NVARCHAR(MAX), Fecha_Inicio, 23)"
                
                .ModoDespliegueInstantaneo = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .BusquedaInstantanea = True
                
                .StrOrderBy = "Fecha_Inicio DESC"
                
                .CustomFontSize = 14
                
                .Show vbModal
                
                Set Arr_Promociones = .ArrResultado
                
                If Arr_Promociones Is Nothing Then
                    txtPromocion.Text = Empty
                    ChkPorPromocion.Value = vbUnchecked
                    RefreshForm Me
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_Promociones
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    txtPromocion.Text = ListaStr
                    
                    txtPromocion_Click
                    
                    ChkPorPromocion.Value = vbChecked
                    
                End If
                
            End With
            
            Set Frm_Super_Consultas = Nothing
            
    End Select
    
End Sub

' Localidades

Private Sub CmdLocalidad_Click()
    Call txtLocalidades_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txtLocalidades_Click()
    
    If Arr_Localidades Is Nothing Then
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_Localidades
            ListaStr = ListaStr & vbNewLine & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensaje(2077) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, txtLocalidades, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
    
End Sub

Private Sub txtLocalidades_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            BaseSelect = "SELECT c_Codigo, c_Descripcion FROM MA_SUCURSALES "
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, StellarMensaje(1251), FrmAppLink.CnADM, , True
                
                .Add_ItemLabels StellarMensaje(142), "c_Codigo", 2000, 0
                .Add_ItemLabels StellarMensaje(143), "c_Descripcion", 9000, 0
                
                .Add_ItemSearching StellarMensaje(143), "c_Descripcion"
                .Add_ItemSearching StellarMensaje(142), "c_Codigo"
                
                .StrOrderBy = "c_Descripcion"
                
                .BusquedaInstantanea = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .ModoDespliegueInstantaneo = True
                
                .CustomFontSize = 16
                
                .Show vbModal
                
                Set Arr_Localidades = .ArrResultado
                
                If Arr_Localidades Is Nothing Then
                    txtLocalidades.Text = Empty
                    ChkPorLocalidad.Value = vbUnchecked
                    RefreshForm Me
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_Localidades
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    txtLocalidades.Text = ListaStr
                    
                    txtLocalidades_Click
                    
                    ChkPorLocalidad.Value = vbChecked
                    
                End If
            
            End With
            
            Set Frm_Super_Consultas = Nothing
            
    End Select
    
End Sub

' Origen Doc.

Private Sub CmdOrigenDoc_Click()
    Call txtOrigenDoc_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txtOrigenDoc_Click()
    
    If Arr_OrigenDoc Is Nothing Then
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_OrigenDoc
            ListaStr = ListaStr & vbNewLine & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensajeLocal(278) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, txtOrigenDoc, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
    
End Sub

Private Sub txtOrigenDoc_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            BaseSelect = _
            "SELECT 'POS' AS TipoOrigen, '" & "" & StellarMensajeLocal(279) & "" & "' AS Descripcion "
            
            '
            
            'BaseSelect = BaseSelect & _
            "UNION ALL " & vbNewLine & _
            "SELECT '' AS TipoOrigen, 'VENTAS XYZ' AS Descripcion " & vbNewLine
            
            BaseSelect = BaseSelect & _
            "SELECT * FROM ( " & vbNewLine & _
            BaseSelect & _
            ") TB " & vbNewLine
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, "" & StellarMensajeLocal(278) & "", FrmAppLink.CnADM, , True
                
                .Add_ItemLabels "" & "" & StellarMensajeLocal(280) & "" & "", "TipoOrigen", 2500, 0
                .Add_ItemLabels "" & "" & StellarMensaje(143) & "" & "", "Descripcion", 7500, 0
                
                .Add_ItemSearching "" & StellarMensaje(143) & "", "Descripcion"
                
                .ModoDespliegueInstantaneo = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .BusquedaInstantanea = True
                
                .StrOrderBy = "TipoOrigen ASC"
                
                .CustomFontSize = 14
                
                .Show vbModal
                
                Set Arr_OrigenDoc = .ArrResultado
                
                If Arr_OrigenDoc Is Nothing Then
                    txtOrigenDoc.Text = Empty
                    ChkPorOrigenDoc.Value = vbUnchecked
                    RefreshForm Me
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_OrigenDoc
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    txtOrigenDoc.Text = ListaStr
                    
                    txtOrigenDoc_Click
                    
                    ChkPorOrigenDoc.Value = vbChecked
                    
                End If
                
            End With
            
            Set Frm_Super_Consultas = Nothing
            
    End Select
    
End Sub

' C�digos de Producto

Private Sub CmdProducto_Click()
    Call txtProducto_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txtProducto_Click()
    
    If Trim(txtProducto.Text) <> Empty Then
        
        Dim mInCodigos As String, mArrProducto, Records
        
        mArrProducto = Split(txtProducto.Text, ",", , vbTextCompare)
        
        For I = 0 To UBound(mArrProducto)
            
            mInCodigos = mInCodigos & _
            IIf(mInCodigos <> Empty, ", ", Empty) & _
            "'" & mArrProducto(I) & "'"
            
        Next
        
        SQL = "SELECT * FROM MA_PRODUCTOS " & _
        "WHERE c_Codigo IN ( " & _
        "SELECT c_CodNasa FROM MA_CODIGOS " & _
        "WHERE c_Codigo IN (" & mInCodigos & ")) "
        
        Set mRs = ExecuteSafeSQL(SQL, FrmAppLink.CnADM, Records, True)
        
        If Not mRs Is Nothing Then
            
            If Not mRs.EOF Then
                
                Dim ListaStr As String
                
                While Not mRs.EOF
                    
                    ListaStr = ListaStr & vbNewLine & _
                    "[" & mRs!c_Codigo & "] " & mRs!c_Descri
                    
                    mRs.MoveNext
                    
                Wend
                
            End If
            
        End If
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensajeLocal(281) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, txtProducto, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
    
End Sub

Private Sub txtProducto_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            'Call MAKE_VIEW("MA_PRODUCTOS", "c_Codigo", "c_Descri", _
            StellarMensaje(254), Me, "GENERICO", , txtCodigoProducto, lblDescripcionProducto) '"P R O D U C T O S"
            mArrProducto = BuscarInfoProducto_Basica(FrmAppLink.CnADM, , , , , VistaPrecioCliente)
            
            If Not IsEmpty(mArrProducto) Then
                txtProducto.Text = mArrProducto(0)
                'txtCodigoProducto_LostFocus
                ChkPorProducto.Value = vbChecked
            End If
            
        Case vbKeyDelete
            
            txtProducto.Text = Empty
            ChkPorProducto.Value = vbUnchecked
            
    End Select
    
End Sub

' Caja

Private Sub CmdCaja_Click()
    Call txtCaja_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txtCaja_Click()
    
    If Arr_Cajas Is Nothing Then
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_Cajas
            ListaStr = ListaStr & vbNewLine & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensajeLocal(282) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, txtCaja, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
    
End Sub

Private Sub txtCaja_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            BaseSelect = "SELECT c_Codigo, c_Desc_Caja FROM MA_CAJA "
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, StellarMensaje(1264), FrmAppLink.CnPos, , True
                
                .Add_ItemLabels StellarMensaje(142), "c_Codigo", 2100, 0
                .Add_ItemLabels StellarMensaje(143), "c_Desc_Caja", 9000, 0
                
                .Add_ItemSearching StellarMensaje(143), "c_Desc_Caja"
                .Add_ItemSearching StellarMensaje(142), "c_Codigo"
                
                .StrOrderBy = "c_Desc_Caja"
                
                .BusquedaInstantanea = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .ModoDespliegueInstantaneo = True
                
                .CustomFontSize = 16
                
                .Show vbModal
                
                Set Arr_Cajas = .ArrResultado
                
                If Arr_Cajas Is Nothing Then
                    txtCaja.Text = Empty
                    ChkPorCaja.Value = vbUnchecked
                    RefreshForm Me
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_Cajas
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    txtCaja.Text = ListaStr
                    
                    txtCaja_Click
                    
                    ChkPorCaja.Value = vbChecked
                    
                End If
            
            End With
            
            Set Frm_Super_Consultas = Nothing
            
    End Select
    
End Sub

' Monedas

Private Sub CmdMoneda_Click()
    Call txtMoneda_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txtMoneda_Click()
    
    If Arr_Monedas Is Nothing Then
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_Monedas
            ListaStr = ListaStr & vbNewLine & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensajeLocal(283) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, txtMoneda, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
    
End Sub

Private Sub txtMoneda_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            BaseSelect = "SELECT c_CodMoneda, c_Descripcion FROM MA_MONEDAS "
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, StellarMensaje(2854), FrmAppLink.CnADM, , True
                
                .Add_ItemLabels StellarMensaje(142), "c_CodMoneda", 3315, 0
                .Add_ItemLabels StellarMensaje(143), "c_Descripcion", 7785, 0
                
                .Add_ItemSearching StellarMensaje(143), "c_Descripcion"
                .Add_ItemSearching StellarMensaje(142), "c_CodMoneda"
                
                .StrOrderBy = "c_Descripcion"
                
                .BusquedaInstantanea = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .ModoDespliegueInstantaneo = True
                
                .CustomFontSize = 18
                
                .Show vbModal
                
                Set Arr_Monedas = .ArrResultado
                
                If Arr_Monedas Is Nothing Then
                    txtMoneda.Text = Empty
                    ChkPorMoneda.Value = vbUnchecked
                    RefreshForm Me
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_Monedas
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    txtMoneda.Text = ListaStr
                    
                    txtMoneda_Click
                    
                    ChkPorMoneda.Value = vbChecked
                    
                End If
            
            End With
            
            Set Frm_Super_Consultas = Nothing
            
    End Select
    
End Sub

' Denominaciones

Private Sub CmdFormaPago_Click()
    Call txtFormaPago_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txtFormaPago_Click()
    
    If Arr_Denominaciones Is Nothing Then
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_Denominaciones
            ListaStr = ListaStr & vbNewLine & Item(2) & " - " & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensajeLocal(284) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, txtFormaPago, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
    
End Sub

Private Sub txtFormaPago_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Dim mFiltroMoneda As String
            
            If Not Arr_Monedas Is Nothing Then
                
                For Each Item In Arr_Monedas
                    mFiltroMoneda = mFiltroMoneda & _
                    IIf(mFiltroMoneda <> Empty, ", ", Empty) & _
                    "'" & Item(0) & "'"
                Next
                
                mFiltroMoneda = "AND DEN.c_CodMoneda IN (" & mFiltroMoneda & ") "
                
            End If
            
            BaseSelect = "SELECT DEN.*, MON.c_Descripcion AS Moneda " & _
            "FROM MA_DENOMINACIONES DEN " & _
            "INNER JOIN MA_MONEDAS MON " & _
            "ON DEN.c_CodMoneda = MON.c_CodMoneda " & _
            "WHERE 1 = 1 " & mFiltroMoneda
            
            BaseSelect = "SELECT c_CodDenomina, c_Denominacion, Moneda " & _
            "FROM (" & BaseSelect & ") TB " & _
            "WHERE 1 = 1 " & _
            "AND (c_Real = 0 OR c_Real = 1 AND n_Valor = 0)"
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, StellarMensaje(2716), FrmAppLink.CnADM, , True
                
                .Add_ItemLabels StellarMensaje(142), "c_CodDenomina", 2000, 0
                .Add_ItemLabels StellarMensaje(143), "c_Denominacion", 4500, 0
                .Add_ItemLabels Replace(StellarMensaje(134), ":", Empty), "Moneda", 4500, 0
                
                .Add_ItemSearching StellarMensaje(143), "c_Denominacion"
                .Add_ItemSearching StellarMensaje(142), "c_CodDenomina"
                .Add_ItemSearching Replace(StellarMensaje(134), ":", Empty), "Moneda"
                
                .StrOrderBy = "c_Denominacion"
                
                .BusquedaInstantanea = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .ModoDespliegueInstantaneo = True
                
                .Show vbModal
                
                Set Arr_Denominaciones = .ArrResultado
                
                If Arr_Denominaciones Is Nothing Then
                    txtFormaPago.Text = Empty
                    ChkPorFormaPago.Value = vbUnchecked
                    RefreshForm Me
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_Denominaciones
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    txtFormaPago.Text = ListaStr
                    
                    txtFormaPago_Click
                    
                    ChkPorFormaPago.Value = vbChecked
                    
                End If
            
            End With
            
            Set Frm_Super_Consultas = Nothing
            
    End Select
    
End Sub

'

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameTitulo_MouseMove Button, Shift, x, y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameTitulo_MouseMove Button, Shift, x, y
End Sub

Private Sub aceptar_Click(Index As Integer)
    'ValidarImpresoraPredeterminadaDelSistemaOperativo
    ReporteDePromocionesAplicadas Index
End Sub

Private Sub Add_Click()
    If Ordenar.ListIndex >= 0 Then
        'Mensaje False, Replace(StellarMensaje(2899), "$(Line)", vbNewLine)
        'If Retorno Then
            'Seleccionados.AddItem Ordenar.List(Ordenar.ListIndex) & "|ASC"
        'Else
            'Seleccionados.AddItem Ordenar.List(Ordenar.ListIndex) & "|DESC"
        'End If
        Seleccionados.AddItem Ordenar.List(Ordenar.ListIndex)
        Ordenar.RemoveItem Ordenar.ListIndex
    End If
End Sub

Private Sub Borrar_Click()
    While Seleccionados.ListCount > 0
        ItmIndex = Seleccionados.ListCount
        BackItem = Split(Seleccionados.List(ItmIndex - 1), "|")
        If UBound(BackItem) >= 0 Then
            Ordenar.AddItem BackItem(0)
            Seleccionados.RemoveItem (ItmIndex - 1)
        End If
    Wend
End Sub

Private Sub cmd_salir_Click()
    Unload Me
End Sub

Private Sub Eliminar_Click()
    If Seleccionados.ListIndex >= 0 Then
        ItmIndex = Seleccionados.ListIndex
        BackItem = Split(Seleccionados.List(ItmIndex), "|")
        If UBound(BackItem) >= 0 Then
            Ordenar.AddItem BackItem(0)
            Seleccionados.RemoveItem ItmIndex
        End If
    End If
End Sub

Private Sub Form_Load()
    
    lbl_Organizacion.Caption = StellarMensaje(3353) ' Reporte de Promociones Aplicadas
    
    chkVerFechas.Caption = StellarMensaje(3045) 'Desde
    lblFechaFin.Caption = StellarMensaje(3046) 'Hasta
    
    ChkPorTipoPromocion.Caption = StellarMensaje(3045) 'Desde
    
    ChkVerTopRows.Caption = "Top"
    lblTopNRegistros.Caption = StellarMensaje(3177) ' Registros
    
    ChkPorTipoPromocion.Caption = StellarMensajeLocal(276) ' Tipo de Promocion
    OptTipoItems.Caption = StellarMensajeLocal(285) ' De Articulos
    OptTipoDetPag.Caption = StellarMensajeLocal(286) ' De Formas de Pago
    OptTipoOtros.Caption = StellarMensajeLocal(287) ' Personalizado / Otro
    ChkPorPromocion.Caption = StellarMensajeLocal(288) ' Promocion
    ChkPorLocalidad.Caption = StellarMensaje(12) ' Localidad
    ChkPorOrigenDoc.Caption = StellarMensajeLocal(289) ' Origen Doc.
    ChkPorDocumento.Caption = StellarMensaje(10016) ' Documento
    ChkPorProducto.Caption = StellarMensaje(10017) ' Producto
    ChkPorCaja.Caption = StellarMensaje(10047) ' Caja
    ChkPorMoneda.Caption = StellarMensajeLocal(290) ' Moneda
    ChkPorFormaPago.Caption = StellarMensajeLocal(291) ' Denominaci�n
    ChkPorTurno.Caption = StellarMensajeLocal(292) ' Turno
    ChkPorBIN.Caption = StellarMensajeLocal(293) ' BIN
    ChkPorTarjeta.Caption = StellarMensajeLocal(294) ' Tarjeta
    
    CmbModalidad.Clear
    
    CmbModalidad.AddItem StellarMensajeLocal(297)  ' "Resumido por Promoci�n por Factura"
    CmbModalidad.AddItem StellarMensajeLocal(298) '"Detallado"
    'CmbModalidad.AddItem StellarMensajeLocal(299)'"Detallado Agrupado por Promoci�n por Factura"
    
    CmbModalidad.Visible = True
    
    lblCriteriosBusqueda.Caption = StellarMensaje(6057) 'Criterios de B�squeda
    lblCriteriosOrden.Caption = StellarMensaje(10006) 'Criterio de Ordenamiento
    lblModalidad.Caption = StellarMensaje(3354) & ":"
    ChkRedondear.Caption = StellarMensajeLocal(296) ' Redondear Cifras
    lblCaptionDec.Caption = StellarMensaje(16325) 'Decimales
    lblMonedaVer.Caption = StellarMensajeLocal(322) 'Vista de Moneda
    
    Ordenar.Clear
    
    Ordenar.AddItem StellarMensaje(128) & " " & "ASC"  'Fecha
    Ordenar.AddItem StellarMensaje(15503) & " " & "ASC"  'Hora
    Ordenar.AddItem StellarMensaje(12) & " " & "ASC"
    Ordenar.AddItem StellarMensaje(10016) & " " & "ASC"
    Ordenar.AddItem StellarMensaje(10047) & " " & "ASC"
    Ordenar.AddItem StellarMensaje(2022) & " " & "ASC"
    Ordenar.AddItem StellarMensajeLocal(288) & " " & "ASC"
    Ordenar.AddItem StellarMensajeLocal(24) & " " & "ASC"
    Ordenar.AddItem StellarMensaje(6144) & " " & "ASC"
    Ordenar.AddItem StellarMensaje(5010) & " " & "ASC"
    Ordenar.AddItem StellarMensajeLocal(300) & " " & "ASC"
    Ordenar.AddItem StellarMensajeLocal(301) & " " & "ASC"
    
    Ordenar.AddItem StellarMensaje(128) & " " & "DESC"  'Fecha
    Ordenar.AddItem StellarMensaje(15503) & " " & "DESC"  'Hora
    Ordenar.AddItem StellarMensaje(12) & " " & "DESC"
    Ordenar.AddItem StellarMensaje(10016) & " " & "DESC"
    Ordenar.AddItem StellarMensaje(10047) & " " & "DESC"
    Ordenar.AddItem StellarMensaje(2022) & " " & "DESC"
    Ordenar.AddItem StellarMensajeLocal(288) & " " & "DESC"
    Ordenar.AddItem StellarMensajeLocal(24) & " " & "DESC"
    Ordenar.AddItem StellarMensaje(6144) & " " & "DESC"
    Ordenar.AddItem StellarMensaje(5010) & " " & "DESC"
    Ordenar.AddItem StellarMensajeLocal(300) & " " & "DESC"
    Ordenar.AddItem StellarMensajeLocal(301) & " " & "DESC"
    
    ' Valores iniciales
    
    CmbModalidad.ListIndex = 0
    
    chkVerFechas.Value = vbChecked
    ChkPorOrigenDoc.Value = vbChecked
    
    ChkPorTipoPromocion.Value = vbUnchecked
    ChkPorPromocion.Value = vbUnchecked
    ChkPorLocalidad.Value = vbUnchecked
    'ChkPorOrigenDoc.Value = vbUnchecked
    ChkPorDocumento.Value = vbUnchecked
    ChkPorProducto.Value = vbUnchecked
    ChkPorCaja.Value = vbUnchecked
    ChkPorMoneda.Value = vbUnchecked
    ChkPorFormaPago.Value = vbUnchecked
    ChkPorTurno.Value = vbUnchecked
    ChkPorBIN.Value = vbUnchecked
    ChkPorTarjeta.Value = vbUnchecked
    
    OptTipoItems.Value = False
    OptTipoDetPag.Value = False
    OptTipoOtros.Value = True
    
    ' Ordenamiento Default
    
    Ordenar.Text = StellarMensaje(128) & " " & "ASC" 'Fecha
    Add_Click
    Ordenar.Text = StellarMensaje(12) & " " & "ASC" 'Fecha
    Add_Click
    Ordenar.Text = StellarMensaje(10016) & " " & "ASC" 'Fecha
    Add_Click
    Ordenar.Text = StellarMensaje(6144) & " " & "ASC" 'Fecha
    Add_Click
    
    ChkRedondear.Value = vbChecked
    txtDec.Text = FrmAppLink.DecMonedaPref
    
    txtOrigenDoc.Text = "POS"
    CmdOrigenDoc.Enabled = False
    ChkPorOrigenDoc.Enabled = False
    
    txtFechaIni.Tag = CDec(Date)
    txtFechaIni.Text = CDate(CDec(txtFechaIni.Tag))
    txtFechaFin.Tag = CDec(EndOfDay(CDate(txtFechaIni.Tag)))
    txtFechaFin.Text = CDate(CDec(txtFechaFin.Tag))
    
    txtMonedaVer.Text = FrmAppLink.CodMonedaPref
    lblDescMonedaVer.Caption = FrmAppLink.DesMonedaPref
    lblDescMonedaVer.Tag = FrmAppLink.SimMonedaPref
    
    AjustarPantalla Me
    
    FrmAppLink.SetFormaDLL = Me
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            oTeclado.Key_Tab
            KeyAscii = 0
        Case vbKeyF12
            Call cmd_salir_Click
    End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set REP_PROMOCIONES_APLICADAS = Nothing
End Sub

Private Sub OptTipoDetPag_Click()
    txtTipoPromocion.Text = "16, 17, 18, 19, 20, 21"
    ChkPorTipoPromocion.Value = vbChecked
End Sub

Private Sub OptTipoItems_Click()
    txtTipoPromocion.Text = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11"
    ChkPorTipoPromocion.Value = vbChecked
End Sub

Private Sub OptTipoOtros_Click()
    txtTipoPromocion.Text = Empty
    ChkPorTipoPromocion.Value = vbUnchecked
    RefreshForm Me
End Sub

Private Sub Ordenar_DblClick()
    Call Add_Click
End Sub

Private Sub Ordenar_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyInsert
            Call Add_Click
        Case vbKeyBack
            Call Borrar_Click
    End Select
End Sub

Private Sub seleccionados_DblClick()
    Call Eliminar_Click
End Sub

Private Sub seleccionados_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyDelete
            Call Eliminar_Click
        Case vbKeyBack
            Call Borrar_Click
    End Select
End Sub

Private Sub txtFechaFin_Click()
    
    Dim mCls As Object 'clsFechaSeleccion
    
    Set mCls = FrmAppLink.GetClassFechaSeleccion
    
    mCls.MostrarInterfazFechaHora 0 'DateTypes.Full
    
    If mCls.Selecciono Then
        txtFechaFin.Text = mCls.Fecha
        txtFechaFin.Tag = CDbl(mCls.Fecha)
    Else
        txtFechaFin.Text = Empty
        txtFechaFin.Tag = txtFechaIni.Text
    End If
    
End Sub

Private Sub txtFechaIni_Click()
    
    Dim mCls As Object 'clsFechaSeleccion
    
    Set mCls = FrmAppLink.GetClassFechaSeleccion
    
    mCls.MostrarInterfazFechaHora 0 'DateTypes.Full
    
    If mCls.Selecciono Then
        txtFechaIni.Text = mCls.Fecha
        txtFechaIni.Tag = CDbl(mCls.Fecha)
    Else
        txtFechaIni.Text = Empty
        txtFechaIni.Tag = txtFechaIni.Text
    End If
    
End Sub

Private Sub CmdOpenXML_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then ' Abrir con Excel
        On Error Resume Next
        ShellEx Me.hWnd, "Open", "EXCEL", """" & rutaXml.FileName & """", vbNullString, 1
    ElseIf Button = vbMiddleButton Then ' Abrir con la App XML Handler / por defecto.
        On Error Resume Next
        ShellEx Me.hWnd, "Open", """" & rutaXml.FileName & """", vbNullString, vbNullString, 1
    ElseIf Button = vbRightButton Then ' Ocultar
        CmdOpenXML.Visible = False
    End If
End Sub

Private Sub ReporteDePromocionesAplicadas(pDonde)
    
    On Error GoTo Error
    
    MousePointer = vbHourglass
    DoEvents
    
    Dim mRs As New ADODB.Recordset, mRsRpt As New ADODB.Recordset
    Dim mSQL As String, mSqlDet As String, mSqlWhere As String
    Dim BaseSelect As String, BaseSelectPOS As String
    
    Dim mTopSQL As String, TopPercent As String
    
    If ChkVerTopRows.Value = vbChecked Then
        
        If InStr(1, txtTopNRows.Text, "%", vbTextCompare) Then
            TopPercent = "PERCENT "
        End If
        
        If Val(Replace(txtTopNRows.Text, "%", Empty, , , vbTextCompare)) > 0 Then
            mTopSQL = "TOP " & Replace(txtTopNRows.Text, "%", Empty, , , vbTextCompare) & " " & TopPercent
        End If
        
    End If
    
    mSqlWhere = Empty
    
    Dim TmpRound As Boolean, mRn1 As String, mRn2
    
    TmpRound = IIf(ChkRedondear.Value = vbChecked And IsNumeric(txtDec), True, False)
    
    If TmpRound Then
        mRn1 = "ROUND("
        mRn2 = ", " & txtDec & ", 0)"
    End If
    
    Dim ListaPromociones As Variant, ListaPromocionesIn As String
    Dim ListaLocalidades As Variant, ListaLocalidadesIn As String
    Dim ListaDocumentos As Variant, ListaDocumentosIn As String
    Dim ListaProductos As Variant, ListaProductosIn As String
    Dim ListaCajas As Variant, ListaCajasIn As String
    Dim ListaMonedas As Variant, ListaMonedasIn As String
    Dim ListaDenominaciones As Variant, ListaDenominacionesIn As String
    Dim ListaBines As Variant, ListaBinesIn As String
    Dim ListaTarjetas As Variant, ListaTarjetasIn As String
    
    If Trim(txtOrigenDoc) = Empty Or UCase(txtOrigenDoc) Like "*POS*" Then
        
        Dim mFactorizar As String, mJoinMonDest, mWhereMonDest, mGroupMonDest
        
        If Trim(txtMonedaVer) <> Empty _
        And UCase(txtMonedaVer) <> UCase(FrmAppLink.CodMonedaPref) Then
            
            mFactorizar = " * (1 / isNULL((SELECT TOP 1 n_FactorPeriodo " & _
            "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_HISTORICO_MONEDAS " & _
            "WHERE c_CodMoneda = MON_DEST.c_CodMoneda AND d_FechaCambioActual <= " & IIf(CmbModalidad.ListIndex = 0, "MIN(DET.Fecha)", "DET.Fecha") & " " & _
            "ORDER BY d_FechaCambioActual DESC, ID DESC), MON_DEST.n_Factor))"
            
            mJoinMonDest = "CROSS JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS MON_DEST " & vbNewLine
            
            mWhereMonDest = "AND MON_DEST.c_CodMoneda = '" & txtMonedaVer & "' " & vbNewLine
            
            mSqlWhere = mSqlWhere & mWhereMonDest
            
            mGroupMonDest = ", MON_DEST.c_CodMoneda, MON_DEST.n_Factor"
            
        End If
        
        Select Case CmbModalidad.ListIndex
            
            Case 0 ' Resumido por Promocion por Factura
                
                BaseSelectPOS = _
                "SELECT 'POS' AS OrigenDoc, CodLocalidad, isNULL(SUC.c_Descripcion, CodLocalidad) AS Localidad, Numero AS Factura, " & vbNewLine & _
                "CodCaja, Turno, CAST(MIN(Fecha) AS DATE) AS Fecha, CAST(MIN(Fecha) AS TIME(0)) AS Hora, " & vbNewLine & _
                "Cod_Promocion_Aplicada AS CodPromo, isNULL(PRM.Descripcion, '') AS Promocion, isNULL(PRM.Campa�a, '') AS Campa�a, " & vbNewLine & _
                mRn1 & "PromoCab_SubtotalDescuento" & mFactorizar & mRn2 & " AS SubtotalDctoPromo, " & _
                mRn1 & "PromoCab_ImpDescuento" & mFactorizar & mRn2 & " AS ImpDescuentoPromo, " & _
                mRn1 & "PromoCab_TotalDescuento" & mFactorizar & mRn2 & " AS TotalDctoPromo, " & vbNewLine & _
                "isNULL(MON.c_Descripcion, 'N/A') AS Moneda, isNULL(DEN.c_Denominacion, 'N/A') AS FormaPago, " & vbNewLine & _
                "CASE WHEN DetPag_TarjetaBIN = '' THEN 'N/A' ELSE DetPag_TarjetaBIN END AS BIN, " & vbNewLine & _
                "CASE WHEN DetPag_TarjetaNumero = '' THEN 'N/A' ELSE DetPag_TarjetaNumero END AS NumTarjeta " & vbNewLine & _
                "FROM MA_TRANSACCION_DETALLE_PROMO DET " & vbNewLine & _
                mJoinMonDest & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PROMOCION PRM " & vbNewLine & _
                "ON DET.Cod_Promocion_Aplicada = PRM.Cod_Promocion " & vbNewLine & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS MON " & vbNewLine & _
                "ON DET.DetPag_CodMonedaAplica = MON.c_CodMoneda " & vbNewLine & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_DENOMINACIONES DEN " & vbNewLine & _
                "ON DET.DetPag_CodMonedaAplica = DEN.c_CodMoneda " & vbNewLine & _
                "AND DET.DetPag_CodDenominaAplica = DEN.c_CodDenomina " & vbNewLine & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_SUCURSALES SUC " & vbNewLine & _
                "ON DET.CodLocalidad = SUC.c_Codigo " & vbNewLine & _
                "WHERE 1 = 1 " & vbNewLine
                
                If chkVerFechas.Value = vbChecked Then
                    
                    If txtFechaIni.Text <> Empty Then
                        mSqlWhere = mSqlWhere & "AND DET.Fecha >= '" & FechaBD(txtFechaIni.Tag, FBD_FULL) & "' " & vbNewLine
                    End If
                    
                    If txtFechaFin.Text <> Empty Then
                        mSqlWhere = mSqlWhere & "AND DET.Fecha <= '" & FechaBD(txtFechaFin.Tag, FBD_FULL) & "' "
                    End If
                    
                End If
                
                If Trim(txtTipoPromocion) <> Empty Then
                    
                    mSqlWhere = mSqlWhere & "AND DET.Tipo_Promocion IN (" & QuitarComillasSimples(txtTipoPromocion) & ") " & vbNewLine
                    
                End If
                
                If Trim(txtPromocion) <> Empty Then
                    
                    txtPromocion = QuitarComillasSimples(txtPromocion.Text)
                    
                    ListaPromocionesIn = Empty
                    ListaPromociones = Split(txtPromocion.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaPromociones)
                        ListaPromocionesIn = ListaPromocionesIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaPromocionesIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.Cod_Promocion_Aplicada IN (" & Mid(ListaPromocionesIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtLocalidades) <> Empty Then
                    
                    txtLocalidades = QuitarComillasSimples(txtLocalidades.Text)
                    
                    ListaLocalidadesIn = Empty
                    ListaLocalidades = Split(txtLocalidades.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaLocalidades)
                        ListaLocalidadesIn = ListaLocalidadesIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaLocalidadesIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.CodLocalidad IN (" & Mid(ListaLocalidadesIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtDocumento) <> Empty Then
                    
                    txtDocumento = QuitarComillasSimples(txtDocumento.Text)
                    
                    ListaDocumentosIn = Empty
                    ListaDocumentos = Split(txtDocumento.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaDocumentosIn)
                        ListaDocumentosIn = ListaDocumentosIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaDocumentosIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.Numero IN (" & Mid(ListaDocumentosIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtProducto) <> Empty Then
                    
                    txtProducto = QuitarComillasSimples(txtProducto.Text)
                    
                    ListaProductosIn = Empty
                    ListaProductos = Split(txtProducto.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaProductos)
                        ListaProductosIn = ListaProductosIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaProductosIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.Cod_Producto_Aplica IN ( " & _
                        "SELECT c_CodNasa FROM MA_CODIGOS " & _
                        "WHERE c_Codigo IN (" & Mid(ListaProductosIn, 2) & ")) " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtCaja) <> Empty Then
                    
                    txtCaja = QuitarComillasSimples(txtCaja.Text)
                    
                    ListaCajasIn = Empty
                    ListaCajas = Split(txtCaja.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaCajas)
                        ListaCajasIn = ListaCajasIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaCajasIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.CodCaja IN (" & Mid(ListaCajasIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtMoneda) <> Empty Then
                    
                    txtMoneda = QuitarComillasSimples(txtMoneda.Text)
                    
                    ListaMonedasIn = Empty
                    ListaMonedas = Split(txtMoneda.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaMonedas)
                        ListaMonedasIn = ListaMonedasIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaMonedasIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.DetPag_CodMonedaAplica IN (" & Mid(ListaMonedasIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtFormaPago) <> Empty Then
                    
                    txtFormaPago = QuitarComillasSimples(txtFormaPago.Text)
                    
                    ListaDenominacionesIn = Empty
                    ListaDenominaciones = Split(txtFormaPago.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaDenominaciones)
                        ListaDenominacionesIn = ListaDenominacionesIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaDenominacionesIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.DetPag_CodDenominaAplica IN (" & Mid(ListaDenominacionesIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtTurno) <> Empty Then
                    
                    mSqlWhere = mSqlWhere & "AND DET.Turno IN (" & QuitarComillasSimples(txtTurno) & ") " & vbNewLine
                    
                End If
                
                If Trim(txtBIN) <> Empty Then
                    
                    txtBIN = QuitarComillasSimples(txtBIN.Text)
                    
                    ListaBinesIn = Empty
                    ListaBines = Split(txtBIN.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaBines)
                        ListaBinesIn = ListaBinesIn & _
                        IIf(Trim(ListaBinesIn) <> Empty, " OR ", Empty) & _
                        "DET.DetPag_TarjetaBIN LIKE '" & Item & "'"
                    Next
                    
                    If Len(ListaBinesIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND (" & ListaBinesIn & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtTarjeta) <> Empty Then
                    
                    txtTarjeta = QuitarComillasSimples(txtTarjeta.Text)
                    
                    ListaTarjetasIn = Empty
                    ListaTarjetas = Split(txtTarjeta.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaTarjetas)
                        ListaTarjetasIn = ListaTarjetasIn & _
                        IIf(Trim(ListaTarjetasIn) <> Empty, " OR ", Empty) & _
                        "DET.DetPag_TarjetaNumero LIKE '" & Item & "'"
                    Next
                    
                    If Len(ListaTarjetasIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND (" & ListaTarjetasIn & ") " & vbNewLine
                    End If
                    
                End If
                
                mSQL = BaseSelectPOS & mSqlWhere & _
                "GROUP BY CodLocalidad, SUC.c_Descripcion, CodCaja, Numero, Turno, Cod_Promocion_Aplicada, PRM.Descripcion, PRM.Campa�a, " & vbNewLine & _
                "PromoCab_SubtotalDescuento, PromoCab_ImpDescuento, PromoCab_TotalDescuento" & mGroupMonDest & ", " & vbNewLine & _
                "MON.c_Descripcion, DEN.c_Denominacion, DET.DetPag_TarjetaBIN, DET.DetPag_TarjetaNumero " & vbNewLine
                
            Case 1 ' Detallado
                 
                BaseSelectPOS = _
                "SELECT 'POS' AS OrigenDoc, CodLocalidad, isNULL(SUC.c_Descripcion, CodLocalidad) AS Localidad, Numero AS Factura, " & vbNewLine & _
                "CodCaja, Turno, CAST(Fecha AS DATE) AS Fecha, CAST(Fecha AS TIME(0)) AS Hora, " & vbNewLine & _
                "Cod_Promocion_Aplicada AS CodPromo, isNULL(PRM.Descripcion, '') AS Promocion, isNULL(PRM.Campa�a, '') AS Campa�a, " & vbNewLine & _
                mRn1 & "PromoCab_SubtotalDescuento" & mFactorizar & mRn2 & " AS SubtotalDctoPromo, " & _
                mRn1 & "PromoCab_ImpDescuento" & mFactorizar & mRn2 & " AS ImpDescuentoPromo, " & _
                mRn1 & "PromoCab_TotalDescuento" & mFactorizar & mRn2 & " AS TotalDctoPromo, " & vbNewLine & _
                "isNULL(MON.c_Descripcion, 'N/A') AS Moneda, isNULL(DEN.c_Denominacion, 'N/A') AS FormaPago, " & vbNewLine & _
                "CASE WHEN DetPag_TarjetaBIN = '' THEN 'N/A' ELSE DetPag_TarjetaBIN END AS BIN, " & vbNewLine & _
                "CASE WHEN DetPag_TarjetaNumero = '' THEN 'N/A' ELSE DetPag_TarjetaNumero END AS NumTarjeta, ' ----> ' AS DETAIL_SEPARATOR, " & vbNewLine & _
                "isNULL(DEP.c_Descripcio, '') AS Dpto, isNULL(GRU.c_Descripcio, '') AS Grupo, " & vbNewLine & _
                "isNULL(SUB.c_Descripcio, '') AS Subgrupo, isNULL(PRO.c_Marca, '') AS Marca, isNULL(PRO.c_Modelo, '') AS Modelo, " & vbNewLine & _
                "Linea, Cod_Producto_Aplica AS Codigo, isNULL(PRO.c_Descri, '') AS Producto, " & vbNewLine & _
                "CantidadLinea, PrecioReal" & mFactorizar & " AS PrecioOriginal, PrecioBase" & mFactorizar & " AS PrecioBase, DescuentoFinal" & mFactorizar & " AS DctoUni, " & vbNewLine & _
                mRn1 & "DescuentoFinal * (1 + ((Impuesto1 + Impuesto2 + Impuesto3) / 100))" & mFactorizar & mRn2 & " AS TotalDctoUni, " & vbNewLine & _
                "CAST(ROUND(DescuentoFinal / PrecioBase * 100, 2, 0) AS NVARCHAR(MAX)) + '%' AS DctoEfectivo, " & _
                mRn1 & "SubtotalDescuento" & mFactorizar & mRn2 & " AS SubtotalDescuento, " & vbNewLine & _
                mRn1 & "SubtotalDescuento * (0 + ((Impuesto1 + Impuesto2 + Impuesto3) / 100))" & mFactorizar & mRn2 & " AS ImpDescuento, " & vbNewLine & _
                mRn1 & "SubtotalDescuento * (1 + ((Impuesto1 + Impuesto2 + Impuesto3) / 100))" & mFactorizar & mRn2 & " AS TotalDescuento, " & vbNewLine & _
                "Precio_Con_Dcto" & mFactorizar & " AS Precio_Con_Dcto, " & _
                mRn1 & "Precio_Con_Dcto * (1 + ((Impuesto1 + Impuesto2 + Impuesto3) / 100))" & mFactorizar & mRn2 & " AS PrecioFinal, " & _
                mRn1 & "SubtotalLn" & mFactorizar & mRn2 & " AS SubtotalLn, " & _
                mRn1 & "ImpuestoLn" & mFactorizar & mRn2 & " AS ImpuestoLn, " & _
                mRn1 & "TotalLn" & mFactorizar & mRn2 & " AS TotalLn " & vbNewLine
                
                BaseSelectPOS = BaseSelectPOS & _
                "FROM MA_TRANSACCION_DETALLE_PROMO DET " & vbNewLine & _
                mJoinMonDest & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PRODUCTOS PRO " & vbNewLine & _
                "ON DET.Cod_Producto_Aplica = PRO.c_Codigo " & vbNewLine & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_DEPARTAMENTOS DEP " & vbNewLine & _
                "ON PRO.c_Departamento = DEP.c_Codigo " & vbNewLine & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_GRUPOS GRU " & vbNewLine & _
                "ON PRO.c_Departamento = GRU.c_Departamento " & vbNewLine & _
                "AND PRO.c_Grupo = GRU.c_Codigo " & vbNewLine & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_SUBGRUPOS SUB " & vbNewLine & _
                "ON PRO.c_Departamento = SUB.c_In_Departamento " & vbNewLine & _
                "AND PRO.c_Grupo = SUB.c_In_Grupo " & vbNewLine & _
                "AND PRO.c_Subgrupo = SUB.c_Codigo " & vbNewLine & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PROMOCION PRM " & vbNewLine & _
                "ON DET.Cod_Promocion_Aplicada = PRM.Cod_Promocion " & vbNewLine & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS MON " & vbNewLine & _
                "ON DET.DetPag_CodMonedaAplica = MON.c_CodMoneda " & vbNewLine & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_DENOMINACIONES DEN " & vbNewLine & _
                "ON DET.DetPag_CodMonedaAplica = DEN.c_CodMoneda " & vbNewLine & _
                "AND DET.DetPag_CodDenominaAplica = DEN.c_CodDenomina " & vbNewLine & _
                "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_SUCURSALES SUC " & vbNewLine & _
                "ON DET.CodLocalidad = SUC.c_Codigo " & vbNewLine & _
                "WHERE 1 = 1 " & vbNewLine
                
                If chkVerFechas.Value = vbChecked Then
                    
                    If txtFechaIni.Text <> Empty Then
                        mSqlWhere = mSqlWhere & "AND DET.Fecha >= '" & FechaBD(txtFechaIni.Tag, FBD_FULL) & "' " & vbNewLine
                    End If
                    
                    If txtFechaFin.Text <> Empty Then
                        mSqlWhere = mSqlWhere & "AND DET.Fecha <= '" & FechaBD(txtFechaFin.Tag, FBD_FULL) & "' "
                    End If
                    
                End If
                
                If Trim(txtTipoPromocion) <> Empty Then
                    
                    mSqlWhere = mSqlWhere & "AND DET.Tipo_Promocion IN (" & QuitarComillasSimples(txtTipoPromocion) & ") " & vbNewLine
                    
                End If
                
                If Trim(txtPromocion) <> Empty Then
                    
                    txtPromocion = QuitarComillasSimples(txtPromocion.Text)
                    
                    ListaPromocionesIn = Empty
                    ListaPromociones = Split(txtPromocion.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaPromociones)
                        ListaPromocionesIn = ListaPromocionesIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaPromocionesIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.Cod_Promocion_Aplicada IN (" & Mid(ListaPromocionesIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtLocalidades) <> Empty Then
                    
                    txtLocalidades = QuitarComillasSimples(txtLocalidades.Text)
                    
                    ListaLocalidadesIn = Empty
                    ListaLocalidades = Split(txtLocalidades.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaLocalidades)
                        ListaLocalidadesIn = ListaLocalidadesIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaLocalidadesIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.CodLocalidad IN (" & Mid(ListaLocalidadesIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtDocumento) <> Empty Then
                    
                    txtDocumento = QuitarComillasSimples(txtDocumento.Text)
                    
                    ListaDocumentosIn = Empty
                    ListaDocumentos = Split(txtDocumento.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaDocumentosIn)
                        ListaDocumentosIn = ListaDocumentosIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaDocumentosIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.Numero IN (" & Mid(ListaDocumentosIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtProducto) <> Empty Then
                    
                    txtProducto = QuitarComillasSimples(txtProducto.Text)
                    
                    ListaProductosIn = Empty
                    ListaProductos = Split(txtProducto.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaProductos)
                        ListaProductosIn = ListaProductosIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaProductosIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.Cod_Producto_Aplica IN ( " & _
                        "SELECT c_CodNasa FROM MA_CODIGOS " & _
                        "WHERE c_Codigo IN (" & Mid(ListaProductosIn, 2) & ")) " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtCaja) <> Empty Then
                    
                    txtCaja = QuitarComillasSimples(txtCaja.Text)
                    
                    ListaCajasIn = Empty
                    ListaCajas = Split(txtCaja.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaCajas)
                        ListaCajasIn = ListaCajasIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaCajasIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.CodCaja IN (" & Mid(ListaCajasIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtMoneda) <> Empty Then
                    
                    txtMoneda = QuitarComillasSimples(txtMoneda.Text)
                    
                    ListaMonedasIn = Empty
                    ListaMonedas = Split(txtMoneda.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaMonedas)
                        ListaMonedasIn = ListaMonedasIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaMonedasIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.DetPag_CodMonedaAplica IN (" & Mid(ListaMonedasIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtFormaPago) <> Empty Then
                    
                    txtFormaPago = QuitarComillasSimples(txtFormaPago.Text)
                    
                    ListaDenominacionesIn = Empty
                    ListaDenominaciones = Split(txtFormaPago.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaDenominaciones)
                        ListaDenominacionesIn = ListaDenominacionesIn & "," & "'" & Item & "'"
                    Next
                    
                    If Len(ListaDenominacionesIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND DET.DetPag_CodDenominaAplica IN (" & Mid(ListaDenominacionesIn, 2) & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtTurno) <> Empty Then
                    
                    mSqlWhere = mSqlWhere & "AND DET.Turno IN (" & QuitarComillasSimples(txtTurno) & ") " & vbNewLine
                    
                End If
                
                If Trim(txtBIN) <> Empty Then
                    
                    txtBIN = QuitarComillasSimples(txtBIN.Text)
                    
                    ListaBinesIn = Empty
                    ListaBines = Split(txtBIN.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaBines)
                        ListaBinesIn = ListaBinesIn & _
                        IIf(Trim(ListaBinesIn) <> Empty, " OR ", Empty) & _
                        "DET.DetPag_TarjetaBIN LIKE '" & Item & "'"
                    Next
                    
                    If Len(ListaBinesIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND (" & ListaBinesIn & ") " & vbNewLine
                    End If
                    
                End If
                
                If Trim(txtTarjeta) <> Empty Then
                    
                    txtTarjeta = QuitarComillasSimples(txtTarjeta.Text)
                    
                    ListaTarjetasIn = Empty
                    ListaTarjetas = Split(txtTarjeta.Text, ",")
                    
                    For Each Item In AsEnumerable(ListaTarjetas)
                        ListaTarjetasIn = ListaTarjetasIn & _
                        IIf(Trim(ListaTarjetasIn) <> Empty, " OR ", Empty) & _
                        "DET.DetPag_TarjetaNumero LIKE '" & Item & "'"
                    Next
                    
                    If Len(ListaTarjetasIn) > 0 Then
                        mSqlWhere = mSqlWhere & "AND (" & ListaTarjetasIn & ") " & vbNewLine
                    End If
                    
                End If
                
                mSQL = BaseSelectPOS & mSqlWhere
                
            Case 2 ' Detallado Agrupado por Promoci�n por Factura
                
        End Select
        
    End If
    
    mSQL = "SELECT " & mTopSQL & "* FROM (" & vbNewLine & vbNewLine & mSQL & vbNewLine & ") TB " & vbNewLine
    
    Dim mOrderByStr As String: mOrderByStr = Empty
    
    For I = 0 To Seleccionados.ListCount - 1
        
        mOrderByStr = mOrderByStr & IIf(Len(mOrderByStr) > 0, ", ", "ORDER BY ")
        mItem = Seleccionados.List(I)
        
        Select Case mItem
            Case StellarMensaje(128) & " ASC"
                mOrderByStr = mOrderByStr & "Fecha ASC"
            Case StellarMensaje(128) & " DESC"
                mOrderByStr = mOrderByStr & "Fecha DESC"
            Case StellarMensaje(15503) & " ASC"
                mOrderByStr = mOrderByStr & "Hora ASC"
            Case StellarMensaje(15503) & " DESC"
                mOrderByStr = mOrderByStr & "Hora DESC"
            Case StellarMensaje(12) & " ASC"
                mOrderByStr = mOrderByStr & "Localidad ASC"
            Case StellarMensaje(12) & " DESC"
                mOrderByStr = mOrderByStr & "Localidad DESC"
            Case StellarMensaje(10016) & " ASC"
                mOrderByStr = mOrderByStr & "Factura ASC"
            Case StellarMensaje(10016) & " DESC"
                mOrderByStr = mOrderByStr & "Factura DESC"
            Case StellarMensaje(10047) & " ASC"
                mOrderByStr = mOrderByStr & "CodCaja ASC"
            Case StellarMensaje(10047) & " DESC"
                mOrderByStr = mOrderByStr & "CodCaja DESC"
            Case StellarMensaje(2022) & " ASC"
                mOrderByStr = mOrderByStr & "Turno ASC"
            Case StellarMensaje(2022) & " DESC"
                mOrderByStr = mOrderByStr & "Turno DESC"
            Case StellarMensajeLocal(288) & " ASC"
                mOrderByStr = mOrderByStr & "Promocion ASC"
            Case StellarMensajeLocal(288) & " DESC"
                mOrderByStr = mOrderByStr & "Promocion DESC"
            Case StellarMensajeLocal(24) & " ASC"
                mOrderByStr = mOrderByStr & "Campa�a ASC"
            Case StellarMensajeLocal(24) & " DESC"
                mOrderByStr = mOrderByStr & "Campa�a DESC"
            Case StellarMensaje(6144) & " ASC"
                If CmbModalidad.ListIndex = 0 Then
                    mOrderByStr = Left(mOrderByStr, Len(mOrderByStr) - 2)
                Else
                    mOrderByStr = mOrderByStr & "Linea ASC"
                End If
            Case StellarMensaje(6144) & " DESC"
                If CmbModalidad.ListIndex = 0 Then
                    mOrderByStr = Left(mOrderByStr, Len(mOrderByStr) - 2)
                Else
                    mOrderByStr = mOrderByStr & "Linea DESC"
                End If
            Case StellarMensaje(5010) & " ASC"
                If CmbModalidad.ListIndex = 0 Then
                    mOrderByStr = Left(mOrderByStr, Len(mOrderByStr) - 2)
                Else
                    mOrderByStr = mOrderByStr & "Cod_Producto_Aplica ASC"
                End If
            Case StellarMensaje(5010) & " DESC"
                If CmbModalidad.ListIndex = 0 Then
                    mOrderByStr = Left(mOrderByStr, Len(mOrderByStr) - 2)
                Else
                    mOrderByStr = mOrderByStr & "Cod_Producto_Aplica DESC"
                End If
            Case StellarMensajeLocal(300) & " ASC"
                mOrderByStr = mOrderByStr & "TotalDctoPromo ASC"
            Case StellarMensajeLocal(300) & " DESC"
                mOrderByStr = mOrderByStr & "TotalDctoPromo DESC"
            Case StellarMensajeLocal(301) & " ASC"
                If CmbModalidad.ListIndex = 0 Then
                    mOrderByStr = Left(mOrderByStr, Len(mOrderByStr) - 2)
                Else
                    mOrderByStr = mOrderByStr & "TotalDescuento ASC"
                End If
            Case StellarMensajeLocal(301) & " DESC"
                If CmbModalidad.ListIndex = 0 Then
                    mOrderByStr = Left(mOrderByStr, Len(mOrderByStr) - 2)
                Else
                    mOrderByStr = mOrderByStr & "TotalDescuento DESC"
                End If
        End Select
        
    Next I
    
    mSQL = mSQL & mOrderByStr
    
    mRs.CursorLocation = adUseClient
    
    mRs.Open mSQL, FrmAppLink.CnPos, adOpenStatic, adLockReadOnly, adCmdText
    mRs.ActiveConnection = Nothing
    
    If Not mRs.EOF Then
        
        If pDonde = 0 Then
            '
        ElseIf pDonde = 1 Then
            '
        ElseIf pDonde = 2 Then
            
            Dim RutaGenerarXML As String
            
            rutaXml.Filter = "XML (*.xml)"
            rutaXml.DefaultExt = "xml"
            
            rutaXml.CancelError = True
            
            On Error GoTo Error
            
            rutaXml.ShowSave
            
            RutaGenerarXML = rutaXml.FileName
            
            Dim Campos As Dictionary: Set Campos = New Dictionary
            
            Select Case CmbModalidad.ListIndex
                
                Case 0
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("OrigenDoc", XMLHeader(StellarMensajeLocal(289), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("CodLocalidad", _
                    XMLHeader(StellarMensajeLocal(302), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Localidad", XMLHeader(StellarMensaje(12), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("CodCaja", XMLHeader(StellarMensaje(10047), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Factura", XMLHeader(StellarMensaje(2501), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Turno", XMLHeader(StellarMensaje(2022), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Fecha", XMLHeader(StellarMensaje(128), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Hora", XMLHeader(StellarMensaje(15503), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("CodPromo", XMLHeader(StellarMensajeLocal(303), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Promocion", XMLHeader(StellarMensajeLocal(288), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Campa�a", XMLHeader(StellarMensajeLocal(24), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("SubtotalDctoPromo", XMLHeader(StellarMensajeLocal(304), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("ImpDescuentoPromo", XMLHeader(StellarMensajeLocal(305), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("TotalDctoPromo", XMLHeader(StellarMensajeLocal(306), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Moneda", XMLHeader(StellarMensajeLocal(290), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("FormaPago", XMLHeader(StellarMensajeLocal(291), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("BIN", XMLHeader(StellarMensajeLocal(293), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("NumTarjeta", XMLHeader(StellarMensajeLocal(294), False))
                    
                Case 1
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("OrigenDoc", XMLHeader(StellarMensajeLocal(289), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("CodLocalidad", _
                    XMLHeader(StellarMensajeLocal(302), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Localidad", XMLHeader(StellarMensaje(12), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("CodCaja", XMLHeader(StellarMensaje(10047), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Factura", XMLHeader(StellarMensaje(2501), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Turno", XMLHeader(StellarMensaje(2022), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Fecha", XMLHeader(StellarMensaje(128), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Hora", XMLHeader(StellarMensaje(15503), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("CodPromo", XMLHeader(StellarMensajeLocal(303), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Promocion", XMLHeader(StellarMensajeLocal(288), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Campa�a", XMLHeader(StellarMensajeLocal(24), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("SubtotalDctoPromo", XMLHeader(StellarMensajeLocal(304), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("ImpDescuentoPromo", XMLHeader(StellarMensajeLocal(305), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("TotalDctoPromo", XMLHeader(StellarMensajeLocal(306), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Moneda", XMLHeader(StellarMensajeLocal(290), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("FormaPago", XMLHeader(StellarMensajeLocal(291), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("BIN", XMLHeader(StellarMensajeLocal(293), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("NumTarjeta", XMLHeader(StellarMensajeLocal(294), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("DETAIL_SEPARATOR", XMLHeader(StellarMensajeLocal(321), True))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Dpto", XMLHeader(StellarMensaje(3028), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Grupo", XMLHeader(StellarMensaje(161), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Subgrupo", XMLHeader(StellarMensaje(3029), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Marca", XMLHeader(StellarMensaje(3022), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Modelo", XMLHeader(StellarMensaje(222), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Codigo", XMLHeader(StellarMensaje(142), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Producto", XMLHeader(StellarMensaje(5010), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("CantidadLinea", XMLHeader(StellarMensajeLocal(307), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("PrecioOriginal", XMLHeader(StellarMensajeLocal(308), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("PrecioBase", XMLHeader(StellarMensajeLocal(309), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("DctoUni", XMLHeader(StellarMensajeLocal(310), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("TotalDctoUni", XMLHeader(StellarMensajeLocal(311), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("DctoEfectivo", XMLHeader(StellarMensajeLocal(312), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("SubtotalDescuento", XMLHeader(StellarMensajeLocal(313), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("ImpDescuento", XMLHeader(StellarMensajeLocal(314), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("TotalDescuento", XMLHeader(StellarMensajeLocal(315), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("Precio_Con_Dcto", XMLHeader(StellarMensajeLocal(316), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("PrecioFinal", XMLHeader(StellarMensajeLocal(317), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("SubtotalLn", XMLHeader(StellarMensajeLocal(318), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("ImpuestoLn", XMLHeader(StellarMensajeLocal(319), False))
                    
                    TmpCount = TmpCount + 1
                    Campos(TmpCount) = Array("TotalLn", XMLHeader(StellarMensajeLocal(320), False))
                    
            End Select
            
            Call FrmAppLink.Crear_Xml_Definido(mRs, RutaGenerarXML, Campos, True)
            
            CmdOpenXML.Visible = True
            
            'ShowTooltip "Click Izq. -> Abrir con Excel" & GetLines & _
            "Click Med. -> Abrir con aplicaci�n predeterminada para archivos XML" & GetLines & _
            "Click Der. -> Ocultar el bot�n." _
            & GetLines & Space(21) & "|" & GetLines & Space(21) & "V" & _
            GetLines, CmdOpenXML.Width * 5, 7500, CmdOpenXML, 1, , , , , "**[RefreshForm]**"
            FrmAppLink.ShowTooltip Replace(Replace(StellarMensaje(475), _
            "$(Line)", vbNewLine), "$(Spaces)", Space(21)), _
            CmdOpenXML.Width * 5, 7500, CmdOpenXML, 1, , , , , "**[RefreshForm]**"
            
            RefreshForm Me
            
        End If
        
    Else
        Mensaje True, StellarMensaje(16441)
    End If
    
Finally:
    
    MousePointer = vbDefault
    RefreshForm Me
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(ReporteDePromocionesAplicadas)"
    
    MousePointer = vbDefault
    
    GoTo Finally
    
End Sub

Private Function Center_Text(Texto As String, Ancho As Integer) As String
    
    Dim Lng As Integer
    Lng = Round((Ancho - Len(Texto)) / 2)
    
    If Lng > 0 Then
        Center_Text = Space(Lng) & Texto & Space(Lng)
        If Len(Center_Text) < Ancho Then
            Center_Text = Space(Ancho - Len(Center_Text)) & Center_Text
        End If
    Else
        Center_Text = Texto
    End If
    
    Center_Text = Left(Center_Text, Ancho)
    
End Function

Private Function LeftJust(Texto As String, Ancho As Integer)
    
    Dim Lng As Integer
    Lng = Ancho - Len(Texto)
    
    If Lng > 0 Then
        LeftJust = Texto & Space(Lng)
    Else
        LeftJust = Texto
    End If
    
    LeftJust = Left(LeftJust, Ancho)
    
End Function

Private Function RightJust(Texto As String, Ancho As Integer)
    
    Dim Lng As Integer
    Lng = Ancho - Len(Texto)
    
    If Lng > 0 Then
        RightJust = Space(Lng) & Texto
    Else
        RightJust = Texto
    End If
    
    RightJust = Left(RightJust, Ancho)
    
End Function

'Private Sub Escribir(cadena As String, Reporte As DataReport, Seccion As String, Objeto As String, Optional RC As Boolean = True)
'    Reporte.Sections(Seccion).Controls(Objeto).Caption = _
'    Reporte.Sections(Seccion).Controls(Objeto).Caption & _
'    cadena & IIf(RC, vbNewLine, "")
'End Sub

Private Sub txtTurno_Change()
    If Trim(txtTurno.Text) = Empty Then
        ChkPorTurno.Value = vbUnchecked
    Else
        ChkPorTurno.Value = vbChecked
    End If
End Sub

Private Sub txtTurno_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        txtTurno.Text = Empty
    End If
End Sub

Private Sub txtBIN_Change()
    If Trim(txtBIN.Text) = Empty Then
        ChkPorBIN.Value = vbUnchecked
    Else
        ChkPorBIN.Value = vbChecked
    End If
End Sub

Private Sub txtBIN_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        txtBIN.Text = Empty
    End If
End Sub

Private Sub txtTarjeta_Change()
    If Trim(txtTarjeta.Text) = Empty Then
        ChkPorTarjeta.Value = vbUnchecked
    Else
        ChkPorTarjeta.Value = vbChecked
    End If
End Sub

Private Sub txtTarjeta_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        txtTarjeta.Text = Empty
    End If
End Sub
