VERSION 5.00
Begin VB.Form frmAsistentePro_TipoPromo 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9540
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9540
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   7395
      Left            =   240
      TabIndex        =   10
      Top             =   1920
      Width           =   11685
      Begin VB.CommandButton Selected 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   60
         MaskColor       =   &H80000005&
         Picture         =   "frmAsistentePro_TipoPromo.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   6360
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton CmdAtras 
         Caption         =   "Atras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8400
         TabIndex        =   6
         Top             =   6720
         Width           =   1455
      End
      Begin VB.CommandButton CmdDescuentosFormaPago 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   6480
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   4200
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton Cmd_Premio_MxN_O_Valor 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   6480
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   3000
         Width           =   1095
      End
      Begin VB.CommandButton CmdMxN_O_Valor 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   6480
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   1800
         Width           =   1095
      End
      Begin VB.CommandButton cmdMontoDescuento 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   840
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   2
         Tag             =   "3"
         Top             =   4200
         Width           =   1095
      End
      Begin VB.CommandButton cmdPorcentajeDescuento 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   840
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   1
         Tag             =   "2"
         Top             =   3000
         Width           =   1095
      End
      Begin VB.CommandButton cmdPrecioOferta 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   840
         MaskColor       =   &H80000005&
         Style           =   1  'Graphical
         TabIndex        =   0
         Tag             =   "1"
         Top             =   1800
         Width           =   1095
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9960
         TabIndex        =   7
         Top             =   6720
         Width           =   1455
      End
      Begin VB.Label lblGuiaUsuario 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione el Tipo de Promoci�n a crear:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   840
         TabIndex        =   18
         Top             =   1080
         Width           =   4335
      End
      Begin VB.Label lblTipoPromoMetodoDePago 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descuentos por Condiciones de M�todos de Pago"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   570
         Left            =   7800
         TabIndex        =   17
         Top             =   4350
         Visible         =   0   'False
         Width           =   3075
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblTipoPromoPremioCondicionante 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descuentos por Condiciones de Productos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   570
         Left            =   7800
         TabIndex        =   16
         Top             =   3120
         Width           =   3075
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblTipoPromoMxN 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descuentos basados en cantidades (MxN)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   570
         Left            =   7800
         TabIndex        =   15
         Top             =   1980
         Width           =   2385
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblTipoPromo3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Monto Descuento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   2160
         TabIndex        =   14
         Top             =   4440
         Width           =   1830
      End
      Begin VB.Label lblTipoPromo2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "% Descuento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   2160
         TabIndex        =   13
         Top             =   3240
         Width           =   1410
      End
      Begin VB.Label LblDescripcionAsistente 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Este asistente le permitir� crear una Promoci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   240
         TabIndex        =   12
         Top             =   210
         Width           =   4995
      End
      Begin VB.Label lblTipoPromo1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Precio Oferta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   2160
         TabIndex        =   11
         Top             =   2040
         Width           =   1380
      End
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   9
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_TipoPromo.frx":1D82
      Top             =   360
      Width           =   2700
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_TipoPromo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private TipoSeleccionado As Integer

Private Function ValidarTipoPromo() As Boolean
    
    If TipoSeleccionado <> TipoPromo And TipoPromo <> -1 Then
        
        If Mensaje(False, StellarMensajeLocal(156)) Then
            
            ' Cuando se cambia el tipo de promoci�n reiniciar todos los valores cuya relevancia son
            ' de acuerdo al tipo de promoci�n ya que sino podr�an generar inconsistencias si se
            ' mantienen y mezclan los datos.
            
            ExecuteSafeSQL "UPDATE TMP_PROMOCION SET " & _
            "Cantidad_Productos_Requerir = 0, Cantidad_Productos_Pagar = 0 " & _
            "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & _
            "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", _
            FrmAppLink.CnADM, RecordsAffected
            
            ExecuteSafeSQL "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & _
            "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & _
            "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ", _
            FrmAppLink.CnADM, RecordsAffected
            
            ValidarSeleccion
            
        Else
            Exit Function
        End If
        
    End If
    
    TipoPromo = TipoSeleccionado
    
    frmAsistentePro_DatosGenerales.Show vbModal
    
    If Not (PromocionCreada Or CancelarPromo_Salir) Then
        ValidarSeleccion
    End If
    
End Function

Private Sub CmdAtras_Click()
    Unload Me
End Sub

Private Sub CmdCancelar_Click()
    If Mensaje(False, Replace(StellarMensaje(2620), "$(Line)", vbNewLine)) Then
        CancelarPromo_Salir = True
        Form_Activate
        Exit Sub
    End If
End Sub

'Private Sub cmdNmasXporValor_Click()
'    frmAsistentePro_TipoPromo3.Show vbModal
'End Sub

Private Sub cmdPrecioOferta_Click()
    TipoSeleccionado = 1
    Call ValidarTipoPromo
End Sub

Private Sub cmdPorcentajeDescuento_Click()
    TipoSeleccionado = 2
    Call ValidarTipoPromo
End Sub

Private Sub cmdMontoDescuento_Click()
    TipoSeleccionado = 3
    Call ValidarTipoPromo
End Sub

Private Sub CmdMxN_O_Valor_Click()
    frmAsistentePro_TipoPromoMxN.Show vbModal
    ValidarSeleccion
End Sub

Private Sub Cmd_Premio_MxN_O_Valor_Click()
    frmAsistentePro_TipoPromo_Premios1.Show vbModal
    ValidarSeleccion
End Sub

Private Sub CmdDescuentosFormaPago_Click()
    frmAsistentePro_TipoPromoDetPag.Show vbModal
    ValidarSeleccion
End Sub

Private Sub Form_Load()
    
    If CancelarPromo_Salir Then Exit Sub
    
    lblTitulo.Caption = StellarMensajeLocal(125)
    LblDescripcionAsistente.Caption = StellarMensajeLocal(101)
    lblGuiaUsuario.Caption = StellarMensajeLocal(103)
    
    CmdAtras.Caption = StellarMensajeLocal(12)
    CmdCancelar.Caption = StellarMensajeLocal(11)
    
    lblTipoPromo1.Caption = StellarMensajeLocal(104)
    lblTipoPromo2.Caption = StellarMensajeLocal(105)
    lblTipoPromo3.Caption = StellarMensajeLocal(106)
    lblTipoPromoMxN.Caption = StellarMensajeLocal(163)
    lblTipoPromoPremioCondicionante.Caption = StellarMensajeLocal(205)
    
    CmdDescuentosFormaPago.Enabled = Val(BuscarReglaNegocioStr( _
    "OyP_PromosxMetodoDePago", 0)) = 1 _
    And ExisteTablaV3("TMP_OYP_PRM_DETPAG", FrmAppLink.CnADM)
    
    CmdDescuentosFormaPago.Visible = CmdDescuentosFormaPago.Enabled
    lblTipoPromoMetodoDePago.Visible = CmdDescuentosFormaPago.Enabled
    lblTipoPromoMetodoDePago.Caption = StellarMensajeLocal(244)
    
    ValidarSeleccion
    
    Call AjustarPantalla(Me)
    
End Sub

Private Sub ValidarSeleccion()
    If TipoPromo = 1 Then
        'cmdPrecioOferta.BackColor = &H9E5300
        cmdPrecioOferta.Picture = Selected.Picture
        cmdPorcentajeDescuento.Picture = LoadPicture()
        cmdMontoDescuento.Picture = LoadPicture()
        CmdMxN_O_Valor.Picture = LoadPicture()
        Cmd_Premio_MxN_O_Valor.Picture = LoadPicture()
        CmdDescuentosFormaPago.Picture = LoadPicture()
    ElseIf TipoPromo = 2 Then
        'cmdPorcentajeDescuento.BackColor = &H9E5300
        cmdPorcentajeDescuento.Picture = Selected.Picture
        cmdPrecioOferta.Picture = LoadPicture()
        cmdMontoDescuento.Picture = LoadPicture()
        CmdMxN_O_Valor.Picture = LoadPicture()
        Cmd_Premio_MxN_O_Valor.Picture = LoadPicture()
        CmdDescuentosFormaPago.Picture = LoadPicture()
    ElseIf TipoPromo = 3 Then
        'cmdMontoDescuento.BackColor = &H9E5300
        cmdMontoDescuento.Picture = Selected.Picture
        cmdPrecioOferta.Picture = LoadPicture()
        cmdPorcentajeDescuento.Picture = LoadPicture()
        CmdMxN_O_Valor.Picture = LoadPicture()
        Cmd_Premio_MxN_O_Valor.Picture = LoadPicture()
        CmdDescuentosFormaPago.Picture = LoadPicture()
    ElseIf TipoPromo = 4 Or TipoPromo = 5 Or TipoPromo = 6 Or TipoPromo = 7 Then
        CmdMxN_O_Valor.Picture = Selected.Picture
        cmdPrecioOferta.Picture = LoadPicture()
        cmdPorcentajeDescuento.Picture = LoadPicture()
        cmdMontoDescuento.Picture = LoadPicture()
        Cmd_Premio_MxN_O_Valor.Picture = LoadPicture()
        CmdDescuentosFormaPago.Picture = LoadPicture()
    ElseIf TipoPromo = 8 Or TipoPromo = 9 Or TipoPromo = 10 Or TipoPromo = 11 Then
        Cmd_Premio_MxN_O_Valor.Picture = Selected.Picture
        CmdMxN_O_Valor.Picture = LoadPicture()
        cmdPrecioOferta.Picture = LoadPicture()
        cmdPorcentajeDescuento.Picture = LoadPicture()
        cmdMontoDescuento.Picture = LoadPicture()
        CmdDescuentosFormaPago.Picture = LoadPicture()
    ElseIf TipoPromo = 16 Or TipoPromo = 17 Or TipoPromo = 18 _
    Or TipoPromo = 19 Or TipoPromo = 20 Or TipoPromo = 21 Then
        CmdDescuentosFormaPago.Picture = Selected.Picture
        Cmd_Premio_MxN_O_Valor.Picture = LoadPicture()
        CmdMxN_O_Valor.Picture = LoadPicture()
        cmdPrecioOferta.Picture = LoadPicture()
        cmdPorcentajeDescuento.Picture = LoadPicture()
        cmdMontoDescuento.Picture = LoadPicture()
    Else 'NotImplementedYet
    End If
End Sub

Private Sub Form_Activate()
    If CancelarPromo_Salir Or PromocionCreada Then
        Unload Me
    End If
End Sub

Private Sub Header_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub
