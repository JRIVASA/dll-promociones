VERSION 5.00
Begin VB.Form frmAsistentePro_Campanna 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9540
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9540
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   7395
      Left            =   240
      TabIndex        =   5
      Top             =   1920
      Width           =   11685
      Begin VB.CommandButton CmdSiguiente 
         Caption         =   "Siguiente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8400
         TabIndex        =   1
         Top             =   6720
         Width           =   1455
      End
      Begin VB.ComboBox cmbCampanna 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   2400
         Width           =   3645
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9960
         TabIndex        =   2
         Top             =   6720
         Width           =   1455
      End
      Begin VB.Label LblDescripcionAsistente 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Este asistente le permitir� crear una Promoci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   240
         TabIndex        =   7
         Top             =   210
         Width           =   4995
      End
      Begin VB.Label lblGuiaUsuario 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione Campa�a:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   795
         TabIndex        =   6
         Top             =   1920
         Width           =   2250
      End
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   4
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_Campanna.frx":0000
      Top             =   480
      Width           =   2700
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_Campanna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private EventoProgramado As Boolean
Private FormaCargada As Boolean

Private Sub cmbCampanna_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        CmdSiguiente_Click
        Exit Sub
    End If
End Sub

Private Sub CmdCancelar_Click()
    If Mensaje(False, Replace(StellarMensaje(2620), "$(Line)", vbNewLine)) Then
        CancelarPromo_Salir = True
        Form_Activate
        Exit Sub
    End If
End Sub

Private Sub Form_Activate()
    
    If CancelarPromo_Salir Or PromocionCreada Then
        Unload Me
    End If
    
    If Not FormaCargada Then
        FormaCargada = True
    End If
    
End Sub

Private Sub Form_Load()
    
    FormaCargada = False
    If CancelarPromo_Salir Then Exit Sub
    
    lblTitulo.Caption = StellarMensajeLocal(125)
    LblDescripcionAsistente.Caption = StellarMensajeLocal(101)
    lblGuiaUsuario.Caption = StellarMensajeLocal(102)
    CmdSiguiente.Caption = StellarMensajeLocal(10)
    CmdCancelar.Caption = StellarMensajeLocal(11)
    
    Call AjustarPantalla(Me)
    
    Set ClsGrupos = FrmAppLink.GetClassGrupo
    
    EventoProgramado = True
    
    ClsGrupos.cTipoGrupo = "CAMP"
    ClsGrupos.CargarComboGrupos FrmAppLink.CnADM, cmbCampanna
    FrmAppLink.ListRemoveItems cmbCampanna, "Ninguno", vbNullString ' Quitar dichos elementos unicamente aqu� sin alterar la funci�n anterior.
    
    EventoProgramado = False
    
    CargarTemporal
    
End Sub

Private Sub CargarTemporal()
    
    On Error GoTo Error
    
    Dim RsPromociones As ADODB.Recordset
    Set RsPromociones = New ADODB.Recordset
    
    Set RsPromociones = FrmAppLink.CnADM.Execute("SELECT TOP 1 * FROM TMP_PROMOCION " & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ")
    
    With RsPromociones
        If Not .EOF Then
            TipoPromo = !Tipo_Promocion
            EventoProgramado = True
                ListSafeItemSelection cmbCampanna, !Campa�a
            EventoProgramado = False
        End If
    End With
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub cmbCampanna_Click()
    
    If Not EventoProgramado Then
        
        Static PosAnterior As Long
        
        If (PosAnterior <> cmbCampanna.ListIndex And cmbCampanna.ListIndex = cmbCampanna.ListCount - 1) Or cmbCampanna.ListCount = 1 Then
            PosAnterior = cmbCampanna.ListIndex
            EventoProgramado = True
            ClsGrupos.AgregarModificarGrupo FrmAppLink.CnADM, cmbCampanna
            ClsGrupos.CargarComboGrupos FrmAppLink.CnADM, cmbCampanna
            FrmAppLink.ListRemoveItems cmbCampanna, "Ninguno", vbNullString ' Quitar dichos elementos unicamente aqu� sin alterar la funci�n anterior.
            EventoProgramado = False
        ElseIf PosAnterior <> cmbCampanna.ListIndex And cmbCampanna.ListIndex < cmbCampanna.ListCount - 1 Then
            PosAnterior = cmbCampanna.ListIndex
        End If
    
        Campanna = cmbCampanna.Text
        
    End If
    
End Sub

Private Sub CmdSiguiente_Click()
    If cmbCampanna.ListCount <= 1 Then
        Mensaje True, StellarMensajeLocal(162) '"Debe a�adir o seleccionar una campa�a para la promoci�n."
        cmbCampanna_Click
        Exit Sub
    End If
    Campanna = cmbCampanna.Text
    frmAsistentePro_TipoPromo.Show vbModal
End Sub

Private Sub Header_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub
