VERSION 5.00
Begin VB.Form frmAsistentePro_TipoMetodoCarga 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9540
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9540
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   7395
      Left            =   240
      TabIndex        =   8
      Top             =   1920
      Width           =   11685
      Begin VB.OptionButton TipoCarga 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Default / Not Selected"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Index           =   3
         Left            =   1200
         MaskColor       =   &H00585A58&
         TabIndex        =   13
         Top             =   6840
         Value           =   -1  'True
         Visible         =   0   'False
         Width           =   3015
      End
      Begin VB.CommandButton cmdAtras 
         Caption         =   "Atras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   6840
         TabIndex        =   3
         Top             =   6720
         Width           =   1455
      End
      Begin VB.OptionButton TipoCarga 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Mixta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Index           =   2
         Left            =   1200
         MaskColor       =   &H00585A58&
         TabIndex        =   2
         Top             =   5100
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.OptionButton TipoCarga 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Por Lista de Productos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Index           =   1
         Left            =   1200
         MaskColor       =   &H00585A58&
         TabIndex        =   0
         Top             =   2580
         Width           =   3250
      End
      Begin VB.OptionButton TipoCarga 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Por Criterios Masivos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Index           =   0
         Left            =   1200
         MaskColor       =   &H00585A58&
         TabIndex        =   1
         Top             =   3720
         Width           =   3250
      End
      Begin VB.CommandButton CmdSiguiente 
         Caption         =   "Siguiente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8400
         TabIndex        =   4
         Top             =   6720
         Width           =   1455
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9960
         TabIndex        =   5
         Top             =   6720
         Width           =   1455
      End
      Begin VB.Label lblDescripcionMasiva 
         BackStyle       =   0  'Transparent
         Caption         =   $"frmAsistentePro_TipoMetodoCarga.frx":0000
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   645
         Left            =   1500
         TabIndex        =   12
         Top             =   4320
         Width           =   9300
      End
      Begin VB.Label lblDescripcionLista 
         BackStyle       =   0  'Transparent
         Caption         =   "La promocion solo se aplica a los productos seleccionados."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   525
         Left            =   1500
         TabIndex        =   11
         Top             =   3180
         Width           =   9300
      End
      Begin VB.Label LblDescripcionAsistente 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Este asistente le permitir� crear una Promoci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   240
         TabIndex        =   10
         Top             =   210
         Width           =   4995
      End
      Begin VB.Label lblGuiaUsuario 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione el m�todo para establecer los Productos de la Promoci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   795
         TabIndex        =   9
         Top             =   1920
         Width           =   7200
      End
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   7
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_TipoMetodoCarga.frx":0095
      Top             =   360
      Width           =   2700
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_TipoMetodoCarga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private mVarEsPremio As Boolean ' True -> Seleccionar Premios, False -> Seleccionar Condiciones.

Public Property Let TipoSeleccion(ByVal pPremios As Boolean)
    mVarEsPremio = pPremios
End Property

Private Sub CmdAtras_Click()
    Unload Me
End Sub

Private Sub CmdCancelar_Click()
    If Mensaje(False, Replace(StellarMensaje(2620), "$(Line)", vbNewLine)) Then
        CancelarPromo_Salir = True
        Form_Activate
        Exit Sub
    End If
End Sub

Private Sub CmdSiguiente_Click()
    
    If TipoCarga(0).Value = True Then ' 0 Carga Masiva
        
        Dim FrmCargaMasiva As New frmAsistentePro_CargarMasiva
        
        Select Case TipoPromo
            Case 8, 9, 10, 11, 12, 13, 14, 15
                FrmCargaMasiva.TipoSeleccion = mVarEsPremio
            Case Else
                FrmCargaMasiva.TipoSeleccion = False
        End Select
        
        FrmCargaMasiva.Show vbModal
        
    Else
        
        Dim FrmCargaLista As New frmAsistentePro_CargarLista
        
        Select Case TipoPromo
            Case 8, 9, 10, 11, 12, 13, 14, 15
                FrmCargaLista.TipoSeleccion = mVarEsPremio
            Case Else
                FrmCargaLista.TipoSeleccion = False
        End Select
        
        FrmCargaLista.Show vbModal
        
    End If
    
End Sub

Private Sub Form_Load()
    
    If CancelarPromo_Salir Then Exit Sub
    
    lblTitulo.Caption = StellarMensajeLocal(125)
    LblDescripcionAsistente.Caption = StellarMensajeLocal(101)
    
    CmdAtras.Caption = StellarMensajeLocal(12)
    CmdSiguiente.Caption = StellarMensajeLocal(10)
    CmdCancelar.Caption = StellarMensajeLocal(11)
    
    Select Case TipoPromo
        
        Case 8, 9, 10, 11, 12, 13, 14, 15 ' Premios
            
            If mVarEsPremio Then
                lblGuiaUsuario.Caption = StellarMensajeLocal(214) '"Seleccione el m�todo para cargar los premios de la promoci�n."
            Else
                lblGuiaUsuario.Caption = StellarMensajeLocal(215) '"Seleccione el m�todo para cargar los productos condicionantes."
            End If
            
            TipoCarga(0).Enabled = False
            
            If TipoCarga(3).Value Then
                TipoCarga(1).Value = True ' Solo permitir listado de productos, en este caso.
            End If
            
        Case 16, 17, 18, 19, 20, 21 ' Formas de Pago
            
            lblGuiaUsuario.Caption = StellarMensajeLocal(157)
            
            If TipoCarga(3).Value Then
                TipoCarga(0).Value = True ' Predeterminar masiva ya que en los descuentos por _
                forma de pago lo com�n es darle descuento a toda la factura.
            End If
            
        Case Else ' Cualquier otra. Promociones comunes.
            
            lblGuiaUsuario.Caption = StellarMensajeLocal(157)
            
            If TipoCarga(3).Value Then
                TipoCarga(1).Value = True ' Predeterminar selecci�n por listado de productos.
            End If
            
    End Select
    
    TipoCarga(0).Caption = StellarMensajeLocal(159) ' Masiva
    lblDescripcionMasiva.Caption = StellarMensajeLocal(171)
    TipoCarga(1).Caption = StellarMensajeLocal(158) ' Lista
    lblDescripcionLista.Caption = StellarMensajeLocal(170)
    'TipoCarga(2).Caption = StellarMensajeLocal(160) ' Mixta ' Disabled
    
    Call AjustarPantalla(Me)
    
End Sub

Private Sub Form_Activate()
    If CancelarPromo_Salir Or PromocionCreada Then
        Unload Me
    End If
End Sub

Private Sub Header_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub TipoCarga_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        CmdSiguiente_Click
        Exit Sub
    End If
End Sub
