VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frmAsistentePro_CargarLista 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12210
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   12210
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Excluir"
      Height          =   8895
      Left            =   180
      TabIndex        =   27
      Top             =   1860
      Width           =   11925
      Begin VB.CheckBox ChkExcluirInactivos 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Excluir Inactivos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   990
         Left            =   10680
         TabIndex        =   59
         Top             =   1200
         Width           =   1275
      End
      Begin VB.CheckBox ChkSeleccionarTodo 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Todos los productos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   510
         Left            =   8880
         TabIndex        =   57
         Top             =   2340
         Width           =   1515
      End
      Begin VB.PictureBox Medir 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   435
         Left            =   540
         ScaleHeight     =   375
         ScaleWidth      =   495
         TabIndex        =   52
         TabStop         =   0   'False
         Top             =   5280
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.CheckBox chkConsideraImpuesto 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Considera Impuesto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   2100
         TabIndex        =   7
         Top             =   3600
         Width           =   3315
      End
      Begin VB.CommandButton CmdMxN 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   9480
         Picture         =   "frmAsistentePro_CargaLista.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   3600
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox PROVEEDOR 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1800
         TabIndex        =   0
         Top             =   480
         Width           =   1425
      End
      Begin VB.CommandButton bProveedor 
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3300
         Picture         =   "frmAsistentePro_CargaLista.frx":1D82
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   480
         Width           =   435
      End
      Begin VB.Frame FrameSeccionMxN 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   3015
         Left            =   2640
         TabIndex        =   47
         Top             =   4980
         Visible         =   0   'False
         Width           =   7100
         Begin VB.CheckBox ChkCombinarProductos 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Aplicar promocion entre todos los productos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   495
            Left            =   3900
            TabIndex        =   14
            Top             =   840
            Width           =   2775
         End
         Begin VB.TextBox txtCantPagar 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   420
            Left            =   2400
            TabIndex        =   13
            Top             =   1200
            Width           =   1275
         End
         Begin VB.TextBox txtCantLlevar 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   420
            Left            =   2400
            TabIndex        =   12
            Top             =   600
            Width           =   1275
         End
         Begin VB.CommandButton CmdMxNSalir 
            Appearance      =   0  'Flat
            Caption         =   "Ocultar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Left            =   300
            Picture         =   "frmAsistentePro_CargaLista.frx":2584
            Style           =   1  'Graphical
            TabIndex        =   15
            Top             =   1800
            Width           =   1275
         End
         Begin VB.Label lblCantLlevar 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Cantidad Requerida:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   300
            TabIndex        =   50
            Top             =   720
            Width           =   1935
         End
         Begin VB.Label lblCantPagar 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Cantidad Promoci�n:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   300
            TabIndex        =   49
            Top             =   1260
            Width           =   1995
         End
         Begin VB.Line LnSeccionMxN 
            BorderColor     =   &H00AE5B00&
            X1              =   2505
            X2              =   6900
            Y1              =   345
            Y2              =   345
         End
         Begin VB.Label lblSeccionMxN 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Valores Promocion M x N"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Left            =   240
            TabIndex        =   48
            Top             =   240
            Width           =   2145
         End
      End
      Begin VB.TextBox txtCodigoProducto 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   1800
         TabIndex        =   6
         Top             =   2940
         Width           =   1740
      End
      Begin VB.CommandButton CmdBuscarProducto 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   3660
         Picture         =   "frmAsistentePro_CargaLista.frx":4306
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   2940
         Width           =   435
      End
      Begin VB.VScrollBar ScrollGrid 
         Height          =   3495
         LargeChange     =   10
         Left            =   11100
         TabIndex        =   44
         Top             =   4500
         Width           =   675
      End
      Begin VB.TextBox TxtNumPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   8220
         Width           =   780
      End
      Begin VB.TextBox txtPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   1980
         TabIndex        =   17
         Top             =   8220
         Width           =   780
      End
      Begin VB.TextBox txtCampoPromo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   2100
         TabIndex        =   8
         Top             =   3900
         Width           =   1740
      End
      Begin VB.TextBox txtCampo2 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   5700
         TabIndex        =   9
         Top             =   3900
         Visible         =   0   'False
         Width           =   960
      End
      Begin VB.TextBox txtCampo3 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   8400
         TabIndex        =   10
         Top             =   3900
         Visible         =   0   'False
         Width           =   960
      End
      Begin VB.TextBox subgrupo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1800
         TabIndex        =   3
         Top             =   1920
         Width           =   1440
      End
      Begin VB.CommandButton bSubgrupo 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3300
         Picture         =   "frmAsistentePro_CargaLista.frx":4B08
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   1920
         Width           =   435
      End
      Begin VB.TextBox grupo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1800
         TabIndex        =   2
         Top             =   1440
         Width           =   1440
      End
      Begin VB.CommandButton bGrupo 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3300
         Picture         =   "frmAsistentePro_CargaLista.frx":530A
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   1440
         Width           =   435
      End
      Begin VB.TextBox departamento 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1800
         TabIndex        =   1
         Top             =   960
         Width           =   1440
      End
      Begin VB.CommandButton bDpto 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   360
         Left            =   3300
         Picture         =   "frmAsistentePro_CargaLista.frx":5B0C
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   960
         Width           =   435
      End
      Begin VB.CommandButton CmdSiguiente 
         Caption         =   "Siguiente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8760
         TabIndex        =   20
         Top             =   8220
         Width           =   1455
      End
      Begin VB.TextBox TxtModelo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   5940
         TabIndex        =   5
         Top             =   2400
         Width           =   2100
      End
      Begin VB.TextBox TxtMarca 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   1800
         TabIndex        =   4
         Top             =   2400
         Width           =   2100
      End
      Begin VB.CommandButton CmdAtras 
         Caption         =   "Atras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7200
         TabIndex        =   19
         Top             =   8220
         Width           =   1455
      End
      Begin VB.CommandButton CmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   10320
         TabIndex        =   21
         Top             =   8220
         Width           =   1455
      End
      Begin MSFlexGridLib.MSFlexGrid Grid 
         DragMode        =   1  'Automatic
         Height          =   3495
         Left            =   120
         TabIndex        =   16
         Top             =   4500
         Width           =   11655
         _ExtentX        =   20558
         _ExtentY        =   6165
         _Version        =   393216
         Cols            =   12
         FixedCols       =   0
         BackColor       =   16448250
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         ScrollBars      =   2
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblArchivoPlano 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Archivo Plano"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   330
         Left            =   10860
         TabIndex        =   58
         Top             =   2940
         Width           =   630
         WordWrap        =   -1  'True
      End
      Begin VB.Image CmdArchivo 
         Height          =   600
         Left            =   10860
         Picture         =   "frmAsistentePro_CargaLista.frx":630E
         Stretch         =   -1  'True
         Top             =   2280
         Width           =   600
      End
      Begin VB.Label lblDescBtnBorrar 
         BackStyle       =   0  'Transparent
         Caption         =   "Eliminar Fila"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   345
         Left            =   5040
         TabIndex        =   56
         Top             =   8280
         Width           =   690
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblDescBtnExcluir 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Excluir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   165
         Left            =   11160
         TabIndex        =   55
         Top             =   4275
         Width           =   510
      End
      Begin VB.Label lblDescBtnIncluir 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Incluir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   165
         Left            =   10380
         TabIndex        =   54
         Top             =   4275
         Width           =   495
      End
      Begin VB.Label lblDescBtnCant 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Cantidad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   165
         Left            =   9480
         TabIndex        =   53
         Top             =   4275
         Visible         =   0   'False
         Width           =   660
      End
      Begin VB.Label lblDescripcionProducto 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   420
         Left            =   4200
         TabIndex        =   25
         Top             =   2940
         UseMnemonic     =   0   'False
         Width           =   6300
      End
      Begin VB.Label lblProducto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   46
         Top             =   3000
         Width           =   855
      End
      Begin VB.Image CmdDelete 
         Height          =   480
         Left            =   4500
         Picture         =   "frmAsistentePro_CargaLista.frx":8090
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Image ImgNext 
         Height          =   480
         Left            =   1140
         Picture         =   "frmAsistentePro_CargaLista.frx":CEB6
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Image ImgPrev 
         Height          =   480
         Left            =   300
         Picture         =   "frmAsistentePro_CargaLista.frx":11C45
         Stretch         =   -1  'True
         Top             =   8220
         Width           =   480
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   2880
         X2              =   3060
         Y1              =   8640
         Y2              =   8280
      End
      Begin VB.Image CmdExcluir 
         Height          =   600
         Left            =   11100
         Picture         =   "frmAsistentePro_CargaLista.frx":1512A
         Stretch         =   -1  'True
         Top             =   3600
         Width           =   600
      End
      Begin VB.Image CmdIncluir 
         Height          =   600
         Left            =   10320
         Picture         =   "frmAsistentePro_CargaLista.frx":18D17
         Stretch         =   -1  'True
         Top             =   3600
         Width           =   600
      End
      Begin VB.Label lblCampoPromo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Monto Descuento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   180
         TabIndex        =   43
         Top             =   4020
         Width           =   1845
      End
      Begin VB.Label lblCampo2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "% Descuento 2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   3960
         TabIndex        =   42
         Top             =   4020
         Visible         =   0   'False
         Width           =   1590
      End
      Begin VB.Label lblCampo3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "% Descuento 3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   6780
         TabIndex        =   41
         Top             =   4020
         Visible         =   0   'False
         Width           =   1590
      End
      Begin VB.Image Tooltip1 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   4080
         Picture         =   "frmAsistentePro_CargaLista.frx":199E1
         Stretch         =   -1  'True
         Top             =   2400
         Width           =   540
      End
      Begin VB.Image Tooltip2 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   8220
         Picture         =   "frmAsistentePro_CargaLista.frx":1B763
         Stretch         =   -1  'True
         Top             =   2400
         Width           =   540
      End
      Begin VB.Label lblSubgrupo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "SubGrupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   40
         Top             =   2040
         Width           =   945
      End
      Begin VB.Label lbl_PROVEEDOR 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3780
         TabIndex        =   39
         Top             =   480
         UseMnemonic     =   0   'False
         Width           =   6780
      End
      Begin VB.Label LBL_SUBGRUPO 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3780
         TabIndex        =   38
         Top             =   1920
         UseMnemonic     =   0   'False
         Width           =   6765
      End
      Begin VB.Label LBL_GRUPO 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3780
         TabIndex        =   37
         Top             =   1440
         UseMnemonic     =   0   'False
         Width           =   6765
      End
      Begin VB.Label LBL_DEPARTAMENTO 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3780
         TabIndex        =   36
         Top             =   960
         UseMnemonic     =   0   'False
         Width           =   6765
      End
      Begin VB.Label lblModelo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Modelo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   4920
         TabIndex        =   35
         Top             =   2520
         Width           =   690
      End
      Begin VB.Label lblMarca 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Marca"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   34
         Top             =   2520
         Width           =   600
      End
      Begin VB.Label lblValores 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Valor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   180
         TabIndex        =   33
         Top             =   3360
         Width           =   870
      End
      Begin VB.Line LnValores 
         BorderColor     =   &H00AE5B00&
         X1              =   1260
         X2              =   11760
         Y1              =   3480
         Y2              =   3480
      End
      Begin VB.Label lblGrupo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   32
         Top             =   1560
         Width           =   585
      End
      Begin VB.Label lblDpto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   31
         Top             =   1080
         Width           =   1410
      End
      Begin VB.Label lblProveedor 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Proveedor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   30
         Top             =   600
         Width           =   990
      End
      Begin VB.Label lblCriterios 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   180
         TabIndex        =   29
         Top             =   120
         Width           =   960
      End
      Begin VB.Line LnCriterios 
         BorderColor     =   &H00AE5B00&
         X1              =   1260
         X2              =   11760
         Y1              =   240
         Y2              =   240
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   6600
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_CargaLista.frx":1D4E5
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_CargaLista.frx":1F277
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_CargaLista.frx":1FF51
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_CargaLista.frx":20C2B
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_CargaLista.frx":229BD
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_CargaLista.frx":2474F
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentePro_CargaLista.frx":264E1
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Image ImgCash 
      Height          =   480
      Left            =   5520
      Picture         =   "frmAsistentePro_CargaLista.frx":28273
      Stretch         =   -1  'True
      Top             =   360
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image CmdConfigurarTeclado 
      Height          =   660
      Left            =   7080
      MousePointer    =   99  'Custom
      Picture         =   "frmAsistentePro_CargaLista.frx":2D252
      Stretch         =   -1  'True
      Top             =   1020
      Width           =   660
   End
   Begin VB.Image Logo 
      Height          =   900
      Left            =   480
      Picture         =   "frmAsistentePro_CargaLista.frx":2EFD4
      Top             =   360
      Width           =   2700
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Asistente para Crear Promoci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   8070
      TabIndex        =   26
      Top             =   1200
      Width           =   3900
   End
   Begin VB.Label Header 
      BackColor       =   &H009E5300&
      Height          =   1695
      Left            =   0
      TabIndex        =   28
      Top             =   0
      Width           =   12255
   End
End
Attribute VB_Name = "frmAsistentePro_CargarLista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private AnchoScrollBar      As Long
Private AnchoCampoScroll    As Long
Private pCargandoArchivo    As Boolean
Private pCargarLinea        As Boolean

Private DragStart, DragStop, DragRows, mDragEndRow

Private Enum GridCol
    ColRowID
    ColTipoCondicion
    ColCodProd
    ColDesProd
    ColMarca
    ColModelo
    ColValorPromo1
    ColValorPromo2
    ColValorPromo3
    ColValorMxN
    [ColCount]
End Enum

Private mVarEsPremio As Boolean ' True -> Seleccionar Premios, False -> Seleccionar Condiciones.

Public Property Let TipoSeleccion(ByVal pPremios As Boolean)
    mVarEsPremio = pPremios
End Property

Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
  
  Dim Ctl As Control
  Dim bHandled As Boolean
  Dim bOver As Boolean
  
  For Each Ctl In Controls
    ' Is the mouse over the control
    On Error Resume Next
    bOver = (Ctl.Visible And IsOver(Ctl.hWnd, Xpos, Ypos))
    On Error GoTo 0
    
    If bOver Then
      ' If so, respond accordingly
      bHandled = True
      Select Case True
      
        Case TypeOf Ctl Is MSFlexGrid
          FlexGridScroll Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is PictureBox
          PictureBoxZoom Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is ListBox, TypeOf Ctl Is TextBox, TypeOf Ctl Is ComboBox
          ' These controls already handle the mousewheel themselves, so allow them to:
          If Ctl.Enabled Then Ctl.SetFocus
          
        Case Else
          bHandled = False

      End Select
      If bHandled Then Exit Sub
    End If
    bOver = False
  Next Ctl
  
  ' Scroll was not handled by any controls, so treat as a general message send to the form
  'Me.Caption = "Form Scroll " & IIf(Rotation < 0, "Down", "Up")
End Sub

Private Sub ChkCombinarProductos_Click()
    
    If ChkCombinarProductos.Value = vbChecked Then
        
        Dim mValidarObligatorios As Boolean
        
        If Not IsNumeric(txtCantLlevar.Text) Then txtCantLlevar.Text = 0
        If Not IsNumeric(txtCantPagar.Text) Then txtCantPagar.Text = 0
        
        Select Case TipoPromo
            Case 4
                mValidarObligatorios = _
                CDbl(txtCantLlevar.Text) > 0 And CDbl(txtCantPagar.Text) > 0 And _
                CDbl(txtCantLlevar.Text) > CDbl(txtCantPagar.Text)
                If Not mValidarObligatorios Then
                    MensajeValidacion = StellarMensajeLocal(172)
                    If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
                End If
            Case 5, 6, 7
                mValidarObligatorios = _
                CDbl(txtCantLlevar.Text) > 0 And CDbl(txtCantPagar.Text) > 0 And _
                CDbl(txtCantLlevar.Text) >= CDbl(txtCantPagar.Text)
                If Not mValidarObligatorios Then
                    MensajeValidacion = StellarMensajeLocal(173)
                    If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
                End If
        End Select
        
        If mValidarObligatorios Then
            
            ExecuteSafeSQL "UPDATE TMP_PROMOCION SET " & vbNewLine & _
            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & vbNewLine & _
            "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
            "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
            FrmAppLink.CnADM
            
            ExecuteSafeSQL "UPDATE TMP_PROMOCION_CONDICION_VALORES SET " & vbNewLine & _
            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), " & vbNewLine & _
            "Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & vbNewLine & _
            "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
            "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
            FrmAppLink.CnADM
            
            txtPag.Tag = "0"
            txtPag_LostFocus
            
            txtCantLlevar.Locked = True
            txtCantPagar.Locked = True
            
        End If
        
    Else
        txtCantLlevar.Locked = False
        txtCantPagar.Locked = False
        ExecuteSafeSQL "UPDATE TMP_PROMOCION SET " & vbNewLine & _
        "Cantidad_Productos_Requerir = 0, Cantidad_Productos_Pagar = 0 " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
        FrmAppLink.CnADM
    End If
End Sub

Private Sub ChkSeleccionarTodo_Click()
    If ChkSeleccionarTodo.Value = vbChecked Then
        ResetearCriterios True
    End If
End Sub

Private Sub CmdArchivo_Click()
    
    On Error GoTo Error
    
    Dim CodigoProducto As String, ValorCampoPromo1 As Double, _
    ValorCampoPromo2 As Double, ValorCampoPromo3 As Double, _
    ValorMxNLlevar As Double, ValorMxNPagar As Double
    
    Set gClsArcBD = CreateObject("recsun.obj_ArchivoDB")
    
    gClsArcBD.Inicializar 0
    
    gClsArcBD.Adicionar_CamposDisponibles "CodProducto", "" & StellarMensaje(16524) & "", 3000
    
    Select Case TipoPromo
        Case 1 'Precio Oferta
CasePrc1:
            If (TipoPromo = 9 Or TipoPromo = 13) And Not mVarEsPremio Then
            Else
                gClsArcBD.Adicionar_CamposDisponibles "Prc", "" & StellarMensajeLocal(104) & "", 3000
            End If
        Case 2 '% Descuento
CasePorc1:
            If (TipoPromo = 10 Or TipoPromo = 14) And Not mVarEsPremio Then
            Else
                gClsArcBD.Adicionar_CamposDisponibles "Desc1", "" & StellarMensajeLocal(105) & " 1", 3000
                gClsArcBD.Adicionar_CamposDisponibles "Desc2", "" & StellarMensajeLocal(105) & " 2", 3000
                gClsArcBD.Adicionar_CamposDisponibles "Desc3", "" & StellarMensajeLocal(105) & " 3", 3000
            End If
        Case 3 'Monto Descuento
CaseMonto1:
            If (TipoPromo = 11 Or TipoPromo = 15) And Not mVarEsPremio Then
            Else
                gClsArcBD.Adicionar_CamposDisponibles "Monto", "" & StellarMensajeLocal(106) & "", 3000
            End If
        Case 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
            Select Case TipoPromo
                Case 4, 8, 12
                    If TipoPromo = 4 Then
                        gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                        gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(222) & "", 3000
                    Else
                        If Not mVarEsPremio Then
                            gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                        Else
                            gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(223) & "", 3000
                        End If
                    End If
                Case 5, 9, 13 'Precio Oferta
                    If TipoPromo = 5 Then
                        gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                        gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(223) & "", 3000
                        GoTo CasePrc1
                    Else
                        If Not mVarEsPremio Then
                            gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                        Else
                            gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(223) & "", 3000
                            GoTo CasePrc1
                        End If
                    End If
                Case 6, 10, 14 '% Descuento
                    If TipoPromo = 6 Then
                        gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                        gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(223) & "", 3000
                        GoTo CasePorc1
                    Else
                        If Not mVarEsPremio Then
                            gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                        Else
                            gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(223) & "", 3000
                            GoTo CasePorc1
                        End If
                    End If
                Case 7, 11, 15 'Monto Descuento
                    If TipoPromo = 7 Then
                        gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                        gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(223) & "", 3000
                        GoTo CaseMonto1
                    Else
                        If Not mVarEsPremio Then
                            gClsArcBD.Adicionar_CamposDisponibles "Llevar", "" & StellarMensajeLocal(221) & "", 3000
                        Else
                            gClsArcBD.Adicionar_CamposDisponibles "Pagar", "" & StellarMensajeLocal(223) & "", 3000
                            GoTo CaseMonto1
                        End If
                    End If
            End Select
        Case 16, 17, 18, 19, 20, 21
            gClsArcBD.Adicionar_CamposDisponibles "Desc1", "" & StellarMensajeLocal(105) & " 1", 3000
    End Select
    
    Dim mValores, LenArr
    mValores = gClsArcBD.EJECUTAR
    If IsEmpty(mValores) Then Exit Sub
    
    MousePointer = vbHourglass
    DoEvents
    pCargandoArchivo = True
    ResetearCriterios
    
    LenArr = UBound(mValores)
    
    Dim pCuentaExito As Double, pCuentaFallo As Double, pCantRegistros As Double
    Dim mInfoFallidos As String: mInfoFallidos = Empty
    
    For I = 1 To LenArr
        
        Dim CodigoEntrada As String
        
        CodigoEntrada = mValores(I)(0)
        
        txtCodigoProducto.Text = CodigoEntrada
        txtCodigoProducto_LostFocus
        
        Select Case TipoPromo
            
            Case 1 'Precio Oferta
CasePrc2:
                If IsNumeric(mValores(I)(1)) Then
                    ValorCampoPromo1 = CDbl(mValores(I)(1))
                Else
                    ValorCampoPromo1 = 0
                End If
                
            Case 2 '% Descuento
CasePorc2:
                If IsNumeric(mValores(I)(1)) Then
                    ValorCampoPromo1 = CDbl(mValores(I)(1))
                Else
                    ValorCampoPromo1 = 0
                End If
                
                If IsNumeric(mValores(I)(2)) Then
                    ValorCampoPromo2 = CDbl(mValores(I)(2))
                Else
                    ValorCampoPromo2 = 0
                End If
                
                If IsNumeric(mValores(I)(3)) Then
                    ValorCampoPromo3 = CDbl(mValores(I)(3))
                Else
                    ValorCampoPromo3 = 0
                End If
                
            Case 3 'Monto Descuento
CaseMonto2:
                If IsNumeric(mValores(I)(1)) Then
                    ValorCampoPromo1 = CDbl(mValores(I)(1))
                Else
                    ValorCampoPromo1 = 0
                End If
                
            Case 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
                
                Select Case TipoPromo
                
                    Case 4
                        
                        If IsNumeric(mValores(I)(1)) Then
                            ValorMxNLlevar = CDbl(mValores(I)(1))
                        Else
                            ValorMxNLlevar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(2)) Then
                            ValorMxNPagar = CDbl(mValores(I)(2))
                        Else
                            ValorMxNPagar = 0
                        End If
                        
                    Case 5 'Precio Oferta
                        
                        If IsNumeric(mValores(I)(1)) Then
                            ValorMxNLlevar = CDbl(mValores(I)(1))
                        Else
                            ValorMxNLlevar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(2)) Then
                            ValorMxNPagar = CDbl(mValores(I)(2))
                        Else
                            ValorMxNPagar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(3)) Then
                            ValorCampoPromo1 = CDbl(mValores(I)(3))
                        Else
                            ValorCampoPromo1 = 0
                        End If
                        
                    Case 6 '% Descuento
                        
                        If IsNumeric(mValores(I)(1)) Then
                            ValorMxNLlevar = CDbl(mValores(I)(1))
                        Else
                            ValorMxNLlevar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(2)) Then
                            ValorMxNPagar = CDbl(mValores(I)(2))
                        Else
                            ValorMxNPagar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(3)) Then
                            ValorCampoPromo1 = CDbl(mValores(I)(3))
                        Else
                            ValorCampoPromo1 = 0
                        End If
                        
                        If IsNumeric(mValores(I)(4)) Then
                            ValorCampoPromo2 = CDbl(mValores(I)(4))
                        Else
                            ValorCampoPromo2 = 0
                        End If
                        
                        If IsNumeric(mValores(I)(5)) Then
                            ValorCampoPromo3 = CDbl(mValores(I)(5))
                        Else
                            ValorCampoPromo3 = 0
                        End If
                        
                    Case 7 'Monto Descuento
                        
                        If IsNumeric(mValores(I)(1)) Then
                            ValorMxNLlevar = CDbl(mValores(I)(1))
                        Else
                            ValorMxNLlevar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(2)) Then
                            ValorMxNPagar = CDbl(mValores(I)(2))
                        Else
                            ValorMxNPagar = 0
                        End If
                        
                        If IsNumeric(mValores(I)(3)) Then
                            ValorCampoPromo1 = CDbl(mValores(I)(3))
                        Else
                            ValorCampoPromo1 = 0
                        End If
                        
                    Case 8, 12
                        
                        'If Not mVarEsPremio Then
                            
                            If IsNumeric(mValores(I)(1)) Then
                                ValorMxNLlevar = CDbl(mValores(I)(1))
                            Else
                                ValorMxNLlevar = 0
                            End If
                            
                        'Else
                            
                            'If IsNumeric(mValores(I)(1)) Then
                                'ValorMxNPagar = CDbl(mValores(I)(1))
                            'Else
                                'ValorMxNPagar = 0
                            'End If
                            
                        'End If
                        
                    Case 9, 13
                        
                        If Not mVarEsPremio Then
                            
                            If IsNumeric(mValores(I)(1)) Then
                                ValorMxNLlevar = CDbl(mValores(I)(1))
                            Else
                                ValorMxNLlevar = 0
                            End If
                            
                        Else
                            
                            If IsNumeric(mValores(I)(1)) Then
                                ValorMxNLlevar = CDbl(mValores(I)(1))
                            Else
                                ValorMxNLlevar = 0
                            End If
                            
                            If IsNumeric(mValores(I)(2)) Then
                                ValorCampoPromo1 = CDbl(mValores(I)(2))
                            Else
                                ValorCampoPromo1 = 0
                            End If
                            
                        End If
                        
                    Case 10, 14
                        
                        If Not mVarEsPremio Then
                            
                            If IsNumeric(mValores(I)(1)) Then
                                ValorMxNLlevar = CDbl(mValores(I)(1))
                            Else
                                ValorMxNLlevar = 0
                            End If
                            
                        Else
                            
                            If IsNumeric(mValores(I)(1)) Then
                                ValorMxNLlevar = CDbl(mValores(I)(1))
                            Else
                                ValorMxNLlevar = 0
                            End If
                            
                            If IsNumeric(mValores(I)(2)) Then
                                ValorCampoPromo1 = CDbl(mValores(I)(2))
                            Else
                                ValorCampoPromo1 = 0
                            End If
                            
                            If IsNumeric(mValores(I)(3)) Then
                                ValorCampoPromo2 = CDbl(mValores(I)(3))
                            Else
                                ValorCampoPromo2 = 0
                            End If
                            
                            If IsNumeric(mValores(I)(4)) Then
                                ValorCampoPromo3 = CDbl(mValores(I)(4))
                            Else
                                ValorCampoPromo3 = 0
                            End If
                            
                        End If
                        
                    Case 11, 15
                        
                        If Not mVarEsPremio Then
                            
                            If IsNumeric(mValores(I)(1)) Then
                                ValorMxNLlevar = CDbl(mValores(I)(1))
                            Else
                                ValorMxNLlevar = 0
                            End If
                            
                        Else
                            
                            If IsNumeric(mValores(I)(1)) Then
                                ValorMxNLlevar = CDbl(mValores(I)(1))
                            Else
                                ValorMxNLlevar = 0
                            End If
                            
                            If IsNumeric(mValores(I)(2)) Then
                                ValorCampoPromo1 = CDbl(mValores(I)(2))
                            Else
                                ValorCampoPromo1 = 0
                            End If
                            
                        End If
                        
                End Select
                
            Case 16, 17, 18, 19, 20, 21
                
                If IsNumeric(mValores(I)(1)) Then
                    ValorCampoPromo1 = CDbl(mValores(I)(1))
                Else
                    ValorCampoPromo1 = 0
                End If
                
                ValorCampoPromo2 = 0
                ValorCampoPromo3 = 0
                
        End Select
        
        txtCampoPromo.Text = ValorCampoPromo1
        txtCampo2.Text = ValorCampoPromo2
        txtCampo3.Text = ValorCampoPromo3
        
        If ChkCombinarProductos.Value = vbChecked Then
            '
        Else
            txtCantLlevar.Text = ValorMxNLlevar
            txtCantPagar.Text = ValorMxNPagar
        End If
        
        'Debug.Assert Not ValorCampoPromo1 = 0
        
        DoEvents
        
        pCargarLinea = False
        
        CmdIncluir_Click
        
        pCantRegistros = pCantRegistros + 1
        
        If pCargarLinea Then
            pCuentaExito = pCuentaExito + 1
        Else
            pCuentaFallo = pCuentaFallo + 1
            mInfoFallidos = mInfoFallidos & _
            "" & StellarMensaje(6144) & ": " & (I) & ", " & StellarMensaje(142) & ": " & CodigoEntrada & vbNewLine
        End If
        
    Next
    
    Dim mMsjEstatus As String
    
    mMsjEstatus = "" & StellarMensajeLocal(224) & ": (" & pCuentaExito & " " & _
    StellarMensajeLocal(169) & " " & pCantRegistros & ")" & vbNewLine
    
    If pCuentaFallo > 0 Then
        ActivarMensajeGrande 100
        mMsjEstatus = mMsjEstatus & _
        "" & StellarMensajeLocal(225) & ": (" & pCuentaFallo & ")" & vbNewLine
        mMsjEstatus = mMsjEstatus & _
        "" & StellarMensajeLocal(226) & ":" & vbNewLine & vbNewLine
        mMsjEstatus = mMsjEstatus & mInfoFallidos
    End If
    
    Mensaje True, mMsjEstatus
    
    CargarTemporal
    
Finally:
    
    pCargandoArchivo = False
    
    MousePointer = vbDefault
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    GoTo Finally
    
End Sub

Private Sub CmdBuscarProducto_Click()
    txtCodigoProducto_KeyDown vbKeyF2, 0
End Sub

Private Sub CmdConfigurarTeclado_Click()
    SeleccionarTexto CampoT
    TecladoAvanzado CampoT
End Sub

Private Sub ValidarIngresarLimiteDescuento()
    
    Dim mMsj1, mMsj2, TmpCodMonedaLimite, TmpMontoLimite
    
    TmpCodMonedaLimite = BuscarValorBD("CodMonedaLimite", _
    "SELECT Top 1 Cod_Dpto As CodMonedaLimite FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
    "AND Tipo_Condicion = 7 ", Empty, FrmAppLink.CnADM)
    
    TmpMontoLimite = BuscarValorBD("MontoLimite", _
    "SELECT Top 1 Monto_Descuento As MontoLimite FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
    "AND Tipo_Condicion = 7 ", Empty, FrmAppLink.CnADM)
    
    If Trim(TmpCodMonedaLimite) <> Empty Then
        
        MonedaTmp.BuscarMonedas , TmpCodMonedaLimite
        
        'mMsj1 = "El L�mite de Descuento actualmente establecido es de " & _
        FormatoDecimalesDinamicos(CDec(TmpMontoLimite), , MonedaTmp.DecMoneda) & " " & _
        MonedaTmp.SimMoneda & " " & "(" & MonedaTmp.DesMoneda & "). "
        
        'mMsj2 = "Si desea modificar el l�mite actual y / o su moneda presione Aceptar."
        
        mMsj1 = Replace(StellarMensajeLocal(253), "$(ValorFmt)", _
        FormatoDecimalesDinamicos(CDec(TmpMontoLimite), , MonedaTmp.DecMoneda) & " " & _
        MonedaTmp.SimMoneda & " " & "(" & MonedaTmp.DesMoneda & ")")
        
        mMsj2 = StellarMensajeLocal(254)
        
    Else
        
        'mMsj1 = "Actualmente no ha definido un Monto L�mite de Descuento para la promoci�n. " & _
        "Si desea establecerlo, presione Aceptar, o Cancelar para regresar."
        
        mMsj1 = StellarMensajeLocal(255)
        
        mMsj2 = Empty
        
    End If
    
    If Mensaje(False, mMsj1 & mMsj2) Then
        
        Set Campo_Txt = txtCampo3
        Set Campo_Lbl = lblCampo3
        
        Campo_Txt = Empty
        Campo_Lbl = Empty
        
        Call MAKE_VIEW("MA_MONEDAS_USO_EN_POS", "c_CodMoneda", "c_Descripcion", _
        "" & StellarMensaje(2855) & "", Me, "GENERICO", True, Campo_Txt, Campo_Lbl)
        
        If Trim(Campo_Txt) <> Empty Then
            
            MonedaTmp.BuscarMonedas , Campo_Txt
            
            'Campo_Lbl = QuickInputRequest("La moneda seleccionada es " & MonedaTmp.DesMoneda & " " & _
            "(" & MonedaTmp.SimMoneda & "). Si es correcto, indique el monto l�mite de descuento.")
            Campo_Lbl = QuickInputRequest(Replace(StellarMensajeLocal(256), "$(CurrencyData)", _
            "" & MonedaTmp.DesMoneda & " " & "(" & MonedaTmp.SimMoneda & ")"))
            
            If SDec(Campo_Lbl) >= 0 Then
                
                'If Mensaje(False, "El monto introducido es: " & _
                FormatoDecimalesDinamicos(SDec(Campo_Lbl), , MonedaTmp.DecMoneda) & " " & _
                MonedaTmp.SimMoneda & vbNewLine & vbNewLine & "Si es correcto presione Aceptar " & _
                "para confirmar el cambio.") Then
                If Mensaje(False, Replace(StellarMensajeLocal(257), "$(Param1)", _
                FormatoDecimalesDinamicos(SDec(Campo_Lbl), , MonedaTmp.DecMoneda) & " " & _
                MonedaTmp.SimMoneda & vbNewLine & vbNewLine)) Then
                    
                    FrmAppLink.CnADM.Execute "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
                    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
                    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
                    "AND Tipo_Condicion = 7 "
                    
                    If SDec(Campo_Lbl) > 0 Then
                        
                        FrmAppLink.CnADM.Execute "INSERT INTO TMP_PROMOCION_CONDICION_VALORES " & _
                        "(Cod_Promocion, CodUsuario, Tipo_Condicion, Cod_Dpto, Monto_Descuento) " & _
                        "SELECT '" & QuitarComillasSimples(mCodPromo_Actual) & "', " & _
                        "'" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "', " & _
                        "7, '" & MonedaTmp.CodMoneda & "', " & _
                        "(" & CDec(FormatoDecimalesDinamicos(CDec(Campo_Lbl), , MonedaTmp.DecMoneda)) & ") "
                        
                    End If
                    
                End If
                
            Else
                Mensaje True, StellarMensaje(16136) ' Dato inv�lido.
            End If
            
        End If
        
    End If
    
End Sub

Private Sub CmdMxN_Click()
    If TipoPromo = 16 Or TipoPromo = 17 Or TipoPromo = 18 _
    Or TipoPromo = 19 Or TipoPromo = 20 Or TipoPromo = 21 Then
        ValidarIngresarLimiteDescuento
    Else
        If Not pCargandoArchivo Then
            Call MostrarFrameMxN(Not FrameSeccionMxN.Visible)
        End If
    End If
End Sub

Private Sub CmdMxNSalir_Click()
    Call MostrarFrameMxN(Not FrameSeccionMxN.Visible)
End Sub

Private Sub MostrarFrameMxN(Valor As Boolean)
    
    If Valor Then
        FrameSeccionMxN.Visible = True
        For I = 0 To FrameSeccionMxN.Width + (Frame1.Left * 2)
            FrameSeccionMxN.Left = FrameSeccionMxN.Left - 1
            If (FrameSeccionMxN.Left Mod 250) = 0 Then DoEvents
        Next I
        FrameSeccionMxN.ZOrder 0
        If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
        SeleccionarTexto txtCantLlevar
    Else
        For I = 0 To FrameSeccionMxN.Width + (Frame1.Left * 2)
            FrameSeccionMxN.Left = FrameSeccionMxN.Left + 1
            If (FrameSeccionMxN.Left Mod 250) = 0 Then DoEvents
        Next I
        FrameSeccionMxN.Visible = False
    End If
    
    'Call HabilitarVentana(Not Valor)
    
End Sub

Private Sub GrabarOtrosDatos()
    Select Case TipoPromo
    Case 8, 9, 10, 11, 12, 13, 14, 15
        If mVarEsPremio Then GoTo Aplica
    Case Else
Aplica:
        ExecuteSafeSQL "UPDATE TMP_PROMOCION SET " & vbNewLine & _
        "Considera_Impuesto = " & IIf(chkConsideraImpuesto.Value = vbChecked, 1, 0) & " " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
        FrmAppLink.CnADM
    End Select
End Sub

Private Sub CmdSiguiente_Click()
    
    If Not ((Grid.TextMatrix(1, ColRowID) <> Empty _
    And Grid.TextMatrix(1, ColTipoCondicion) <> Empty) _
    Or SVal(TxtNumPag) > 1) Then
        Mensaje True, StellarMensajeLocal(197)
        Exit Sub
    End If
    
    GrabarOtrosDatos
    
    Select Case TipoPromo
        Case 8, 9, 10, 11, 12, 13, 14, 15
            If mVarEsPremio Then
                GoTo Aplica
            Else
                Dim FrmTipoCarga As New frmAsistentePro_TipoMetodoCarga
                FrmTipoCarga.TipoSeleccion = True
                FrmTipoCarga.Show vbModal
            End If
        Case 16, 17, 18, 19, 20, 21
            frmAsistentePro_ConfigFormaPago.Show vbModal
        Case Else
Aplica:
            frmAsistentePro_CargarSucursales.Show vbModal
    End Select
    
End Sub

Private Sub departamento_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub departamento_GotFocus()
    Set CampoT = departamento
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Cancel = 0 Then
        Call WheelUnHook(Me.hWnd)
    End If
End Sub

Private Sub Grid_Scroll()
    'DoEvents
    'ScrollGrid.Value = Grid.Row
End Sub

Private Sub grupo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub grupo_GotFocus()
    Set CampoT = grupo
End Sub

Private Sub PROVEEDOR_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub PROVEEDOR_GotFocus()
    Set CampoT = PROVEEDOR
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
    Exit Sub
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Private Sub CmdAtras_Click()
    Unload Me
End Sub

Private Sub CmdCancelar_Click()
    If Mensaje(False, Replace(StellarMensaje(2620), "$(Line)", vbNewLine)) Then
        CancelarPromo_Salir = True
        Form_Activate
        Exit Sub
    End If
End Sub

Private Sub Grid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        CmdDelete_Click
    End If
End Sub

Private Sub Grid_DragDrop(Source As Control, x As Single, y As Single)
    
    If Grid.RowHeightMin = 0 Then Exit Sub
    
    On Error Resume Next
    
    DragStop = y
    
    Drag = (DragStop - DragStart)
    
    If Abs(Drag / Grid.RowHeightMin) > 1 Then
        
        DragRows = (Drag / Grid.RowHeightMin)
        
        mDragEndRow = (Grid.TopRow + (-1 * DragRows))
        
        If mDragEndRow < Grid.FixedRows Then
            mDragEndRow = Grid.FixedRows
        ElseIf mDragEndRow > Grid.Rows Then
            mDragEndRow = Grid.Rows - 1
        End If
        
        Grid.TopRow = mDragEndRow
        ScrollGrid.Value = Grid.TopRow
        
    End If
    
    DragStart = 0
    
End Sub

Private Sub Grid_DragOver(Source As Control, x As Single, y As Single, State As Integer)
    
    On Error Resume Next
    
    If DragStart = 0 Then
        DragStart = y
    End If
    
End Sub

Private Sub bproveedor_Click()
    PROVEEDOR.SetFocus
    Call PROVEEDOR_KeyDown(vbKeyF2, 0)
End Sub

Private Sub ImgNext_Click()
    
    If SVal(txtPag.Text) <= SVal(TxtNumPag.Text) Then
        txtPag.Text = SVal(txtPag.Text) + 1
        If SVal(txtPag.Text) > SVal(TxtNumPag.Text) Then txtPag.Text = SVal(TxtNumPag.Text)
    Else
        txtPag.Text = SVal(TxtNumPag.Text)
    End If
    
    CargarTemporal
    
End Sub

Private Sub ImgPrev_Click()
    
    If SVal(txtPag.Text) >= 1 Then
        txtPag.Text = SVal(txtPag.Text) - 1
        If SVal(txtPag.Text) <= 1 Then txtPag.Text = 1
    Else
        txtPag.Text = 1
    End If
    
    CargarTemporal
    
End Sub

Private Sub PROVEEDOR_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            PROVEEDOR.Text = Empty
            Call MAKE_VIEW("MA_PROVEEDORES", "c_CodProveed", "c_Descripcio", _
            StellarMensaje(403), Me, "GENERICO", , PROVEEDOR, lbl_PROVEEDOR) '"P R O V E E D O R E S"
            
        Case Is = vbKeyDelete
            
            PROVEEDOR = Empty
            lbl_PROVEEDOR = Empty
            
        Case Is = vbKeyReturn
            PROVEEDOR_LostFocus
            
    End Select
    
End Sub

Private Sub PROVEEDOR_LostFocus()
    
    Dim RsProveedor As New ADODB.Recordset, cProveedor As String
    
    If Trim(PROVEEDOR.Text) <> Empty Then
        RsProveedor.Open "SELECT * FROM MA_PROVEEDORES WHERE c_CodProveed = '" & _
        QuitarComillasSimples(PROVEEDOR.Text) & "' ", _
        FrmAppLink.CnADM, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsProveedor.EOF Then
            lbl_PROVEEDOR.Caption = RsProveedor!c_Descripcio
        Else
            lbl_PROVEEDOR.Caption = Empty
            PROVEEDOR.Text = Empty
            Mensaje True, StellarMensaje(16105) '"No existe proveedor con este c�digo.")
        End If
        
        RsProveedor.Close
    Else
        lbl_PROVEEDOR.Caption = Empty
        PROVEEDOR.Text = Empty
    End If
    
End Sub

Private Sub bdpto_Click()
    departamento.SetFocus
    Call departamento_KeyDown(vbKeyF2, 0)
End Sub

Private Sub departamento_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Set Forma = Me
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Tabla = "MA_DEPARTAMENTOS"
            Titulo = UCase(Stellar_Mensaje(361)) '"D E P A R T A M E N T O S"
            Call DGS(Forma, Titulo, Tabla)
            
        Case Is = vbKeyDelete
            
            departamento.Tag = Empty
            departamento.Text = Empty
            LBL_DEPARTAMENTO.Caption = Empty
            grupo.Tag = Empty
            grupo.Text = Empty
            LBL_GRUPO.Caption = Empty
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
            
        Case Is = vbKeyReturn
            
            departamento_LostFocus
            
    End Select
    
End Sub

Private Sub departamento_LostFocus()
    If departamento.Text <> Empty Then
        Set Forma = Me
        Tabla = "MA_DEPARTAMENTOS"
        Titulo = UCase(Stellar_Mensaje(361)) '"D E P A R T A M E N T O S"
        Call PrepararDGS(Forma, Titulo, Tabla)
        Call Carga_DGS(departamento, "MA_DEPARTAMENTOS", 0)
    Else
        Call departamento_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub bgrupo_Click()
    grupo.SetFocus
    Call grupo_KeyDown(vbKeyF2, 0)
End Sub

Private Sub grupo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Set Forma = Me
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Tabla = "MA_GRUPOS"
            Titulo = UCase(Stellar_Mensaje(362)) 'G R U P O S"
            Call DGS(Forma, Titulo, Tabla)
            
        Case Is = vbKeyDelete
            
            grupo.Tag = Empty
            grupo.Text = Empty
            LBL_GRUPO.Caption = Empty
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
            
        Case Is = vbKeyReturn
            grupo_LostFocus
            
    End Select
    
End Sub

Private Sub grupo_LostFocus()
    If grupo.Text <> Empty Then
        Set Forma = Me
        Tabla = "MA_GRUPOS"
        Titulo = UCase(Stellar_Mensaje(362)) 'G R U P O S"
        Call PrepararDGS(Forma, Titulo, Tabla)
        Call Carga_DGS(grupo, "MA_GRUPOS", 1)
    Else
        Call grupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub bsubgrupo_Click()
    subgrupo.SetFocus
    Call subgrupo_KeyDown(vbKeyF2, 0)
End Sub

Private Sub subgrupo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub subgrupo_GotFocus()
    Set CampoT = subgrupo
End Sub

Private Sub subgrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Set Forma = Me
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Tabla = "MA_SUBGRUPOS"
            Titulo = UCase(Stellar_Mensaje(363)) 'S U B - G R U P O S"
            Call DGS(Forma, Titulo, Tabla)
            
        Case Is = vbKeyDelete
            subgrupo.Tag = Empty
            subgrupo.Text = Empty
            LBL_SUBGRUPO.Caption = Empty
            
        Case Is = vbKeyReturn
            subgrupo_LostFocus
            
    End Select
    
End Sub

Private Sub subgrupo_LostFocus()
    If subgrupo.Text <> Empty Then
        Set Forma = Me
        Tabla = "MA_SUBGRUPOS"
        Titulo = UCase(Stellar_Mensaje(363)) 'S U B - G R U P O S"
        Call PrepararDGS(Forma, Titulo, Tabla)
        Call Carga_DGS(subgrupo, "MA_SUBGRUPOS", 2)
    Else
        Call subgrupo_KeyDown(vbKeyDelete, 0)
    End If
End Sub

Private Sub CargarGrid()
    
    With Grid
        
        .Rows = 1
        .Row = 0
        .Clear
        .Cols = ColCount
        .RowHeightMin = 600
        
        .Col = ColRowID
        .TextMatrix(0, .Col) = "Ln" '"RowID"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColTipoCondicion
        .TextMatrix(0, .Col) = "TipoCondicion"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColCodProd
        .TextMatrix(0, .Col) = StellarMensaje(142) 'C�digo
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesProd
        .TextMatrix(0, .Col) = StellarMensaje(5010) '"Producto"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColMarca
        .TextMatrix(0, .Col) = StellarMensaje(3022) '"Marca"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColModelo
        .TextMatrix(0, .Col) = StellarMensaje(222) '"Modelo"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColValorPromo1
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 1, 5, 9, 13
                .TextMatrix(0, .Col) = StellarMensajeLocal(104) '"Precio Oferta"
            Case 2, 6, 10, 14
                .TextMatrix(0, .Col) = StellarMensajeLocal(105) '"% Descuento"
            Case 3, 7, 11, 15
                .TextMatrix(0, .Col) = StellarMensajeLocal(106) '"Descuento Fijo"
            Case 16, 17, 18, 19, 20, 21
                .TextMatrix(0, .Col) = StellarMensajeLocal(105) '"% Descuento"
            Case Else 'NotImplementedYet
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .Col = ColValorPromo2
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 2, 6, 10, 14
                .TextMatrix(0, .Col) = StellarMensajeLocal(120) '"% Desc 2"
            Case Else 'NotImplementedYet
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .Col = ColValorPromo3
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 2, 6, 10, 14
                .TextMatrix(0, .Col) = StellarMensajeLocal(121) '"% Desc 3"
            Case Else 'NotImplementedYet
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .Col = ColValorMxN
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 4, 5, 6, 7
                .TextMatrix(0, .Col) = "MxN"
            Case 8, 9, 10, 11, 12, 13, 14, 15
                .TextMatrix(0, .Col) = "Req."
            Case Else
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .DragMode = 0
        .AllowUserResizing = flexResizeColumns
        '.AllowUserResizing = flexResizeNone
        .ColWidth(ColRowID) = 375
        .ColWidth(ColTipoCondicion) = 0
        .ColWidth(ColCodProd) = 1335
        .ColWidth(ColDesProd) = 4235
        .ColWidth(ColMarca) = 1445
        .ColWidth(ColModelo) = 1445
        
        Select Case TipoPromo
            Case 2, 6, 10, 14
                .ColWidth(ColValorPromo1) = 1300
                .ColWidth(ColValorPromo2) = 750
                .ColWidth(ColValorPromo3) = 750
            Case Else
                .ColWidth(ColValorPromo1) = 2800
                .ColWidth(ColValorPromo2) = 0
                .ColWidth(ColValorPromo3) = 0
        End Select
        
        Select Case TipoPromo
            Case 4, 8, 12
                .ColWidth(ColValorMxN) = 1500
                .ColWidth(ColDesProd) = (.ColWidth(ColDesProd) - .ColWidth(ColValorMxN))
                .ColWidth(ColDesProd) = (.ColWidth(ColDesProd) + .ColWidth(ColValorPromo1))
                .ColWidth(ColValorPromo1) = 0
            Case 5, 6, 7, 9, 10, 11, 13, 14, 15
                .ColWidth(ColValorMxN) = 800
                .ColWidth(ColDesProd) = (.ColWidth(ColDesProd) - .ColWidth(ColValorMxN))
            Case Else
                .ColWidth(ColValorMxN) = 0
        End Select
        
        AnchoCampoScroll = .ColWidth(ColDesProd)
        
    End With
    
End Sub

Private Sub Form_Load()
    
    If CancelarPromo_Salir Then Exit Sub
    
    lblTitulo.Caption = StellarMensajeLocal(125)
    lblCriterios.Caption = StellarMensaje(10164) 'criterios
    lblValores.Caption = StellarMensaje(2045) 'valor
    lblArchivoPlano.Caption = StellarMensajeLocal(216) 'archivo plano
    lblSeccionMxN.Caption = StellarMensajeLocal(217) 'valores promocion m x n
    lblCantLlevar.Caption = StellarMensajeLocal(218) & ":" 'cantidad requerida
    lblCantPagar.Caption = StellarMensajeLocal(219) & ":" 'cantidad promocion
    CmdMxNSalir.Caption = StellarMensajeLocal(220) 'ocultar
    
    CmdAtras.Caption = StellarMensajeLocal(12)
    CmdSiguiente.Caption = StellarMensajeLocal(10)
    CmdCancelar.Caption = StellarMensajeLocal(11)
    
    lblProveedor.Caption = StellarMensaje(16446) '"Proveedor"
    lblDpto.Caption = StellarMensaje(3028) '"Departamento"
    lblGrupo.Caption = StellarMensaje(161) '"Grupo"
    lblSubgrupo.Caption = StellarMensaje(3029) '"SubGrupo"
    lblMarca.Caption = StellarMensaje(3022) '"Marca"
    lblModelo.Caption = StellarMensaje(222) '"Modelo"
    lblProducto.Caption = StellarMensaje(5010) '"Producto
    lblDescBtnCant.Caption = StellarMensajeLocal(174) '"Cantidad
    lblDescBtnIncluir.Caption = StellarMensajeLocal(175) '"Incluir
    lblDescBtnExcluir.Caption = StellarMensajeLocal(176) '"Excluir
    lblDescBtnBorrar.Caption = StellarMensajeLocal(177) '"Eliminar Fila
    ChkCombinarProductos.Caption = StellarMensajeLocal(178) '"Aplicar promocion entre todos los productos.
    ChkExcluirInactivos.Caption = StellarMensajeLocal(203)
    
    If TipoPromo = 16 Or TipoPromo = 17 Or TipoPromo = 18 _
    Or TipoPromo = 19 Or TipoPromo = 20 Or TipoPromo = 21 Then
        ChkSeleccionarTodo.Caption = StellarMensajeLocal(183) '"Todos los productos
        'If Val(BuscarReglaNegocioStr("Promociones_OpcionTodosLosProductos", 1)) = 1 Then
            'ChkSeleccionarTodo.Value = vbChecked
        'Else
            ChkSeleccionarTodo.Value = vbUnchecked
        'End If
    Else
        If Val(BuscarReglaNegocioStr("Promociones_OpcionTodosLosProductos", 1)) = 1 Then
            ChkSeleccionarTodo.Caption = StellarMensajeLocal(183) '"Todos los productos
        Else
            ChkSeleccionarTodo.Visible = False
        End If
    End If
    
    FrameSeccionMxN.Width = 7100 '3855
    FrameSeccionMxN.Top = 360
    FrameSeccionMxN.Height = 3015
    'FrameSeccionMxN.Left = (8040 + FrameSeccionMxN.Width)
    'FrameSeccionMxN.Left = (Me.Width - FrameSeccionMxN.Width - Frame1.Left * 2)
    FrameSeccionMxN.Left = Me.Width
    'FrameSeccionMxN.Visible = True' Debug
    
    'If CombinarProductos Then
        'txtCantLlevar.Text = CantLlevar
        'txtCantPagar.Text = CantPagar
        'txtCantLlevar.Locked = True
        'txtCantPagar.Locked = True
    'Else
        txtCantLlevar.Text = Empty
        txtCantPagar.Text = Empty
    'End If
    
    Set Medir.Font = lblCampoPromo.Font
    
    txtCampoPromo.Text = FormatNumber("0")
    
    Select Case TipoPromo
        
        Case 1 'Precio Oferta
CasePrc:
            lblCampoPromo.Caption = StellarMensajeLocal(104)
            TmpLen = Medir.TextWidth(lblCampoPromo.Caption)
            txtCampoPromo.Left = (lblCampoPromo.Left + TmpLen + 200)
            chkConsideraImpuesto.Visible = True
        Case 2 '% Descuento
CasePorc:
            
            lblCampoPromo.Top = 3850
            txtCampoPromo.Top = (lblCampoPromo.Top - 60)
            
            lblCampoPromo.Caption = StellarMensajeLocal(105) '% Descuento
            TmpLen = Medir.TextWidth(lblCampoPromo.Caption)
            txtCampoPromo.Left = (lblCampoPromo.Left + TmpLen + 200)
            txtCampoPromo.Width = txtCampo2.Width
            
            lblCampo2.Caption = StellarMensajeLocal(120)
            lblCampo2.Visible = True
            txtCampo2.Visible = True
            lblCampo2.Left = txtCampoPromo.Left + txtCampoPromo.Width + 200
            txtCampo2.Left = lblCampo2.Left + lblCampo2.Width + 200
            
            lblCampo3.Caption = StellarMensajeLocal(121)
            lblCampo3.Visible = True
            txtCampo3.Visible = True
            lblCampo3.Left = txtCampo2.Left + txtCampo2.Width + 200
            txtCampo3.Left = lblCampo3.Left + lblCampo3.Width + 200
            
            lblCampo2.Top = lblCampoPromo.Top
            txtCampo2.Top = txtCampoPromo.Top
            lblCampo3.Top = lblCampoPromo.Top
            txtCampo3.Top = txtCampoPromo.Top
            
            chkConsideraImpuesto.Visible = False
            
        Case 3 'Monto Descuento
CaseMonto:
            lblCampoPromo.Caption = StellarMensajeLocal(106)
            chkConsideraImpuesto.Visible = True
        Case 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
            CmdMxN.Visible = True
            lblDescBtnCant.Visible = True
            Select Case TipoPromo
                Case 4, 8, 12
                    chkConsideraImpuesto.Visible = False
                    lblCampoPromo.Visible = False
                    txtCampoPromo.Visible = False
                Case 5, 9, 13 'Precio Oferta
                    GoTo CasePrc
                Case 6, 10, 14 '% Descuento
                    GoTo CasePorc
                Case 7, 11, 15 'Monto Descuento
                    GoTo CaseMonto
            End Select
            
        Case 16, 17, 18, 19, 20, 21 ' Formas de Pago. Solo por porcentaje para mantenerlo simple.
            
            'lblCampoPromo.Top = 3400
            'txtCampoPromo.Top = (lblCampoPromo.Top - 60)
            
            lblCampoPromo.Caption = StellarMensajeLocal(105) '% Descuento
            TmpLen = Medir.TextWidth(lblCampoPromo.Caption)
            txtCampoPromo.Left = (lblCampoPromo.Left + TmpLen + 200)
            txtCampoPromo.Width = txtCampo2.Width
            
            lblCampo2.Caption = StellarMensajeLocal(120)
            lblCampo2.Visible = False
            txtCampo2.Visible = False
            lblCampo2.Left = txtCampoPromo.Left + txtCampoPromo.Width + 200
            txtCampo2.Left = lblCampo2.Left + lblCampo2.Width + 200
            
            lblCampo3.Caption = StellarMensajeLocal(121)
            lblCampo3.Visible = False
            txtCampo3.Visible = False
            lblCampo3.Left = txtCampo2.Left + txtCampo2.Width + 200
            txtCampo3.Left = lblCampo3.Left + lblCampo3.Width + 200
            
            lblCampo2.Top = lblCampoPromo.Top
            txtCampo2.Top = txtCampoPromo.Top
            lblCampo3.Top = lblCampoPromo.Top
            txtCampo3.Top = txtCampoPromo.Top
            
            chkConsideraImpuesto.Visible = False
            
            CmdMxN.Visible = True
            CmdMxN.Picture = ImgCash.Picture
            lblDescBtnCant.Caption = StellarMensajeLocal(252) ' L�m. Desc.
            lblDescBtnCant.Visible = True
            
    End Select
    
    chkConsideraImpuesto.Left = txtCampoPromo.Left
    chkConsideraImpuesto.Caption = StellarMensajeLocal(168) ' Considera Impuesto
    
    Select Case TipoPromo
        Case 8, 9, 10, 11, 12, 13, 14, 15
            If Not mVarEsPremio Then
                lblCampoPromo.Visible = False
                lblCampo2.Visible = False
                lblCampo3.Visible = False
                txtCampoPromo.Visible = False
                txtCampo2.Visible = False
                txtCampo3.Visible = False
                chkConsideraImpuesto.Visible = False
                ChkCombinarProductos.Visible = False
                lblCantPagar.Visible = False
                txtCantPagar.Visible = False
            Else
                lblCantPagar.Visible = False
                txtCantPagar.Visible = False
                ChkCombinarProductos.Visible = False
            End If
            
            txtCantLlevar.Text = "1"
            txtCampoPromo.Text = "0"
            
    End Select
    
    CargarGrid
    CargarTemporal
    
    Call AjustarPantalla(Me)
    
    If Not FrmAppLink.DebugVBApp Then
        Call WheelHook(Me.hWnd)
    End If
    
End Sub

Private Sub Form_Activate()
    If CancelarPromo_Salir Or PromocionCreada Then
        Unload Me
    End If
End Sub

Private Sub Header_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        Case Is = "AGREGAR"
            'Call Form_KeyDown(vbKeyF3, 0)
        Case Is = "BUSCAR"
            'Call Form_KeyDown(vbKeyF2, 0)
        
        Case Is = "MODIFICAR"
            'Call Form_KeyDown(vbKeyF5, 0)
        
        Case Is = "CANCELAR"
            'Call Form_KeyDown(vbKeyF7, 0)
        
        Case Is = "BORRAR"
            ''Call Form_KeyDown(vbKeyF6, 0)
        
        Case Is = "GRABAR"
            'Call Grabar_Registro
            'Call Form_KeyDown(vbKeyF4, 0)
        
        Case Is = "IMPRIMIR"
            'Call imprimir_registro
        
        Case Is = "SALIR"
            Unload Me
            'Call Form_KeyDown(vbKeyF12, 0)
        
        Case Is = "AYUDA"
                
    End Select
    
End Sub

Private Sub AyudaPatronLike()
    '"Cuadro Vac�o => No tomar en cuenta para la b�squeda" & GetLines & _
    "*Criterio* => Buscar que contenga la palabra ""Criterio"" " & GetLines & _
    "Criterio* => Buscar lo que empieze por la palabra ""Criterio"" " & GetLines & _
    "*Criterio => Buscar lo que termine con la palabra ""Criterio"" " & GetLines & _
    "[] => Buscar los que esten vac�os" & GetLines
    ShowTooltip Replace(StellarMensajeLocal(122), "$(Line)", vbNewLine), _
    TxtMarca.Width * 3, 7500, CampoT, 1
End Sub

Private Sub txtCampo2_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCampo3_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCampoPromo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCampoPromo_GotFocus()
    Set CampoT = txtCampoPromo
    SeleccionarTexto CampoT
End Sub

Private Sub txtCampo2_GotFocus()
    Set CampoT = txtCampo2
    SeleccionarTexto CampoT
End Sub

Private Sub txtCampo3_GotFocus()
    Set CampoT = txtCampo3
    SeleccionarTexto CampoT
End Sub

Private Sub txtCantLlevar_Change()
    Select Case TipoPromo
        Case 5, 6, 7
            txtCantPagar.Text = txtCantLlevar.Text
    End Select
End Sub

Private Sub txtCantLlevar_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCantLlevar_GotFocus()
    Set CampoT = txtCantLlevar
End Sub

Private Sub txtCantPagar_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCantPagar_GotFocus()
    Set CampoT = txtCantPagar
End Sub

Private Sub txtCodigoProducto_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtCodigoProducto_GotFocus()
    Set CampoT = txtCodigoProducto
End Sub

Private Sub txtCodigoProducto_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            'Call MAKE_VIEW("MA_PRODUCTOS", "c_Codigo", "c_Descri", _
            StellarMensaje(254), Me, "GENERICO", , txtCodigoProducto, lblDescripcionProducto) '"P R O D U C T O S"
            mArrProducto = BuscarInfoProducto_Basica(FrmAppLink.CnADM, , , , , VistaPrecioCliente)
            
            If Not IsEmpty(mArrProducto) Then
                txtCodigoProducto.Text = mArrProducto(0)
                txtCodigoProducto_LostFocus
            End If
            
        Case Is = vbKeyReturn
            
            txtCodigoProducto_LostFocus
            
    End Select
    
End Sub

Private Sub txtCodigoProducto_LostFocus()
    
    On Error GoTo Error
    
    Dim RsProducto As New ADODB.Recordset
    
    If Trim(txtCodigoProducto.Text) <> Empty Then
    
        Call Apertura_Recordset(RsProducto)
        
        RsProducto.Open "SELECT * FROM MA_PRODUCTOS " & _
        "WHERE c_Codigo IN ( " & _
        "SELECT c_CodNasa FROM MA_CODIGOS " & _
        "WHERE c_Codigo = '" & QuitarComillasSimples(txtCodigoProducto.Text) & "') ", _
        FrmAppLink.CnADM, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsProducto.EOF Then
            txtCodigoProducto.Text = RsProducto!c_Codigo
            lblDescripcionProducto.Caption = RsProducto!c_Descri
            If Not pCargandoArchivo Then
                If ChkExcluirInactivos.Value = vbChecked And RsProducto!n_Activo = 0 Then
                    'Mensaje True, "El producto [" & txtCodigoProducto & "][" & lblDescripcionProducto & "] est� inactivo."
                    Mensaje True, Replace(StellarMensajeLocal(204), "$(Info)", " [" & txtCodigoProducto & "][" & lblDescripcionProducto & "] ")
                    GoTo Limpiar1
                End If
            Else
                If ChkExcluirInactivos.Value = vbChecked And RsProducto!n_Activo = 0 Then
                    GoTo Limpiar1
                End If
            End If
            If TipoPromo = 1 Or TipoPromo = 5 Or (TipoPromo = 9 And mVarEsPremio) Or (TipoPromo = 13 And mVarEsPremio) Then
                txtCampo2.Visible = True
                txtCampo2.Locked = True
                txtCampo2.Width = 2250
                lblCampo2.Visible = True
                lblCampo2.Caption = StellarMensajeLocal(201) & ": "  '" Prc. Actual Ref.: "
                txtCampo2.Text = FormatNumber(RsProducto!n_Precio1 * (1 + IIf(chkConsideraImpuesto = vbChecked, ((RsProducto!n_Impuesto1 + RsProducto!n_Impuesto2 + RsProducto!n_Impuesto3) / 100), 0)))
            End If
        Else
            If Not pCargandoArchivo Then
                Call Mensaje(True, Stellar_Mensaje(16285)) '"No existe un producto con este c�digo.")
            End If
Limpiar1:
            lblDescripcionProducto.Caption = Empty
            txtCodigoProducto.Text = Empty
        End If
        
        RsProducto.Close
        
    Else
        
        lblDescripcionProducto.Caption = Empty
        txtCodigoProducto.Text = Empty
        
    End If
    
    Exit Sub
    
Error:
    
End Sub

Private Sub TxtMarca_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtMarca_GotFocus()
    Set CampoT = TxtMarca
End Sub

Private Sub TxtModelo_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtModelo_GotFocus()
    Set CampoT = TxtModelo
End Sub

Private Sub Tooltip1_Click()
    If PuedeObtenerFoco(TxtMarca) Then TxtMarca.SetFocus
    AyudaPatronLike
End Sub

Private Sub Tooltip2_Click()
    If PuedeObtenerFoco(TxtModelo) Then TxtModelo.SetFocus
    AyudaPatronLike
End Sub

Private Sub InsertarCondicion(ByVal pTipo As Integer)
    
    On Error GoTo Error
    
    Dim RsCondicion As New ADODB.Recordset
    Dim RsValor As New ADODB.Recordset
    Dim SQL As String
    Dim SqlV As String
    
    Dim CampoPromo1 As Double
    Dim CampoPromo2 As Double
    Dim CampoPromo3 As Double
    
    If Trim(PROVEEDOR.Text) <> Empty Or Trim(departamento.Text) <> Empty Or _
    Trim(grupo.Text) <> Empty Or Trim(subgrupo.Text) <> Empty Or _
    Trim(TxtMarca.Text) <> Empty Or Trim(TxtModelo.Text) <> Empty _
    Or Trim(txtCodigoProducto.Text) <> Empty _
    Or ChkSeleccionarTodo.Value = vbChecked Then
        
        If ((pTipo = 1 Or pTipo = 3) And IsNumeric(txtCampoPromo.Text)) Or (pTipo = 2 Or pTipo = 4) Then
            
            If pTipo = 2 And Not IsNumeric(txtCampoPromo.Text) Then txtCampoPromo.Text = FormatNumber(0)
            
            CampoPromo1 = CDbl(txtCampoPromo.Text)
            
            If Not IsNumeric(txtCantLlevar.Text) Then txtCantLlevar.Text = 0
            If Not IsNumeric(txtCantPagar.Text) Then txtCantPagar.Text = 0
            
            Dim mValidarObligatorios As Boolean
            
            Select Case TipoPromo
                Case 8, 9, 10, 11, 12, 13, 14, 15
                    If mVarEsPremio Then
                        mValidarObligatorios = ((CampoPromo1 > 0) Or pTipo = 2 Or TipoPromo = 8 Or TipoPromo = 12)
                    Else
                        mValidarObligatorios = True
                    End If
                Case Else
                    mValidarObligatorios = ((CampoPromo1 > 0) Or pTipo = 2 Or TipoPromo = 4)
            End Select
            
            Dim MensajeValidacion As String
            
            Select Case TipoPromo
                Case 4
                    If mValidarObligatorios Then
                        mValidarObligatorios = (CDbl(txtCantLlevar.Text) > 0 And CDbl(txtCantPagar.Text) > 0 And _
                        CDbl(txtCantLlevar.Text) > CDbl(txtCantPagar.Text)) Or pTipo = 2
                        If Not mValidarObligatorios And Not FrameSeccionMxN.Visible Then
                            CmdMxN_Click
                        End If
                        If Not mValidarObligatorios Then
                            MensajeValidacion = StellarMensajeLocal(172)
                            If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
                        End If
                    Else
                        MensajeValidacion = StellarMensajeLocal(123)
                        If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                    End If
                Case 5, 6, 7
                    If mValidarObligatorios Then
                        mValidarObligatorios = (CDbl(txtCantLlevar.Text) > 0 And CDbl(txtCantPagar.Text) > 0 And _
                        CDbl(txtCantLlevar.Text) >= CDbl(txtCantPagar.Text)) Or pTipo = 2
                        If Not mValidarObligatorios And Not FrameSeccionMxN.Visible Then
                            CmdMxN_Click
                        End If
                        If Not mValidarObligatorios Then
                            MensajeValidacion = StellarMensajeLocal(173)
                            If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
                        End If
                    Else
                        MensajeValidacion = StellarMensajeLocal(123)
                        If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                    End If
                Case 8, 9, 10, 11, 12, 13, 14, 15
                    If mValidarObligatorios Then
                        mValidarObligatorios = (CDbl(txtCantLlevar.Text) > 0) Or pTipo = 2 Or pTipo = 4
                        If Not mValidarObligatorios And Not FrameSeccionMxN.Visible Then
                            CmdMxN_Click
                        End If
                        If Not mValidarObligatorios Then
                            MensajeValidacion = StellarMensajeLocal(172)
                            If PuedeObtenerFoco(txtCantLlevar) Then txtCantLlevar.SetFocus
                        End If
                    Else
                        MensajeValidacion = StellarMensajeLocal(123)
                        If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                    End If
                Case Else
                    If Not mValidarObligatorios Then
                        MensajeValidacion = StellarMensajeLocal(123)
                        If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                    End If
            End Select
            
            If mValidarObligatorios Then
                
                If Not IsNumeric(txtCampo2.Text) Then txtCampo2.Text = 0
                If Not IsNumeric(txtCampo3.Text) Then txtCampo3.Text = 0
                
                If Not CDbl(txtCampo2.Text) >= 0 Then
                    If Not pCargandoArchivo Then
                        Mensaje True, StellarMensajeLocal(123) '"Los datos deben ser num�ricos y positivos."
                        If PuedeObtenerFoco(txtCampo2) Then txtCampo2.SetFocus
                        ShowTooltip "<--", 500, 800, txtCampo2, 3, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
                    End If
                    Exit Sub
                End If
                
                If Not CDbl(txtCampo3.Text) >= 0 Then
                    If Not pCargandoArchivo Then
                        Mensaje True, StellarMensajeLocal(123) '"Los datos deben ser num�ricos y positivos."
                        If PuedeObtenerFoco(txtCampo3) Then txtCampo3.SetFocus
                        ShowTooltip "<--", 500, 800, txtCampo3, 3, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
                    End If
                    Exit Sub
                End If
                
                CampoPromo2 = CDbl(txtCampo2.Text)
                CampoPromo3 = CDbl(txtCampo3.Text)
                
                Dim mRsDatos As ADODB.Recordset
                Set mRsDatos = New ADODB.Recordset
                Apertura_RecordsetC mRsDatos
                
                Dim mCriterio As String
                
                If ChkSeleccionarTodo.Value = vbChecked Then
                    mCriterio = Empty
                ElseIf Trim(txtCodigoProducto.Text) <> Empty Then
                    mCriterio = "AND PRO.c_Codigo = '" & QuitarComillasSimples(txtCodigoProducto.Text) & "' "
                Else
                    mCriterio = _
                    IIf(Trim(PROVEEDOR.Text) <> Empty, _
                    "AND PRO.c_Codigo IN (SELECT c_Codigo FROM MA_PRODxPROV WHERE c_CodProvee = " & _
                    "'" & QuitarComillasSimples(PROVEEDOR.Text) & "') ", Empty) & _
                    IIf(Trim(departamento.Text) <> Empty, _
                    "AND PRO.c_Departamento = '" & QuitarComillasSimples(departamento.Text) & "' ", Empty) & _
                    IIf(Trim(grupo.Text) <> Empty, _
                    "AND PRO.c_Grupo = '" & QuitarComillasSimples(grupo.Text) & "' ", Empty) & _
                    IIf(Trim(subgrupo.Text) <> Empty, _
                    "AND PRO.c_Subgrupo = '" & QuitarComillasSimples(subgrupo.Text) & "' ", Empty) & _
                    IIf(Trim(TxtMarca.Text) <> Empty, _
                    IIf(TxtMarca.Text = "[]", "AND PRO.c_Marca = ''", _
                    "AND PRO.c_Marca LIKE '" & QuitarComillasSimples(TxtMarca.Text) & "' "), Empty) & _
                    IIf(Trim(TxtModelo.Text) <> Empty, _
                    IIf(TxtModelo.Text = "[]", "AND PRO.c_Modelo = ''", _
                    "AND PRO.c_Modelo LIKE '" & QuitarComillasSimples(TxtModelo.Text) & "' "), Empty) & _
                    IIf(ChkExcluirInactivos.Value = vbChecked, "AND PRO.n_Activo = 1 ", Empty)
                End If
                
                SQL = "SELECT * FROM MA_PRODUCTOS PRO " & _
                "WHERE 1 = 1 " & mCriterio
                
                mRsDatos.Open SQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If mRsDatos.RecordCount <= 0 Then
                    If Not pCargandoArchivo Then
                        Call Mensaje(True, StellarMensaje(16441))
                    End If
                    Exit Sub
                End If
                
                Dim mCriterioTipo As String
                
                If pTipo = 1 Or pTipo = 3 Then
                    
                    Select Case TipoPromo
                        Case 8, 9, 10, 11, 12, 13, 14, 15 ' Premios
                            If mVarEsPremio Then
                                mCriterioTipo = "AND COND.Tipo_Condicion IN (3, 4) " & GetLines
                            Else
                                mCriterioTipo = "AND COND.Tipo_Condicion IN (1, 2) " & GetLines
                            End If
                        Case Else
                            mCriterioTipo = "AND COND.Tipo_Condicion IN (1, 2) " & GetLines
                    End Select
                    
                    SQL = _
                    "INSERT INTO TMP_PROMOCION_CONDICION_VALORES" & GetLines & _
                    "(CodUsuario, Tipo_Condicion, Cod_Promocion, Marca, Modelo, " & GetLines & _
                    "Cod_Producto, Des_Producto)" & _
                    "SELECT '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' AS CodUsuario, " & _
                    pTipo & " AS Tipo_Condicion, '" & QuitarComillasSimples(mCodPromo_Actual) & "' AS Cod_Promocion, " & _
                    vbNewLine & _
                    "PRO.c_Marca AS Marca, PRO.c_Modelo AS Modelo," & GetLines & _
                    "PRO.c_Codigo AS Cod_Producto, PRO.c_Descri AS Des_Producto" & GetLines & _
                    "FROM MA_PRODUCTOS PRO LEFT JOIN TMP_PROMOCION_CONDICION_VALORES COND" & GetLines & _
                    "ON PRO.c_Codigo = COND.Cod_Producto" & GetLines & _
                    "AND COND.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
                    "AND COND.CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "'" & GetLines & _
                    mCriterioTipo & _
                    "WHERE 1 = 1" & GetLines & _
                    mCriterio & GetLines & _
                    "AND COND.Cod_Producto IS NULL "
                    
                    FrmAppLink.CnADM.Execute SQL, RecordsAffected
                    
                    SQL = "UPDATE TMP_PROMOCION_CONDICION_VALORES" & GetLines
                    
                    Select Case TipoPromo
                        Case 1
                            SQL = SQL & "SET Precio_Oferta = (" & CampoPromo1 & ")" & GetLines
                        Case 2
                            SQL = SQL & "SET Porcentaje_Descuento1 = (" & CampoPromo1 & "), " & GetLines & _
                            "Porcentaje_Descuento2 = (" & CampoPromo2 & ")," & GetLines & _
                            "Porcentaje_Descuento3 = (" & CampoPromo3 & ") " & GetLines
                        Case 3
                            SQL = SQL & "SET Monto_Descuento = (" & CampoPromo1 & ")" & GetLines
                        Case 4
                            SQL = SQL & "SET " & GetLines & _
                            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), " & GetLines & _
                            "Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & GetLines
                        Case 5
                            SQL = SQL & "SET Precio_Oferta = (" & CampoPromo1 & "), " & GetLines & _
                            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), " & GetLines & _
                            "Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & GetLines
                        Case 6
                            SQL = SQL & "SET Porcentaje_Descuento1 = (" & CampoPromo1 & "), " & GetLines & _
                            "Porcentaje_Descuento2 = (" & CampoPromo2 & ")," & GetLines & _
                            "Porcentaje_Descuento3 = (" & CampoPromo3 & "), " & GetLines & _
                            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), " & GetLines & _
                            "Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & GetLines
                        Case 7
                            SQL = SQL & "SET Monto_Descuento = (" & CampoPromo1 & "), " & GetLines & _
                            "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & "), " & GetLines & _
                            "Cantidad_Productos_Pagar = (" & CDbl(txtCantPagar.Text) & ") " & GetLines
                        Case 8, 9, 10, 11, 12, 13, 14, 15
                            If mVarEsPremio Then
                                SQL = SQL & "SET " & GetLines & _
                                "Cantidad_Productos_Pagar = (" & CDbl(txtCantLlevar.Text) & ")"
                            Else
                                SQL = SQL & "SET " & GetLines & _
                                "Cantidad_Productos_Requerir = (" & CDbl(txtCantLlevar.Text) & ")"
                            End If
                            If pTipo = 3 Then
                                If TipoPromo = 9 Or TipoPromo = 13 Then
                                    SQL = SQL & ", " & vbNewLine & _
                                    "Precio_Oferta = (" & CampoPromo1 & ")"
                                ElseIf TipoPromo = 10 Or TipoPromo = 14 Then
                                    SQL = SQL & ", " & vbNewLine & _
                                    "Porcentaje_Descuento1 = (" & CampoPromo1 & "), " & vbNewLine & _
                                    "Porcentaje_Descuento2 = (" & CampoPromo2 & ")," & vbNewLine & _
                                    "Porcentaje_Descuento3 = (" & CampoPromo3 & ")"
                                ElseIf TipoPromo = 11 Or TipoPromo = 15 Then
                                    SQL = SQL & ", " & vbNewLine & _
                                    "Monto_Descuento = (" & CampoPromo1 & ")"
                                End If
                            End If
                        Case 16, 17, 18, 19, 20, 21
                            SQL = SQL & "SET Porcentaje_Descuento1 = (" & CampoPromo1 & ")" & GetLines
                        Case Else 'NotImplementedYet
                    End Select
                    
                    Select Case TipoPromo
                        Case 8, 9, 10, 11, 12, 13, 14, 15 ' Premios
                            If mVarEsPremio Then
                                mCriterioTipo = "AND COND.Tipo_Condicion IN (3, 4) " & GetLines
                            Else
                                mCriterioTipo = "AND COND.Tipo_Condicion IN (1, 2) " & GetLines
                            End If
                        Case Else
                            mCriterioTipo = "AND COND.Tipo_Condicion IN (1, 2) " & GetLines
                    End Select
                    
                    SQL = SQL & _
                    "FROM MA_PRODUCTOS PRO INNER JOIN TMP_PROMOCION_CONDICION_VALORES COND" & GetLines & _
                    "ON PRO.c_Codigo = COND.Cod_Producto" & GetLines & _
                    "AND COND.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
                    "AND COND.CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "'" & GetLines & _
                    "WHERE 1 = 1" & GetLines & _
                    mCriterio & GetLines & _
                    mCriterioTipo
                    
                    FrmAppLink.CnADM.Execute SQL, RecordsAffected
                    
                    If Not pCargandoArchivo Then
                        ShowTooltip StellarMensajeLocal(185), 700, 1250, CmdIncluir, 0, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "7", True)
                    End If
                    
                    pCargarLinea = True
                    
                ElseIf pTipo = 2 Or pTipo = 4 Then
                    
                    Select Case TipoPromo
                        Case 8, 9, 10, 11, 12, 13, 14, 15 ' Premios
                            If mVarEsPremio Then
                                mCriterioTipo = "AND COND.Tipo_Condicion IN (3, 4) " & GetLines
                            Else
                                mCriterioTipo = "AND COND.Tipo_Condicion IN (1, 2) " & GetLines
                            End If
                        Case Else
                            mCriterioTipo = "AND COND.Tipo_Condicion IN (1, 2) " & GetLines
                    End Select
                    
                    SQL = "DELETE TMP_PROMOCION_CONDICION_VALORES" & GetLines
                    
                    SQL = SQL & _
                    "FROM MA_PRODUCTOS PRO INNER JOIN TMP_PROMOCION_CONDICION_VALORES COND" & GetLines & _
                    "ON PRO.c_Codigo = COND.Cod_Producto" & GetLines & _
                    "AND COND.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
                    "AND COND.CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "'" & GetLines & _
                    "WHERE 1 = 1" & GetLines & _
                    mCriterio & GetLines & _
                    mCriterioTipo
                    
                    FrmAppLink.CnADM.Execute SQL, RecordsAffected
                    
                    ShowTooltip StellarMensajeLocal(186), 700, 1250, CmdExcluir, 0, _
                    &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "7", True)
                    
                End If
                
                ResetearCriterios
                
            Else
                If Not pCargandoArchivo Then
                    Mensaje True, MensajeValidacion
                    If Screen.ActiveControl Is txtCampoPromo Then
                        ShowTooltip "<--", 500, 800, txtCampoPromo, 3, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
                    ElseIf Screen.ActiveControl Is txtCantLlevar Then
                        ShowTooltip "-->", 500, 2000, txtCantLlevar, 2, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
                    End If
                End If
                Exit Sub
            End If
            
        Else
            If Not pCargandoArchivo Then
                Mensaje True, StellarMensajeLocal(123) '"Los datos deben ser num�ricos y positivos."
                If PuedeObtenerFoco(txtCampoPromo) Then txtCampoPromo.SetFocus
                ShowTooltip "<--", 500, 800, txtCampoPromo, 3, _
                &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True)
            End If
            Exit Sub
        End If
        
    Else
        If Not pCargandoArchivo Then
            Mensaje True, StellarMensaje(16404) '"Debe seleccionar al menos un criterio"
        End If
    End If
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    If Not pCargandoArchivo Then
        MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(InsertarCondicion)"
    End If
    
End Sub

Private Sub CmdIncluir_Click()
    If mVarEsPremio Then
        InsertarCondicion 3
    Else
        InsertarCondicion 1
    End If
    If Not pCargandoArchivo Then
        CargarTemporal
    End If
End Sub

Private Sub CmdExcluir_Click()
    If mVarEsPremio Then
        InsertarCondicion 4
    Else
        InsertarCondicion 2
    End If
    CargarTemporal
End Sub

Private Sub CargarTemporal()
    
    On Error GoTo Error
    
    'CargarGrid
    
    Dim LastRow As Integer, LastCtl As Object
    Dim NumRows As Double, NumPag As Double
    Dim RsCondicion As ADODB.Recordset
    Set RsCondicion = New ADODB.Recordset
    
    TmpConsideraImpuesto = CBool(BuscarValorBD("Considera_Impuesto", _
    "SELECT Considera_Impuesto FROM TMP_PROMOCION " & vbNewLine & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
    "0", FrmAppLink.CnADM))
    
    chkConsideraImpuesto.Value = IIf(TmpConsideraImpuesto, vbChecked, vbUnchecked)
    
    TmpCombinarProductos = Split(BuscarValorBD("CombinarProductos", _
    "SELECT (CAST(CASE WHEN Cantidad_Productos_Requerir > 0 AND Cantidad_Productos_Pagar > 0 " & _
    "THEN 1 ELSE 0 END AS NVARCHAR(MAX)) + '|' + CAST(Cantidad_Productos_Requerir AS NVARCHAR(MAX)) " & _
    "+ '|' + CAST(Cantidad_Productos_Pagar AS NVARCHAR(MAX))) AS CombinarProductos FROM TMP_PROMOCION " & vbNewLine & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine, _
    "0|0|0", FrmAppLink.CnADM), "|")
    
    If CBool(Val(TmpCombinarProductos(0))) Then
        txtCantLlevar.Text = TmpCombinarProductos(1)
        txtCantPagar.Text = TmpCombinarProductos(2)
        ChkCombinarProductos.Value = vbChecked
    Else
        ChkCombinarProductos.Value = vbUnchecked
    End If
    
    Dim mSQL As String
    
    mSQL = "SELECT isNULL(COUNT(ID), 0) AS NumReg " & GetLines & _
    "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & GetLines & _
    "AND Cod_Producto <> '' " & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
    IIf(mVarEsPremio, "AND Tipo_Condicion IN (3, 4)", "AND Tipo_Condicion IN (1, 2)")
    
    Set RsCondicion = FrmAppLink.CnADM.Execute(mSQL)
    
    With RsCondicion
        
        If Not .EOF Then
            
            NumRows = !NumReg
            
            If NumRows > 0 Then
                
                NumPag = Round(NumRows / MaxGridRowsxPag, 8)
                If (NumPag - Fix(NumPag)) <> 0 Then NumPag = Fix(NumPag + 1)
                
                TxtNumPag.Text = NumPag
                
                txtPag.Text = Fix(SVal(txtPag))
                
                If SVal(txtPag.Text) >= 1 And SVal(txtPag.Text) <= NumPag Then
                    txtPag.Text = Fix(SVal(txtPag))
                Else
                    txtPag.Text = 1
                End If
                
                mSQL = "WITH AllRows AS (" & GetLines & _
                "SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowID, *" & GetLines & _
                "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
                "WHERE CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
                "AND Cod_Producto <> '' " & _
                "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
                IIf(mVarEsPremio, "AND Tipo_Condicion IN (3, 4)", "AND Tipo_Condicion IN (1, 2)") & _
                ")" & GetLines & _
                "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                "((" & txtPag.Text & " - 1) * (" & MaxGridRowsxPag & ") + 1) AND " & _
                "(" & txtPag.Text & " * " & MaxGridRowsxPag & ")" & GetLines
                
                RsCondicion.Close
                
                Set RsCondicion = New Recordset
                Apertura_RecordsetC RsCondicion
                
                .Open mSQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If Not .EOF Then
                    
                    .Sort = "RowID"
                    
                    Dim I As Integer
                    
                    LastRow = Grid.Row
                    Set LastCtl = Screen.ActiveControl
                    
                    Grid.Visible = False
                    
                    Grid.Rows = 1
                    
                    ScrollGrid.Min = 0
                    ScrollGrid.Max = .RecordCount
                    ScrollGrid.Value = ScrollGrid.Min
                    
                    While Not .EOF
                    
                        Grid.Rows = Grid.Rows + 1
                        I = I + 1
                        
                        Grid.TextMatrix(I, ColRowID) = I
                        Grid.TextMatrix(I, ColTipoCondicion) = !Tipo_Condicion
                        Grid.TextMatrix(I, ColMarca) = !Marca
                        Grid.TextMatrix(I, ColModelo) = !Modelo
                        Grid.TextMatrix(I, ColCodProd) = !Cod_Producto
                        Grid.TextMatrix(I, ColDesProd) = !Des_Producto
                        
                        Select Case TipoPromo
                            Case 1
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Precio_Oferta)
                            Case 2
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                                Grid.TextMatrix(I, ColValorPromo2) = FormatNumber(!Porcentaje_Descuento2)
                                Grid.TextMatrix(I, ColValorPromo3) = FormatNumber(!Porcentaje_Descuento3)
                            Case 3
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Monto_Descuento)
                            Case 4
                                Grid.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Requerir & "x" & !Cantidad_Productos_Pagar)
                            Case 5
                                Grid.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar & " " & StellarMensajeLocal(169) & " " & !Cantidad_Productos_Requerir)
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Precio_Oferta)
                            Case 6
                                Grid.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar & " " & StellarMensajeLocal(169) & " " & !Cantidad_Productos_Requerir)
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                                Grid.TextMatrix(I, ColValorPromo2) = FormatNumber(!Porcentaje_Descuento2)
                                Grid.TextMatrix(I, ColValorPromo3) = FormatNumber(!Porcentaje_Descuento3)
                            Case 7
                                Grid.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar & " " & StellarMensajeLocal(169) & " " & !Cantidad_Productos_Requerir)
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Monto_Descuento)
                            Case 8, 12
                                If mVarEsPremio Then
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Pagar
                                Else
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Requerir
                                End If
                            Case 9, 13
                                If mVarEsPremio Then
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Pagar
                                    Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Precio_Oferta)
                                Else
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Requerir
                                End If
                            Case 10, 14
                                If mVarEsPremio Then
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Pagar
                                    Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                                    Grid.TextMatrix(I, ColValorPromo2) = FormatNumber(!Porcentaje_Descuento2)
                                    Grid.TextMatrix(I, ColValorPromo3) = FormatNumber(!Porcentaje_Descuento3)
                                Else
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Requerir
                                End If
                            Case 11, 15
                                If mVarEsPremio Then
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Pagar
                                    Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Monto_Descuento)
                                Else
                                    Grid.TextMatrix(I, ColValorMxN) = !Cantidad_Productos_Requerir
                                End If
                            Case 16, 17, 18, 19, 20, 21
                                Grid.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                            Case Else
                                'NotImplementedYet
                        End Select
                        
                        .MoveNext
                        
                    Wend
                    
                    Grid.Visible = True
                    
                    If LastRow >= 1 And LastRow <= Grid.Rows - 1 Then
                        Grid.Row = LastRow
                        Grid.TopRow = Grid.Row
                    Else
                        Grid.Row = 1
                        Grid.TopRow = Grid.Row
                    End If
                    
                    If Not LastCtl Is Nothing Then
                        If PuedeObtenerFoco(LastCtl) Then LastCtl.SetFocus
                    End If
                    
                End If
                
            Else
                NumPag = 1
                txtPag.Text = NumPag
                Grid.Rows = 1
                Grid.Rows = 2
            End If
            
            If Grid.Rows > 5 Then
                ScrollGrid.Visible = True
                Grid.ScrollBars = flexScrollBarVertical
                AnchoScrollBar = ScrollGrid.Width
                Grid.ColWidth(ColDesProd) = (AnchoCampoScroll - AnchoScrollBar)
            Else
                Grid.ScrollBars = flexScrollBarHorizontal
                ScrollGrid.Visible = False
                AnchoScrollBar = 0
                Grid.ColWidth(ColDesProd) = AnchoCampoScroll
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    'Resume ' Debug
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CargarTemporal)"
    
    CargarGrid
    Grid.Visible = True
    
End Sub

Private Sub CmdDelete_Click()
    
    If Grid.Row > 0 And Grid.TextMatrix(Grid.Row, ColTipoCondicion) <> Empty Then
        
        On Error GoTo Error
        
        Dim RowD As Integer, mSQL As String
        
        RowD = Grid.Row
        
        mSQL = "DELETE FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
        "WHERE Cod_Producto = '" & QuitarComillasSimples(Grid.TextMatrix(RowD, ColCodProd)) & "' " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
        "AND CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
        "AND Tipo_Condicion = " & QuitarComillasSimples(Grid.TextMatrix(RowD, ColTipoCondicion)) & " " & GetLines
        
        FrmAppLink.CnADM.Execute mSQL
        
        CargarTemporal
        
        Grid.ColSel = ColCount - 1
        
    End If
    
    Exit Sub
    
Error:

    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub txtPag_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtPag_GotFocus()
    Set CampoT = txtPag
    txtPag.Tag = SVal(txtPag.Text)
End Sub

Private Sub txtPag_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtPag_LostFocus
    End If
End Sub

Private Sub txtPag_LostFocus()
    If SVal(txtPag.Text) <> SVal(txtPag.Tag) Then
        CargarTemporal
    End If
End Sub

Private Sub ResetearCriterios( _
Optional pCheckAll As Boolean = False)
    PROVEEDOR.Text = Empty
    PROVEEDOR_LostFocus
    departamento.Text = Empty
    departamento_LostFocus
    TxtMarca.Text = Empty
    TxtModelo.Text = Empty
    txtCodigoProducto.Text = Empty
    txtCodigoProducto_LostFocus
    If Not pCheckAll Then
        ChkSeleccionarTodo.Value = vbUnchecked
    End If
    If TipoPromo = 1 Or TipoPromo = 5 Or TipoPromo = 9 Or TipoPromo = 13 Then
        txtCampo2.Visible = False
        lblCampo2.Visible = False
    End If
End Sub
