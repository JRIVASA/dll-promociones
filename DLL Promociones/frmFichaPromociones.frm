VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.ocx"
Begin VB.Form frmFichaPromociones 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   0
      ScaleHeight     =   1050
      ScaleWidth      =   15330
      TabIndex        =   5
      Top             =   480
      Width           =   15360
      Begin VB.Frame frame_toolbar 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   810
         Left            =   30
         TabIndex        =   6
         Top             =   140
         Width           =   15105
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   120
            TabIndex        =   7
            Top             =   0
            Width           =   14715
            _ExtentX        =   25956
            _ExtentY        =   1429
            ButtonWidth     =   3228
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   10
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F3) Importar a Nueva"
                  Key             =   "Agregar"
                  Object.ToolTipText     =   "Agregar una Nueva Ficha"
                  ImageIndex      =   2
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Visible         =   0   'False
                        Key             =   "APRODUCTO"
                        Object.Tag             =   "APRODUCTO"
                        Text            =   "Agregar Importar"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Visible         =   0   'False
                        Key             =   "AIMPORTAR"
                        Object.Tag             =   "AIMPORTAR"
                        Text            =   "Agregar Importar "
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Object.Visible         =   0   'False
                        Key             =   "imparchivo"
                        Text            =   "Importar Archivo"
                     EndProperty
                  EndProperty
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F2) &Buscar"
                  Key             =   "Buscar"
                  Object.ToolTipText     =   "Buscar una Ficha"
                  ImageIndex      =   6
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F5) &Modificar"
                  Key             =   "Modificar"
                  Object.ToolTipText     =   "Modificar esta Ficha"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F7) &Cancelar"
                  Key             =   "Cancelar"
                  Object.ToolTipText     =   "Cancelar esta Ficha"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F6) Bo&rrar"
                  Key             =   "Eliminar"
                  Object.ToolTipText     =   "Borrar esta Ficha"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F4) &Grabar"
                  Key             =   "Grabar"
                  Object.ToolTipText     =   "Grabar esta Ficha"
                  ImageIndex      =   7
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F12) &Salir"
                  Key             =   "Salir"
                  Object.ToolTipText     =   "Salir de Ficheros"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Caption         =   "&Ayuda"
                  Key             =   "Ayuda"
                  Object.ToolTipText     =   "Ayuda"
               EndProperty
            EndProperty
            Begin VB.Timer Timer_LoadOtherInfo 
               Enabled         =   0   'False
               Interval        =   100
               Left            =   11280
               Top             =   240
            End
         End
      End
   End
   Begin TabDlg.SSTab Sections 
      Height          =   9015
      Left            =   120
      TabIndex        =   3
      Top             =   1680
      Width           =   15135
      _ExtentX        =   26696
      _ExtentY        =   15901
      _Version        =   393216
      Tabs            =   5
      Tab             =   3
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmFichaPromociones.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "FrameGeneral"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Sucursales"
      TabPicture(1)   =   "frmFichaPromociones.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame2"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Detalles"
      TabPicture(2)   =   "frmFichaPromociones.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "chkConsideraImpuesto"
      Tab(2).Control(1)=   "ChkCombinarProductos"
      Tab(2).Control(2)=   "TxtNumPagCond"
      Tab(2).Control(3)=   "txtPagCond"
      Tab(2).Control(4)=   "ScrollGridCond"
      Tab(2).Control(5)=   "GrdCond"
      Tab(2).Control(6)=   "LnPagCond"
      Tab(2).Control(7)=   "ImgNextCond"
      Tab(2).Control(8)=   "ImgPrevCond"
      Tab(2).Control(9)=   "Line6"
      Tab(2).Control(10)=   "lblSeccionCondicionesValores"
      Tab(2).ControlCount=   11
      TabCaption(3)   =   "Formas de Pago"
      TabPicture(3)   =   "frmFichaPromociones.frx":0054
      Tab(3).ControlEnabled=   -1  'True
      Tab(3).Control(0)=   "lblSeccionDetPag"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "LnSeccionDetPag"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "LnPagDetPag"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).Control(3)=   "ImgNextDetPag"
      Tab(3).Control(3).Enabled=   0   'False
      Tab(3).Control(4)=   "ImgPrevDetPag"
      Tab(3).Control(4).Enabled=   0   'False
      Tab(3).Control(5)=   "GrdDetPag"
      Tab(3).Control(5).Enabled=   0   'False
      Tab(3).Control(6)=   "ScrollGridDetPag"
      Tab(3).Control(6).Enabled=   0   'False
      Tab(3).Control(7)=   "txtNumPagDetPag"
      Tab(3).Control(7).Enabled=   0   'False
      Tab(3).Control(8)=   "txtPagDetPag"
      Tab(3).Control(8).Enabled=   0   'False
      Tab(3).Control(9)=   "chkPagoMinimo"
      Tab(3).Control(9).Enabled=   0   'False
      Tab(3).Control(10)=   "txtValorMinimo"
      Tab(3).Control(10).Enabled=   0   'False
      Tab(3).Control(11)=   "ChkMontoLimiteDescuentoPromo"
      Tab(3).Control(11).Enabled=   0   'False
      Tab(3).Control(12)=   "txtMontoLimiteDescuentoPromo"
      Tab(3).Control(12).Enabled=   0   'False
      Tab(3).ControlCount=   13
      TabCaption(4)   =   "Lista de BINES"
      TabPicture(4)   =   "frmFichaPromociones.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "txtMontoLimiteDescuentoTarjeta"
      Tab(4).Control(1)=   "ChkMontoLimiteDescuentoTarjeta"
      Tab(4).Control(2)=   "txtPagBIN"
      Tab(4).Control(3)=   "txtNumPagBIN"
      Tab(4).Control(4)=   "ScrollGridBIN"
      Tab(4).Control(5)=   "GrdBIN"
      Tab(4).Control(6)=   "ImgPrevBIN"
      Tab(4).Control(7)=   "ImgNextBIN"
      Tab(4).Control(8)=   "LnPagBIN"
      Tab(4).Control(9)=   "LnSeccionBIN"
      Tab(4).Control(10)=   "lblSeccionBIN"
      Tab(4).ControlCount=   11
      Begin VB.TextBox txtMontoLimiteDescuentoTarjeta 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   -62880
         TabIndex        =   86
         Top             =   1320
         Visible         =   0   'False
         Width           =   2460
      End
      Begin VB.CheckBox ChkMontoLimiteDescuentoTarjeta 
         BackColor       =   &H00E7E8E8&
         Caption         =   "L�mite de Descuento por Tarjeta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   -66840
         TabIndex        =   85
         Top             =   1425
         Visible         =   0   'False
         Width           =   3795
      End
      Begin VB.TextBox txtPagBIN 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   -67620
         TabIndex        =   84
         Top             =   2040
         Width           =   780
      End
      Begin VB.TextBox txtNumPagBIN 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   -66300
         Locked          =   -1  'True
         TabIndex        =   83
         Top             =   2040
         Width           =   780
      End
      Begin VB.VScrollBar ScrollGridBIN 
         Height          =   5835
         LargeChange     =   10
         Left            =   -60960
         TabIndex        =   82
         Top             =   2880
         Width           =   675
      End
      Begin VB.TextBox txtMontoLimiteDescuentoPromo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   12120
         Locked          =   -1  'True
         TabIndex        =   79
         Top             =   1320
         Width           =   2460
      End
      Begin VB.CheckBox ChkMontoLimiteDescuentoPromo 
         BackColor       =   &H00E7E8E8&
         Caption         =   "L�mite de Descuento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   9120
         TabIndex        =   78
         Top             =   1425
         Width           =   2955
      End
      Begin VB.TextBox txtValorMinimo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   77
         Top             =   1320
         Width           =   2460
      End
      Begin VB.CheckBox chkPagoMinimo 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Pago M�nimo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   76
         Top             =   1425
         Width           =   2955
      End
      Begin VB.TextBox txtPagDetPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   7380
         TabIndex        =   75
         Top             =   2040
         Width           =   780
      End
      Begin VB.TextBox txtNumPagDetPag 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   8700
         Locked          =   -1  'True
         TabIndex        =   74
         Top             =   2040
         Width           =   780
      End
      Begin VB.VScrollBar ScrollGridDetPag 
         Height          =   5835
         LargeChange     =   10
         Left            =   14040
         TabIndex        =   73
         Top             =   2880
         Width           =   675
      End
      Begin VB.CheckBox chkConsideraImpuesto 
         Caption         =   "Considera Impuesto"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   -73620
         TabIndex        =   66
         Top             =   1260
         Width           =   2715
      End
      Begin VB.CheckBox ChkCombinarProductos 
         Caption         =   "Aplicar promocion entre todos los productos"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   -66060
         TabIndex        =   65
         Top             =   1140
         Width           =   2775
      End
      Begin VB.TextBox TxtNumPagCond 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   -67440
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   1140
         Width           =   780
      End
      Begin VB.TextBox txtPagCond 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   -68760
         TabIndex        =   18
         Top             =   1140
         Width           =   780
      End
      Begin VB.VScrollBar ScrollGridCond 
         Height          =   6795
         LargeChange     =   10
         Left            =   -61140
         TabIndex        =   17
         Top             =   1905
         Width           =   675
      End
      Begin VB.Frame FrameGeneral 
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Codigo"
         Height          =   8415
         Left            =   -74880
         TabIndex        =   11
         Top             =   720
         Width           =   14325
         Begin VB.TextBox txtCodigo 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   1380
            TabIndex        =   43
            Top             =   600
            Width           =   1800
         End
         Begin VB.ComboBox cmbGrupoClientes 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1320
            Style           =   2  'Dropdown List
            TabIndex        =   42
            Top             =   5700
            Width           =   2085
         End
         Begin VB.TextBox txtDescripcion 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   4680
            TabIndex        =   41
            Top             =   600
            Width           =   5000
         End
         Begin VB.TextBox txtCampanna 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   1560
            Locked          =   -1  'True
            TabIndex        =   40
            Top             =   1200
            Width           =   8100
         End
         Begin VB.TextBox txtMontoMinimo 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   2220
            TabIndex        =   39
            Top             =   1860
            Width           =   1860
         End
         Begin VB.TextBox txtMontoMaximo 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   6240
            TabIndex        =   38
            Top             =   1860
            Width           =   1920
         End
         Begin VB.CheckBox chkCombinada 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Esta promocion aplica en conjunto con otras promociones"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   495
            Left            =   240
            TabIndex        =   37
            Top             =   6540
            Width           =   10395
         End
         Begin VB.TextBox txtPrioridad 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   1380
            TabIndex        =   36
            Top             =   7080
            Width           =   1620
         End
         Begin VB.CheckBox chkDias 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Lunes"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Index           =   1
            Left            =   1560
            TabIndex        =   35
            Top             =   3420
            Width           =   1245
         End
         Begin VB.CheckBox chkDias 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Martes"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Index           =   2
            Left            =   3000
            TabIndex        =   34
            Top             =   3420
            Width           =   1245
         End
         Begin VB.CheckBox chkDias 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Miercoles"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Index           =   3
            Left            =   4320
            TabIndex        =   33
            Top             =   3420
            Width           =   1485
         End
         Begin VB.CheckBox chkDias 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Jueves"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Index           =   4
            Left            =   5880
            TabIndex        =   32
            Top             =   3420
            Width           =   1455
         End
         Begin VB.CheckBox chkDias 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Viernes"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Index           =   5
            Left            =   7440
            TabIndex        =   31
            Top             =   3420
            Width           =   1485
         End
         Begin VB.CheckBox chkDias 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Sabado"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Index           =   6
            Left            =   9000
            TabIndex        =   30
            Top             =   3420
            Width           =   1485
         End
         Begin VB.CheckBox chkDias 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Domingo"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Index           =   7
            Left            =   10560
            TabIndex        =   29
            Top             =   3420
            Width           =   1485
         End
         Begin VB.OptionButton optTodos 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Todos"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   360
            MaskColor       =   &H00585A58&
            TabIndex        =   28
            Top             =   5100
            Width           =   1455
         End
         Begin VB.OptionButton optAfiliados 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Afiliados"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   4080
            MaskColor       =   &H00585A58&
            TabIndex        =   27
            Top             =   5100
            Width           =   1575
         End
         Begin VB.CheckBox chkCumpleanneros 
            BackColor       =   &H00E7E8E8&
            Caption         =   "Solo en fecha de Cumplea�os"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   4080
            TabIndex        =   26
            Top             =   5700
            Width           =   4095
         End
         Begin VB.TextBox txtFechaDesde 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1680
            TabIndex        =   25
            Top             =   2940
            Width           =   3195
         End
         Begin VB.TextBox TxtFechaHasta 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   6840
            TabIndex        =   24
            Top             =   2940
            Width           =   3375
         End
         Begin VB.TextBox txtHoraInicio 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   1680
            TabIndex        =   23
            Top             =   4020
            Width           =   3225
         End
         Begin VB.TextBox txtHoraFin 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            Left            =   6840
            TabIndex        =   22
            Top             =   4020
            Width           =   3405
         End
         Begin VB.OptionButton optNoAfiliados 
            BackColor       =   &H00E7E8E8&
            Caption         =   "No Afiliados"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   1980
            MaskColor       =   &H00585A58&
            TabIndex        =   21
            Top             =   5100
            Width           =   1695
         End
         Begin MSComCtl2.DTPicker dtpFechaInicio 
            CausesValidation=   0   'False
            Height          =   360
            Left            =   1680
            TabIndex        =   44
            Top             =   2940
            Visible         =   0   'False
            Width           =   3525
            _ExtentX        =   6218
            _ExtentY        =   635
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CustomFormat    =   "dd/MM/yyyy hh:mm:ss tt"
            Format          =   113573891
            CurrentDate     =   36446
         End
         Begin MSComCtl2.DTPicker dtpFechaFin 
            CausesValidation=   0   'False
            Height          =   360
            Left            =   6840
            TabIndex        =   45
            Top             =   2940
            Visible         =   0   'False
            Width           =   3705
            _ExtentX        =   6535
            _ExtentY        =   635
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CustomFormat    =   "dd/MM/yyyy hh:mm:ss tt"
            Format          =   113573891
            CurrentDate     =   36446
         End
         Begin MSComCtl2.DTPicker dtpHoraInicio 
            Height          =   360
            Left            =   1680
            TabIndex        =   46
            Top             =   4020
            Visible         =   0   'False
            Width           =   3540
            _ExtentX        =   6244
            _ExtentY        =   635
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   113573890
            CurrentDate     =   36336
         End
         Begin MSComCtl2.DTPicker dtpHoraFin 
            Height          =   360
            Left            =   6840
            TabIndex        =   47
            Top             =   4020
            Visible         =   0   'False
            Width           =   3720
            _ExtentX        =   6562
            _ExtentY        =   635
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   113573890
            CurrentDate     =   36336
         End
         Begin VB.Label lblFechaCreacionValor 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "12/08/2020 01:06:54 p.m."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   630
            Left            =   12120
            TabIndex        =   70
            Top             =   2520
            Width           =   1995
         End
         Begin VB.Label lblFechaCreacion 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "En fecha:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   12240
            TabIndex        =   69
            Top             =   2040
            Width           =   1275
         End
         Begin VB.Label lblDescCreadoPor 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "NOMBRE DEL USUARIO RESPONSABLE"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   870
            Left            =   12120
            TabIndex        =   68
            Top             =   1080
            Width           =   2025
         End
         Begin VB.Label lblCreadoPor 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Creada por:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   12240
            TabIndex        =   67
            Top             =   600
            Width           =   1275
         End
         Begin VB.Line Line5 
            BorderColor     =   &H00AE5B00&
            X1              =   2280
            X2              =   11700
            Y1              =   300
            Y2              =   300
         End
         Begin VB.Label lblSeccionDatosPromo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Datos de la Promocion"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Left            =   240
            TabIndex        =   64
            Top             =   180
            Width           =   1920
         End
         Begin VB.Label lblCodigo 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Codigo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   360
            TabIndex        =   63
            Top             =   660
            Width           =   825
         End
         Begin VB.Label lblDescripcion 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   3420
            TabIndex        =   62
            Top             =   660
            Width           =   1200
         End
         Begin VB.Label lblGrupo 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   360
            TabIndex        =   61
            Top             =   5700
            Width           =   825
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00AE5B00&
            X1              =   1800
            X2              =   11800
            Y1              =   2700
            Y2              =   2700
         End
         Begin VB.Label lblSeccionPeriodo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Periodo Activo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Left            =   240
            TabIndex        =   60
            Top             =   2550
            Width           =   1215
         End
         Begin VB.Label lblMontoMinimo 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Monto Minimo de Compra"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   540
            Left            =   360
            TabIndex        =   59
            Top             =   1800
            Width           =   1770
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblMontoMaximo 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Monto Maximo de Compra"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   540
            Left            =   4260
            TabIndex        =   58
            Top             =   1800
            Width           =   1770
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblCampanna 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Campa�a"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   360
            TabIndex        =   57
            Top             =   1260
            Width           =   1050
         End
         Begin VB.Label lblPrioridad 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Prioridad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   240
            TabIndex        =   56
            Top             =   7140
            Width           =   1050
         End
         Begin VB.Label lblDias 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Dias"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   360
            TabIndex        =   55
            Top             =   3420
            Width           =   405
         End
         Begin VB.Label lblFechaInicio 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   360
            TabIndex        =   54
            Top             =   2940
            Width           =   1155
         End
         Begin VB.Label lblFechaFin 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   5640
            TabIndex        =   53
            Top             =   2940
            Width           =   990
         End
         Begin VB.Label lblHoraInicio 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Hora Inicio"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   360
            TabIndex        =   52
            Top             =   4020
            Width           =   1035
         End
         Begin VB.Label lblHoraFin 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Hora Fin"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   5640
            TabIndex        =   51
            Top             =   4020
            Width           =   930
         End
         Begin VB.Label lblSeccionClientes 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Clientes"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Left            =   240
            TabIndex        =   50
            Top             =   4620
            Width           =   675
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00AE5B00&
            X1              =   1320
            X2              =   11800
            Y1              =   4740
            Y2              =   4740
         End
         Begin VB.Line LnSeccionOtros 
            BorderColor     =   &H00AE5B00&
            Visible         =   0   'False
            X1              =   1320
            X2              =   11800
            Y1              =   6360
            Y2              =   6360
         End
         Begin VB.Label lblSeccionOtros 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Otros"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Left            =   240
            TabIndex        =   49
            Top             =   6240
            Visible         =   0   'False
            Width           =   465
         End
         Begin VB.Label lblAvisoImpuestosMontos 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "* Los montos minimo y maximo no consideran impuestos *"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   540
            Left            =   8340
            TabIndex        =   48
            Top             =   1800
            Width           =   3405
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Height          =   8355
         Left            =   -74880
         TabIndex        =   8
         Top             =   780
         Width           =   14865
         Begin VB.Frame FrameGridSucursales 
            BackColor       =   &H00E7E8E8&
            BorderStyle     =   0  'None
            Height          =   7875
            Left            =   180
            TabIndex        =   12
            Top             =   420
            Width           =   14685
            Begin VB.TextBox TxtNumPagSuc 
               Appearance      =   0  'Flat
               BackColor       =   &H00FAFAFA&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   480
               Left            =   7560
               Locked          =   -1  'True
               TabIndex        =   15
               Top             =   200
               Width           =   780
            End
            Begin VB.TextBox txtPagSuc 
               Appearance      =   0  'Flat
               BackColor       =   &H00FAFAFA&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   11.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   480
               Left            =   6240
               TabIndex        =   14
               Top             =   200
               Width           =   780
            End
            Begin VB.VScrollBar ScrollGridSuc 
               Height          =   6795
               LargeChange     =   10
               Left            =   13860
               TabIndex        =   13
               Top             =   960
               Width           =   675
            End
            Begin MSFlexGridLib.MSFlexGrid GrdSuc 
               Height          =   6795
               Left            =   0
               TabIndex        =   16
               Top             =   960
               Width           =   14535
               _ExtentX        =   25638
               _ExtentY        =   11986
               _Version        =   393216
               Cols            =   3
               FixedCols       =   0
               ForeColor       =   3355443
               BackColorFixed  =   5000268
               ForeColorFixed  =   16777215
               BackColorSel    =   15658734
               ForeColorSel    =   0
               BackColorBkg    =   16448250
               WordWrap        =   -1  'True
               ScrollTrack     =   -1  'True
               FocusRect       =   0
               FillStyle       =   1
               GridLinesFixed  =   0
               ScrollBars      =   2
               SelectionMode   =   1
               BorderStyle     =   0
               Appearance      =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Line LnPagSuc 
               BorderColor     =   &H00AE5B00&
               X1              =   7200
               X2              =   7380
               Y1              =   600
               Y2              =   240
            End
            Begin VB.Image ImgNextSuc 
               Height          =   480
               Left            =   5340
               Picture         =   "frmFichaPromociones.frx":008C
               Stretch         =   -1  'True
               Top             =   195
               Width           =   480
            End
            Begin VB.Image ImgPrevSuc 
               Height          =   480
               Left            =   4500
               Picture         =   "frmFichaPromociones.frx":4E1B
               Stretch         =   -1  'True
               Top             =   195
               Width           =   480
            End
         End
         Begin VB.Label lblSeccionSucursales 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Sucursales aplicables"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Left            =   120
            TabIndex        =   9
            Top             =   120
            Width           =   1830
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00AE5B00&
            X1              =   2160
            X2              =   14340
            Y1              =   240
            Y2              =   240
         End
      End
      Begin MSFlexGridLib.MSFlexGrid GrdCond 
         Height          =   6795
         Left            =   -75000
         TabIndex        =   20
         Top             =   1905
         Width           =   14535
         _ExtentX        =   25638
         _ExtentY        =   11986
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         ScrollBars      =   2
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid GrdDetPag 
         Height          =   5835
         Left            =   240
         TabIndex        =   71
         Top             =   2880
         Width           =   14535
         _ExtentX        =   25638
         _ExtentY        =   10292
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         ScrollBars      =   2
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid GrdBIN 
         Height          =   5835
         Left            =   -74760
         TabIndex        =   80
         Top             =   2880
         Width           =   14535
         _ExtentX        =   25638
         _ExtentY        =   10292
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         ScrollBars      =   2
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Image ImgPrevBIN 
         Height          =   480
         Left            =   -69360
         Picture         =   "frmFichaPromociones.frx":8300
         Stretch         =   -1  'True
         Top             =   2040
         Width           =   480
      End
      Begin VB.Image ImgNextBIN 
         Height          =   480
         Left            =   -68520
         Picture         =   "frmFichaPromociones.frx":B7E5
         Stretch         =   -1  'True
         Top             =   2040
         Width           =   480
      End
      Begin VB.Line LnPagBIN 
         BorderColor     =   &H00AE5B00&
         X1              =   -66660
         X2              =   -66480
         Y1              =   2445
         Y2              =   2085
      End
      Begin VB.Line LnSeccionBIN 
         BorderColor     =   &H00AE5B00&
         X1              =   -71880
         X2              =   -60400
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Label lblSeccionBIN 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Lista de BIN Aplicable"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   -74760
         TabIndex        =   81
         Top             =   960
         Width           =   2790
      End
      Begin VB.Image ImgPrevDetPag 
         Height          =   480
         Left            =   5640
         Picture         =   "frmFichaPromociones.frx":10574
         Stretch         =   -1  'True
         Top             =   2040
         Width           =   480
      End
      Begin VB.Image ImgNextDetPag 
         Height          =   480
         Left            =   6480
         Picture         =   "frmFichaPromociones.frx":13A59
         Stretch         =   -1  'True
         Top             =   2040
         Width           =   480
      End
      Begin VB.Line LnPagDetPag 
         BorderColor     =   &H00AE5B00&
         X1              =   8340
         X2              =   8520
         Y1              =   2445
         Y2              =   2085
      End
      Begin VB.Line LnSeccionDetPag 
         BorderColor     =   &H00AE5B00&
         X1              =   3240
         X2              =   14600
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Label lblSeccionDetPag 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Formas de Pago Aplicables"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   240
         TabIndex        =   72
         Top             =   960
         Width           =   2910
      End
      Begin VB.Line LnPagCond 
         BorderColor     =   &H00AE5B00&
         X1              =   -67800
         X2              =   -67620
         Y1              =   1545
         Y2              =   1185
      End
      Begin VB.Image ImgNextCond 
         Height          =   480
         Left            =   -69660
         Picture         =   "frmFichaPromociones.frx":187E8
         Stretch         =   -1  'True
         Top             =   1140
         Width           =   480
      End
      Begin VB.Image ImgPrevCond 
         Height          =   480
         Left            =   -70500
         Picture         =   "frmFichaPromociones.frx":1D577
         Stretch         =   -1  'True
         Top             =   1140
         Width           =   480
      End
      Begin VB.Line Line6 
         BorderColor     =   &H00AE5B00&
         X1              =   -72720
         X2              =   -60120
         Y1              =   900
         Y2              =   900
      End
      Begin VB.Label lblSeccionCondicionesValores 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Condiciones y Valores"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   -74760
         TabIndex        =   10
         Top             =   780
         Width           =   1875
      End
   End
   Begin VB.Frame FrameBarraTitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15360
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14700
         Picture         =   "frmFichaPromociones.frx":20A5C
         Top             =   15
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12375
         TabIndex        =   1
         Top             =   120
         Width           =   2115
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   11880
      Top             =   1320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFichaPromociones.frx":227DE
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFichaPromociones.frx":24570
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFichaPromociones.frx":2524A
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFichaPromociones.frx":25F24
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFichaPromociones.frx":27CB6
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFichaPromociones.frx":29A48
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFichaPromociones.frx":2B7DA
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Line Line4 
      BorderColor     =   &H00AE5B00&
      X1              =   840
      X2              =   11640
      Y1              =   120
      Y2              =   120
   End
   Begin VB.Label C�digo 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Clientes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   240
      Index           =   3
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   675
   End
End
Attribute VB_Name = "frmFichaPromociones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Conexion As ADODB.Connection
Public ConexionBsn As ADODB.Connection
Public ModificarEspera As Boolean
Public ImportarNuevo As Boolean
Private StrSQL As String

Private FormaCargada        As Boolean

Private Enum GridColCond
    ColRowID
    ColTipoCondicion
    ColCodProv
    ColDesProv
    ColCodDpto
    ColDesDpto
    ColCodGrp
    ColDesGrp
    ColCodSubg
    ColDesSubg
    ColMarca
    ColModelo
    ColCodProd
    ColDesProd
    ColValorPromo1
    ColValorPromo2
    ColValorPromo3
    ColValorMxN
    ColTipoCriterio
    [ColCount]
End Enum

Private Enum GridColSuc
    ColRowID
    ColCodSuc
    ColDescSuc
    ColDireccion
    ColGerente
    [ColCount]
End Enum

Private Enum GridColDetPag
    ColRowID
    ColCodMoneda
    ColDesMoneda
    ColCodDenomina
    ColDesDenomina
    [ColCount]
End Enum

Private Enum GridColBIN
    ColRowID
    ColItemNumber
    [ColCount]
End Enum

Private AnchoScrollBar_GrdCond          As Long
Private AnchoCampoScroll_GrdCond        As Long

Private AnchoScrollBar_GrdSuc           As Long
Private AnchoCampoScroll_GrdSuc         As Long

Private AnchoScrollBar_GrdDetPag        As Long
Private AnchoCampoScroll_GrdDetPag      As Long

Private AnchoScrollBar_GrdBIN           As Long
Private AnchoCampoScroll_GrdBIN         As Long

Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
  
  Dim Ctl As Control
  Dim bHandled As Boolean
  Dim bOver As Boolean
  
  For Each Ctl In Controls
    ' Is the mouse over the control
    On Error Resume Next
    bOver = (Ctl.Visible And IsOver(Ctl.hWnd, Xpos, Ypos))
    On Error GoTo 0
    
    If bOver Then
      ' If so, respond accordingly
      bHandled = True
      Select Case True
      
        Case TypeOf Ctl Is MSFlexGrid
          FlexGridScroll Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is PictureBox
          PictureBoxZoom Ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case TypeOf Ctl Is ListBox, TypeOf Ctl Is TextBox, TypeOf Ctl Is ComboBox
          ' These controls already handle the mousewheel themselves, so allow them to:
          If Ctl.Enabled Then Ctl.SetFocus
          
        Case Else
          bHandled = False

      End Select
      If bHandled Then Exit Sub
    End If
    bOver = False
  Next Ctl
  
  ' Scroll was not handled by any controls, so treat as a general message send to the form
  'Me.Caption = "Form Scroll " & IIf(Rotation < 0, "Down", "Up")
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    Sections.TabCaption(0) = StellarMensajeLocal(146)
    Sections.TabCaption(1) = StellarMensajeLocal(147)
    Sections.TabCaption(2) = StellarMensajeLocal(148)
    Sections.TabCaption(3) = StellarMensajeLocal(267)
    Sections.TabCaption(4) = StellarMensajeLocal(259)
    
    Toolbar1.Buttons("Agregar").Caption = StellarMensajeLocal(227)  'f3 importar a nueva
    Toolbar1.Buttons("Modificar").Caption = StellarMensaje(207) 'modificar
    
    If EstatusPromo = "DWT" Then
        Toolbar1.Buttons("Modificar").Enabled = True
        Toolbar1.Buttons("Agregar").Enabled = False
        Toolbar1.Buttons("Agregar").Visible = False
    Else
        Toolbar1.Buttons("Modificar").Enabled = False
        Toolbar1.Buttons("Agregar").Enabled = True
        Toolbar1.Buttons("Agregar").Visible = True
    End If
    
    Toolbar1.Buttons("Cancelar").Caption = StellarMensaje(105) 'cancelar
    Toolbar1.Buttons("Salir").Caption = StellarMensaje(2629) ' F12 Salir
    
    Set MonedaDoc = FrmAppLink.ClaseMonedaDocumento
    Set MonedaTmp = FrmAppLink.ClaseMonedaTemp
    
    lblSeccionDatosPromo.Caption = StellarMensajeLocal(149)
    lblSeccionPeriodo.Caption = StellarMensajeLocal(150)
    lblSeccionClientes.Caption = StellarMensajeLocal(151)
    lblSeccionOtros.Caption = StellarMensajeLocal(187)
    lblSeccionSucursales.Caption = StellarMensajeLocal(152)
    lblSeccionCondicionesValores.Caption = StellarMensajeLocal(153)
    lblSeccionDetPag.Caption = StellarMensajeLocal(271)
    lblSeccionBIN.Caption = StellarMensajeLocal(272)
    
    lblCodigo.Caption = StellarMensajeLocal(13)
    lblDescripcion.Caption = StellarMensajeLocal(14)
    lblPrioridad.Caption = StellarMensajeLocal(15)
    lblCampanna.Caption = StellarMensajeLocal(24)
    lblAvisoImpuestosMontos.Caption = StellarMensajeLocal(184)
    lblFechaInicio.Caption = StellarMensajeLocal(16)
    lblFechaFin.Caption = StellarMensajeLocal(17)
    lblMontoMinimo.Caption = StellarMensajeLocal(20)
    lblMontoMaximo.Caption = StellarMensajeLocal(21)
    lblDias.Caption = StellarMensajeLocal(22)
    lblHoraInicio.Caption = StellarMensajeLocal(18)
    lblHoraFin.Caption = StellarMensajeLocal(19)
    lblGrupo.Caption = StellarMensajeLocal(23)
    
    lblCreadoPor.Caption = StellarMensajeLocal(198)
    lblFechaCreacion.Caption = StellarMensajeLocal(199)
    
    chkCombinada.Caption = StellarMensajeLocal(25)
    chkConsideraImpuesto.Caption = StellarMensajeLocal(168)
    chkDias(1).Caption = StellarMensajeLocal(26)
    chkDias(2).Caption = StellarMensajeLocal(27)
    chkDias(3).Caption = StellarMensajeLocal(28)
    chkDias(4).Caption = StellarMensajeLocal(29)
    chkDias(5).Caption = StellarMensajeLocal(30)
    chkDias(6).Caption = StellarMensajeLocal(31)
    chkDias(7).Caption = StellarMensajeLocal(32)
    chkCumpleanneros.Caption = StellarMensajeLocal(33)
    
    optTodos.Caption = StellarMensajeLocal(34)
    optAfiliados.Caption = StellarMensajeLocal(35)
    
    Set ClsGrupos = FrmAppLink.GetClassGrupo
    ClsGrupos.cTipoGrupo = "CLI"
    ClsGrupos.CargarComboGrupos FrmAppLink.CnADM, cmbGrupoClientes, False
    FrmAppLink.ListRemoveItems cmbGrupoClientes, "Ninguno" ' Quitar dichos elementos unicamente aqu� sin alterar la funci�n anterior.
    
    Select Case TipoPromo
        
        Case 1, 2, 3
            
            chkCombinada.Enabled = False
            chkCombinada.Visible = True
            lblSeccionOtros.Visible = True
            LnSeccionOtros.Visible = True
            ChkCombinarProductos.Visible = False
            
        Case 16, 17, 18, 19, 20, 21
            
            chkCombinada.Enabled = False
            lblSeccionOtros.Visible = True
            LnSeccionOtros.Visible = True
            lblPrioridad.Visible = False
            txtPrioridad.Visible = False
            txtPrioridad.Text = 0
            chkCombinada.Height = 1000
            
        Case Else
            
            chkCombinada.Visible = False
            lblSeccionOtros.Visible = False
            LnSeccionOtros.Visible = False
            txtPrioridad.Visible = False
            lblPrioridad.Visible = False
            ChkCombinarProductos.Visible = True
            
    End Select
    
    Select Case TipoPromo
        Case 1, 3, 5, 7, 9, 11, 13, 15
            chkConsideraImpuesto.Visible = True
        Case Else
            chkConsideraImpuesto.Visible = False
    End Select
    
    CargarGridCondiciones
    CargarGridSucursales
    
    Sections.TabVisible(3) = False
    Sections.TabVisible(4) = False
    
    Select Case TipoPromo
        
        Case 16, 17, 18
            
            Sections.TabVisible(3) = True
            CargarGridFormaPago
            
        Case 19, 20, 21
            
            Sections.TabVisible(3) = True
            CargarGridFormaPago
            Sections.TabVisible(4) = True
            CargarGridListaBIN
            
    End Select
    
    Call AjustarPantalla(Me)
    
    If Not FrmAppLink.DebugVBApp Then
        Call WheelHook(Me.hWnd)
    End If
    
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        CargarPromo_DatosGenerales
        
        Sections.Tab = 0
        
        'CargarPromo_Sucursales
        'CargarPromo_Condiciones_Valores
        Timer_LoadOtherInfo.Enabled = True
        
        If PuedeObtenerFoco(Sections) Then Sections.SetFocus
        
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF3
            If Toolbar1.Buttons("Agregar").Enabled Then
                ImportarNuevo = True
                Unload Me
                Exit Sub
            End If
        Case vbKeyF5
            If Toolbar1.Buttons("Modificar").Enabled Then
                ModificarEspera = True
                Unload Me
                Exit Sub
            End If
        Case vbKeyF7, vbKeyF12
            ModificarEspera = False
            Unload Me
            Exit Sub
    End Select
End Sub

Private Sub CargarGridCondiciones()
    
    With GrdCond
        
        .Rows = 1
        .Row = 0
        .Clear
        .Cols = GridColCond.ColCount
        .RowHeight(.Row) = 425
        
        .Col = GridColCond.ColRowID
        .TextMatrix(0, .Col) = "Ln" '"RowID"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColTipoCondicion
        .TextMatrix(0, .Col) = "TipoCondicion"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = ColCodProv
        .TextMatrix(0, .Col) = "Cod_Prov"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesProv
        .TextMatrix(0, .Col) = StellarMensaje(16446) '"Proveedor"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodDpto
        .TextMatrix(0, .Col) = "Cod_Dpto"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesDpto
        .TextMatrix(0, .Col) = StellarMensaje(3028) '"Departamento"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodGrp
        .TextMatrix(0, .Col) = "Cod_Grupo"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesGrp
        .TextMatrix(0, .Col) = StellarMensaje(161) '"Grupo"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodSubg
        .TextMatrix(0, .Col) = "Cod_Subgrupo"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesSubg
        .TextMatrix(0, .Col) = StellarMensaje(3029) '"SubGrupo"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColMarca
        .TextMatrix(0, .Col) = StellarMensaje(3022) '"Marca"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColModelo
        .TextMatrix(0, .Col) = StellarMensaje(222) '"Modelo"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodProd
        .TextMatrix(0, .Col) = "Cod_Prod"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesProd
        .TextMatrix(0, .Col) = StellarMensaje(5010) '"Producto"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColValorPromo1
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 1, 5, 9, 13
                .TextMatrix(0, .Col) = StellarMensajeLocal(104) '"Precio Oferta"
            Case 2, 6, 10, 14
                .TextMatrix(0, .Col) = StellarMensajeLocal(105) '"% Descuento"
            Case 3, 7, 11, 15
                .TextMatrix(0, .Col) = StellarMensajeLocal(106) '"Descuento Fijo"
            Case 16, 17, 18, 19, 20, 21
                .TextMatrix(0, .Col) = StellarMensajeLocal(105) '"% Descuento"
            Case Else 'NotImplementedYet
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .Col = ColValorPromo2
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 2, 6, 10, 14
                .TextMatrix(0, .Col) = StellarMensajeLocal(120) '"% Desc 2"
            Case Else 'NotImplementedYet
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .Col = ColValorPromo3
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 2, 6, 10, 14
                .TextMatrix(0, .Col) = StellarMensajeLocal(121) '"% Desc 3"
            Case Else 'NotImplementedYet
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .Col = ColValorMxN
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        Select Case TipoPromo
            Case 4, 5, 6, 7
                .TextMatrix(0, .Col) = "MxN"
            Case 8, 9, 10, 11, 12, 13, 14, 15
                .TextMatrix(0, .Col) = "Req."
            Case Else
                .TextMatrix(0, .Col) = Empty
                .ColAlignment(.Col) = flexAlignRightCenter
                .CellAlignment = flexAlignRightCenter
        End Select
        
        .Col = ColTipoCriterio
        .TextMatrix(0, .Col) = StellarMensaje(10005) '"Modo"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .DragMode = 0
        .AllowUserResizing = flexResizeColumns
        '.AllowUserResizing = flexResizeNone
        .ColWidth(GridColCond.ColRowID) = 450
        .ColWidth(ColTipoCondicion) = 0
        .ColWidth(ColCodProv) = 0
        .ColWidth(ColDesProv) = 1500
        .ColWidth(ColCodDpto) = 0
        .ColWidth(ColDesDpto) = 1380
        .ColWidth(ColCodGrp) = 0
        .ColWidth(ColDesGrp) = 1200
        .ColWidth(ColCodSubg) = 0
        .ColWidth(ColDesSubg) = 1200
        .ColWidth(ColMarca) = 1200
        .ColWidth(ColModelo) = 1200
        .ColWidth(ColCodProd) = 0
        .ColWidth(ColDesProd) = 2145
        
        Select Case TipoPromo
            Case 2, 6, 10, 14
                .ColWidth(ColValorPromo1) = 1300
                .ColWidth(ColValorPromo2) = 750
                .ColWidth(ColValorPromo3) = 750
            Case Else
                .ColWidth(ColValorPromo1) = 2800
                .ColWidth(ColValorPromo2) = 0
                .ColWidth(ColValorPromo3) = 0
        End Select
        
        Select Case TipoPromo
            Case 4, 8, 12
                .ColWidth(ColValorMxN) = 800
                
                .ColWidth(ColDesProv) = (.ColWidth(ColDesProv) - (.ColWidth(ColValorMxN) / 5))
                .ColWidth(ColDesDpto) = (.ColWidth(ColDesDpto) - (.ColWidth(ColValorMxN) / 5))
                .ColWidth(ColDesGrp) = (.ColWidth(ColDesGrp) - (.ColWidth(ColValorMxN) / 5))
                .ColWidth(ColDesSubg) = (.ColWidth(ColDesSubg) - (.ColWidth(ColValorMxN) / 5))
                .ColWidth(ColDesProd) = (.ColWidth(ColDesProd) - (.ColWidth(ColValorMxN) / 5))
                
                .ColWidth(ColDesProv) = (.ColWidth(ColDesProv) + (.ColWidth(ColValorPromo1) / 5) - 5)
                .ColWidth(ColDesDpto) = (.ColWidth(ColDesDpto) + (.ColWidth(ColValorPromo1) / 5) - 5)
                .ColWidth(ColDesGrp) = (.ColWidth(ColDesGrp) + (.ColWidth(ColValorPromo1) / 5) - 5)
                .ColWidth(ColDesSubg) = (.ColWidth(ColDesSubg) + (.ColWidth(ColValorPromo1) / 5) - 5)
                .ColWidth(ColDesProd) = (.ColWidth(ColDesProd) + (.ColWidth(ColValorPromo1) / 5) - 5)
                
                .ColWidth(ColValorPromo1) = 0
            Case 5, 6, 7, 9, 10, 11, 13, 14, 15
                .ColWidth(ColValorMxN) = 800
                .ColWidth(ColDesProv) = (.ColWidth(ColDesProv) - (.ColWidth(ColValorMxN) / 5))
                .ColWidth(ColDesDpto) = (.ColWidth(ColDesDpto) - (.ColWidth(ColValorMxN) / 5))
                .ColWidth(ColDesGrp) = (.ColWidth(ColDesGrp) - (.ColWidth(ColValorMxN) / 5))
                .ColWidth(ColDesSubg) = (.ColWidth(ColDesSubg) - (.ColWidth(ColValorMxN) / 5))
                .ColWidth(ColDesProd) = (.ColWidth(ColDesProd) - (.ColWidth(ColValorMxN) / 5))
            Case Else
                .ColWidth(ColValorMxN) = 0
        End Select
        
        .ColWidth(ColTipoCriterio) = 1440
        AnchoCampoScroll_GrdCond = .ColWidth(ColTipoCriterio)
        
        .RowHeightMin = 900
        
    End With
    
End Sub

Private Sub CargarGridSucursales()
    
    With GrdSuc
        
        .Rows = 1
        .Row = 0
        .Clear
        .Cols = GridColSuc.ColCount
        .RowHeightMin = 600
        
        .Col = GridColSuc.ColRowID
        .TextMatrix(0, .Col) = "Ln" '"RowID"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodSuc
        .TextMatrix(0, .Col) = StellarMensaje(142) '"Codigo"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDescSuc
        .TextMatrix(0, .Col) = StellarMensaje(212) '"Sucursal"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDireccion
        .TextMatrix(0, .Col) = Replace(StellarMensaje(130), ":", Empty) 'Direccion
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColGerente
        .TextMatrix(0, .Col) = StellarMensaje(6099) '"Gerente"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .DragMode = 0
        '.AllowUserResizing = flexResizeColumns
        .AllowUserResizing = flexResizeNone
        .ColWidth(GridColSuc.ColRowID) = 600
        .ColWidth(ColCodSuc) = 3000
        .ColWidth(ColDescSuc) = 6000
        .ColWidth(ColDireccion) = 4000
        .ColWidth(ColGerente) = 0
        AnchoCampoScroll_GrdSuc = .ColWidth(ColDescSuc)
        
    End With
    
End Sub

Private Sub CargarGridFormaPago()
    
    With GrdDetPag
        
        .Rows = 1
        .Row = 0
        .Clear
        .Cols = GridColDetPag.ColCount
        .RowHeightMin = 600
        
        .Col = GridColSuc.ColRowID
        .TextMatrix(0, .Col) = "Ln" '"RowID"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodMoneda
        .TextMatrix(0, .Col) = StellarMensaje(142) '"Codigo"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesMoneda
        .TextMatrix(0, .Col) = Replace(StellarMensaje(134), ":", Empty) '"Moneda"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodDenomina
        .TextMatrix(0, .Col) = StellarMensaje(142) '"Codigo"
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDesDenomina
        .TextMatrix(0, .Col) = StellarMensaje(15515) '"Forma de Pago"
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .DragMode = 0
        '.AllowUserResizing = flexResizeColumns
        .AllowUserResizing = flexResizeNone
        .ColWidth(GridColDetPag.ColRowID) = 600
        .ColWidth(ColCodMoneda) = 3450
        .ColWidth(ColDesMoneda) = 3450
        .ColWidth(ColCodDenomina) = 3450
        .ColWidth(ColDesDenomina) = 3450
        AnchoCampoScroll_GrdDetPag = .ColWidth(ColCodMoneda)
        
    End With
    
End Sub

Private Sub CargarGridListaBIN()
    
    With GrdBIN
        
        .Rows = 1
        .Row = 0
        .Clear
        .Cols = GridColBIN.ColCount
        .RowHeightMin = 600
        
        .Col = GridColBIN.ColRowID
        .TextMatrix(0, .Col) = "Ln" '"RowID"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColItemNumber
        .TextMatrix(0, .Col) = "BIN / IIN (BANK / ISSUER IDENTIFICATION NUMBER)"
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .DragMode = 0
        '.AllowUserResizing = flexResizeColumns
        .AllowUserResizing = flexResizeNone
        .ColWidth(GridColSuc.ColRowID) = 600
        .ColWidth(ColItemNumber) = 14400
        AnchoCampoScroll_GrdBIN = .ColWidth(ColItemNumber)
        
    End With
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Cancel = 0 Then
        Call WheelUnHook(Me.hWnd)
        FormaCargada = False
    End If
End Sub

Private Sub FrameBarraTitulo_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub ImgNextCond_Click()
    
    If SVal(txtPagCond.Text) <= SVal(TxtNumPagCond.Text) Then
        txtPagCond.Text = SVal(txtPagCond.Text) + 1
        If SVal(txtPagCond.Text) > SVal(TxtNumPagCond.Text) Then txtPagCond.Text = SVal(TxtNumPagCond.Text)
    Else
        txtPagCond.Text = SVal(TxtNumPagCond.Text)
    End If
    
    CargarPromo_Condiciones_Valores
    
End Sub

Private Sub ImgPrevCond_Click()
    
    If SVal(txtPagCond.Text) >= 1 Then
        txtPagCond.Text = SVal(txtPagCond.Text) - 1
        If SVal(txtPagCond.Text) <= 1 Then txtPagCond.Text = 1
    Else
        txtPagCond.Text = 1
    End If
    
    CargarPromo_Condiciones_Valores
    
End Sub

Private Sub Timer_LoadOtherInfo_Timer()
    If Timer_LoadOtherInfo.Enabled Then
        
        Timer_LoadOtherInfo.Enabled = False
        
        CargarPromo_Sucursales
        CargarPromo_Condiciones_Valores
        
        If Sections.TabVisible(3) Then
            CargarPromo_DetPag
            CargarPagoMinimo
            CargarLimiteDescuentoPromo
        End If
        
        If Sections.TabVisible(4) Then
            CargarPromo_ListaBIN
            CargarLimiteDescuentoTarjeta
        End If
        
    End If
End Sub

Private Sub txtPagCond_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtPagCond_GotFocus()
    Set CampoT = txtPagCond
    txtPagCond.Tag = SVal(txtPagCond.Text)
End Sub

Private Sub txtPagCond_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtPagCond_LostFocus
    End If
End Sub

Private Sub txtPagCond_LostFocus()
    If SVal(txtPagCond.Text) <> SVal(txtPagCond.Tag) Then
        CargarPromo_Condiciones_Valores
    End If
End Sub

Private Sub ScrollGridCond_Change()
    On Error GoTo ErrScroll
    If ScrollGridCond.Value <> GrdCond.Row Then
        GrdCond.TopRow = ScrollGridCond.Value
        GrdCond.Row = ScrollGridCond.Value
        If PuedeObtenerFoco(GrdCond) Then GrdCond.SetFocus
    End If
    Exit Sub
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGridCond_Scroll()
    'Debug.Print ScrollGridCond.value
    ScrollGridCond_Change
End Sub

Private Sub ImgNextSuc_Click()
    
    If SVal(txtPagSuc.Text) <= SVal(TxtNumPagSuc.Text) Then
        txtPagSuc.Text = SVal(txtPagSuc.Text) + 1
        If SVal(txtPagSuc.Text) > SVal(TxtNumPagSuc.Text) Then txtPagSuc.Text = SVal(TxtNumPagSuc.Text)
    Else
        txtPagSuc.Text = SVal(TxtNumPagSuc.Text)
    End If
    
    CargarPromo_Sucursales
    
End Sub

Private Sub ImgPrevSuc_Click()
    
    If SVal(txtPagSuc.Text) >= 1 Then
        txtPagSuc.Text = SVal(txtPagSuc.Text) - 1
        If SVal(txtPagSuc.Text) <= 1 Then txtPagSuc.Text = 1
    Else
        txtPagSuc.Text = 1
    End If
    
    CargarPromo_Sucursales
    
End Sub

Private Sub txtPagSuc_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtPagSuc_GotFocus()
    Set CampoT = txtPagSuc
    txtPagSuc.Tag = SVal(txtPagSuc.Text)
End Sub

Private Sub txtPagSuc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtPagSuc_LostFocus
    End If
End Sub

Private Sub txtPagSuc_LostFocus()
    If SVal(txtPagSuc.Text) <> SVal(txtPagSuc.Tag) Then
        CargarPromo_Sucursales
    End If
End Sub

Private Sub ScrollGridSuc_Change()
    On Error GoTo ErrScroll
    If ScrollGridSuc.Value <> GrdSuc.Row Then
        GrdSuc.TopRow = ScrollGridSuc.Value
        GrdSuc.Row = ScrollGridSuc.Value
        If PuedeObtenerFoco(GrdSuc) Then GrdSuc.SetFocus
    End If
    Exit Sub
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGridSuc_Scroll()
    'Debug.Print ScrollGridSuc.value
    ScrollGridSuc_Change
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        
        Case Is = "AGREGAR"
            Call Form_KeyDown(vbKeyF3, 0)
        
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF7, 0)
        
        Case Is = "MODIFICAR"
            Call Form_KeyDown(vbKeyF5, 0)
        
        Case Is = "BORRAR"
            ''Call Form_KeyDown(vbKeyF6, 0)
        
        Case Is = "SALIR"
            Call Form_KeyDown(vbKeyF12, 0)
        
    End Select
    
End Sub

Private Sub CargarPromo_DatosGenerales()
    
    On Error GoTo Error
    
    Dim RsPromociones As ADODB.Recordset
    Set RsPromociones = New ADODB.Recordset
    
    Dim mTabla As String
    
    If EstatusPromo = "DWT" Then
        mTabla = "TMP_PROMOCION"
    Else
        mTabla = "MA_PROMOCION"
    End If
    
    Set RsPromociones = FrmAppLink.CnADM.Execute( _
    "SELECT TOP 1 * FROM " & mTabla & " " & _
    "WHERE 1 = 1 " & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ")
    
    With RsPromociones
        
        If Not .EOF Then
            
            txtCodigo.Text = mCodPromo_Actual
            txtDescripcion.Text = !Descripcion
            txtCampanna.Text = !Campa�a
            
            txtFechaDesde.Tag = CDbl(!Fecha_Inicio)
            txtFechaDesde.Text = SDate(CDate(txtFechaDesde.Tag))
            TxtFechaHasta.Tag = CDbl(!Fecha_Fin)
            TxtFechaHasta.Text = SDate(CDate(TxtFechaHasta.Tag))
            txtHoraInicio.Tag = CDbl(CDate(Split(!Aplica_Hora_Inicio, ".")(0)))
            txtHoraInicio.Text = STime(CDate(txtHoraInicio.Tag))
            txtHoraFin.Tag = CDbl(CDate(Split(!Aplica_Hora_Fin, ".")(0)))
            txtHoraFin.Text = STime(CDate(txtHoraFin.Tag))
            
            txtMontoMinimo.Text = FormatNumber(!Monto_Minimo_Venta)
            txtMontoMaximo.Text = FormatNumber(!Monto_Maximo_Venta)
            
            For I = 1 To 7
                chkDias(I).Value = IIf(!Aplica_Dia_Semana Like "*" & I & "*", vbChecked, vbUnchecked)
            Next I
            
            chkCombinada.Value = IIf(!Combina_Promocion, vbChecked, vbUnchecked)
            
            If chkCombinada.Value = vbChecked Then
                lblPrioridad.Visible = True
                txtPrioridad.Visible = True
            Else
                lblPrioridad.Visible = False
                txtPrioridad.Visible = False
            End If
            
            txtPrioridad.Text = SDec(!Prioridad_Promocion)
            
            chkCumpleanneros.Visible = False
            
            Select Case !Tipo_Cliente
                Case 0
                    optTodos.Value = True
                Case 1
                    optAfiliados.Value = True
                    chkCumpleanneros.Value = vbUnchecked
                Case 2
                    optNoAfiliados.Value = True
                Case 3
                    optAfiliados.Value = True
                    chkCumpleanneros.Value = vbChecked
            End Select
            
            FrmAppLink.ListSafeItemSelection cmbGrupoClientes, !Grupo_Cliente
            
            If ChkCombinarProductos.Visible Then
                If !Cantidad_Productos_Requerir > 0 _
                And !Cantidad_Productos_Pagar > 0 Then
                    ChkCombinarProductos.Value = vbChecked
                Else
                    ChkCombinarProductos.Value = vbUnchecked
                End If
            End If
            
            lblDescCreadoPor.Caption = Buscar_Usuario(CStr(!Cod_Usuario_Add))
            lblFechaCreacionValor.Caption = GDate(CDate(!Fecha_Add))
            
            If FrmAppLink.Promocion_CampoMonedaMontos Then
                
                If Trim(!Cod_Moneda_Monto) <> Empty Then
                    txt_Moneda = !Cod_Moneda_Monto
                Else
                    txt_Moneda = FrmAppLink.CodMonedaPref
                End If
                
                MonedaDoc.BuscarMonedas , txt_Moneda
                
                'lbl_Moneda.Caption = MonedaDoc.DesMoneda
                'lbl_Moneda.Tag = MonedaDoc.SimMoneda
                
                lblMontoMinimo.Caption = "(" & MonedaDoc.SimMoneda & ") " & StellarMensajeLocal(20)
                lblMontoMaximo.Caption = "(" & MonedaDoc.SimMoneda & ") " & StellarMensajeLocal(21)
                
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub CargarPromo_Condiciones_Valores()
    
    On Error GoTo Error
    
    Dim LastRow As Integer, LastCtl As Object
    Dim NumRows As Double, NumPag As Double
    Dim RsCondicion As ADODB.Recordset
    Set RsCondicion = New ADODB.Recordset
    
    Dim mSQL As String
    
    Dim mTabla As String
    
    If EstatusPromo = "DWT" Then
        mSQL = "SELECT isNULL(COUNT(ID), 0) AS NumReg " & GetLines & _
        "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
        "AND Tipo_Condicion IN (1, 2, 3, 4) "
    Else
        mSQL = "SELECT isNULL(COUNT(Cod_Promocion), 0) AS NumReg " & GetLines & _
        "FROM TR_PROMOCION_CONDICION " & GetLines & _
        "WHERE 1 = 1 " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
        "AND Tipo_Condicion IN (1, 2, 3, 4) "
    End If
    
    Set RsCondicion = FrmAppLink.CnADM.Execute(mSQL)
    
    With RsCondicion
        
        If Not .EOF Then
        
            NumRows = !NumReg
            
            If NumRows > 0 Then
                
                NumPag = Round(NumRows / MaxGridRowsxPag, 8)
                If (NumPag - Fix(NumPag)) <> 0 Then NumPag = Fix(NumPag + 1)
                
                TxtNumPagCond.Text = NumPag
                
                txtPagCond.Text = Fix(SVal(txtPagCond))
                
                If SVal(txtPagCond.Text) >= 1 And SVal(txtPagCond.Text) <= NumPag Then
                    txtPagCond.Text = Fix(SVal(txtPagCond))
                Else
                    txtPagCond.Text = 1
                End If
                
                If EstatusPromo = "DWT" Then
                    mSQL = "WITH AllRows AS (" & GetLines & _
                    "SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowID, *" & GetLines & _
                    "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
                    "WHERE 1 = 1 " & GetLines & _
                    "AND Tipo_Condicion IN (1, 2, 3, 4) " & GetLines & _
                    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & _
                    "' " & GetLines & ")" & GetLines & _
                    "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                    "((" & txtPagCond.Text & " - 1) * (" & MaxGridRowsxPag & " + 1)) AND " & _
                    "(" & txtPagCond.Text & " * " & MaxGridRowsxPag & ")" & GetLines
                Else
                    
                    ' Bad Performance
                    
                    'mSQL = "WITH AllRows AS (" & GetLines & _
                    "SELECT ROW_NUMBER() OVER (ORDER BY TPC.Cod_Promocion, TPC.Linea_Condicion) AS RowID, TPC.*, " & GetLines & _
                    "TPV.Cantidad_Productos_Requerir, TPV.Cantidad_Productos_Pagar, " & GetLines & _
                    "TPV.Precio_Oferta, TPV.Monto_Descuento, TPV.Porcentaje_Descuento1, " & GetLines & _
                    "TPV.Porcentaje_Descuento2, TPV.Porcentaje_Descuento3, " & GetLines & _
                    "isNULL(PRV.c_Descripcio, '') AS Des_Proveedor, " & GetLines & _
                    "isNULL(DPT.c_Descripcio, '') AS Des_Dpto, " & GetLines & _
                    "isNULL(GRP.c_Descripcio, '') AS Des_Grupo, " & GetLines & _
                    "isNULL(SBG.c_Descripcio, '') AS Des_Subgrupo, " & GetLines & _
                    "isNULL(PRO.c_Descri, '') AS Des_Producto " & GetLines & _
                    "FROM TR_PROMOCION_CONDICION TPC INNER JOIN TR_PROMOCION_VALORES TPV " & GetLines & _
                    "ON TPC.Cod_Promocion = TPV.Cod_Promocion AND TPC.Linea_Valor = TPV.Linea_Valor " & GetLines & _
                    "LEFT JOIN MA_PROVEEDORES PRV ON TPC.Cod_Proveedor = PRV.c_CodProveed " & GetLines & _
                    "LEFT JOIN MA_DEPARTAMENTOS DPT ON TPC.Cod_Dpto = DPT.c_Codigo " & GetLines & _
                    "LEFT JOIN MA_GRUPOS GRP ON TPC.Cod_Grupo = GRP.c_Codigo AND TPC.Cod_Dpto = GRP.c_Departamento " & GetLines & _
                    "LEFT JOIN MA_SUBGRUPOS SBG ON TPC.Cod_Subgrupo = SBG.c_Codigo " & _
                    "AND TPC.Cod_Grupo = SBG.c_In_Grupo AND TPC.Cod_Dpto = SBG.c_In_Departamento" & GetLines & _
                    "LEFT JOIN MA_PRODUCTOS PRO ON "
                    
                    'mSQL = mSQL & _
                    "CASE" & GetLines & _
                    "   WHEN TPC.Cod_Producto <> '' THEN" & GetLines & _
                    "       CASE WHEN TPC.Cod_Producto = PRO.c_Codigo THEN 1 ELSE 0 END" & GetLines & _
                    "   ELSE" & GetLines & _
                    "       CASE WHEN" & GetLines & _
                    "           CASE WHEN LTRIM(RTRIM(TPC.Marca)) = '' THEN 0 ELSE CASE WHEN TPC.Marca LIKE PRO.c_Marca THEN 1 ELSE 0 END END +" & GetLines & _
                    "           CASE WHEN LTRIM(RTRIM(TPC.Modelo)) = '' THEN 0 ELSE CASE WHEN TPC.Modelo LIKE PRO.c_Modelo THEN 1 ELSE 0 END END " & GetLines & _
                    "           = 2" & GetLines & _
                    "       THEN 1" & GetLines & _
                    "       ELSE 0 " & GetLines & _
                    "   END" & GetLines & _
                    "END = 1" & GetLines & _
                    "WHERE 1 = 1 " & GetLines & _
                    "AND TPC.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & _
                    "' " & GetLines & ") " & GetLines & _
                    "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                    "((" & txtPagCond.Text & " - 1) * (" & MaxGridRowsxPag & " + 1)) AND " & _
                    "(" & txtPagCond.Text & " * " & MaxGridRowsxPag & ")" & GetLines
                    
                    mSQL = "WITH AllRows AS (" & GetLines & _
                    "SELECT ROW_NUMBER() OVER (ORDER BY TPC.Linea_Condicion) AS RowID, TPC.*, " & GetLines & _
                    "TPV.Cantidad_Productos_Requerir, TPV.Cantidad_Productos_Pagar, " & GetLines & _
                    "TPV.Precio_Oferta, TPV.Monto_Descuento, TPV.Porcentaje_Descuento1, " & GetLines & _
                    "TPV.Porcentaje_Descuento2, TPV.Porcentaje_Descuento3, " & GetLines & _
                    "'' AS Des_Proveedor, '' AS Des_Dpto, '' AS Des_Grupo, " & GetLines & _
                    "'' AS Des_Sugrupo, '' AS Des_Producto " & GetLines & _
                    "FROM TR_PROMOCION_CONDICION TPC INNER JOIN TR_PROMOCION_VALORES TPV " & GetLines & _
                    "ON TPC.Cod_Promocion = TPV.Cod_Promocion AND TPC.Linea_Valor = TPV.Linea_Valor " & GetLines & _
                    "WHERE 1 = 1 " & GetLines & _
                    "AND TPC.Tipo_Condicion IN (1, 2, 3, 4) " & GetLines & _
                    "AND TPC.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & _
                    "' " & GetLines & ") " & GetLines & _
                    "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                    "((" & txtPagCond.Text & " - 1) * (" & MaxGridRowsxPag & ") + 1) AND " & _
                    "(" & txtPagCond.Text & " * " & MaxGridRowsxPag & ")" & GetLines
                    
                End If
                
                RsCondicion.Close
                
                Set RsCondicion = New Recordset
                Apertura_RecordsetC RsCondicion
                
                .Open mSQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If Not .EOF Then
                    
                    .Sort = "RowID"
                    
                    Dim I As Integer
                    
                    'LastRow = Grid.Row
                    'Set LastCtl = Screen.ActiveControl
                    
                    GrdCond.Visible = False
                    
                    GrdCond.Rows = 1
                    
                    ScrollGridCond.Min = 0
                    ScrollGridCond.Max = .RecordCount
                    ScrollGridCond.Value = ScrollGridCond.Min
                    
                    While Not .EOF
                    
                        GrdCond.Rows = GrdCond.Rows + 1
                        I = I + 1 '!RowID
                        
                        GrdCond.TextMatrix(I, GridColCond.ColRowID) = I
                        GrdCond.TextMatrix(I, ColTipoCondicion) = !Tipo_Condicion
                        
                        Select Case TipoPromo
                            Case 8, 9, 10, 11, 12, 13, 14, 15
                                Select Case !Tipo_Condicion
                                    Case 1
                                        GrdCond.TextMatrix(I, ColTipoCriterio) = _
                                        StellarMensajeLocal(192)
                                    Case 2
                                        GrdCond.TextMatrix(I, ColTipoCriterio) = _
                                        StellarMensajeLocal(193)
                                    Case 3
                                        GrdCond.TextMatrix(I, ColTipoCriterio) = _
                                        StellarMensajeLocal(194)
                                    Case 4
                                        GrdCond.TextMatrix(I, ColTipoCriterio) = _
                                        StellarMensajeLocal(195)
                                End Select
                            Case Else
                                GrdCond.TextMatrix(I, ColTipoCriterio) = _
                                IIf(!Tipo_Condicion = 2, StellarMensajeLocal(155), StellarMensajeLocal(154))
                        End Select
                        
                        GrdCond.TextMatrix(I, ColCodProv) = !Cod_Proveedor
                        GrdCond.TextMatrix(I, ColCodProd) = !Cod_Producto
                        GrdCond.TextMatrix(I, ColCodDpto) = !Cod_Dpto
                        GrdCond.TextMatrix(I, ColCodGrp) = !Cod_Grupo
                        GrdCond.TextMatrix(I, ColCodSubg) = !Cod_Subgrupo
                        
                        'GrdCond.TextMatrix(I, ColDesProv) = !Des_Proveedor
                        'GrdCond.TextMatrix(I, ColDesProd) = !Des_Producto
                        'GrdCond.TextMatrix(I, ColDesDpto) = !Des_Dpto
                        'GrdCond.TextMatrix(I, ColDesGrp) = !Des_Grupo
                        'GrdCond.TextMatrix(I, ColDesSubg) = !Des_Subgrupo
                        
                        If Trim(GrdCond.TextMatrix(I, ColCodProv)) <> Empty Then
                            GrdCond.TextMatrix(I, ColDesProv) = BuscarValorBD("c_Descripcio", _
                            "SELECT c_Descripcio FROM MA_PROVEEDORES " & _
                            "WHERE c_CodProveed = '" & QuitarComillasSimples(GrdCond.TextMatrix(I, ColCodProv)) & "' ")
                        End If
                        
                        If Trim(GrdCond.TextMatrix(I, ColCodProd)) <> Empty Then
                            GrdCond.TextMatrix(I, ColDesProd) = BuscarValorBD("c_Descri", _
                            "SELECT ('[' + c_Codigo + ']' + ' ' + c_Descri) AS c_Descri " & _
                            "FROM MA_PRODUCTOS " & _
                            "WHERE c_Codigo = '" & QuitarComillasSimples(GrdCond.TextMatrix(I, ColCodProd)) & "' ")
                        End If
                        
                        If Trim(GrdCond.TextMatrix(I, ColCodDpto)) <> Empty Then
                            GrdCond.TextMatrix(I, ColDesDpto) = BuscarValorBD("c_Descripcio", _
                            "SELECT c_Descripcio FROM MA_DEPARTAMENTOS " & _
                            "WHERE c_Codigo = '" & QuitarComillasSimples(GrdCond.TextMatrix(I, ColCodDpto)) & "' ")
                        End If
                        
                        If Trim(GrdCond.TextMatrix(I, ColCodGrp)) <> Empty Then
                            GrdCond.TextMatrix(I, ColDesGrp) = BuscarValorBD("c_Descripcio", _
                            "SELECT c_Descripcio FROM MA_GRUPOS " & _
                            "WHERE c_Codigo = '" & QuitarComillasSimples(GrdCond.TextMatrix(I, ColCodGrp)) & "' " & _
                            "AND c_Departamento = '" & QuitarComillasSimples(GrdCond.TextMatrix(I, ColCodDpto)) & "' ")
                        End If
                        
                        If Trim(GrdCond.TextMatrix(I, ColCodSubg)) <> Empty Then
                            GrdCond.TextMatrix(I, ColDesSubg) = BuscarValorBD("c_Descripcio", _
                            "SELECT c_Descripcio FROM MA_SUBGRUPOS " & _
                            "WHERE c_Codigo = '" & QuitarComillasSimples(GrdCond.TextMatrix(I, ColCodSubg)) & "' " & _
                            "AND c_In_Departamento = '" & QuitarComillasSimples(GrdCond.TextMatrix(I, ColCodDpto)) & "' " & _
                            "AND c_In_Grupo = '" & QuitarComillasSimples(GrdCond.TextMatrix(I, ColCodGrp)) & "' ")
                        End If
                        
                        GrdCond.TextMatrix(I, ColMarca) = !Marca
                        GrdCond.TextMatrix(I, ColModelo) = !Modelo
                        
                        Select Case TipoPromo
                            Case 1
                                GrdCond.TextMatrix(I, ColValorPromo1) = FormatNumber(!Precio_Oferta)
                            Case 2
                                GrdCond.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                                GrdCond.TextMatrix(I, ColValorPromo2) = FormatNumber(!Porcentaje_Descuento2)
                                GrdCond.TextMatrix(I, ColValorPromo3) = FormatNumber(!Porcentaje_Descuento3)
                            Case 3
                                GrdCond.TextMatrix(I, ColValorPromo1) = FormatNumber(!Monto_Descuento)
                            Case 4
                                GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Requerir & "x" & !Cantidad_Productos_Pagar)
                            Case 5
                                GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar & " " & StellarMensajeLocal(169) & " " & !Cantidad_Productos_Requerir)
                                GrdCond.TextMatrix(I, ColValorPromo1) = FormatNumber(!Precio_Oferta)
                            Case 6
                                GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar & " " & StellarMensajeLocal(169) & " " & !Cantidad_Productos_Requerir)
                                GrdCond.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                                GrdCond.TextMatrix(I, ColValorPromo2) = FormatNumber(!Porcentaje_Descuento2)
                                GrdCond.TextMatrix(I, ColValorPromo3) = FormatNumber(!Porcentaje_Descuento3)
                            Case 7
                                GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar & " " & StellarMensajeLocal(169) & " " & !Cantidad_Productos_Requerir)
                                GrdCond.TextMatrix(I, ColValorPromo1) = FormatNumber(!Monto_Descuento)
                            Case 8, 12
                                If !Tipo_Condicion = 1 Then
                                    GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Requerir)
                                ElseIf !Tipo_Condicion = 3 Then
                                    GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar)
                                End If
                            Case 9, 13
                                If !Tipo_Condicion = 1 Then
                                    GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Requerir)
                                ElseIf !Tipo_Condicion = 3 Then
                                    GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar)
                                    GrdCond.TextMatrix(I, ColValorPromo1) = FormatNumber(!Precio_Oferta)
                                End If
                            Case 10, 14
                                If !Tipo_Condicion = 1 Then
                                    GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Requerir)
                                ElseIf !Tipo_Condicion = 3 Then
                                    GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar)
                                    GrdCond.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                                    GrdCond.TextMatrix(I, ColValorPromo2) = FormatNumber(!Porcentaje_Descuento2)
                                    GrdCond.TextMatrix(I, ColValorPromo3) = FormatNumber(!Porcentaje_Descuento3)
                                End If
                            Case 11, 15
                                If !Tipo_Condicion = 1 Then
                                    GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Requerir)
                                ElseIf !Tipo_Condicion = 3 Then
                                    GrdCond.TextMatrix(I, ColValorMxN) = (!Cantidad_Productos_Pagar)
                                    GrdCond.TextMatrix(I, ColValorPromo1) = FormatNumber(!Monto_Descuento)
                                End If
                            Case 16, 17, 18, 19, 20, 21
                                GrdCond.TextMatrix(I, ColValorPromo1) = FormatNumber(!Porcentaje_Descuento1)
                            Case Else
                                'NotImplementedYet
                        End Select
                        
                        .MoveNext
                        
                    Wend
                    
                    GrdCond.Visible = True
                    
                    'If LastRow >= 1 And LastRow <= GrdCond.Rows - 1 Then
                        'GrdCond.Row = LastRow
                        'GrdCond.TopRow = GrdCond.Row
                    'Else
                        'GrdCond.Row = 1
                        'GrdCond.TopRow = GrdCond.Row
                    'End If
                    
                    'If Not LastCtl Is Nothing Then
                        'If PuedeObtenerFoco(LastCtl) Then LastCtl.SetFocus
                    'End If
                    
                End If
                
            Else
                NumPag = 1
                txtPagCond.Text = NumPag
                GrdCond.Rows = 1
                GrdCond.Rows = 2
            End If
            
            If GrdCond.Rows > 5 Then
                ScrollGridCond.Visible = True
                GrdCond.ScrollBars = flexScrollBarVertical
                AnchoScrollBar_GrdCond = ScrollGridCond.Width
                GrdCond.ColWidth(ColTipoCriterio) = (AnchoCampoScroll_GrdCond - AnchoScrollBar_GrdCond)
            Else
                GrdCond.ScrollBars = flexScrollBarHorizontal
                ScrollGridCond.Visible = False
                AnchoScrollBar_GrdCond = 0
                GrdCond.ColWidth(ColTipoCriterio) = AnchoCampoScroll_GrdCond
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    'Resume ' Debug
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    CargarGridCondiciones
    GrdCond.Visible = True
    
End Sub

Private Sub CargarPromo_Sucursales()
    
    On Error GoTo Error
    
    Dim LastRow As Integer
    Dim NumRows As Double, NumPag As Double
    Dim RsSucursales As ADODB.Recordset
    Set RsSucursales = New ADODB.Recordset
    
    Dim mSQL As String
    
    Dim mTabla As String
    
    If EstatusPromo = "DWT" Then
        mSQL = "SELECT isNULL(COUNT(ID), 0) AS NumReg " & GetLines & _
        "FROM TMP_PROMOCION_SUCURSAL " & GetLines & _
        "WHERE CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines
    Else
        mSQL = "SELECT isNULL(COUNT(Cod_Promocion), 0) AS NumReg " & GetLines & _
        "FROM MA_PROMOCION_SUCURSAL " & GetLines & _
        "WHERE 1 = 1 " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines
    End If
    
    Set RsSucursales = FrmAppLink.CnADM.Execute(mSQL)
    
    With RsSucursales
        
        If Not .EOF Then
        
            NumRows = !NumReg
            
            If NumRows > 0 Then
                
                NumPag = Round(NumRows / MaxGridRowsxPag, 8)
                If (NumPag - Fix(NumPag)) <> 0 Then NumPag = Fix(NumPag + 1)
                
                TxtNumPagSuc.Text = NumPag
                
                txtPagSuc.Text = Fix(SVal(txtPagSuc))
                
                If SVal(txtPagSuc.Text) >= 1 And SVal(txtPagSuc.Text) <= NumPag Then
                    txtPagSuc.Text = Fix(SVal(txtPagSuc))
                Else
                    txtPagSuc.Text = 1
                End If
                
                If EstatusPromo = "DWT" Then
                    mSQL = "WITH AllRows AS (" & GetLines & _
                    "SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowID, *" & GetLines & _
                    "FROM TMP_PROMOCION_SUCURSAL" & GetLines & _
                    "WHERE CodUsuario = '" & FrmAppLink.GetCodUsuario & "' " & GetLines & _
                    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
                    ")" & GetLines & _
                    "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                    "((" & txtPagSuc.Text & " - 1) * (" & MaxGridRowsxPag & " + 1)) AND " & _
                    "(" & txtPagSuc.Text & " * " & MaxGridRowsxPag & ")" & GetLines
                Else
                    mSQL = "WITH AllRows AS (" & GetLines & _
                    "SELECT ROW_NUMBER() OVER (ORDER BY Cod_Promocion) AS RowID, MPS.*," & GetLines & _
                    "isNULL(SUC.c_Descripcion, 'N/A') AS Des_Localidad, isNULL(SUC.c_Direccion, 'N/A') AS Dir_Localidad" & GetLines & _
                    "FROM MA_PROMOCION_SUCURSAL MPS INNER JOIN MA_SUCURSALES SUC" & GetLines & _
                    "ON MPS.Cod_Localidad = SUC.c_Codigo" & GetLines & _
                    "WHERE 1 = 1 " & GetLines & _
                    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "'" & GetLines & _
                    ")" & GetLines & _
                    "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                    "((" & txtPagSuc.Text & " - 1) * (" & MaxGridRowsxPag & ") + 1) AND " & _
                    "(" & txtPagSuc.Text & " * " & MaxGridRowsxPag & ")" & GetLines
                End If
                
                RsSucursales.Close
                
                Set RsSucursales = New Recordset
                Apertura_RecordsetC RsSucursales
                
                .Open mSQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If Not .EOF Then
                    
                    .Sort = "RowID"
                    
                    Dim I As Integer
                    
                    GrdSuc.Visible = False
                    
                    GrdSuc.Rows = 1
                    
                    ScrollGridSuc.Min = 0
                    ScrollGridSuc.Max = .RecordCount
                    ScrollGridSuc.Value = ScrollGridSuc.Min
                    
                    While Not .EOF
                    
                        GrdSuc.Rows = GrdSuc.Rows + 1
                        I = I + 1 '!RowID
                        
                        GrdSuc.TextMatrix(I, GridColSuc.ColRowID) = I
                        GrdSuc.TextMatrix(I, ColCodSuc) = !Cod_Localidad
                        GrdSuc.TextMatrix(I, ColDescSuc) = !Des_Localidad
                        GrdSuc.TextMatrix(I, ColDireccion) = !Dir_Localidad
                        'GrdSuc.TextMatrix(I, ColGerente) = !Ger_Localidad
                        
                        .MoveNext
                        
                    Wend
                    
                    GrdSuc.Visible = True
                    
                    'If LastRow >= 1 And LastRow <= GrdSuc.Rows - 1 Then
                        'GrdSuc.Row = LastRow
                        'GrdSuc.TopRow = GrdSuc.Row
                    'Else
                        'GrdSuc.Row = 1
                        'GrdSuc.TopRow = GrdSuc.Row
                    'End If
                    
                End If
                
            Else
                NumPag = 1
                txtPagSuc.Text = NumPag
                GrdSuc.Rows = 1
                GrdSuc.Rows = 2
            End If
            
            If GrdSuc.Rows > 8 Then
                ScrollGridSuc.Visible = True
                GrdSuc.ScrollBars = flexScrollBarVertical
                AnchoScrollBar_GrdSuc = ScrollGridSuc.Width
                GrdSuc.ColWidth(ColDescSuc) = (AnchoCampoScroll_GrdSuc - AnchoScrollBar_GrdSuc)
            Else
                GrdSuc.ScrollBars = flexScrollBarHorizontal
                ScrollGridSuc.Visible = False
                AnchoScrollBar_GrdSuc = 0
                GrdSuc.ColWidth(ColDescSuc) = AnchoCampoScroll_GrdSuc
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    CargarGridSucursales
    GrdSuc.Visible = True
    
End Sub

Private Sub CargarPromo_DetPag()
    
    On Error GoTo Error
    
    Dim LastRow As Integer
    Dim NumRows As Double, NumPag As Double
    Dim RsDetPag As ADODB.Recordset
    Set RsDetPag = New ADODB.Recordset
    
    Dim mSQL As String
    
    Dim mTabla As String
    
    If EstatusPromo = "DWT" Then
        mSQL = "SELECT isNULL(COUNT(ID), 0) AS NumReg " & GetLines & _
        "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
        "AND Tipo_Condicion = 5 "
    Else
        mSQL = "SELECT isNULL(COUNT(Cod_Promocion), 0) AS NumReg " & GetLines & _
        "FROM TR_PROMOCION_CONDICION " & GetLines & _
        "WHERE 1 = 1 " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
        "AND Tipo_Condicion = 5 "
    End If
    
    Set RsDetPag = FrmAppLink.CnADM.Execute(mSQL)
    
    With RsDetPag
        
        If Not .EOF Then
        
            NumRows = !NumReg
            
            If NumRows > 0 Then
                
                NumPag = Round(NumRows / MaxGridRowsxPag_DetPag, 8)
                If (NumPag - Fix(NumPag)) <> 0 Then NumPag = Fix(NumPag + 1)
                
                txtNumPagDetPag.Text = NumPag
                
                txtPagDetPag.Text = Fix(SVal(txtPagDetPag))
                
                If SVal(txtPagDetPag.Text) >= 1 And SVal(txtPagDetPag.Text) <= NumPag Then
                    txtPagDetPag.Text = Fix(SVal(txtPagDetPag))
                Else
                    txtPagDetPag.Text = 1
                End If
                
                If EstatusPromo = "DWT" Then
                    mSQL = "WITH AllRows AS (" & GetLines & _
                    "SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowID, *" & GetLines & _
                    "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
                    "WHERE 1 = 1 " & GetLines & _
                    "AND Tipo_Condicion = 5 " & GetLines & _
                    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & _
                    "' " & GetLines & ")" & GetLines & _
                    "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                    "((" & txtPagDetPag.Text & " - 1) * (" & MaxGridRowsxPag_DetPag & " + 1)) AND " & _
                    "(" & txtPagDetPag.Text & " * " & MaxGridRowsxPag_DetPag & ")" & GetLines
                Else
                    mSQL = "WITH AllRows AS (" & GetLines & _
                    "SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowID, *" & GetLines & _
                    "FROM TR_PROMOCION_CONDICION " & GetLines & _
                    "WHERE 1 = 1 " & GetLines & _
                    "AND Tipo_Condicion = 5 " & GetLines & _
                    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & _
                    "' " & GetLines & ")" & GetLines & _
                    "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                    "((" & txtPagDetPag.Text & " - 1) * (" & MaxGridRowsxPag_DetPag & " + 1)) AND " & _
                    "(" & txtPagDetPag.Text & " * " & MaxGridRowsxPag_DetPag & ")" & GetLines
                End If
                
                RsDetPag.Close
                
                Set RsDetPag = New Recordset
                Apertura_RecordsetC RsDetPag
                
                .Open mSQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If Not .EOF Then
                    
                    .Sort = "RowID"
                    
                    Dim I As Integer
                    
                    GrdDetPag.Visible = False
                    
                    GrdDetPag.Rows = 1
                    
                    ScrollGridDetPag.Min = 0
                    ScrollGridDetPag.Max = .RecordCount
                    ScrollGridDetPag.Value = ScrollGridDetPag.Min
                    
                    While Not .EOF
                    
                        GrdDetPag.Rows = GrdDetPag.Rows + 1
                        I = I + 1 '!RowID
                        
                        GrdDetPag.TextMatrix(I, GridColDetPag.ColRowID) = I
                        
                        GrdDetPag.TextMatrix(I, ColCodMoneda) = !Cod_Dpto
                        If Trim(GrdDetPag.TextMatrix(I, ColCodMoneda)) <> Empty Then
                            GrdDetPag.TextMatrix(I, ColDesMoneda) = BuscarValorBD("c_Descripcion", _
                            "SELECT c_Descripcion FROM MA_MONEDAS " & _
                            "WHERE c_CodMoneda = '" & QuitarComillasSimples(GrdDetPag.TextMatrix(I, ColCodMoneda)) & "' ")
                        End If
                        
                        GrdDetPag.TextMatrix(I, ColCodDenomina) = !Cod_Grupo
                        If Trim(GrdDetPag.TextMatrix(I, ColCodDenomina)) <> Empty Then
                            GrdDetPag.TextMatrix(I, ColDesDenomina) = BuscarValorBD("c_Denominacion", _
                            "SELECT c_Denominacion FROM MA_DENOMINACIONES " & _
                            "WHERE c_CodMoneda = '" & QuitarComillasSimples(GrdDetPag.TextMatrix(I, ColCodMoneda)) & "' " & _
                            "AND c_CodDenomina = '" & QuitarComillasSimples(GrdDetPag.TextMatrix(I, ColCodDenomina)) & "' ")
                        End If
                        
                        .MoveNext
                        
                    Wend
                    
                    GrdDetPag.Visible = True
                    
                    'If LastRow >= 1 And LastRow <= GrdDetPag.Rows - 1 Then
                        'GrdDetPag.Row = LastRow
                        'GrdDetPag.TopRow = GrdDetPag.Row
                    'Else
                        'GrdDetPag.Row = 1
                        'GrdDetPag.TopRow = GrdDetPag.Row
                    'End If
                    
                End If
                
            Else
                NumPag = 1
                txtPagDetPag.Text = NumPag
                GrdDetPag.Rows = 1
                GrdDetPag.Rows = 2
            End If
            
            If GrdDetPag.Rows > 9 Then
                ScrollGridDetPag.Visible = True
                GrdDetPag.ScrollBars = flexScrollBarVertical
                AnchoScrollBar_GrdDetPag = ScrollGridDetPag.Width
                GrdDetPag.ColWidth(ColCodMoneda) = (AnchoCampoScroll_GrdDetPag - AnchoScrollBar_GrdDetPag)
            Else
                GrdDetPag.ScrollBars = flexScrollBarHorizontal
                ScrollGridDetPag.Visible = False
                AnchoScrollBar_GrdDetPag = 0
                GrdDetPag.ColWidth(ColCodMoneda) = AnchoCampoScroll_GrdDetPag
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CargarPromo_DetPag)"
    
    CargarGridFormaPago
    GrdDetPag.Visible = True
    
End Sub

Private Sub CargarPromo_ListaBIN()
    
    On Error GoTo Error
    
    Dim LastRow As Integer
    Dim NumRows As Double, NumPag As Double
    Dim RsDetPag As ADODB.Recordset
    Set RsDetPag = New ADODB.Recordset
    
    Dim mSQL As String
    
    Dim mTabla As String
    
    If EstatusPromo = "DWT" Then
        mSQL = "SELECT isNULL(COUNT(ID), 0) AS NumReg " & GetLines & _
        "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
        "AND Tipo_Condicion = 8 "
    Else
        mSQL = "SELECT isNULL(COUNT(Cod_Promocion), 0) AS NumReg " & GetLines & _
        "FROM TR_PROMOCION_CONDICION " & GetLines & _
        "WHERE 1 = 1 " & GetLines & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & _
        "AND Tipo_Condicion = 8 "
    End If
    
    Set RsDetPag = FrmAppLink.CnADM.Execute(mSQL)
    
    With RsDetPag
        
        If Not .EOF Then
        
            NumRows = !NumReg
            
            If NumRows > 0 Then
                
                NumPag = Round(NumRows / MaxGridRowsxPag, 8)
                If (NumPag - Fix(NumPag)) <> 0 Then NumPag = Fix(NumPag + 1)
                
                txtNumPagBIN.Text = NumPag
                
                txtPagBIN.Text = Fix(SVal(txtPagBIN))
                
                If SVal(txtPagBIN.Text) >= 1 And SVal(txtPagBIN.Text) <= NumPag Then
                    txtPagBIN.Text = Fix(SVal(txtPagBIN))
                Else
                    txtPagBIN.Text = 1
                End If
                
                If EstatusPromo = "DWT" Then
                    mSQL = "WITH AllRows AS (" & GetLines & _
                    "SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowID, *" & GetLines & _
                    "FROM TMP_PROMOCION_CONDICION_VALORES " & GetLines & _
                    "WHERE 1 = 1 " & GetLines & _
                    "AND Tipo_Condicion = 8 " & GetLines & _
                    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & _
                    "' " & GetLines & ")" & GetLines & _
                    "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                    "((" & txtPagBIN.Text & " - 1) * (" & MaxGridRowsxPag & " + 1)) AND " & _
                    "(" & txtPagBIN.Text & " * " & MaxGridRowsxPag & ")" & GetLines
                Else
                    mSQL = "WITH AllRows AS (" & GetLines & _
                    "SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowID, *" & GetLines & _
                    "FROM TR_PROMOCION_CONDICION " & GetLines & _
                    "WHERE 1 = 1 " & GetLines & _
                    "AND Tipo_Condicion = 8 " & GetLines & _
                    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & _
                    "' " & GetLines & ")" & GetLines & _
                    "SELECT * FROM AllRows WHERE RowID BETWEEN " & GetLines & _
                    "((" & txtPagBIN.Text & " - 1) * (" & MaxGridRowsxPag & " + 1)) AND " & _
                    "(" & txtPagBIN.Text & " * " & MaxGridRowsxPag & ")" & GetLines
                End If
                
                RsDetPag.Close
                
                Set RsDetPag = New Recordset
                Apertura_RecordsetC RsDetPag
                
                .Open mSQL, FrmAppLink.CnADM, adOpenStatic, adLockReadOnly, adCmdText
                
                If Not .EOF Then
                    
                    .Sort = "RowID"
                    
                    Dim I As Integer
                    
                    GrdBIN.Visible = False
                    
                    GrdBIN.Rows = 1
                    
                    ScrollGridBIN.Min = 0
                    ScrollGridBIN.Max = .RecordCount
                    ScrollGridBIN.Value = ScrollGridBIN.Min
                    
                    While Not .EOF
                    
                        GrdBIN.Rows = GrdBIN.Rows + 1
                        I = I + 1 '!RowID
                        
                        GrdBIN.TextMatrix(I, GridColBIN.ColRowID) = I
                        
                        GrdBIN.TextMatrix(I, ColItemNumber) = !Marca
                        
                        .MoveNext
                        
                    Wend
                    
                    GrdBIN.Visible = True
                    
                    'If LastRow >= 1 And LastRow <= GrdBIN.Rows - 1 Then
                        'GrdBIN.Row = LastRow
                        'GrdBIN.TopRow = GrdBIN.Row
                    'Else
                        'GrdBIN.Row = 1
                        'GrdBIN.TopRow = GrdBIN.Row
                    'End If
                    
                End If
                
            Else
                NumPag = 1
                txtPagBIN.Text = NumPag
                GrdBIN.Rows = 1
                GrdBIN.Rows = 2
            End If
            
            If GrdBIN.Rows > 9 Then
                ScrollGridBIN.Visible = True
                GrdBIN.ScrollBars = flexScrollBarVertical
                AnchoScrollBar_GrdBIN = ScrollGridBIN.Width
                GrdBIN.ColWidth(ColItemNumber) = (AnchoCampoScroll_GrdBIN - AnchoScrollBar_GrdBIN)
            Else
                GrdBIN.ScrollBars = flexScrollBarHorizontal
                ScrollGridBIN.Visible = False
                AnchoScrollBar_GrdBIN = 0
                GrdBIN.ColWidth(ColItemNumber) = AnchoCampoScroll_GrdBIN
            End If
            
        End If
        
    End With
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CargarPromo_ListaBIN)"
    
    CargarGridListaBIN
    GrdBIN.Visible = True
    
End Sub

Private Sub CargarPagoMinimo()
    
    Dim TmpCodMoneda, TmpValorMinimo
    
    If EstatusPromo = "DWT" Then
        
        TmpCodMoneda = BuscarValorBD("CodMoneda", _
        "SELECT Top 1 Cod_Dpto As CodMoneda FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND Tipo_Condicion = 6 ", Empty, FrmAppLink.CnADM)
        
        TmpValorMinimo = BuscarValorBD("ValorMinimo", _
        "SELECT Top 1 Porcentaje_Descuento1 As ValorMinimo FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND Tipo_Condicion = 6 ", Empty, FrmAppLink.CnADM)
        
    Else
        
        TmpCodMoneda = BuscarValorBD("CodMoneda", _
        "SELECT Top 1 Cod_Dpto As CodMoneda FROM TR_PROMOCION_CONDICION " & vbNewLine & _
        "WHERE 1 = 1 " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND Tipo_Condicion = 6 ", Empty, FrmAppLink.CnADM)
        
        TmpValorMinimo = BuscarValorBD("ValorMinimo", _
        "SELECT Top 1 TPV.Porcentaje_Descuento1 As ValorMinimo" & vbNewLine & _
        "FROM TR_PROMOCION_CONDICION TPC" & vbNewLine & _
        "INNER JOIN TR_PROMOCION_VALORES TPV" & vbNewLine & _
        "ON TPC.Cod_Promocion = TPV.Cod_Promocion AND TPC.Linea_Valor = TPV.Linea_Valor" & vbNewLine & _
        "WHERE 1 = 1 " & vbNewLine & _
        "AND TPC.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND TPC.Tipo_Condicion = 6 ", Empty, FrmAppLink.CnADM)
        
    End If
    
    If TipoPromo = 16 Or TipoPromo = 19 Then
        chkPagoMinimo.Caption = StellarMensajeLocal(238)
        chkPagoMinimo.Value = vbChecked
        txtValorMinimo = "100%"
    Else
        
        If Trim(TmpCodMoneda) <> Empty _
        And SDec(TmpValorMinimo) > 0 Then
            
            chkPagoMinimo.Value = vbChecked
            
            If TipoPromo = 17 Or TipoPromo = 20 Then
                chkPagoMinimo.Caption = StellarMensajeLocal(238)
                txtValorMinimo = FormatoDecimalesDinamicos(SDec(TmpValorMinimo), 0, 2) & "%"
            ElseIf TipoPromo = 18 Or TipoPromo = 21 Then
                MonedaTmp.BuscarMonedas , TmpCodMoneda
                chkPagoMinimo.Caption = Replace(StellarMensajeLocal(239), "$(Curr)", "(" & MonedaTmp.SimMoneda & ")")
                txtValorMinimo = FormatoDecimalesDinamicos(SDec(TmpValorMinimo))
            End If
            
        Else
            
            chkPagoMinimo.Value = vbUnchecked
            txtValorMinimo = "N/A"
            
        End If
        
    End If
    
End Sub

Private Sub CargarLimiteDescuentoPromo()
    
    Dim TmpCodMonedaLimite, TmpMontoLimite
    
    If EstatusPromo = "DWT" Then
        
        TmpCodMonedaLimite = BuscarValorBD("CodMonedaLimite", _
        "SELECT Top 1 Cod_Dpto As CodMonedaLimite FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND Tipo_Condicion = 7 ", Empty, FrmAppLink.CnADM)
        
        TmpMontoLimite = BuscarValorBD("MontoLimite", _
        "SELECT Top 1 Monto_Descuento As MontoLimite FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND Tipo_Condicion = 7 ", Empty, FrmAppLink.CnADM)
        
    Else
        
        TmpCodMonedaLimite = BuscarValorBD("CodMonedaLimite", _
        "SELECT Top 1 Cod_Dpto As CodMonedaLimite FROM TR_PROMOCION_CONDICION " & vbNewLine & _
        "WHERE 1 = 1 " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND Tipo_Condicion = 7 ", Empty, FrmAppLink.CnADM)
        
        TmpMontoLimite = BuscarValorBD("MontoLimite", _
        "SELECT Top 1 TPV.Monto_Descuento As MontoLimite" & vbNewLine & _
        "FROM TR_PROMOCION_CONDICION TPC" & vbNewLine & _
        "INNER JOIN TR_PROMOCION_VALORES TPV" & vbNewLine & _
        "ON TPC.Cod_Promocion = TPV.Cod_Promocion AND TPC.Linea_Valor = TPV.Linea_Valor" & vbNewLine & _
        "WHERE 1 = 1 " & vbNewLine & _
        "AND TPC.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND TPC.Tipo_Condicion = 7 ", Empty, FrmAppLink.CnADM)
        
    End If
    
    If Trim(TmpCodMonedaLimite) <> Empty _
    And SDec(TmpMontoLimite) > 0 Then
        
        MonedaTmp.BuscarMonedas , TmpCodMonedaLimite
        
        ChkMontoLimiteDescuentoPromo.Caption = StellarMensajeLocal(251) & " (" & MonedaTmp.SimMoneda & ")"
        ChkMontoLimiteDescuentoPromo.Value = vbChecked
        txtMontoLimiteDescuentoPromo = FormatoDecimalesDinamicos(SDec(TmpMontoLimite))
        
    Else
        
        ChkMontoLimiteDescuentoPromo.Value = vbUnchecked
        txtMontoLimiteDescuentoPromo = "N/A"
        
    End If
    
End Sub

Private Sub CargarLimiteDescuentoTarjeta()
    
    Dim TmpCodMonedaLimite, TmpMontoLimite
    
    If EstatusPromo = "DWT" Then
        
        TmpCodMonedaLimite = BuscarValorBD("CodMonedaLimite", _
        "SELECT Top 1 Cod_Dpto As CodMonedaLimite FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND Tipo_Condicion = 9 ", Empty, FrmAppLink.CnADM)
        
        TmpMontoLimite = BuscarValorBD("MontoLimite", _
        "SELECT Top 1 Monto_Descuento As MontoLimite FROM TMP_PROMOCION_CONDICION_VALORES " & vbNewLine & _
        "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND Tipo_Condicion = 9 ", Empty, FrmAppLink.CnADM)
        
    Else
        
        TmpCodMonedaLimite = BuscarValorBD("CodMonedaLimite", _
        "SELECT Top 1 Cod_Dpto As CodMonedaLimite FROM TR_PROMOCION_CONDICION " & vbNewLine & _
        "WHERE 1 = 1 " & vbNewLine & _
        "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND Tipo_Condicion = 9 ", Empty, FrmAppLink.CnADM)
        
        TmpMontoLimite = BuscarValorBD("MontoLimite", _
        "SELECT Top 1 TPV.Monto_Descuento As MontoLimite" & vbNewLine & _
        "FROM TR_PROMOCION_CONDICION TPC" & vbNewLine & _
        "INNER JOIN TR_PROMOCION_VALORES TPV" & vbNewLine & _
        "ON TPC.Cod_Promocion = TPV.Cod_Promocion AND TPC.Linea_Valor = TPV.Linea_Valor" & vbNewLine & _
        "WHERE 1 = 1 " & vbNewLine & _
        "AND TPC.Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' " & vbNewLine & _
        "AND TPC.Tipo_Condicion = 9 ", Empty, FrmAppLink.CnADM)
        
    End If
    
    If Trim(TmpCodMonedaLimite) <> Empty _
    And SDec(TmpMontoLimite) > 0 Then
        
        MonedaTmp.BuscarMonedas , TmpCodMonedaLimite
        
        ChkMontoLimiteDescuentoTarjeta.Caption = StellarMensajeLocal(269) & " (" & MonedaTmp.SimMoneda & ")"
        ChkMontoLimiteDescuentoTarjeta.Value = vbChecked
        txtMontoLimiteDescuentoTarjeta = FormatoDecimalesDinamicos(SDec(TmpMontoLimite))
        
    Else
        
        ChkMontoLimiteDescuentoTarjeta.Value = vbUnchecked
        txtMontoLimiteDescuentoTarjeta = "N/A"
        
    End If
    
End Sub

Private Sub ImgNextDetPag_Click()
    
    If SVal(txtPagDetPag.Text) <= SVal(txtNumPagDetPag.Text) Then
        txtPagDetPag.Text = SVal(txtPagDetPag.Text) + 1
        If SVal(txtPagDetPag.Text) > SVal(txtNumPagDetPag.Text) Then txtPagDetPag.Text = SVal(txtNumPagDetPag.Text)
    Else
        txtPagDetPag.Text = SVal(txtNumPagDetPag.Text)
    End If
    
    CargarPromo_DetPag
    
End Sub

Private Sub ImgPrevDetPag_Click()
    
    If SVal(txtPagDetPag.Text) >= 1 Then
        txtPagDetPag.Text = SVal(txtPagDetPag.Text) - 1
        If SVal(txtPagDetPag.Text) <= 1 Then txtPagDetPag.Text = 1
    Else
        txtPagDetPag.Text = 1
    End If
    
    CargarPromo_DetPag
    
End Sub

Private Sub txtPagDetPag_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtPagDetPag_GotFocus()
    Set CampoT = txtPagDetPag
    txtPagDetPag.Tag = SVal(txtPagDetPag.Text)
End Sub

Private Sub txtPagDetPag_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtPagDetPag_LostFocus
    End If
End Sub

Private Sub txtPagDetPag_LostFocus()
    If SVal(txtPagDetPag.Text) <> SVal(txtPagDetPag.Tag) Then
        CargarPromo_DetPag
    End If
End Sub

Private Sub ScrollGridDetPag_Change()
    On Error GoTo ErrScroll
    If ScrollGridDetPag.Value <> GrdDetPag.Row Then
        GrdDetPag.TopRow = ScrollGridDetPag.Value
        GrdDetPag.Row = ScrollGridDetPag.Value
        If PuedeObtenerFoco(GrdDetPag) Then GrdDetPag.SetFocus
    End If
    Exit Sub
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGridDetPag_Scroll()
    'Debug.Print ScrollGridDetPag.value
    ScrollGridDetPag_Change
End Sub

Private Sub ImgNextBIN_Click()
    
    If SVal(txtPagBIN.Text) <= SVal(txtNumPagBIN.Text) Then
        txtPagBIN.Text = SVal(txtPagBIN.Text) + 1
        If SVal(txtPagBIN.Text) > SVal(txtNumPagBIN.Text) Then txtPagBIN.Text = SVal(txtNumPagBIN.Text)
    Else
        txtPagBIN.Text = SVal(txtNumPagBIN.Text)
    End If
    
    CargarPromo_ListaBIN
    
End Sub

Private Sub ImgPrevBIN_Click()
    
    If SVal(txtPagBIN.Text) >= 1 Then
        txtPagBIN.Text = SVal(txtPagBIN.Text) - 1
        If SVal(txtPagBIN.Text) <= 1 Then txtPagBIN.Text = 1
    Else
        txtPagBIN.Text = 1
    End If
    
    CargarPromo_ListaBIN
    
End Sub

Private Sub txtPagBIN_Click()
    If FrmAppLink.ModoTouch Then
        TecladoAvanzado CampoT
    End If
End Sub

Private Sub txtPagBIN_GotFocus()
    Set CampoT = txtPagBIN
    txtPagBIN.Tag = SVal(txtPagBIN.Text)
End Sub

Private Sub txtPagBIN_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtPagBIN_LostFocus
    End If
End Sub

Private Sub txtPagBIN_LostFocus()
    If SVal(txtPagBIN.Text) <> SVal(txtPagBIN.Tag) Then
        CargarPromo_ListaBIN
    End If
End Sub

Private Sub ScrollGridBIN_Change()
    On Error GoTo ErrScroll
    If ScrollGridBIN.Value <> GrdBIN.Row Then
        GrdBIN.TopRow = ScrollGridBIN.Value
        GrdBIN.Row = ScrollGridBIN.Value
        If PuedeObtenerFoco(GrdBIN) Then GrdBIN.SetFocus
    End If
    Exit Sub
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGridBIN_Scroll()
    'Debug.Print ScrollGridBIN.value
    ScrollGridBIN_Change
End Sub
